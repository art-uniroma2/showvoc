import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { ResourceViewModule } from '../resource-view/resource-view.module';
import { StructuresModule } from '../structures/structures.module';
import { LocalizedEditorModalComponent } from '../widget/localized-editor/localized-editor-modal';
import { RenderingEditorModalComponent } from '../widget/rendering-editor/rendering-editor-modal.component';
import { StorageManagerModalComponent } from '../widget/storage-manager/storage-manager-modal.component';
import { WidgetModule } from '../widget/widget.module';
import { AlertModalComponent } from './basic-modals/alert-modal/alert-modal.component';
import { BasicModalsServices } from './basic-modals/basic-modals.service';
import { ConfirmCheckModalComponent } from './basic-modals/confirm-modal/confirm-check-modal.component';
import { ConfirmModalComponent } from './basic-modals/confirm-modal/confirm-modal.component';
import { DownloadModalComponent } from './basic-modals/download-modal/download-modal.component';
import { FilePickerModalComponent } from './basic-modals/file-picker-modal/file-picker-modal.component';
import { PromptModalComponent } from './basic-modals/prompt-modal/prompt-modal.component';
import { PromptNumberModalComponent } from './basic-modals/prompt-modal/prompt-number-modal.component';
import { ResourceSelectionModalComponent } from './basic-modals/selection-modal/resource-selection-modal.component';
import { SelectionModalComponent } from './basic-modals/selection-modal/selection-modal.component';
import { BrowsingModalsServices } from './browsing-modals/browsing-modals.service';
import { ClassIndividualTreeModalComponent } from './browsing-modals/class-individual-tree-modal/class-individual-tree-modal.component';
import { ClassTreeModalComponent } from './browsing-modals/class-tree-modal/class-tree-modal';
import { CollectionTreeModalComponent } from './browsing-modals/collection-tree-modal/collection-tree-modal';
import { ConceptTreeModalComponent } from './browsing-modals/concept-tree-modal/concept-tree-modal';
import { LexicalEntryListModalComponent } from './browsing-modals/lexical-entry-list-modal/lexical-entry-list-modal';
import { LexiconListModalComponent } from './browsing-modals/lexicon-list-modal/lexicon-list-modal';
import { PropertyTreeModalComponent } from './browsing-modals/property-tree-modal/property-tree-modal';
import { SchemeListModalComponent } from './browsing-modals/scheme-list-modal/scheme-list-modal';
import { CreationModalServices } from './creation-modals/creation-modals.service';
import { NewTypedLiteralModalComponent } from './creation-modals/new-typed-literal-modal/new-typed-literal-modal.component';
import { LoadConfigurationModalComponent } from './shared-modals/configuration-store-modal/load-configuration-modal.component';
import { StoreConfigurationModalComponent } from './shared-modals/configuration-store-modal/store-configuration-modal.component';
import { LanguageSelectorModalComponent } from './shared-modals/languages-selector-modal/languages-selector-modal.component';
import { PluginConfigurationModalComponent } from './shared-modals/plugin-config-modal/plugin-config-modal.component';
import { ResourcePickerModalComponent } from './shared-modals/resource-picker-modal/resource-picker-modal.component';
import { SharedModalsServices } from './shared-modals/shared-modals.service';
import { DatasetSelectionModalComponent } from './shared-modals/dataset-list-modal/dataset-selection-modal.component';

@NgModule({
    declarations: [
        AlertModalComponent,
        ClassIndividualTreeModalComponent,
        ClassTreeModalComponent,
        CollectionTreeModalComponent,
        ConceptTreeModalComponent,
        ConfirmCheckModalComponent,
        ConfirmModalComponent,
        DatasetSelectionModalComponent,
        DownloadModalComponent,
        FilePickerModalComponent,
        LanguageSelectorModalComponent,
        LexicalEntryListModalComponent,
        LexiconListModalComponent,
        LoadConfigurationModalComponent,
        LocalizedEditorModalComponent,
        NewTypedLiteralModalComponent,
        PluginConfigurationModalComponent,
        PromptModalComponent,
        PromptNumberModalComponent,
        PropertyTreeModalComponent,
        RenderingEditorModalComponent,
        ResourcePickerModalComponent,
        ResourceSelectionModalComponent,
        SchemeListModalComponent,
        SelectionModalComponent,
        StorageManagerModalComponent,
        StoreConfigurationModalComponent
    ],
    imports: [
        CommonModule,
        DragDropModule,
        FormsModule,
        NgbDropdownModule,
        ResourceViewModule,
        StructuresModule,
        TranslateModule,
        WidgetModule,
    ],
    providers: [
        BasicModalsServices,
        BrowsingModalsServices,
        CreationModalServices,
        SharedModalsServices
    ]
})
export class ModalsModule { }
