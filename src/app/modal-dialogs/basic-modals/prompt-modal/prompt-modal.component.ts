import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'prompt-modal',
  templateUrl: './prompt-modal.component.html',
  styleUrls: ['../../modals.css'],
  standalone: false
})
export class PromptModalComponent {

  @Input() title: string;
  @Input() label: { value: string, tooltip?: string };
  @Input() message: string;
  @Input() value: string;
  @Input() hideClose: boolean = false;
  @Input() inputOptional: boolean = false;

  constructor(public activeModal: NgbActiveModal) { }

  isInputValid(): boolean {
    return (this.value != undefined && this.value.trim() != "");
  }

  ok() {
    if (this.inputOptional || this.isInputValid()) {
      this.activeModal.close(this.value);
    }
  }

  close() {
    this.activeModal.dismiss();
  }

}
