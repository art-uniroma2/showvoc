import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractConfirmModal } from './abstract-confirm-modal';

@Component({
  selector: 'confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['../../modals.css'],
  standalone: false
})
export class ConfirmModalComponent extends AbstractConfirmModal {

  constructor(public activeModal: NgbActiveModal) {
    super(activeModal);
  }

  ok() {
    this.activeModal.close();
  }

  close() {
    this.activeModal.dismiss();
  }

}
