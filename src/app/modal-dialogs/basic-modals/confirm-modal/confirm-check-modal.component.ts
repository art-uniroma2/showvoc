import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CheckOptions } from '../../Modals';
import { AbstractConfirmModal } from './abstract-confirm-modal';

@Component({
  selector: 'confirm-check-modal',
  templateUrl: './confirm-check-modal.component.html',
  styleUrls: ['../../modals.css'],
  standalone: false
})
export class ConfirmCheckModalComponent extends AbstractConfirmModal {

  @Input() checkOpts: CheckOptions[];


  constructor(public activeModal: NgbActiveModal) {
    super(activeModal);
  }

  ok() {
    this.activeModal.close(this.checkOpts);
  }

  close() {
    this.activeModal.dismiss(this.checkOpts);
  }

}