import { Component, ElementRef, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IRI, Resource } from 'src/app/models/Resources';
import { AbstractStructureModal } from '../abstract-structure-modal';

@Component({
  selector: 'class-tree-modal',
  templateUrl: './class-tree-modal.html',
  standalone: false
})
export class ClassTreeModalComponent extends AbstractStructureModal<Resource> {

  @Input() roots: IRI[];

  constructor(activeModal: NgbActiveModal, elementRef: ElementRef) {
    super(activeModal, elementRef);
  }

}
