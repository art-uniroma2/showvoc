import { Component, ElementRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IRI } from 'src/app/models/Resources';
import { AbstractStructureModal } from '../abstract-structure-modal';

@Component({
  selector: 'scheme-list-modal',
  templateUrl: './scheme-list-modal.html',
  standalone: false
})
export class SchemeListModalComponent extends AbstractStructureModal<IRI> {

  constructor(activeModal: NgbActiveModal, elementRef: ElementRef) {
    super(activeModal, elementRef);
  }

}
