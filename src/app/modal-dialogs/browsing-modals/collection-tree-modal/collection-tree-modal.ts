import { Component, ElementRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Resource } from 'src/app/models/Resources';
import { AbstractStructureModal } from '../abstract-structure-modal';

@Component({
  selector: 'collection-tree-modal',
  templateUrl: './collection-tree-modal.html',
  standalone: false
})
export class CollectionTreeModalComponent extends AbstractStructureModal<Resource> {

  constructor(activeModal: NgbActiveModal, elementRef: ElementRef) {
    super(activeModal, elementRef);
  }

}
