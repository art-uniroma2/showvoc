import { Component, ElementRef, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IRI } from 'src/app/models/Resources';
import { AbstractStructureModal } from '../abstract-structure-modal';

@Component({
  selector: 'property-tree-modal',
  templateUrl: './property-tree-modal.html',
  standalone: false
})
export class PropertyTreeModalComponent extends AbstractStructureModal<IRI> {

  @Input() rootProperties: IRI[];
  @Input() resource: IRI;
  @Input() allowShowAll: boolean = false;

  rootProps: IRI[];
  domainRes: IRI;
  showAll: boolean = false;

  constructor(activeModal: NgbActiveModal, elementRef: ElementRef) {
    super(activeModal, elementRef);
  }

  ngOnInit() {
    this.domainRes = this.resource;
    this.rootProps = this.rootProperties;
  }

  /**
   * When the checkbox "select all properties" changes status
   * Resets the selectedProperty and update the domainRes that represents 
   * the resource which its type should be the domain of the properties in the tree
   */
  onShowAllChanged() {
    this.selectedNode = null;
    if (this.showAll) {
      this.domainRes = null;
      this.rootProps = null;
    } else {
      this.domainRes = this.resource;
      this.rootProps = this.rootProperties;
    }
  }

}
