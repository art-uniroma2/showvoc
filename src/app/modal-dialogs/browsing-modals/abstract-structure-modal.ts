import { Directive, ElementRef, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Project } from 'src/app/models/Project';
import { AnnotatedValue, Resource } from 'src/app/models/Resources';
import { NodeSelectEvent } from 'src/app/structures/abstract-node';
import { UIUtils } from 'src/app/utils/UIUtils';

@Directive()
export class AbstractStructureModal<T extends Resource> {

  @Input() title: string;
  @Input() project?: Project;

  selectedNode: AnnotatedValue<T>;

  constructor(protected activeModal: NgbActiveModal, protected elementRef: ElementRef) { }

  ngAfterViewInit() {
    UIUtils.setFullSizeModal(this.elementRef);
  }

  onNodeSelected(event?: NodeSelectEvent) {
    this.selectedNode = event?.value as AnnotatedValue<T>;
  }

  ok() {
    this.activeModal.close(this.selectedNode);
  }

  close() {
    this.activeModal.dismiss();
  }

}
