import { Component, ElementRef, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AnnotatedValue, Resource } from 'src/app/models/Resources';
import { NodeSelectEvent } from "src/app/structures/abstract-node";
import { UIUtils } from 'src/app/utils/UIUtils';

@Component({
  selector: "class-individual-tree-modal",
  templateUrl: "./class-individual-tree-modal.component.html",
  standalone: false
})
export class ClassIndividualTreeModalComponent {
  @Input() title: string;
  @Input() classes: Resource[];

  selectedInstance: AnnotatedValue<Resource>;

  constructor(public activeModal: NgbActiveModal, private elementRef: ElementRef) { }

  ngAfterViewInit() {
    UIUtils.setFullSizeModal(this.elementRef);
  }

  ok() {
    this.activeModal.close(this.selectedInstance);
  }

  close() {
    this.activeModal.dismiss();
  }

  onInstanceSelected(event?: NodeSelectEvent) {
    this.selectedInstance = event?.value;
  }

}