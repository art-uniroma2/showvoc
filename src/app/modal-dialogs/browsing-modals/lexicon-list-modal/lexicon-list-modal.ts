import { Component, ElementRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractStructureModal } from '../abstract-structure-modal';
import { IRI } from 'src/app/models/Resources';

@Component({
  selector: 'lexicon-list-modal',
  templateUrl: './lexicon-list-modal.html',
  standalone: false
})
export class LexiconListModalComponent extends AbstractStructureModal<IRI> {

  constructor(activeModal: NgbActiveModal, elementRef: ElementRef) {
    super(activeModal, elementRef);
  }

}
