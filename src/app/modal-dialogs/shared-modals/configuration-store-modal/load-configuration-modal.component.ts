import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Configuration, Reference } from 'src/app/models/Configuration';
import { ScopeUtils } from "src/app/models/Plugins";
import { ConfigurationsServices } from 'src/app/services/configurations.service';
import { AuthorizationEvaluator } from "src/app/utils/AuthorizationEvaluator";
import { BasicModalsServices } from "../../basic-modals/basic-modals.service";
import { ModalType } from "../../Modals";

@Component({
  selector: "load-configuration",
  templateUrl: "./load-configuration-modal.component.html",
  standalone: false
})
export class LoadConfigurationModalComponent {
  @Input() title: string;
  @Input() configurationComponent: string;
  @Input() allowLoad: boolean = true;
  @Input() allowDelete: boolean = true;
  @Input() allowRename: boolean = true;
  @Input() additionalReferences: Reference[];

  references: Reference[];
  selectedRef: Reference;

  filter: string = "";

  constructor(public activeModal: NgbActiveModal, private basicModals: BasicModalsServices, private configurationService: ConfigurationsServices) { }

  ngOnInit() {
    this.initReferences();
  }

  private initReferences() {
    this.configurationService.getConfigurationReferences(this.configurationComponent).subscribe(
      refs => {
        this.references = refs;

        this.references.forEach(r => {
          r['renameAllowed'] = this.allowRename && AuthorizationEvaluator.isConfigurationActionAuthorized(r.getReferenceScope(), "U");
          r['deleteAllowed'] = this.allowDelete && AuthorizationEvaluator.isConfigurationActionAuthorized(r.getReferenceScope(), "D");
        });
      }
    );
  }

  selectReference(reference: Reference) {
    if (this.selectedRef == reference) {
      this.selectedRef = null;
    } else {
      this.selectedRef = reference;
    }
  }

  deleteReference(reference: Reference) {
    this.configurationService.deleteConfiguration(this.configurationComponent, reference.relativeReference).subscribe(
      () => {
        this.selectedRef = null;
        this.initReferences();
      }
    );
  }

  onReferenceRenamed(reference: Reference, newIdentifier: string) {
    if (!Reference.identifierRegex.test(newIdentifier)) {
      this.basicModals.alert({ key: "COMMONS.STATUS.INVALID_VALUE" }, { key: "MESSAGES.NOT_ALLOWED_CHARS_IN_ID" }, ModalType.warning);
      //reinit reference so that the input-editable restore the original identifier
      reference = new Reference(reference.user, reference.project, reference.identifier, reference.relativeReference);
      return;
    }

    let newRef = ScopeUtils.serializeScope(reference.getReferenceScope()) + ":" + newIdentifier;
    this.configurationService.renameConfiguration(this.configurationComponent, reference.relativeReference, newRef).subscribe(
      () => {
        this.initReferences();
      }
    );
  }

  isReferenceVisible(reference: Reference): boolean {
    if (this.filter != "") {
      return reference.identifier.toLocaleLowerCase().includes(this.filter.toLocaleLowerCase());
    } else {
      return true;
    }
  }


  ok() {
    if (!this.allowLoad) {
      let returnData: LoadConfigurationModalReturnData = {
        configuration: null,
        reference: this.selectedRef
      };
      this.activeModal.close(returnData);
    } else {
      //selected reference is from the additionals => do not load the configuration, but let handling it to the component that opened the modal
      if (this.additionalReferences != null && this.additionalReferences.indexOf(this.selectedRef) != -1) {
        let returnData: LoadConfigurationModalReturnData = {
          configuration: null,
          reference: this.selectedRef
        };
        this.activeModal.close(returnData);
      } else {
        this.configurationService.getConfiguration(this.configurationComponent, this.selectedRef.relativeReference).subscribe(
          conf => {
            let returnData: LoadConfigurationModalReturnData = {
              configuration: conf,
              reference: this.selectedRef
            };
            this.activeModal.close(returnData);
          }
        );
      }
    }
  }

  cancel() {
    this.activeModal.dismiss();
  }

}


export class LoadConfigurationModalReturnData {
  configuration: Configuration;
  reference: Reference;
}