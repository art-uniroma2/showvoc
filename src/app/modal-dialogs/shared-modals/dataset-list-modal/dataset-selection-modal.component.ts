import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Project } from "src/app/models/Project";

@Component({
  selector: "dataset-selection-modal",
  templateUrl: "./dataset-selection-modal.component.html",
  standalone: false
})
export class DatasetSelectionModalComponent {
  @Input() title: string;
  @Input() message: string;
  @Input() onlyOpen: boolean;

  selectedProject: Project;

  constructor(public activeModal: NgbActiveModal) { }

  selectProject(project: Project) {
    this.selectedProject = project;
  }

  ok() {
    this.activeModal.close(this.selectedProject);
  }

  cancel() {
    this.activeModal.dismiss();
  }
}