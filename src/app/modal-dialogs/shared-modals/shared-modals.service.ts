import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Reference } from 'src/app/models/Configuration';
import { Settings } from 'src/app/models/Plugins';
import { Project } from 'src/app/models/Project';
import { AnnotatedValue, Resource, Value } from 'src/app/models/Resources';
import { TextOrTranslation, TranslationUtils } from 'src/app/utils/TranslationUtils';
import { LocalizedEditorModalComponent } from 'src/app/widget/localized-editor/localized-editor-modal';
import { LocalizedMap } from 'src/app/widget/localized-editor/localized-editor.component';
import { ResourcePickerConfig } from 'src/app/widget/pickers/value-picker/resource-picker.component';
import { StorageManagerModalComponent } from 'src/app/widget/storage-manager/storage-manager-modal.component';
import { ResourceViewModalComponent } from '../../resource-view/modals/resource-view-modal.component';
import { ResourceSelectionModalComponent } from '../basic-modals/selection-modal/resource-selection-modal.component';
import { ModalOptions } from '../Modals';
import { LoadConfigurationModalComponent } from './configuration-store-modal/load-configuration-modal.component';
import { StoreConfigurationModalComponent } from './configuration-store-modal/store-configuration-modal.component';
import { DatasetSelectionModalComponent } from './dataset-list-modal/dataset-selection-modal.component';
import { LanguageSelectorModalComponent } from './languages-selector-modal/languages-selector-modal.component';
import { PluginConfigurationModalComponent, PluginSettingsHandler } from './plugin-config-modal/plugin-config-modal.component';
import { ResourcePickerModalComponent } from './resource-picker-modal/resource-picker-modal.component';

@Injectable()
export class SharedModalsServices {

  constructor(private modalService: NgbModal, private translateService: TranslateService) { }

  /**
   * Opens a resource view in a modal
   * @param resource 
   */
  openResourceView(resource: Resource, project?: Project, options?: ModalOptions) {
    let _options: ModalOptions = new ModalOptions("lg").merge(options);
    const modalRef: NgbModalRef = this.modalService.open(ResourceViewModalComponent, _options);
    modalRef.componentInstance.resource = resource;
    modalRef.componentInstance.project = project;
    return modalRef.result;
  }

  /**
   * Opens a modal to select multiple languages
   * @param title
   * @param languages languages already selected
   * @param radio if true, exactly one language should be selected
   * @param projectAware if true, allow selection only of languages available in the current project
   * @param project allow to customize the available languages for the contextual project
   */
  selectLanguages(title: TextOrTranslation, languages?: string[], radio?: boolean, projectAware?: boolean, project?: Project): Promise<string[]> {
    const modalRef: NgbModalRef = this.modalService.open(LanguageSelectorModalComponent, new ModalOptions());
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    if (languages != null) modalRef.componentInstance.languages = languages;
    if (radio != null) modalRef.componentInstance.radio = radio;
    if (projectAware != null) modalRef.componentInstance.projectAware = projectAware;
    if (project != null) modalRef.componentInstance.project = project;
    return modalRef.result;
  }

  selectDataset(title: TextOrTranslation, msg: TextOrTranslation, onlyOpen?: boolean): Promise<Project> {
    const modalRef: NgbModalRef = this.modalService.open(DatasetSelectionModalComponent, new ModalOptions());
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    modalRef.componentInstance.message = TranslationUtils.getTranslatedText(msg, this.translateService);
    modalRef.componentInstance.onlyOpen = onlyOpen;
    return modalRef.result;
  }

  /**
   * 
   * @param title 
   * @param roles 
   * @param editable 
   */
  pickResource(title: TextOrTranslation, config?: ResourcePickerConfig, editable?: boolean): Promise<AnnotatedValue<Resource>> {
    const modalRef: NgbModalRef = this.modalService.open(ResourcePickerModalComponent, new ModalOptions());
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    if (config != null) modalRef.componentInstance.config = config;
    if (editable != null) modalRef.componentInstance.editable = editable;
    return modalRef.result;
  }

  /**
   * Opens a modal with an message and a list of selectable options.
   * @param title the title of the modal dialog
   * @param message the message to show in the modal dialog body. If null no message will be in the modal
   * @param resourceList array of available resources
   * @param rendering in case of array of resources, it tells whether the resources should be rendered
   * @return if the modal closes with ok returns a promise containing a list of selected resource
   */
  selectResource<T extends Value>(title: TextOrTranslation, message: TextOrTranslation, resourceList: AnnotatedValue<T>[], rendering?: boolean, multiselection?: boolean, emptySelectionAllowed?: boolean, selectedResources?: Value[]): Promise<AnnotatedValue<T>[]> {
    const modalRef: NgbModalRef = this.modalService.open(ResourceSelectionModalComponent, new ModalOptions());
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    modalRef.componentInstance.message = TranslationUtils.getTranslatedText(message, this.translateService);
    modalRef.componentInstance.resourceList = resourceList;
    if (rendering != null) modalRef.componentInstance.rendering = rendering;
    if (multiselection != null) modalRef.componentInstance.multiselection = multiselection;
    if (emptySelectionAllowed != null) modalRef.componentInstance.emptySelectionAllowed = emptySelectionAllowed;
    if (selectedResources != null) modalRef.componentInstance.selectedResources = selectedResources;
    return modalRef.result;
  }

  storageManager(title: TextOrTranslation, selectedFiles: string[], multiselection?: boolean): Promise<string[]> {
    const modalRef: NgbModalRef = this.modalService.open(StorageManagerModalComponent, new ModalOptions('lg'));
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    modalRef.componentInstance.selectedFiles = selectedFiles;
    modalRef.componentInstance.multiselection = multiselection;
    return modalRef.result;
  }

  /**
   * Opens a modal to change a plugin configuration.
   * Returns a new PluginConfiguration, the input configuration doesn't mutate.
   * @param configuration
   */
  configurePlugin(configuration: Settings, title?: TextOrTranslation, handler?: PluginSettingsHandler) {
    const modalRef: NgbModalRef = this.modalService.open(PluginConfigurationModalComponent, new ModalOptions("xl"));
    if (title != null) {
      modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    } else {
      modalRef.componentInstance.title = TranslationUtils.getTranslatedText({ key: 'COMMONS.CONFIG.CONFIGURATION_OF' }, this.translateService) + " '" + configuration.shortName + "'";
    }
    modalRef.componentInstance.configuration = configuration;
    modalRef.componentInstance.handler = handler;
    return modalRef.result;
  }

  /**
   * Open a modal that allows to store a configuration. If the configuration is succesfully stored, returns it relativeReference.
   * @param title 
   * @param configurationComponent 
   * @param configurationObject 
   * @param relativeRef if provided suggest to override a previously saved configuration
   * @return the relativeReference of the stored configuration
   */
  storeConfiguration(title: TextOrTranslation, configurationComponent: string, configurationObject: { [key: string]: any }, relativeRef?: string) {
    const modalRef: NgbModalRef = this.modalService.open(StoreConfigurationModalComponent, new ModalOptions("lg"));
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    modalRef.componentInstance.configurationComponent = configurationComponent;
    modalRef.componentInstance.configurationObject = configurationObject;
    if (relativeRef != null) modalRef.componentInstance.relativeRef = relativeRef;
    return modalRef.result;
  }

  /**
   * @param title 
   * @param configurationComponent 
   * @param allowLoad 
   *      if true (default), the dialog loads and returns the selected configuration;
   *      if false just returns the selected configuration without loading it.
   * @param allowDelete
   *      if true (default) the UI provides buttons for deleting the configuration;
   *      if false the deletion of the configuration is disabled.
   * @param additionalReferences additional references not deletable. 
   *  If one of these references is chosen, it is just returned, its configuration is not loaded
   * 
   * 
   * @return returns a LoadConfigurationModalReturnData object with configuration and relativeReference
   */
  loadConfiguration(title: TextOrTranslation, configurationComponent: string, allowLoad?: boolean, allowDelete?: boolean, additionalReferences?: Reference[]) {
    const modalRef: NgbModalRef = this.modalService.open(LoadConfigurationModalComponent, new ModalOptions("lg"));
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    modalRef.componentInstance.configurationComponent = configurationComponent;
    if (allowLoad !== undefined) modalRef.componentInstance.allowLoad = allowLoad;
    if (allowDelete !== undefined) modalRef.componentInstance.allowDelete = allowDelete;
    if (additionalReferences !== undefined) modalRef.componentInstance.additionalReferences = additionalReferences;
    return modalRef.result;
  }

  localizedEditor(title: TextOrTranslation, localizedMap: LocalizedMap, allowEmpty?: boolean): Promise<LocalizedMap> {
    const modalRef: NgbModalRef = this.modalService.open(LocalizedEditorModalComponent, new ModalOptions('lg'));
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    modalRef.componentInstance.localizedMap = localizedMap;
    modalRef.componentInstance.allowEmpty = allowEmpty;
    return modalRef.result;
  }

}