import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Project } from "src/app/models/Project";
import { ProjectsServices } from "src/app/services/projects.service";
import { SettingsManager } from "src/app/utils/SettingsManager";

@Component({
  selector: "dataset-list-panel",
  templateUrl: "./dataset-list-panel.component.html",
  host: { class: "vbox" },
  standalone: false
})
export class DatasetListPanelComponent {

  @Input() consumer: Project;
  @Input() onlyOpen: boolean;

  @Output() projectSelected: EventEmitter<Project> = new EventEmitter<Project>();

  projects: Project[];
  selectedProject: Project;

  rendering: boolean;

  nameFilter: string = "";

  constructor(private projectService: ProjectsServices, private settingsMgr: SettingsManager) { }

  ngOnInit() {
    this.rendering = this.settingsMgr.getProjectRendering();

    this.projectService.listProjects(this.consumer, this.onlyOpen).subscribe(
      projects => {
        this.projects = projects;
      }
    );
  }

  switchRendering() {
    this.rendering = !this.rendering;
    this.settingsMgr.setProjectRendering(this.rendering).subscribe();
  }

  selectProject(project: Project) {
    this.selectedProject = project;
    this.projectSelected.emit(this.selectedProject);
  }

  isProjectVisible(project: Project): boolean {
    return this.nameFilter == "" || project.getName(this.rendering).toLocaleLowerCase().includes(this.nameFilter.toLocaleLowerCase());
  }

}
