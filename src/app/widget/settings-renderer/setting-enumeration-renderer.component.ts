import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Enumeration, SettingsPropType } from '../../models/Plugins';
import { CommonsUtils } from 'src/app/utils/CommonsUtils';

@Component({
    selector: 'setting-enumeration-renderer',
    templateUrl: './setting-enumeration-renderer.component.html',
    standalone: false
})
export class SettingEnumerationRendererComponent {

    @Input() type: SettingsPropType;
    @Input() enumeration: Enumeration;
    @Input() value: any;
    @Input() disabled: boolean = false;
    @Input() required: boolean = false;

    @Output() valueChanged = new EventEmitter<any>();

    randomId = CommonsUtils.getCryptoStrongRandomString(5);

    constructor() { }

    updateSetValue(value: any[]) {
        this.value = value;
        this.onModelChange();
    }
    
    onModelChange() {
        this.valueChanged.emit(this.value);
    }

}