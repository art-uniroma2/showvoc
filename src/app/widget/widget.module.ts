import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CodemirrorModule } from '@ctrl/ngx-codemirror';
import { NgbDropdownModule, NgbToastModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { QueryParameterFormComponent } from '../datasets/sparql/query-parameterization/query-parameter-form.component';
import { YasguiComponent } from '../datasets/sparql/yasgui.component';
import { BarChartComponent } from './charts/bar-chart.component';
import { LineChartComponent } from './charts/line-chart.component';
import { PieChartComponent } from './charts/pie-chart.component';
import { SeriesChartLegendComponent } from './charts/series-chart-legend.component';
import { HtmlEditorComponent } from './codemirror/html-editor/html-editor.component';
import { JsonEditorComponent } from './codemirror/json-editor/json-editor.component';
import { MustacheEditorComponent } from './codemirror/mustache-editor/mustache-editor.component';
import { DatasetListPanelComponent } from './dataset-list/dataset-list-panel.component';
import { DatasetsSelectorComponent } from './datasets-selector/dataset-selector.component';
import { CtrlKeyDirective } from './directives/ctrl-key.directive';
import { SanitizerDirective } from './directives/sanitizer.directive';
import { ShiftKeyDirective } from './directives/shift-key.directive';
import { ExpandableAlertComponent } from './expandable-alert/expandable-alert.component';
import { ExtensionConfiguratorComponent } from './extension-configurator/extension-configurator.component';
import { InputEditableComponent } from './input-editable/input-editable.component';
import { LangStringEditorComponent } from './lang-string-editor/lang-string-editor.component';
import { LanguageItemComponent } from './language-item/language-item.component';
import { LeafletMapComponent } from './leaflet-map/leaflet-map.component';
import { PasswordInputComponent } from './password-input/password-input.component';
import { FilePickerComponent } from './pickers/file-picker/file-picker.component';
import { InlineResourceListEditorComponent } from './pickers/inline-resource-list/inline-resource-list-editor.component';
import { LangPickerComponent } from './pickers/lang-picker/lang-picker.component';
import { LiteralPickerComponent } from './pickers/value-picker/literal-picker.component';
import { ResourcePickerComponent } from './pickers/value-picker/resource-picker.component';
import { ValuePickerComponent } from './pickers/value-picker/value-picker.component';
import { ManchesterExprComponent } from './rdf-resource/manchester-expr.component';
import { RdfResourceComponent } from './rdf-resource/rdf-resource.component';
import { ResourceListSelectionComponent } from './rdf-resource/resource-list-selection.component';
import { ResourceListComponent } from './rdf-resource/resource-list.component';
import { RenderingEditorComponent } from './rendering-editor/rendering-editor.component';
import { ResizableLayoutComponent } from './resizable-layout/resizable-layout.component';
import { DataSizeRendererComponent } from './settings-renderer/datasize-renderer.component';
import { NestedSettingSetRendererComponent } from './settings-renderer/nested-settings-renderer.component';
import { SettingEnumerationRendererComponent } from './settings-renderer/setting-enumeration-renderer.component';
import { SettingMapRendererComponent } from './settings-renderer/setting-map-renderer.component';
import { SettingPropRendererComponent } from './settings-renderer/setting-prop-renderer.component';
import { SettingSetRendererComponent } from './settings-renderer/setting-set-renderer.component';
import { SettingValueRendererComponent } from './settings-renderer/setting-value-renderer.component';
import { SettingsRendererPanelComponent } from './settings-renderer/settings-renderer-panel.component';
import { SettingsRendererComponent } from './settings-renderer/settings-renderer.component';
import { ToastsContainerComponent } from './toast/toast-container.component';
import { ToastService } from './toast/toast-service';
import { TypedLiteralInputComponent } from './typed-literal-input/typed-literal-input.component';
import { LocalizedEditorComponent } from './localized-editor/localized-editor.component';

@NgModule({
  imports: [
    CodemirrorModule,
    CommonModule,
    DragDropModule,
    FormsModule,
    NgbDropdownModule,
    NgbToastModule,
    NgxChartsModule,
    TranslateModule,
  ],
  declarations: [
    BarChartComponent,
    DataSizeRendererComponent,
    DatasetListPanelComponent,
    DatasetsSelectorComponent,
    ExpandableAlertComponent,
    ExtensionConfiguratorComponent,
    FilePickerComponent,
    HtmlEditorComponent,
    InlineResourceListEditorComponent,
    InputEditableComponent,
    JsonEditorComponent,
    LangPickerComponent,
    LangStringEditorComponent,
    LanguageItemComponent,
    LeafletMapComponent,
    LiteralPickerComponent,
    LineChartComponent,
    LocalizedEditorComponent,
    ManchesterExprComponent,
    MustacheEditorComponent,
    NestedSettingSetRendererComponent,
    PasswordInputComponent,
    PieChartComponent,
    QueryParameterFormComponent,
    RdfResourceComponent,
    RenderingEditorComponent,
    ResourceListComponent,
    ResourceListSelectionComponent,
    ResourcePickerComponent,
    ResizableLayoutComponent,
    SeriesChartLegendComponent,
    SettingEnumerationRendererComponent,
    SettingMapRendererComponent,
    SettingPropRendererComponent,
    SettingSetRendererComponent,
    SettingsRendererPanelComponent,
    SettingsRendererComponent,
    SettingValueRendererComponent,
    ToastsContainerComponent,
    TypedLiteralInputComponent,
    ValuePickerComponent,
    YasguiComponent,

    //directives
    CtrlKeyDirective,
    SanitizerDirective,
    ShiftKeyDirective,
  ],
  exports: [
    BarChartComponent,
    DataSizeRendererComponent,
    DatasetListPanelComponent,
    DatasetsSelectorComponent,
    ExpandableAlertComponent,
    ExtensionConfiguratorComponent,
    FilePickerComponent,
    HtmlEditorComponent,
    InlineResourceListEditorComponent,
    InputEditableComponent,
    JsonEditorComponent,
    LangPickerComponent,
    LangStringEditorComponent,
    LanguageItemComponent,
    LeafletMapComponent,
    LiteralPickerComponent,
    LineChartComponent,
    LocalizedEditorComponent,
    ManchesterExprComponent,
    MustacheEditorComponent,
    PasswordInputComponent,
    PieChartComponent,
    QueryParameterFormComponent,
    RdfResourceComponent,
    RenderingEditorComponent,
    ResourceListComponent,
    ResourceListSelectionComponent,
    ResourcePickerComponent,
    ResizableLayoutComponent,
    SettingMapRendererComponent,
    SettingSetRendererComponent,
    SettingsRendererPanelComponent,
    SettingsRendererComponent,
    ToastsContainerComponent,
    TypedLiteralInputComponent,
    ValuePickerComponent,
    YasguiComponent,

    //directives
    CtrlKeyDirective,
    SanitizerDirective,
    ShiftKeyDirective,
  ],
  providers: [
    ToastService
  ]
})
export class WidgetModule { }
