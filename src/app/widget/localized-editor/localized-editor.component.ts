import { Component, forwardRef, Input } from "@angular/core";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { Language } from "src/app/models/LanguagesCountries";
import { SVContext } from "src/app/utils/SVContext";

@Component({
  selector: "localized-editor",
  templateUrl: "./localized-editor.component.html",
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => LocalizedEditorComponent), multi: true,
  }],
  standalone: false
})
export class LocalizedEditorComponent {

  @Input() size: "sm" | "lg";

  localizedMap: LocalizedMap = new Map();

  items: LocalizedItem[] = [];

  pendingItem: LocalizedItem;
  pendingLangs: string[]; //languages for which still doesn't exist a item

  btnClass: string = "";
  inputClass: string = "";

  constructor(private translateService: TranslateService) { }

  ngOnInit() {
    if (this.size) {
      this.btnClass = "btn-" + this.size;
      this.inputClass = "form-control-" + this.size;
    }
  }

  private init() {
    this.items = [];
    this.localizedMap.forEach((value, lang) => {
      let l: LocalizedItem = new LocalizedItem();
      l.lang = lang;
      l.value = value;
      this.items.push(l);
    });

    this.updatePendingLangs();
  }

  private updatePendingLangs() {
    this.pendingLangs = SVContext.getSystemSettings().languages
      .filter((l: Language) => !this.items.some(i => i.lang == l.tag)) //keep only those language not already in items list
      .map((l: Language) => l.tag); //get only the tag
  }

  addItem() {
    this.updatePendingLangs();
    let currentLang = this.translateService.currentLang;
    let newItem = new LocalizedItem();
    if (this.pendingLangs.includes(currentLang)) { //current i18n language not yet in items list => use it for new item
      newItem.lang = currentLang;
    }
    this.pendingItem = newItem;
  }

  isNewItemOk(): boolean {
    //item cannot be added if incomplete or if language is already used
    if (this.pendingItem.value == null || this.pendingItem.value.trim() == "" || this.pendingItem.lang == null) {
      return false;
    }
    if (this.items.some(l => l.lang == this.pendingItem.lang)) {
      return false;
    }
    return true;
  }

  confirmNewItem() {
    this.items.push(this.pendingItem);
    this.pendingItem = null;
    this.onModelChange();
  }

  removeItem(item: LocalizedItem) {
    this.items.splice(this.items.indexOf(item), 1);
    this.updatePendingLangs();
    this.onModelChange();
  }

  onModelChange() {
    this.localizedMap = new Map();
    this.items.forEach(l => {
      this.localizedMap.set(l.lang, l.value);
    });
    this.propagateChange(this.localizedMap);
  }


  //---- method of ControlValueAccessor and Validator interfaces ----

  writeValue(obj: LocalizedMap) {
    if (obj) {
      this.localizedMap = obj;
      this.init();
    }
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };


}

export interface LocalizedMap extends Map<string, string> { }

class LocalizedItem {
  lang: string;
  value: string;
}