import { Injectable } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { TextOrTranslation, TranslationUtils } from 'src/app/utils/TranslationUtils';
import { Toast, ToastOpt } from "./Toasts";

@Injectable()
export class ToastService {

    constructor(private translateService: TranslateService) {}
    
    toasts: Toast[] = [];

    show(title: TextOrTranslation, message: TextOrTranslation, options?: ToastOpt) {
        options = this.mergeOptions(options);
        let t = TranslationUtils.getTranslatedText(title, this.translateService);
        let msg = TranslationUtils.getTranslatedText(message, this.translateService);
        this.toasts.push({ title: t, message: msg, options: options });
    }

    private mergeOptions(options?: ToastOpt): ToastOpt {
        //if options is provided and its parameters is not null, override the defaults
        let opt: ToastOpt = {
            toastClass: options && options.toastClass != null ? options.toastClass : "bg-info",
            textClass: options && options.textClass != null ? options.textClass : "text-white",
            delay: options && options.delay != null ? options.delay : 4000,
        };
        return opt;
    }

    remove(toast: Toast) {
        this.toasts = this.toasts.filter(t => t !== toast);
    }

}