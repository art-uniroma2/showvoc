import { Component } from '@angular/core';
import { ToastService } from './toast-service';

@Component({
  selector: 'app-toasts',
  templateUrl: "toast-container.component.html",
  host: { '[class.ngb-toasts]': 'true' },
  standalone: false
})
export class ToastsContainerComponent {

  constructor(public toastService: ToastService) { }

}