import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SharedModalsServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { ConfigurationsServices } from 'src/app/services/configurations.service';
import { Configuration } from '../../models/Configuration';
import { ConfigurableExtensionFactory, ExtensionConfigurationStatus, Settings } from '../../models/Plugins';

@Component({
  selector: 'extension-configurator',
  templateUrl: './extension-configurator.component.html',
  standalone: false
})
export class ExtensionConfiguratorComponent {

  @Input() extensions: ConfigurableExtensionFactory[];
  @Input() storeable: boolean = true; //tells if the component should allow to store and load configuration
  @Input() disabled: boolean = false;
  @Output() extensionUpdated = new EventEmitter<ConfigurableExtensionFactory>();
  @Output() configurationUpdated = new EventEmitter<Settings>();
  @Output() configStatusUpdated = new EventEmitter<{ status: ExtensionConfigurationStatus, relativeReference?: string }>();

  selectedExtension: ConfigurableExtensionFactory;
  selectedConfiguration: Settings;

  private status: ExtensionConfigurationStatus;

  constructor(private configurationService: ConfigurationsServices, private sharedModals: SharedModalsServices) { }

  ngOnInit() {
    this.selectedExtension = this.extensions[0];
    this.extensionUpdated.emit(this.selectedExtension);

    if (this.selectedExtension.configurations != null) {
      this.selectedConfiguration = this.selectedExtension.configurations[0];
      this.configurationUpdated.emit(this.selectedConfiguration);
    }

    this.status = ExtensionConfigurationStatus.unsaved;
    this.configStatusUpdated.emit({ status: this.status });
  }

  onChangeExtension() {
    this.extensionUpdated.emit(this.selectedExtension);
    if (this.selectedExtension.configurations != null) { //if extension has configurations
      this.selectedConfiguration = this.selectedExtension.configurations[0];
      this.configurationUpdated.emit(this.selectedConfiguration);

      this.status = ExtensionConfigurationStatus.unsaved;
      this.configStatusUpdated.emit({ status: this.status });
    }
  }

  onChangeConfig() {
    this.configurationUpdated.emit(this.selectedConfiguration);

    this.status = ExtensionConfigurationStatus.unsaved;
    this.configStatusUpdated.emit({ status: this.status });
  }

  configure() {
    this.sharedModals.configurePlugin(this.selectedConfiguration).then(
      (cfg: Settings) => {
        //update the selected configuration...
        this.selectedConfiguration = cfg;
        //...and the configuration among the availables
        let configs: Settings[] = this.selectedExtension.configurations;
        for (let i = 0; i < configs.length; i++) {
          if (configs[i].shortName == this.selectedConfiguration.shortName) {
            configs[i] = this.selectedConfiguration;
          }
        }

        this.extensionUpdated.emit(this.selectedExtension);
        this.configurationUpdated.emit(this.selectedConfiguration);

        this.status = ExtensionConfigurationStatus.unsaved;
        this.configStatusUpdated.emit({ status: this.status });
      },
      () => { }
    );
  }

  saveConfig() {
    //TODO currently there is no extension that allow to save configuration
    // let config: { [key: string]: any } = this.selectedConfiguration.getPropertiesAsMap();
    // this.sharedModals.storeConfiguration("Store configuration", this.selectedExtension.id, config).then(
    //     (relativeRef: string) => {
    //         this.basicModals.alert("Save configuration", "Configuration saved succesfully");

    //         this.status = ExtensionConfigurationStatus.saved;
    //         this.configStatusUpdated.emit({ status: this.status, relativeReference: relativeRef });
    //     },
    //     () => {}
    // );
  }

  loadConfig() {
    //TODO currently there is no extension that allow to load configuration
    // this.sharedModals.loadConfiguration("Load configuration", this.selectedExtension.id).then(
    //     (config: LoadConfigurationModalReturnData) => {
    //         for (let i = 0; i < this.selectedExtension.configurations.length; i++) {
    //             if (this.selectedExtension.configurations[i].type == config.configuration.type) {
    //                 this.selectedExtension.configurations[i] = config.configuration;
    //                 this.selectedConfiguration = this.selectedExtension.configurations[i];
    //             }
    //         }
    //         this.configurationUpdated.emit(this.selectedConfiguration);

    //         this.status = ExtensionConfigurationStatus.saved;
    //         this.configStatusUpdated.emit({ status: this.status, relativeReference: config.reference.relativeReference });
    //     },
    //     () => {}
    // );
  }

  //useful to change the selected extension and configuration from a parent component
  public selectExtensionAndConfiguration(extensionID: string, configurationType: string) {
    for (const ext of this.extensions) {
      if (ext.id == extensionID) {
        this.selectedExtension = ext;
        this.extensionUpdated.emit(this.selectedExtension);
        break;
      }
    }
    for (const conf of this.selectedExtension.configurations) {
      if (conf.type == configurationType) {
        this.selectedConfiguration = conf;
        this.configurationUpdated.emit(this.selectedConfiguration);
      }
    }
  }

  public forceConfiguration(extensionID: string, config: Settings) {
    //select extension
    for (let ext of this.extensions) {
      if (ext.id == extensionID) {
        this.selectedExtension = ext;
        this.extensionUpdated.emit(this.selectedExtension);
        break;
      }
    }
    //force configuration
    this.selectConfigOfSelectedExtension(config);
  }

  //useful only for the filter chain, in order to force the load of a single filter
  public forceConfigurationByRef(extensionID: string, configRef: string) {
    //select extension
    for (let ext of this.extensions) {
      if (ext.id == extensionID) {
        this.selectedExtension = ext;
        this.extensionUpdated.emit(this.selectedExtension);
        break;
      }
    }
    //load the configuration
    this.configurationService.getConfiguration(this.selectedExtension.id, configRef).subscribe(
      (conf: Configuration) => {
        this.selectConfigOfSelectedExtension(conf);

        this.status = ExtensionConfigurationStatus.saved;
        this.configStatusUpdated.emit({ status: this.status, relativeReference: configRef });
      }
    );
  }

  private selectConfigOfSelectedExtension(config: Settings) {
    for (let i = 0; i < this.selectedExtension.configurations.length; i++) {
      if (this.selectedExtension.configurations[i].type == config.type) {
        this.selectedExtension.configurations[i] = config;
        this.selectedConfiguration = this.selectedExtension.configurations[i];
        break;
      }
    }
    this.configurationUpdated.emit(this.selectedConfiguration);
  }


}