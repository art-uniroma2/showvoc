import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { Project, ProjectVisibility } from 'src/app/models/Project';
import { ProjectsServices } from 'src/app/services/projects.service';

@Component({
  selector: 'datasets-selector',
  templateUrl: './dataset-selector.component.html',
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => DatasetsSelectorComponent), multi: true,
  }],
  standalone: false
})
export class DatasetsSelectorComponent {

  loading: boolean;

  datasets: DatasetStruct[];

  selection: Project[] = []; //datasets to be select on init

  filterString: string;
  onlyOpen: boolean;

  constructor(private projectsService: ProjectsServices) { }

  ngOnInit() {
    this.loading = true;
    this.projectsService.listProjects(null, false, [ProjectVisibility.PUBLIC]).pipe(
      finalize(() => { this.loading = false; })
    ).subscribe(
      publicProjects => {
        this.datasets = publicProjects.map(d => {
          return {
            checked: false,
            project: d,
            visible: true
          };
        });
        this.updateSelection();
      }
    );
  }

  updateSelection() {
    if (!this.datasets) return; //datasets not yet initialized
    this.datasets.forEach(d => {
      d.checked = this.selection.some(s => s.getName() == d.project.getName());
    });
  }

  toggleCheck(dataset: DatasetStruct) {
    dataset.checked = !dataset.checked;
    this.onChanges();
  }

  checkAll(check: boolean) {
    this.datasets.forEach(f => { f.checked = check; });
    this.onChanges();
  }

  toggleOnlyOpen() {
    this.onlyOpen = !this.onlyOpen;
    this.applyFilters();
  }

  applyFilters() {
    this.datasets.forEach(d => {
      let matchFilterString: boolean = this.filterString == null || this.filterString.trim() == "" || d.project.getName(true).toUpperCase().includes(this.filterString.toUpperCase());
      let matchOnlyOpen: boolean = !this.onlyOpen || d.project.isOpen();
      d.visible = matchFilterString && matchOnlyOpen;
    });
  }

  onChanges() {
    this.selection = this.datasets.filter(d => d.checked).map(d => d.project);
    this.propagateChange(this.selection);
  }

  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(selection: Project[]) {
    if (selection) {
      this.selection = selection;
      this.updateSelection();
    }
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };

  //--------------------------------------------------


}


interface DatasetStruct {
  checked: boolean;
  project: Project;
  visible: boolean;
}