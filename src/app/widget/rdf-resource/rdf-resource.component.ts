import { Component, Input, SimpleChanges } from '@angular/core';
import { Language, Languages } from 'src/app/models/LanguagesCountries';
import { AnnotatedValue, IRI, Literal, ResAttribute, Resource, ResourceNature, Value } from 'src/app/models/Resources';
import { ResViewUtils } from 'src/app/models/ResourceView';
import { XmlSchema } from 'src/app/models/Vocabulary';
import { ResourceUtils } from 'src/app/utils/ResourceUtils';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { UIUtils } from 'src/app/utils/UIUtils';

@Component({
  selector: 'rdf-resource',
  templateUrl: './rdf-resource.component.html',
  styleUrls: ["./rdf-resource.component.scss"],
  standalone: false
})
export class RdfResourceComponent {

  @Input() resource: AnnotatedValue<Value>;
  @Input() rendering: boolean = true; //if true the resource should be rendered with the show, with the qname otherwise
  @Input() showIcon: boolean = true;

  renderingLabel: string;
  renderingClass: string = "";

  language: Language; //language of the resource
  showLang: boolean = true;

  datatype: IRI; //datatype of the resource

  literalWithLink: boolean = false; //true if the resource is a literal which contains url
  splittedLiteral: string[]; //when literalWithLink is true, even elements are plain text, odd elements are url

  imgSrc: string; //src of the image icon
  natureTooltip: string;

  valueTooltip: string;

  isManchExpr: boolean = false;

  constructor(private settingsMgr: SettingsManager) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['resource'] && changes['resource'].currentValue) {
      this.init();
    } if (changes['rendering']) {
      this.initRenderingLabel();
    }
  }

  init() {
    this.initRenderingLabel();
    this.initImgSrc();
    this.initLang();
    this.initDatatype();
    this.initLiteralWithLink();
    this.initRenderingClass();
    this.initNatureTooltip();
    this.initValueTooltip();
    this.initManchExpr();
  }

  private initRenderingLabel() {
    this.renderingLabel = ResourceUtils.getRendering(this.resource, this.rendering);
  }

  /**
   * Initializes the class of the resource text: green if the resource is in the staging-add-graph, red if it's in the staging-remove-graph
   */
  private initRenderingClass() {
    this.renderingClass = "";
    let value = this.resource.getValue();
    if (value instanceof Resource) {
      if (ResourceUtils.isResourceInStagingAdd(this.resource)) {
        this.renderingClass += " proposedAddRes";
      } else if (ResourceUtils.isResourceInStagingRemove(this.resource)) {
        this.renderingClass += " proposedRemoveRes";
      }
    }

    if (ResourceUtils.isTripleInStagingAdd(this.resource)) {
      this.renderingClass += " proposedAddTriple";
    } else if (ResourceUtils.isTripleInStagingRemove(this.resource)) {
      this.renderingClass += " proposedRemoveTriple";
    }
  }

  private initDatatype(): void {
    //reset
    this.datatype = null;
    //init
    let value = this.resource.getValue();

    if (value instanceof Literal) { // if it is a literal
      this.datatype = value.getDatatype();
    } else { // otherwise, it is a resource, possibly with an additional property dataType (as it could be from a custom form preview)
      let dtAttr = this.resource.getAttribute(ResAttribute.DATA_TYPE);
      if (dtAttr != null) {
        this.datatype = new IRI(dtAttr);
      }
    }
  }

  private initValueTooltip() {
    this.valueTooltip = this.resource.getValue().toNT();
  }

  private initNatureTooltip() {
    let value = this.resource.getValue();
    this.natureTooltip = null;
    if (value instanceof Resource) {
      let natureList: ResourceNature[] = this.resource.getNature();
      if (natureList.length > 0) {
        let natureListSerlalized: string[] = [];
        natureList.forEach(n => {
          let graphsToNT: string[] = [];
          n.graphs.forEach(g => {
            graphsToNT.push(g.toNT());
          });
          natureListSerlalized.push(ResourceUtils.getResourceRoleLabel(n.role) + ", defined in: " + graphsToNT.join(", "));
        });
        this.natureTooltip = natureListSerlalized.join("\n\n");
      } else { //nature empty => could be the case of a reified resource => check language or datatype (representing the one of the preview value)
        if (this.language != null) {
          this.natureTooltip = this.language.tag;
        } else if (this.datatype != null) {
          this.natureTooltip = this.datatype.toNT();
        }
      }
    } else if (value instanceof Literal) {
      if (this.language != null) {
        this.natureTooltip = this.language.tag;
      } else if (this.datatype != null) {
        this.natureTooltip = this.datatype.toNT();
      }
    }
  }

  /**
   * If the resource is a literal with a link, splits the literal value so it can be rendered with different elements
   * like <span> for plain text (even elements of array) or <a> for url (odd elements)
   */
  private initLiteralWithLink() {
    let literalWithLinkInfo = ResViewUtils.getLiteralWithLinkInfo(this.resource.getValue());
    this.literalWithLink = literalWithLinkInfo.literalWithLink;
    this.splittedLiteral = literalWithLinkInfo.splittedLiteral;
  }

  private initManchExpr() {
    this.isManchExpr = this.resource.getValue().isBNode() && this.resource.getAttribute(ResAttribute.SHOW_INTERPR) != null;
  }

  /**
   * Initializes the source of the icon image
   */
  private initImgSrc() {
    this.imgSrc = UIUtils.getImageSrc(this.resource);
  }

  /**
   * Returns the language tag of the current resource in order to show it as title of resource icon (flag)
   * Note: this should be used in template only when isResourceWithLang returns true
   */
  private initLang(): void {
    //reset
    this.language = null;
    //init
    let value = this.resource.getValue();
    let lang: string;
    if (value.isResource()) { //even if it is a resource, get the lang (it could be a custom form preview)
      lang = this.resource.getAttribute(ResAttribute.LANG);
    } else if (value instanceof Literal) {
      lang = value.getLanguage();
      if (value.getDatatype().equals(XmlSchema.language)) {
        lang = value.getLanguage();
      }
    }
    if (lang != null) {
      this.language = Languages.getLanguageFromTag(lang);
    }

    let project = SVContext.getProject();
    if (project) {
      this.showLang = !this.settingsMgr.getResourceViewHideLiteral(project);
    }
  }

  /**
   * Tells if the described resource is explicit.
   * Useful for flag icons since they have not the "transparent" version (as for the concept/class/property... icons)
   */
  isExplicit(): boolean {
    return this.resource.getAttribute(ResAttribute.EXPLICIT) || this.resource.getAttribute(ResAttribute.EXPLICIT) == undefined;
  }

}