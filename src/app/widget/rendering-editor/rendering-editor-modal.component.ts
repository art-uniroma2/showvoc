import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Scope } from 'src/app/models/Plugins';
import { Project } from 'src/app/models/Project';
import { SettingsServices } from 'src/app/services/settings.service';
import { SettingsManager, SettingsManagerID } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { ToastService } from '../toast/toast-service';

@Component({
  selector: "rendering-editor-modal",
  templateUrl: "./rendering-editor-modal.component.html",
  standalone: false
})
export class RenderingEditorModalComponent {

  project: Project;

  // differentFromDefault: boolean;

  defaultPrefLangs: string[]; //default rendering languages
  renderingLangs: string[];

  constructor(public activeModal: NgbActiveModal, private settingsMgr: SettingsManager, private settingsService: SettingsServices, private toastService: ToastService) { }

  ngOnInit() {
    this.project = SVContext.getWorkingProject();
    this.initLanguages();
  }

  initLanguages() {
    this.renderingLangs = this.settingsMgr.getRenderingLanguages(this.project);
  }


  onRenderingChange() {
    this.settingsMgr.setRenderingLanguages(this.project, this.renderingLangs).subscribe();
  }

  restoreDefault() {
    this.settingsMgr.setRenderingLanguages(this.project, null).subscribe(
      () => {
        this.toastService.show({ key: "COMMONS.STATUS.OPERATION_DONE" }, { key: "MESSAGES.DEFAULT_RESTORED" }, { toastClass: "bg-success" });
        //reinit the rendering engine settings, so that the defaults are computed again
        this.settingsMgr.initSettings(Scope.PROJECT_USER, SettingsManagerID.RenderingEngine, this.project).subscribe(
          () => {
            this.initLanguages();
          }
        );
      }
    );
  }

  setAsUserDefault() {
    this.settingsMgr.setDefaultRenderingLanguages(this.renderingLangs, Scope.USER).subscribe(
      () => {
        this.toastService.show({ key: "COMMONS.STATUS.OPERATION_DONE" }, { key: "MESSAGES.DEFAULT_UPDATED" }, { toastClass: "bg-success" });
      }
    );
  }

  close() {
    this.activeModal.dismiss();
  }

}