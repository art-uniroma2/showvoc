import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { LinksetMetadata } from 'src/app/models/Metadata';
import { MapleServices } from 'src/app/services/maple.service';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { AlignmentsTreeComponent } from './alignments-tree.component';

@Component({
    selector: 'alignments-tree-panel',
    templateUrl: './alignments-tree-panel.component.html',
    host: { class: "vbox" },
    standalone: false
})
export class AlignmentsTreePanelComponent {

    @ViewChild(AlignmentsTreeComponent) viewChildTree: AlignmentsTreeComponent;
    @Output() linksetSelected = new EventEmitter<LinksetMetadata>();

    selectedLinkset: LinksetMetadata;

    showPercentage: boolean = false;
    loadingProfile: boolean = false;

    datasetRendering: boolean;

    constructor(private settingsMgr: SettingsManager, private mapleService: MapleServices, private basicModals: BasicModalsServices) { }

    ngOnInit() {
        this.datasetRendering = this.settingsMgr.getProjectRendering();
    }

    refresh() {
        this.viewChildTree.init();
    }

    refreshProfile() {
        this.basicModals.confirm({ key: "DATASETS.ACTIONS.PROFILE_DATASET" },
            { key: "MESSAGES.REFRESH_METADATA_CONFIRM", params: { datasetName: SVContext.getWorkingProject().getName() } }, ModalType.info).then(
                () => {
                    this.loadingProfile = true;
                    this.mapleService.profileProject().pipe(
                        finalize(() => { this.loadingProfile = false; })
                    ).subscribe(
                        () => {
                            this.refresh();
                        }
                    );
                },
                () => {}
            );
    }

    onLinksetSelected(linkset: LinksetMetadata) {
        this.selectedLinkset = linkset;
        this.linksetSelected.emit(linkset);
    }

    switchRendering() {
        /*
        I prefer to not make the change of dataset rendering persistent here, 
        I think it is better to be just a temp thing and leave the general setting
        changeable in the Datasets page and Datasets Administration panel
        */
        this.datasetRendering = !this.datasetRendering;
    }

}