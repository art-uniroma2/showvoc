import { Component, Input, SimpleChanges } from '@angular/core';
import FileSaver from 'file-saver';
import { concat, forkJoin, Observable, of } from 'rxjs';
import { defaultIfEmpty, finalize, map, toArray } from 'rxjs/operators';
import { BrowsingModalsServices } from '../modal-dialogs/browsing-modals/browsing-modals.service';
import { SharedModalsServices } from '../modal-dialogs/shared-modals/shared-modals.service';
import { LinksetMetadata, PrefixMapping } from '../models/Metadata';
import { Project } from '../models/Project';
import { RDFFormat } from '../models/RDFFormat';
import { AnnotatedValue, IRI, ResAttribute, Triple } from '../models/Resources';
import { OWL, RDFS, SKOS } from '../models/Vocabulary';
import { AlignmentServices } from '../services/alignment.service';
import { ExportServices } from '../services/export.service';
import { MetadataServices } from '../services/metadata.service';
import { ResourcesServices } from '../services/resources.service';
import { STRequestOptions } from '../utils/HttpManager';
import { LocalStorageManager } from '../utils/LocalStorageManager';
import { NTriplesUtil, ResourceUtils } from '../utils/ResourceUtils';
import { SettingsManager } from '../utils/SettingsManager';
import { SVContext } from '../utils/SVContext';
import { AlignmentSearchData } from './alignments-searchbar.component';
import { BasicModalsServices } from '../modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from '../modal-dialogs/Modals';

@Component({
  selector: 'alignments-view',
  templateUrl: './alignments-view.component.html',
  host: { class: "vbox" },
  standalone: false
})
export class AlignmentsViewComponent {

  @Input() source: Project; //source project of the alignment, if not provided it is taken from the SVContext
  @Input() linkset: LinksetMetadata;

  sourceProject: Project;
  targetProject: Project;

  filterPredicates: AnnotatedValue<IRI>[] = [];

  loading: boolean = false;
  annotatedMappings: Triple<AnnotatedValue<IRI>>[];

  private filteredMappings: Triple<IRI>[]; //cache of the last filterMappings call, useful for 
  private lastSearch: AlignmentSearchData;

  mappingsCount: number;

  exportFormats: RDFFormat[];
  exporting: boolean = false;

  prefixNsMapping: PrefixMapping[];

  rendering: boolean = true; //of aligned resources
  datasetRendering: boolean;

  constructor(
    private alignmentService: AlignmentServices,
    private metadataService: MetadataServices,
    private resourcesService: ResourcesServices,
    private exportService: ExportServices,
    private settingsMgr: SettingsManager,
    private basicModals: BasicModalsServices,
    private sharedModals: SharedModalsServices,
    private browsingModals: BrowsingModalsServices
  ) { }

  ngOnInit() {
    let renderingStored = LocalStorageManager.getItem(LocalStorageManager.ALIGNMENT_VIEW_RENDERING);
    if (typeof renderingStored == "boolean") {
      this.rendering = renderingStored;
    }

    this.datasetRendering = this.settingsMgr.getProjectRendering();

    this.exportService.getOutputFormats().subscribe(
      formats => {
        this.exportFormats = formats;
      }
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['linkset']) {
      this.init();
    }
  }

  private init() {
    //reset data about previos filters (if any)
    this.filteredMappings = null;
    this.lastSearch = null;
    this.page = 0;
    this.pageSelected = this.page;
    this.filterPredicates = [];

    /* init the two dataset context */

    if (this.source == null) { //get the current project if no source project is provided
      this.source = SVContext.getWorkingProject();
    }
    let linksetTargetProject: Project = this.linkset.targetDatasetProject;

    let setupProjectsFn: Observable<any>[] = [];

    //- source project: init the settings only if it's not the one currently accessed (otherwise it is assumed that its settings are already initialize)
    if (SVContext.getWorkingProject() == null || SVContext.getWorkingProject().getName() != this.source.getName()) {
      setupProjectsFn = setupProjectsFn.concat([
        this.settingsMgr.initSettingsAtProjectAccess(this.source)
      ]);
    }

    //- target
    if (linksetTargetProject != null) { //this means that target resources belong to a project hosted on showvoc
      setupProjectsFn = setupProjectsFn.concat([
        this.settingsMgr.initSettingsAtProjectAccess(linksetTargetProject)
      ]);
    }


    forkJoin(setupProjectsFn)
      .pipe(defaultIfEmpty(null)) //in case source project is the active one and target linkset is not related to a project in SV
      .subscribe(
        () => {
          //bound projects only when their settings are initialized (preventing error il alignment-searchbar that expects them available)
          this.sourceProject = this.source;
          this.targetProject = linksetTargetProject;

          this.ensurePrefixMappingReady().subscribe(
            () => {
              this.countAndInitAllMappings();
            }
          );
        }
      );
  }

  private ensurePrefixMappingReady(): Observable<void> {
    if (this.prefixNsMapping != null) {
      return of(null); //already initialized here
    } else if (SVContext.getWorkingProject() != null && SVContext.getWorkingProject().getName() == this.sourceProject.getName()) {
      //not initialized, but the source project is the currently accessed, so take them from SVContext
      this.prefixNsMapping = SVContext.getPrefixMappings(); //source
      return of(null);
    } else {
      //never initialized, do it now
      return this.metadataService.getNamespaceMappings(new STRequestOptions({ ctxProject: this.sourceProject })).pipe(
        map(prefNs => {
          this.prefixNsMapping = prefNs;
        })
      );
    }
  }


  onSearch(searchData: AlignmentSearchData) {
    this.page = 0;
    //empty search => reinit "standard" mappings (not filtered)
    if (searchData.searchString == null || searchData.searchString.trim() == "") {
      this.lastSearch = null;
      this.countAndInitAllMappings();
    } else {
      this.lastSearch = searchData;
      this.listFilteredMappings();
    }
  }

  /**
   * Init all unfiltered (and paginated) mappings. To be called the first time or whenever user execute an empty search after (so that the page count is reinitialized)
   */
  private countAndInitAllMappings() {
    let mappingProps = this.filterPredicates.map(p => p.getValue());
    let includeSubProperties: boolean = (mappingProps.length > 0) ? false : null; //default null (not true/false) so that, if not explicitly set, the default is determined server side
    this.alignmentService.getMappingCount(this.linkset.targetDataset.uriSpace, mappingProps, includeSubProperties, null, this.pageSize, new STRequestOptions({ ctxProject: this.sourceProject })).subscribe(
      count => {
        this.mappingsCount = count;
        this.totPage = Math.ceil(count / this.pageSize);
        this.pageSelectorOpts = Array.from({ length: this.totPage }, (v, k) => k);
        this.pageSelected = 0;
        this.listAllMappings();
      }
    );
  }

  private listAllMappings() {
    this.loading = true;
    let renderingLangs = this.settingsMgr.getRenderingLanguages(this.sourceProject);
    let sortLang = renderingLangs?.length > 0 ? renderingLangs[0] : null;
    let mappingProps = this.filterPredicates.map(p => p.getValue());
    let includeSubProperties: boolean = (mappingProps.length > 0) ? false : null; //default null (not true/false) so that, if not explicitly set, the default is determined server side
    this.alignmentService.getMappings(this.linkset.targetDataset.uriSpace, this.page, this.pageSize, mappingProps, includeSubProperties, sortLang, new STRequestOptions({ ctxProject: this.sourceProject }))
      .pipe(finalize(() => { this.loading = false; }))
      .subscribe(
        mappings => {
          this.annotatedMappings = [];
          mappings.forEach(m => {
            let annPredicate = new AnnotatedValue(m.getMiddle(), { [ResAttribute.QNAME]: ResourceUtils.getQName(m.getMiddle().getIRI(), this.prefixNsMapping) });
            this.annotatedMappings.push(new Triple(new AnnotatedValue(m.getLeft()), annPredicate, new AnnotatedValue(m.getRight())));
          });
          this.annotateMappingResources();
        }
      );
  }


  private listFilteredMappings() {
    this.loading = true;
    let renderingLangs = this.settingsMgr.getRenderingLanguages(this.sourceProject);
    let sortLang = renderingLangs?.length > 0 ? renderingLangs[0] : null;
    let mappingProps = this.filterPredicates.map(p => p.getValue());
    this.alignmentService.filterMappings(this.linkset.targetDataset.uriSpace, this.lastSearch.searchString, this.lastSearch.searchMode,
      mappingProps, true, this.lastSearch.useLocalName, this.lastSearch.useURI, this.lastSearch.useNotes,
      null, sortLang, null, new STRequestOptions({ ctxProject: this.sourceProject }))
      .pipe(finalize(() => { this.loading = false; }))
      .subscribe(
        mappings => {
          this.filteredMappings = mappings;
          this.mappingsCount = this.filteredMappings.length;
          //paging
          this.totPage = Math.ceil(this.mappingsCount / this.pageSize);
          this.pageSelectorOpts = Array.from({ length: this.totPage }, (v, k) => k);
          this.pageSelected = 0;
          this.simulateSearchResultPagination();
        }
      );
  }

  private annotateMappingResources() {
    let leftEntities: IRI[] = [];
    let rightEntities: IRI[] = [];
    this.annotatedMappings.forEach(m => {
      leftEntities.push(m.getLeft().getValue());
      rightEntities.push(m.getRight().getValue());
    });
    let annotateFunctions: Observable<void>[] = [];

    //annotate left
    if (leftEntities.length > 0) {
      let annotateLeft: Observable<void> = this.resourcesService.getResourcesInfo(leftEntities, new STRequestOptions({ ctxProject: this.sourceProject })).pipe(
        finalize(() => {
        }),
        map(annotated => {
          annotated.forEach(a => {
            this.annotatedMappings.forEach(mapping => {
              if (mapping.getLeft().getValue().equals(a.getValue())) {
                mapping.setLeft(a);
              }
            });
          });
        })
      );
      annotateFunctions.push(annotateLeft);
    }

    //annotate right (only if right project is available)
    if (rightEntities.length > 0 && this.targetProject != null) {
      let annotateRight: Observable<void> = this.resourcesService.getResourcesInfo(rightEntities, new STRequestOptions({ ctxProject: this.targetProject, ctxConsumer: this.sourceProject })).pipe(
        map(annotated => {
          annotated.forEach(a => {
            this.annotatedMappings.forEach(mapping => {
              if (mapping.getRight().getValue().equals(a.getValue())) {
                mapping.setRight(a);
              }
            });
          });
        })
      );
      annotateFunctions.push(annotateRight);
    }
    concat(...annotateFunctions).pipe(
      toArray()
    ).subscribe();
  }

  openSourceResource(resource: AnnotatedValue<IRI>) {
    this.sharedModals.openResourceView(resource.getValue(), this.sourceProject);
  }

  openTargetResource(resource: AnnotatedValue<IRI>) {
    let ctxProject: Project = this.sourceProject; //by default use the source project as ctx project
    if (this.linkset.targetDatasetProject != null) { //if target project is known, set it as context project
      ctxProject = this.linkset.targetDatasetProject;
    }
    this.sharedModals.openResourceView(resource.getValue(), ctxProject);
  }


  exportMappings(format: RDFFormat) {
    this.exporting = true;
    this.alignmentService.exportMappings(this.linkset.targetDataset.uriSpace, format, null, new STRequestOptions({ ctxProject: this.sourceProject })).subscribe(
      blob => {
        this.exporting = false;
        FileSaver.saveAs(blob, "alignment_export." + format.defaultFileExtension);
      }
    );
  }

  pickFilterPredicate() {
    let rootProps = [SKOS.mappingRelation, OWL.sameAs, OWL.differentFrom, OWL.equivalentClass, OWL.disjointWith, RDFS.subClassOf];
    this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }, rootProps, null, true, this.sourceProject).then(
      (prop: AnnotatedValue<IRI>) => {
        this.addFilterPredicate(prop);
      },
      () => { }
    );
  }

  addFilterPredicateManually() {
    this.basicModals.prompt({ key: "COMMONS.ACTIONS.ADD_MANUALLY" }, { value: "IRI" }).then(
      valueIRI => {
        let prop: IRI;
        if (ResourceUtils.testIRI(valueIRI)) { //valid iri (e.g. "http://test")
          prop = new IRI(valueIRI);
        } else { //not an IRI, try to parse as NTriples
          try {
            prop = NTriplesUtil.parseIRI(valueIRI);
          } catch { //neither a valid ntriple iri (e.g. "<http://test>")
            this.basicModals.alert({ key: "COMMONS.STATUS.INVALID_VALUE" }, { key: "MESSAGES.INVALID_IRI", params: { iri: valueIRI } }, ModalType.warning);
            return;
          }
        }
        this.resourcesService.getResourcesInfo([prop], new STRequestOptions({ ctxProject: this.sourceProject })).subscribe(
          (annPred: AnnotatedValue<IRI>[]) => {
            this.addFilterPredicate(annPred[0]);
          }
        );
      },
      () => { }
    );
  }

  private addFilterPredicate(prop: AnnotatedValue<IRI>) {
    if (!this.filterPredicates.some(p => p.equals(prop))) { //if not already in
      this.filterPredicates.push(prop);
      if (this.lastSearch != null) {
        this.listFilteredMappings();
      } else {
        this.countAndInitAllMappings();
      }
    }
  }

  removeFilterPredicate(predicate: AnnotatedValue<IRI>) {
    this.filterPredicates.splice(this.filterPredicates.indexOf(predicate), 1);
    if (this.lastSearch != null) {
      this.listFilteredMappings();
    } else {
      this.countAndInitAllMappings();
    }
  }

  switchRendering() {
    this.rendering = !this.rendering;
    LocalStorageManager.setItem(LocalStorageManager.ALIGNMENT_VIEW_RENDERING, this.rendering);
  }

  refresh() {
    if (this.lastSearch != null) {
      this.listFilteredMappings();
    } else {
      this.listAllMappings();
    }
  }


  /* ==========================
   * Paging
   * ==========================*/

  //pagination
  page: number = 0;
  totPage: number;
  pageSize: number = 50;

  pageSelectorOpts: number[] = [];
  pageSelected: number;

  from: number = 0;
  to: number = this.pageSize;

  prevPage() {
    this.page--;
    if (this.lastSearch) {
      this.simulateSearchResultPagination();
    } else {
      this.listAllMappings();
    }
  }

  nextPage() {
    this.page++;
    if (this.lastSearch) {
      this.simulateSearchResultPagination();
    } else {
      this.listAllMappings();
    }
  }

  goToPage() {
    if (this.page != this.pageSelected) {
      this.page = this.pageSelected;
      if (this.lastSearch) {
        this.simulateSearchResultPagination();
      } else {
        this.listAllMappings();
      }
    }
  }


  private simulateSearchResultPagination() {
    this.annotatedMappings = [];
    let start = this.page * this.pageSize;
    let end = start + this.pageSize;
    let paginatedMappings = this.filteredMappings.slice(start, end);
    paginatedMappings.forEach(m => {
      let annPredicate = new AnnotatedValue(m.getMiddle(), { [ResAttribute.QNAME]: ResourceUtils.getQName(m.getMiddle().getIRI(), this.prefixNsMapping) });
      this.annotatedMappings.push(new Triple(new AnnotatedValue(m.getLeft()), annPredicate, new AnnotatedValue(m.getRight())));
    });
    this.annotateMappingResources();
  }


}