import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { TranslateModule } from "@ngx-translate/core";
import { MultiverseComponent } from "./multiverse.component";
import { NgModule } from "@angular/core";

@NgModule({
    declarations: [
        MultiverseComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
    ],
    exports: [
        MultiverseComponent
    ],
    providers: []
})
export class MultiverseModule { }
