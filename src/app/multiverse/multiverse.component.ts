import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ModalType } from '../modal-dialogs/Modals';
import { BasicModalsServices } from '../modal-dialogs/basic-modals/basic-modals.service';
import { WorldInfo } from '../models/Multiverse';
import { Scope } from '../models/Plugins';
import { ShowVocUrlParams } from '../models/ShowVoc';
import { MultiverseServices } from '../services/multiverse.service';
import { QueryParamService } from '../utils/QueryParamService';
import { SVContext } from '../utils/SVContext';
import { SettingsManager } from '../utils/SettingsManager';

@Component({
    selector: 'multiverse-component',
    templateUrl: './multiverse.component.html',
    host: { class: "pageComponent" },
    standalone: false
})
export class MultiverseComponent {

    worlds: WorldInfo[];
    selectedWorld: WorldInfo;
    activeWorldName: string;

    constructor(
        private multiverseService: MultiverseServices,
        private settingsMgr: SettingsManager,
        private basicModals: BasicModalsServices,
        private queryParamService: QueryParamService,
        private location: Location,
        private router: Router
    ) { }

    ngOnInit() {
        this.init();
    }

    init() {
        this.activeWorldName = SVContext.getWorld();
        this.multiverseService.listAlternativeWorldInfos().subscribe(
            worlds => {
                this.worlds = worlds;
            }
        );
    }

    createWorld() {
        this.basicModals.prompt({ key: "MULTIVERSE.CREATE_WORLD" }, { value: { key: "COMMONS.NAME" } }).then(
            name => {
                this.multiverseService.createWorld(name).subscribe(
                    () => {
                        this.init();
                    }
                );
            },
            () => { }
        );
    }

    activateWorld(world: WorldInfo) {

        if (world == null) {
            this.activeWorldName = null;
        } else {
            this.activeWorldName = world.worldName;
        }

        //set the world in context, so that it is added to all ST requests
        this.queryParamService.setCtxWorld(this.activeWorldName);
        //reset project from context and mark the routes to be reset (so CustomReuseStrategy drops all detached routes)
        SVContext.setResetRoutes(true);
        SVContext.removeWorkingProject();
        //reset settings at all scopes except SYSTEM
        this.settingsMgr.resetSettings(Scope.PROJECT);
        this.settingsMgr.resetSettings(Scope.PROJECT_USER);
        this.settingsMgr.resetSettings(Scope.USER);
        //reset all defaults
        this.settingsMgr.resetDefaults(Scope.PROJECT);
        this.settingsMgr.resetDefaults(Scope.PROJECT_USER, Scope.PROJECT);
        this.settingsMgr.resetDefaults(Scope.PROJECT_USER, Scope.USER);
        this.settingsMgr.resetDefaults(Scope.PROJECT_USER, Scope.SYSTEM);
        this.settingsMgr.resetDefaults(Scope.USER);

        //reinit settings with new world
        this.settingsMgr.initSettingsAtUserAccess().subscribe();

        let queryParams = this.queryParamService.getPreservedQueryParams();
        queryParams[ShowVocUrlParams.ctx_world] = this.activeWorldName;
        const urlTree = this.router.createUrlTree([], {
            queryParams: queryParams,
            queryParamsHandling: 'merge',
            preserveFragment: true
        });
        this.location.go(urlTree.toString());
    }

    deleteWorld(world: WorldInfo) {
        if (world.userCount > 0) {
            this.basicModals.alert({ key: "COMMONS.STATUS.WARNING" }, { key: "MULTIVERSE.CANNOT_DELETE_WORLD_IN_USE" }, ModalType.warning);
            return;
        }
        this.basicModals.confirm({ key: "COMMONS.STATUS.WARNING" }, { key: "MULTIVERSE.DELTE_WORLD_WARN", params: { worldName: world.worldName } }, ModalType.warning).then(
            () => {
                this.multiverseService.destroyWorld(world.worldName).subscribe(
                    () => {
                        this.init();
                    }
                );
            },
            () => { }
        );
    }


}