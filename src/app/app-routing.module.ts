import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminDashboardComponent } from './administration/admin-dashboard.component';
import { HttpResolutionComponent } from './administration/http-resolution/http-resolution.component';
import { ProjectsManagerComponent } from './administration/projects-manager/projects-manager.component';
import { UserDefaultsEditorComponent } from './administration/settings/user-defaults/user-defaults-editor.component';
import { InitialConfigurationComponent } from './administration/system-configuration/initial-configuration.component';
import { SystemConfigurationComponent } from './administration/system-configuration/system-configuration.component';
import { UsersManagerComponent } from './administration/users-manager/users-manager.component';
import { AlignmentsComponent } from './alignments/alignments.component';
import { ContributionsManagerComponent } from './contribution/administration/contributions-manager.component';
import { ContributionComponent } from './contribution/contribution.component';
import { LoadDevResourceComponent } from './contribution/development/load-dev.component';
import { LoadStableResourceComponent } from './contribution/stable/load-stable.component';
import { CustomServiceRouterComponent } from './custom-services/custom-service-router.component';
import { CustomViewsDashboardComponent } from './custom-views/custom-views-dashboard.component';
import { DataComponent } from './datasets/data.component';
import { DatasetDataComponent } from './datasets/data/dataset-data.component';
import { DatasetViewComponent } from './datasets/dataset-view/dataset-view.component';
import { DatasetsPageComponent } from './datasets/datasets-page/datasets-page.component';
import { DownloadsComponent } from './datasets/downloads/downloads.component';
import { MetadataComponent } from './datasets/metadata/metadata.component';
import { SparqlComponent } from './datasets/sparql/sparql.component';
import { HomeComponent } from './home/home.component';
import { MetadataRegistryComponent } from './metadata-registry/metadata-registry.component';
import { MultiverseComponent } from './multiverse/multiverse.component';
import { NotFoundComponent } from './not-found.component';
import { SearchComponent } from './search/search.component';
import { TranslationComponent } from './translation/translation.component';
import { LoginComponent } from './user/login.component';
import { RegistrationComponent } from './user/registration.component';
import { ResetPasswordComponent } from './user/reset-password.component';
import { UserActionsComponent } from './user/user-actions.component';
import { UserProfileComponent } from './user/user-profile.component';
import { STActionsEnum } from './utils/AuthorizationEvaluator';
import { adminAuthGuard, authenticatedUserAuthGuard, capabilitiesAuthGuard, projectGuard, visitorAuthGuard } from './utils/CanActivateGuards';
import { UserResolver } from './utils/UserResolver';

const routes: Routes = [
  { path: '', redirectTo: "/home", pathMatch: 'full' },
  { path: "home", component: HomeComponent, resolve: { user: UserResolver }, runGuardsAndResolvers: 'always' },
  { path: "data", component: DataComponent, canActivate: [visitorAuthGuard] },
  { path: "login", component: LoginComponent },
  { path: "registration/:firstAccess", component: RegistrationComponent },
  { path: "profile", component: UserProfileComponent, canActivate: [authenticatedUserAuthGuard] },
  {
    /*
    Administration pages are generally accessible only by admin, "projects" page is the only accessible by SuperUser.
    So, set superUserAuthGuard as basic guard, then all routes, except "projects", are protected by adminAuthGuard
     */
    /*
    Update: Since v4 it is possible to set an authenticated user as "manager" of a project. 
    Due to this, required guard has been downgraded from superUserAuthGuard to authenticatedUserAuthGuard, 
    so that any authenticated users can access admin/projects page to manage projects where they are PM
    (additional control is in user-menu that shows link to admin/projects only if user is PM of at least one project)
    */
    path: 'admin', component: AdminDashboardComponent, canActivate: [authenticatedUserAuthGuard],
    children: [
      { path: '', redirectTo: "projects", pathMatch: 'full' },
      { path: 'projects', component: ProjectsManagerComponent },
      { path: 'users', component: UsersManagerComponent, canActivate: [adminAuthGuard] },
      { path: 'contributions', component: ContributionsManagerComponent, canActivate: [adminAuthGuard] },
      { path: 'config', component: SystemConfigurationComponent, canActivate: [adminAuthGuard] },
      { path: 'http-res', component: HttpResolutionComponent, canActivate: [adminAuthGuard] },
      { path: 'mdr', component: MetadataRegistryComponent, canActivate: [adminAuthGuard] },
      { path: "multiverse", component: MultiverseComponent, canActivate: [adminAuthGuard] },
      { path: 'user-defaults', component: UserDefaultsEditorComponent, canActivate: [adminAuthGuard] },
      // { path: 'proj-defaults', component: ProjectDefaultsEditorComponent, canActivate: [adminAuthGuard] },
    ]
  },
  { path: "sysconfig", component: InitialConfigurationComponent, canActivate: [adminAuthGuard] },
  { path: "contribution", component: ContributionComponent, canActivate: [visitorAuthGuard] },
  { path: "load/stable/:token", component: LoadStableResourceComponent, canActivate: [visitorAuthGuard] },
  { path: "load/dev/:format/:token", component: LoadDevResourceComponent, canActivate: [visitorAuthGuard] },
  { path: "ResetPassword/:token", component: ResetPasswordComponent },
  { path: "UserActions", component: UserActionsComponent },
  { path: 'datasets', component: DatasetsPageComponent, canActivate: [visitorAuthGuard], data: { reuseComponent: true } },
  {
    path: 'datasets/:id', component: DatasetViewComponent, canActivate: [projectGuard], //projectGuard implicitly requires visitorAuthGuard
    children: [
      { path: '', redirectTo: "data", pathMatch: 'full' },
      { path: 'metadata', component: MetadataComponent, data: { reuseComponent: true } },
      { path: 'data', component: DatasetDataComponent, data: { reuseComponent: true } },
      { path: 'downloads', component: DownloadsComponent },
      { path: 'sparql', component: SparqlComponent, data: { reuseComponent: true } },
      {
        path: "custom-services", component: CustomServiceRouterComponent, canActivate: [capabilitiesAuthGuard],
        data: { actions: [STActionsEnum.customServiceRead, STActionsEnum.invokableReporterRead] }
      },
      {
        path: "custom-views", component: CustomViewsDashboardComponent, canActivate: [capabilitiesAuthGuard],
        data: { actions: [STActionsEnum.customFormGetCollections, STActionsEnum.customFormGetFormMappings, STActionsEnum.customFormGetForms] }
      },
    ]
  },
  { path: 'search', component: SearchComponent, canActivate: [visitorAuthGuard], data: { reuseComponent: true } },
  { path: 'translation', component: TranslationComponent, canActivate: [visitorAuthGuard], data: { reuseComponent: true } },
  { path: 'alignments', component: AlignmentsComponent, canActivate: [visitorAuthGuard] },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
