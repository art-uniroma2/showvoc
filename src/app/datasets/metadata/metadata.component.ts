import { Component, OnInit } from "@angular/core";
import { SharedModalsServices } from "src/app/modal-dialogs/shared-modals/shared-modals.service";
import { DatasetMetadata, ProjectDatasetMapping } from "src/app/models/Metadata";
import { Settings } from "src/app/models/Plugins";
import { Project, ProjectUtils } from "src/app/models/Project";
import { AnnotatedValue, IRI } from "src/app/models/Resources";
import { MetadataRegistryServices } from "src/app/services/metadata-registry.service";
import { ProjectsServices } from "src/app/services/projects.service";
import { AuthorizationEvaluator, STActionsEnum } from "src/app/utils/AuthorizationEvaluator";
import { SVContext } from "src/app/utils/SVContext";

@Component({
    selector: 'metadata-component',
    templateUrl: './metadata.component.html',
    host: { class: "pageComponent" },
    styles: [`
    .table > tbody > tr:first-child > td {
        border-top: none;
    }
    `],
    standalone: false
})
export class MetadataComponent implements OnInit {

    isUpdateFacetsAuthorized: boolean;

    project: Project;
    facets: { [key: string]: any };
    dataset: AnnotatedValue<IRI>;
    datasetMetadata: DatasetMetadata;

    ontologyResource: IRI;


    constructor(private metadataRegistryService: MetadataRegistryServices, private projectService: ProjectsServices, 
        private sharedModals: SharedModalsServices) { }

    ngOnInit() {

        this.isUpdateFacetsAuthorized = AuthorizationEvaluator.isAuthorized(STActionsEnum.projectSetProjectFacets);

        this.project = SVContext.getWorkingProject();

        this.ontologyResource = new IRI(this.project.getBaseURI());

        this.metadataRegistryService.findDatasetForProjects([this.project]).subscribe(
            (datasetsMapping: ProjectDatasetMapping) => {
                this.dataset = datasetsMapping[this.project.getName()];
                if (this.dataset != null) { //if project has not been profiled, its dataset IRI is not available in MDR
                    this.metadataRegistryService.getDatasetMetadata(this.dataset.getValue()).subscribe(
                        (metadata: DatasetMetadata) => {
                            this.datasetMetadata = metadata;
                        }
                    );
                }
            }
        );


        this.initFacets();

    }

    /* ==============
     * Facets
     * ============== */

    initFacets() {
        let pFacets: Settings = this.project.getFacets();
        let facetsMap = pFacets.getPropertiesAsMap();
        if (facetsMap['customFacets'] == null) {
            delete facetsMap['customFacets'];
        }
        this.facets = ProjectUtils.flattenizeFacetsMap(facetsMap);
    }

    editFacets() {
        this.sharedModals.configurePlugin(this.project.getFacets()).then(
            facets => {
                this.projectService.setProjectFacets(this.project, facets).subscribe(
                    () => {
                        this.project.setFacets(facets); //update facets in project
                        this.initFacets();
                    }
                );
            },
            () => { }
        );
    }

    isArray(value: any): boolean {
        return Array.isArray(value);
    }

}
