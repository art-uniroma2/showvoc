import { ChangeDetectorRef, Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalOptions } from 'src/app/modal-dialogs/Modals';
import { Project } from 'src/app/models/Project';
import { DatasetTab } from 'src/app/models/Properties';
import { AuthorizationEvaluator, STActionsEnum } from 'src/app/utils/AuthorizationEvaluator';
import { QueryParamService } from 'src/app/utils/QueryParamService';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { TabsSettingsModalComponent } from './tabs-settings-modal.component';

@Component({
  selector: 'dataset-view',
  templateUrl: './dataset-view.component.html',
  host: { class: "pageComponent" },
  styles: [`
      .nav li a {
          padding: 10px;
          font-size: 15px;
          color: #17a2b8;
      }
      .dataset-name {
          position: static; 
          max-width: unset !important; /* override the one set in ngStyle in template */
      }
      .dataset-name-absolute {
          position: absolute;
          left: 10px; 
          top: 5px;
      }
  `],
  standalone: false
})
export class DatasetViewComponent {

  @ViewChild("navtabs") navtabsEl: ElementRef;

  DatasetTab = DatasetTab; //for using the enum in the html

  project: Project;

  hideDatasetName: boolean;

  isCustomServiceAuthorized: boolean;
  isCustomViewsAuthorized: boolean;

  titleMaxWidth: number = 0;

  visibleTabs: DatasetTab[] = [];
  hideDataTab: boolean = false; //tells if the Data tab is hidden (in case no additional tab is visible)

  constructor(
    private settingsMgr: SettingsManager,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private modalService: NgbModal,
    private queryParamService: QueryParamService
  ) { }

  ngOnInit() {
    this.project = SVContext.getWorkingProject();

    //custom services and invokable reporters page
    this.isCustomServiceAuthorized = AuthorizationEvaluator.isAuthorized(STActionsEnum.customServiceRead) &&
      AuthorizationEvaluator.isAuthorized(STActionsEnum.invokableReporterRead);
    //custom view page (uses the same auth of cform services)
    this.isCustomViewsAuthorized = AuthorizationEvaluator.isAuthorized(STActionsEnum.customFormGetCollections) &&
      AuthorizationEvaluator.isAuthorized(STActionsEnum.customFormGetFormMappings) &&
      AuthorizationEvaluator.isAuthorized(STActionsEnum.customFormGetForms);

    this.hideDatasetName = this.queryParamService.getHideDatasetName() == "true";
    this.initTabs();
  }

  ngAfterViewInit() {
    this.initTitleMaxWidth();
  }

  private initTabs() {
    //reset status
    this.hideDataTab = false;
    this.visibleTabs = [DatasetTab.sparql, DatasetTab.metadata, DatasetTab.downloads];
    //tools is visible only if at least one between CustomServices and CustomViews pages can be accessed
    if (this.isCustomServiceAuthorized || this.isCustomViewsAuthorized) {
      this.visibleTabs.push(DatasetTab.tools);
    }
    //init tabs
    let hiddenTabs = this.settingsMgr.getHiddenTabs(this.project);
    this.visibleTabs = this.visibleTabs.filter(t => !hiddenTabs.includes(t));

    //check if the tab related to the current route is visible. If not, redirect to data
    let activeRoute = this.getActiveRoute();
    let activeRouteTab: DatasetTab = DatasetTab.data;
    if (activeRoute == "custom-services") { //custom-service is the only route under "tools"
      activeRouteTab = DatasetTab.tools;
    } else if (Object.values(DatasetTab).includes(activeRoute as DatasetTab)) {
      //the current route is one of the DatasetTab values
      activeRouteTab = activeRoute as DatasetTab;
    }
    if (hiddenTabs.includes(activeRouteTab)) {
      this.goToRoute('data');
    }
    //if no additional tab is visible, hide Data tab
    if (this.visibleTabs.length == 0) {
      this.hideDataTab = true;
    }
  }

  isTabVisible(tabName: DatasetTab) {
    return this.visibleTabs.includes(tabName);
  }

  tabsSettings() {
    const modalRef: NgbModalRef = this.modalService.open(TabsSettingsModalComponent, new ModalOptions());
    modalRef.componentInstance.project = this.project;
    modalRef.result.then(
      () => {
        this.initTabs();
      },
      () => {
        /*
        re-init tabs also in case user closed TabsSettingsModal with cancel/close,
        because he could have changed the setting not directly in the dialog but in the Defaults editor
        */
        this.initTabs();
      }
    );
  }

  isActiveRoute(route: string) {
    let active = this.getActiveRoute();
    return active == route;
  }

  private getActiveRoute(): string {
    let url: string = this.router.url.split("?")[0];
    let splittedUrl = url.split("/");
    let active = splittedUrl[splittedUrl.length - 1];
    return active;
  }

  goToRoute(route: string) {
    let queryParams = this.queryParamService.getPreservedQueryParams();
    this.router.navigate([route], { relativeTo: this.activatedRoute, queryParams: queryParams });
  }

  @HostListener('window:resize', ['$event'])
  initTitleMaxWidth() {
    let tabsWidth: number = 0;
    let tabs: HTMLCollection = this.navtabsEl.nativeElement.children;
    for (let i = 0; i < tabs.length; i++) {
      let tab: Element = tabs.item(i);
      if (tab instanceof HTMLElement) {
        tabsWidth += tab.offsetWidth;
      }
    }
    this.titleMaxWidth = this.navtabsEl.nativeElement.offsetWidth - tabsWidth - 40; //40 gives an additional margin
    this.cdRef.detectChanges();
  }

}