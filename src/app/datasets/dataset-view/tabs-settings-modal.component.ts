import { Component, Input } from "@angular/core";
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { BasicModalsServices } from "src/app/modal-dialogs/basic-modals/basic-modals.service";
import { ModalOptions, ModalType } from "src/app/modal-dialogs/Modals";
import { Scope } from "src/app/models/Plugins";
import { Project } from 'src/app/models/Project';
import { DatasetTab } from "src/app/models/Properties";
import { SettingsManager, SettingsManagerID } from 'src/app/utils/SettingsManager';
import { TabsSettingsDefaultsModalComponent } from "./tabs-settings-defaults-modal.component";

@Component({
  selector: "tabs-settings-modal",
  templateUrl: "./tabs-settings-modal.component.html",
  standalone: false
})
export class TabsSettingsModalComponent {

  @Input() project: Project;

  hiddenTabsPref: DatasetTab[];

  constructor(
    public activeModal: NgbActiveModal,
    private settingsMgr: SettingsManager,
    private basicModals: BasicModalsServices,
    private modalService: NgbModal
  ) { }


  ngOnInit() {
    this.init();
  }

  private init() {
    this.hiddenTabsPref = this.settingsMgr.getHiddenTabs(this.project);
  }

  setAsDefault() {
    this.basicModals.confirm({ key: "COMMONS.ACTIONS.SET_AS_DEFAULT" }, { key: "MESSAGES.SET_USER_DEFAULT_CONFIRM" }, ModalType.warning).then(
      () => {
        this.settingsMgr.setDefaultHiddenTabs(this.hiddenTabsPref, Scope.USER).subscribe();
      },
      () => { }
    );
  }

  showDefaults() {
    const modalRef: NgbModalRef = this.modalService.open(TabsSettingsDefaultsModalComponent, new ModalOptions('lg'));
    modalRef.result.then(
      () => {
        this.settingsMgr.initSettings(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project).subscribe(
          () => {
            this.init();
          }
        );
      },
      () => { }
    );
  }

  ok() {
    this.settingsMgr.setHiddenTabs(this.project, this.hiddenTabsPref).subscribe(
      () => {
        this.activeModal.close();
      }
    );
  }


  cancel() {
    this.activeModal.dismiss();
  }

}