import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DatasetTab } from "src/app/models/Properties";
import { AbstractDefaultsModal } from "src/app/preferences/default-settings-modal/abstract-defaults-modal";
import { SettingsServices } from "src/app/services/settings.service";
import { SettingsManager } from "src/app/utils/SettingsManager";
import { SVContext } from "src/app/utils/SVContext";

@Component({
  selector: "tabs-settings-defaults-modal",
  templateUrl: "./tabs-settings-defaults-modal.component.html",
  standalone: false
})
export class TabsSettingsDefaultsModalComponent extends AbstractDefaultsModal {

  settingName: string = SettingsManager.PROP_NAME.hiddenShowVocTabs;

  userDefault: DatasetTab[];
  projectDefault: DatasetTab[];

  constructor(
    activeModal: NgbActiveModal,
    settingsService: SettingsServices,
    settingsMgr: SettingsManager
  ) {
    super(activeModal, settingsService, settingsMgr);
  }

  restoreProjDefaultImpl() {
    return this.settingsMgr.setHiddenTabs(SVContext.getWorkingProject(), null);
  }

  applyUserDefaultImpl() {
    return this.settingsMgr.setHiddenTabs(SVContext.getWorkingProject(), this.userDefault);
  }


}