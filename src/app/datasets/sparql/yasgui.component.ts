import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import Yasqe, { Hint } from '@triply/yasqe';
import { AutocompletionToken, CompleterConfig } from '@triply/yasqe/build/ts/src/autocompleters';
import { firstValueFrom, map, Observable } from 'rxjs';
import { SearchMode } from 'src/app/models/Properties';
import { AnnotatedValue, Resource } from 'src/app/models/Resources';
import { QueryChangedEvent, QueryMode } from 'src/app/models/Sparql';
import { SearchServices } from 'src/app/services/search.service';
import { SparqlServices } from 'src/app/services/sparql.service';
import { SVContext } from 'src/app/utils/SVContext';

@Component({
  selector: 'yasgui',
  templateUrl: './yasgui.component.html',
  host: { class: "vbox" },
  standalone: false,
  styleUrls: ['./yasgui.component.css']
})
export class YasguiComponent {
  @Input() query: string;
  @Input() readonly: boolean = false;
  @Input() hideButtons: boolean = false;
  @Output() querychange = new EventEmitter<QueryChangedEvent>();

  @ViewChild('yasqe') yasqeDivEl: ElementRef;

  private CLASS_COMPLETER_NAME = "customClassCompleter";
  private PREFIX_COMPLETER_NAME = "customPrefixCompleter";
  private PROPERTY_COMPLETER_NAME = "customPropertyCompleter";
  private SERVICE_COMPLETER_NAME = "customServiceCompleter";

  fetchFromPrefixCheck: boolean = false;
  fullScreen: boolean = false;

  private yasqe: Yasqe;

  constructor(private searchService: SearchServices, private sparqlServices: SparqlServices) { }

  ngAfterViewInit() {
    this.initializeYasqe();
  }

  private initializeYasqe() {
    Yasqe.defaults.indentUnit = 4;
    Yasqe.defaults.autocompleters = ["variables"];
    Yasqe.defaults.collapsePrefixesOnLoad = true;
    Yasqe.defaults.indentWithTabs = true;
    Yasqe.defaults.persistenceId = null;
    Yasqe.defaults.showQueryButton = false;
    Yasqe.defaults.resizeable = false;
    Yasqe.defaults.extraKeys['Ctrl-7'] = Yasqe.defaults.extraKeys['Ctrl-/'];
    Yasqe.defaults.editorHeight = "100%";
    Yasqe.defaults.value = "";

    Yasqe.registerAutocompleter(this.customPrefixCompleter);
    Yasqe.registerAutocompleter(this.customPropertyCompleter);
    Yasqe.registerAutocompleter(this.customClassCompleter);
    Yasqe.registerAutocompleter(this.customServiceCompleter);

    this.yasqe = new Yasqe(
      this.yasqeDivEl.nativeElement,
      {
        readOnly: this.readonly
      }
    );
    if (this.query) {
      this.yasqe.setValue(this.query);
    }
    this.collapsePrefixDeclaration();

    //called on changes in yasqe editor
    this.yasqe.on('change', () => {
      //update query in parent component
      this.querychange.emit({ query: this.yasqe.getValue(), valid: this.yasqe.queryValid, mode: this.yasqe.getQueryMode() as QueryMode });
      //Check whether typed prefix is declared. If not, automatically add declaration using list from prefix.cc
      //taken from prefixes.js, since I don't use prefixes autocompleter I nees to register this listener
      // Yasqe.Autocompleters.prefixes.appendPrefixIfNeeded(instance, this.PREFIX_COMPLETER_NAME);
    });

  }

  /**
   * If query is changed in code from the parent component, the @Input query changes, but content of the yasqe editor is not updated.
   * I need to force it by setting the value with setValue().
   * Note: this operation reset the caret at the beginning of the editor, so use it with caution.
   */
  public forceContentUpdate() {
    // if (this.query != null) {
    this.yasqe.setValue(this.query);
    // }
    // this.yasqe.refresh(); //this fixes strange issues with gutter (see https://github.com/codemirror/CodeMirror/issues/4412)
    this.collapsePrefixDeclaration();
  }

  private collapsePrefixDeclaration() {
    //collapse prefixes declaration if any
    if (Object.keys(this.yasqe.getPrefixesFromQuery()).length != 0) {
      this.yasqe.collapsePrefixes(true);
    }
  }

  toggleFullScreen() {
    this.fullScreen = !this.fullScreen;
  }

  /**
   * Listener on change event of checkbox to enable the prefix.cc prefix fetching.
   */
  onCheckboxChange() {
    //disable the completer, register it with fetchFromPrefixCC changed, then enable it again
    this.yasqe.disableCompleter(this.PREFIX_COMPLETER_NAME);
    Yasqe.registerAutocompleter(this.customPrefixCompleter);
    this.yasqe.enableCompleter(this.PREFIX_COMPLETER_NAME);
  }

  /**
   * Custom completers
   */

  /**
 * Override the default "prefixes" autocompleter. This autocompleter looks for prefixes in the local triple store
 * and on prefix.cc if the fetchFromPrefixCC parameter is true
 */
  customPrefixCompleter: CompleterConfig = {
    name: this.PREFIX_COMPLETER_NAME,
    isValidCompletionPosition: Yasqe.Autocompleters.prefixes.isValidCompletionPosition,
    get: (yasqe: Yasqe) => {
      if (this.fetchFromPrefixCheck) {
        return Yasqe.Autocompleters.prefixes.get(yasqe);
      } else {
        return SVContext.getPrefixMappings().map(m => m.prefix + ": <" + m.namespace + ">").sort();
      }
    },
    preProcessToken: Yasqe.Autocompleters.prefixes.preProcessToken,
    bulk: Yasqe.Autocompleters.prefixes.bulk,
    autoShow: Yasqe.Autocompleters.prefixes.autoShow,
  };

  /**
   * Override the default "properties" autocompleter. This autocompleter looks for properties in the local triple store
   */
  customPropertyCompleter: CompleterConfig = {
    name: this.PROPERTY_COMPLETER_NAME,
    isValidCompletionPosition: Yasqe.Autocompleters.property.isValidCompletionPosition,
    get: (yasqe: Yasqe, token: AutocompletionToken) => {
      let searchProp: Observable<string[]> = this.searchService.searchResource(token.autocompletionString, ["property"], false, true, false, SearchMode.startsWith).pipe(
        map((results: AnnotatedValue<Resource>[]) => {
          let resArray = results.map(r => r.getValue().stringValue());
          return Array.from(new Set(resArray)); //removes duplicates
        })
      );
      return firstValueFrom(searchProp);
    },
    preProcessToken: Yasqe.Autocompleters.property.preProcessToken,
    postProcessSuggestion: Yasqe.Autocompleters.property.postProcessSuggestion,
    bulk: Yasqe.Autocompleters.property.bulk,
  };

  /**
   * Override the default "classes" autocompleter. This autocompleter looks for classes in the local triple store
   */
  customClassCompleter: CompleterConfig = {
    name: this.CLASS_COMPLETER_NAME,
    isValidCompletionPosition: Yasqe.Autocompleters.class.isValidCompletionPosition,
    get: (yasqe: Yasqe, token: AutocompletionToken) => {
      if (token.autocompletionString.trim() != "") { //prevent searchResource from throwing error if searchString is empty
        let searchProp: Observable<string[]> = this.searchService.searchResource(token.autocompletionString, ["cls"], false, true, false, SearchMode.startsWith).pipe(
          map((results: AnnotatedValue<Resource>[]) => {
            let resArray = results.map(r => r.getValue().stringValue());
            return Array.from(new Set(resArray)); //removes duplicates
          })
        );
        return firstValueFrom(searchProp);
      } else {
        return null;
      }
    },
    preProcessToken: Yasqe.Autocompleters.class.preProcessToken,
    postProcessSuggestion: Yasqe.Autocompleters.class.postProcessSuggestion,
    bulk: Yasqe.Autocompleters.class.bulk,
  };


  customServiceCompleter: CompleterConfig = {
    name: this.SERVICE_COMPLETER_NAME,
    isValidCompletionPosition: (yasqe: Yasqe) => {
      let token = yasqe.getCompleteToken();
      if (token.string.indexOf("_") == 0) return false;
      let cur = yasqe.getCursor();
      let previousToken = yasqe.getPreviousNonWsToken(cur.line, token);
      if (previousToken != null && previousToken.string.toLowerCase() == "silent") {
        previousToken = yasqe.getPreviousNonWsToken(cur.line, previousToken);
      }
      if (previousToken != null && previousToken.string.toLowerCase() == "service") return true;
      else return false;
    },
    get: (yasqe: Yasqe, token: AutocompletionToken) => {
      if (token.autocompletionString.trim() != "") {
        let suggestEndpoint: Observable<string[]> = this.sparqlServices.suggestEndpointsForFederation(token.autocompletionString).pipe(
          map(results => {
            let suggestions: string[] = results.map(res => res.endpointLabel + " | <" + res.endpointURL + ">");
            return suggestions;
          })
        );
        return firstValueFrom(suggestEndpoint);
      } else {
        return null;
      }
    },
    preProcessToken: Yasqe.Autocompleters.class.preProcessToken,
    postProcessSuggestion: (yasqe: Yasqe, token: AutocompletionToken, suggestedString: string) => {
      if (token.tokenPrefix && token.autocompletionString && token.tokenPrefixUri) {
        // we need to get the suggested string back to prefixed form
        suggestedString = token.tokenPrefix + suggestedString.substring(token.tokenPrefixUri.length);
      } else if (suggestedString.indexOf("| <") == -1) { // avoid to mangle the cases in which we return the repository label and the URI 
        // it is a regular uri. add '<' and '>' to string
        suggestedString = "<" + suggestedString + ">";
      }
      return suggestedString;
    },
    postprocessHints: (yasqe: Yasqe, hints: Hint[]) => {
      hints.forEach(h => {
        let displayText = h.displayText;
        if (displayText.startsWith("<")) return; // the completion is already an IRI, nothing to do
        let pipeIndex = displayText.lastIndexOf("| <");
        if (pipeIndex == -1) return; // no pipe to split the name
        let text = displayText.substring(pipeIndex + 2, displayText.length).trim();
        h.text = text;
      });
      return hints;
    },
    bulk: false,
    autoShow: false,
    persistenceId: null,
  };

}