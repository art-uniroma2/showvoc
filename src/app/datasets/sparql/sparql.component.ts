import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbNav } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { SVContext } from 'src/app/utils/SVContext';

@Component({
  selector: 'sparql-component',
  templateUrl: './sparql.component.html',
  host: { class: "pageComponent" },
  standalone: false
})
export class SparqlComponent implements OnInit {

  @ViewChild(NgbNav) viewChildNavbar: NgbNav;

  tabs: Tab[] = [];
  tabLimit: number = 10;
  private idCount: number = 1;

  TabType = TabType;
  readonly TAB_ID_PREFIX: string = "tab";

  isAuthenticatedUser: boolean;

  constructor(private translateService: TranslateService) { }

  ngOnInit() {
    this.isAuthenticatedUser = SVContext.getLoggedUser() != null;
    this.addTab(TabType.query);
  }

  addTab(type: TabType) {

    let currentActiveTab = this.getActiveTab();
    if (currentActiveTab != null) {
      currentActiveTab.active = false;
    }

    let tab: Tab = { active: true, name: null, type: type, saved: false };
    this.translateService.get(type == TabType.query ? "Query" : "SPARQL.QUERY.PARAMETERIZED_QUERY")
      .subscribe(translation => { tab.name = translation; });
    this.tabs.push(tab);
  }

  selectTab(t: Tab) {
    this.getActiveTab().active = false;
    t.active = true;
  }

  closeTab(t: Tab) {
    let tabIdx = this.tabs.indexOf(t);
    //if the closed tab is active, change the active tab (only if it wasn't the only open tab)
    if (t.active && this.tabs.length > 1) {
      if (tabIdx == this.tabs.length - 1) { //if the closed tab was the last one, active the previous
        this.tabs[tabIdx - 1].active = true;
      } else { //otherwise active the next
        this.tabs[tabIdx + 1].active = true;
      }
    }
    this.tabs.splice(tabIdx, 1);
  }

  private getActiveTab(): Tab {
    return this.tabs.find(t => t.active);
  }

}

class Tab {
  active: boolean;
  name: string;
  type: TabType;
  saved: boolean;
}

enum TabType {
  query = "query",
  parameterization = "parameterization",
}