import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import FileSaver from 'file-saver';
import { RDFFormat } from 'src/app/models/RDFFormat';
import { ExportServices } from 'src/app/services/export.service';
import { SparqlServices } from 'src/app/services/sparql.service';

@Component({
  selector: 'export-result-rdf-modal',
  templateUrl: './export-result-rdf-modal.component.html',
  standalone: false
})
export class ExportResultRdfModalComponent {

  @Input() query: string;
  @Input() inferred: boolean;

  exportFormats: RDFFormat[];
  selectedExportFormat: RDFFormat;

  loading: boolean = false;

  constructor(public activeModal: NgbActiveModal, private exportService: ExportServices, private sparqlService: SparqlServices) { }

  ngOnInit() {
    this.exportService.getOutputFormats().subscribe(
      formats => {
        this.exportFormats = formats;
        //select RDF/XML as default
        for (const format of this.exportFormats) {
          if (format.name == "RDF/XML") {
            this.selectedExportFormat = format;
            return;
          }
        }
      }
    );
  }

  ok() {
    this.loading = true;
    this.sparqlService.exportGraphQueryResultAsRdf(this.query, this.selectedExportFormat, this.inferred).subscribe(
      blob => {
        this.loading = false;
        FileSaver.saveAs(blob, "sparql_export." + this.selectedExportFormat.defaultFileExtension);
      }
    );
    this.activeModal.close();
  }

  close() {
    this.activeModal.dismiss();
  }

}
