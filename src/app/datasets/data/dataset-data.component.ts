import { Location } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LinksetMetadata } from 'src/app/models/Metadata';
import { AnnotatedValue, IRI, Resource } from 'src/app/models/Resources';
import { ValueClickEvent } from 'src/app/models/ResourceView';
import { ShowVocUrlParams } from 'src/app/models/ShowVoc';
import { ResourceViewTabsetComponent } from 'src/app/resource-view/resource-view-tabset/resource-view-tabset.component';
import { ResourcesServices } from 'src/app/services/resources.service';
import { NodeSelectEvent } from 'src/app/structures/abstract-node';
import { StructureTabsetComponent } from 'src/app/structures/structure-tabset/structure-tabset.component';
import { QueryParamService } from 'src/app/utils/QueryParamService';

@Component({
  selector: 'dataset-data-component',
  templateUrl: './dataset-data.component.html',
  host: { class: "pageComponent" },
  standalone: false
})
export class DatasetDataComponent implements OnInit {

  @ViewChild(StructureTabsetComponent) viewChildStructureTabset: StructureTabsetComponent;
  @ViewChild("mainTabset") viewChildResViewTabset: ResourceViewTabsetComponent;
  @ViewChild("sideTabset") viewChildSideTabset: ResourceViewTabsetComponent;

  selectedResource: AnnotatedValue<Resource> = null;
  sideResource: AnnotatedValue<Resource> = null;

  selectedLinkset: LinksetMetadata;
  selectedCorrespondence: AnnotatedValue<IRI> = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location,
    private resourcesService: ResourcesServices,
    private cdRef: ChangeDetectorRef,
    private queryParamService: QueryParamService
  ) { }

  ngOnInit() {
    /**
     * The subscription to queryParams is MANDATORY in order to trigger the handler each time the resId param changes
     * (not only at the first initialization of the component, but also when user changes resId by going back to a previous page in the browser)
     */
    this.activatedRoute.queryParams.subscribe(
      params => {
        let resId: string = params[ShowVocUrlParams.resId];
        if (resId != null) {
          this.resourcesService.getResourceDescription(new IRI(resId)).subscribe(
            (annotated: AnnotatedValue<Resource>) => {
              let annotatedRes = annotated as AnnotatedValue<IRI>;
              this.viewChildStructureTabset.selectResource(annotatedRes);
            }
          );
        }
      }
    );

  }

  onNodeSelected(event?: NodeSelectEvent) {
    if (event == null) return;

    //reset other selections
    this.selectedLinkset = null;
    this.selectedCorrespondence = null;

    /* 
     * Update the url with the current selected resource IRI as resId parameter
     * Note: here it uses location.go() instead of router.navigate() in order to avoid to trigger the change detection to queryParams
     * (see code in ngOnInit())
     */
    let queryParams = this.queryParamService.getPreservedQueryParams();
    queryParams[ShowVocUrlParams.resId] = event.value.getValue().stringValue();
    const urlTree = this.router.createUrlTree([], {
      queryParams: queryParams,
      queryParamsHandling: 'merge',
      preserveFragment: true
    });
    this.location.go(urlTree.toString());

    //if shift+click and resource in the main tabset is already selected => open resource in side tabset
    if (event.shift && this.selectedResource != null) {
      this.sideResource = event.value;
      this.cdRef.detectChanges();
      this.viewChildSideTabset.selectResource(this.sideResource, event.ctrl);
    } else {
      this.selectedResource = event.value;
      this.cdRef.detectChanges();
      this.viewChildResViewTabset.selectResource(this.selectedResource, event.ctrl);
    }
  }

  onValueClick(event: ValueClickEvent) {
    if (event.shift) {
      this.sideResource = event.value;
      this.cdRef.detectChanges();
      this.viewChildSideTabset.selectResource(this.sideResource, event.ctrl);
    }
  }

  onLinksetSelected(linkset: LinksetMetadata) {
    this.selectedLinkset = linkset;
    this.selectedResource = null;
    this.selectedCorrespondence = null;
    //update the url removing the query parameter resId eventually set by a previously selected node in other structures (but keep hideNav and hideDatasetName).
    //For this purpose use the same logic used in onNodeSelected()
    const urlTree = this.router.createUrlTree([], {
      queryParams: { [ShowVocUrlParams.resId]: null },
      queryParamsHandling: 'merge',
      preserveFragment: true
    });
    this.location.go(urlTree.toString());
  }

  onCorrespondenceSelected(event?: NodeSelectEvent) {
    if (event == null) return;

    this.selectedCorrespondence = event.value as AnnotatedValue<IRI>;
    this.selectedResource = null;
    this.selectedLinkset = null;
    //update the url removing the query parameter resId eventually set by a previously selected node in other structures (but keep hideNav and hideDatasetName).
    //For this purpose use the same logic used in onNodeSelected()
    const urlTree = this.router.createUrlTree([], {
      queryParams: { [ShowVocUrlParams.resId]: null },
      queryParamsHandling: 'merge',
      preserveFragment: true
    });
    this.location.go(urlTree.toString());
  }

  onMainTabsetEmpty() {
    this.selectedResource = null;
    this.cdRef.detectChanges();
  }

  onSideTabsetEmpty() {
    this.sideResource = null;
    this.cdRef.detectChanges();
  }

}
