import { Component, OnInit } from "@angular/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import { Observable, Subscription } from "rxjs";
import { map } from "rxjs/operators";
import { CreateAlignmentDownloadModalComponent } from 'src/app/administration/projects-manager/create-alignment-download-modal.component';
import { CreateDownloadModalComponent } from "src/app/administration/projects-manager/create-download-modal.component";
import { CreateExternalDownloadModalComponent } from 'src/app/administration/projects-manager/create-external-download-modal.component';
import { LoadDownloadModalComponent } from "src/app/administration/projects-manager/load-download-modal.component";
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalOptions, ModalType } from "src/app/modal-dialogs/Modals";
import { SharedModalsServices } from "src/app/modal-dialogs/shared-modals/shared-modals.service";
import { Languages } from "src/app/models/LanguagesCountries";
import { Project } from "src/app/models/Project";
import { AnnotatedValue, IRI } from "src/app/models/Resources";
import { CommonUtils } from "src/app/models/Shared";
import { DirectoryEntryInfo, DownloadsMap, EntryType, SingleDownload, SingleDownloadType } from "src/app/models/Storage";
import { DownloadServices } from "src/app/services/download.service";
import { StorageServices } from 'src/app/services/storage.service';
import { AuthorizationEvaluator, STActionsEnum } from "src/app/utils/AuthorizationEvaluator";
import { SVContext } from "src/app/utils/SVContext";
import { TranslationUtils } from 'src/app/utils/TranslationUtils';
import { LocalizedMap } from "src/app/widget/localized-editor/localized-editor.component";

@Component({
  selector: 'downloads-component',
  templateUrl: './downloads.component.html',
  host: { class: "pageComponent" },
  standalone: false
})
export class DownloadsComponent implements OnInit {

  isUpdateDownloadsAuthorized: boolean;

  project: Project;
  facets: { [key: string]: any };
  dataset: AnnotatedValue<IRI>;

  private readonly PROJ_DOWNLOAD_DIR_PATH = "proj:/download";
  distributions: DownloadInfo[] = [];
  files: DownloadInfo[] = [];

  private eventSubscriptions: Subscription[] = [];

  constructor(private downloadService: DownloadServices, private storageService: StorageServices,
    private basicModals: BasicModalsServices, private sharedModals: SharedModalsServices,
    private modalService: NgbModal, private translateService: TranslateService
  ) {
    this.eventSubscriptions.push(
      translateService.onLangChange.subscribe(() => {
        this.distributions.forEach(d => this.initDownloadLocalizedLabel(d));
        this.files.forEach(f => this.initDownloadLocalizedLabel(f));
      })
    );
  }

  ngOnInit() {

    //auth required for management of downloads and files are more or less the same, so I realy on just one auth check
    this.isUpdateDownloadsAuthorized = AuthorizationEvaluator.isAuthorized(STActionsEnum.downloadGenericAction); //for create/delete/edit distribution downloads

    this.project = SVContext.getWorkingProject();

    this.initAll();
  }

  private initAll() {
    this.initDistributions().subscribe(
      () => {
        /*
        uploaded files are in the same storage folder of distributions. 
        They need to be filtered excluding those files already considered by distributions, 
        so uploaded files need to be initialized only after distributions initialization
        */
        this.initLegacyFiles();
      }
    );
  }

  ngOnDestroy() {
    this.eventSubscriptions.forEach(s => s.unsubscribe());
  }

  private initDistributions(): Observable<void> {
    this.distributions = [];
    this.files = [];
    return this.downloadService.getDownloadInfoList().pipe(
      map(downloadsInfo => {
        let downloadMap: DownloadsMap = downloadsInfo.getPropertyValue("fileNameToSingleDownloadMap");
        for (let dlName in downloadMap) {
          let d: SingleDownload = downloadMap[dlName];
          let download: DownloadInfo = {
            fileName: d.fileName,
            format: d.format,
            langToLocalizedMap: d.langToLocalizedMap,
            localizedLabel: null,
            date: new Date(d.timestamp),
            dateLocal: CommonUtils.datetimeToLocale(new Date(d.timestamp)),
            type: d.type,
          };
          this.initDownloadLocalizedLabel(download);
          if (d.distribution) {
            this.distributions.push(download);
          } else {
            this.files.push(download);
          }
        }
        this.distributions.sort((d1, d2) => d1.date.getTime() - d2.date.getTime());
        this.files.sort((f1, f2) => f1.date.getTime() - f2.date.getTime());
      })
    );
  }

  private initDownloadLocalizedLabel(download: DownloadInfo) {
    /*
    get the localized label according the following order:
    - the language set for interface translation
    - the browser language
    - priority languages in SV
    - first localized available
    */
    let translateLang = this.translateService.currentLang;
    let localizedLabel: string = download.langToLocalizedMap[translateLang];
    if (localizedLabel != null) {
      download.localizedLabel = localizedLabel;
      return;
    }

    //if not found, try with the browser language
    let browserLang: string = navigator.language || navigator['userLanguage'];
    localizedLabel = download.langToLocalizedMap[browserLang];
    if (localizedLabel != null) {
      download.localizedLabel = localizedLabel;
      return;
    }
    //if the browser language has a country code (e.g. en-GB, it-IT), look only for the lang code
    if (browserLang.includes("-")) {
      browserLang = browserLang.substring(0, browserLang.indexOf("-"));
      localizedLabel = download.langToLocalizedMap[browserLang];
      if (localizedLabel != null) {
        download.localizedLabel = localizedLabel;
        return;
      }
    }
    //if not found, try with the priority languages
    let prioritizedLang = Languages.priorityLangs.find(l => download.langToLocalizedMap[l] != null);
    if (prioritizedLang != null) {
      localizedLabel = download.langToLocalizedMap[prioritizedLang];
      if (localizedLabel != null) {
        download.localizedLabel = localizedLabel;
        return;
      }
    }
    //if not found returns the first one in the map
    download.localizedLabel = download.langToLocalizedMap[Object.keys(download.langToLocalizedMap)[0]];
  }

  private initLegacyFiles() {
    this.storageService.list(this.PROJ_DOWNLOAD_DIR_PATH).subscribe(
      (entries: DirectoryEntryInfo[]) => {
        //filter those entries which are files (not dir) and the name is not already returned in the downloads list
        let legacyFiles: DownloadInfo[] = entries
          .filter(e => e.type == EntryType.FILE && !this.distributions.some(d => d.fileName == e.name) && !this.files.some(f => f.fileName == e.name))
          .map(e => {
            let lf: DownloadInfo = {
              fileName: e.name,
              format: null,
              langToLocalizedMap: null,
              localizedLabel: e.name,
              type: SingleDownloadType.local,
              date: new Date(e.creationTimestamp),
              dateLocal: CommonUtils.datetimeToLocale(new Date(e.creationTimestamp)),
              legacy: true,
            };
            return lf;
          });
        if (legacyFiles.length > 0) {
          this.files = this.files.concat(legacyFiles);
          this.files.sort((f1, f2) => f1.date.getTime() - f2.date.getTime());
        }
      }
    );
  }

  createDistribution() {
    const modalRef: NgbModalRef = this.modalService.open(CreateDownloadModalComponent, new ModalOptions("lg"));
    modalRef.componentInstance.project = this.project;
    modalRef.result.then(
      () => {
        this.initDistributions().subscribe();
      },
      () => { }
    );
  }

  addExternalDistribution() {
    const modalRef: NgbModalRef = this.modalService.open(CreateExternalDownloadModalComponent, new ModalOptions("lg"));
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText({ key: "METADATA.DISTRIBUTIONS.ADD_EXTERNAL_DIST_DATASET" }, this.translateService);
    modalRef.componentInstance.project = this.project;
    modalRef.result.then(
      () => {
        this.initDistributions().subscribe();
      },
      () => { }
    );
  }

  addExternalLink() {
    const modalRef: NgbModalRef = this.modalService.open(CreateExternalDownloadModalComponent, new ModalOptions("lg"));
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText({ key: "METADATA.DISTRIBUTIONS.ADD_EXTERNAL_LINK_FILE" }, this.translateService);
    modalRef.componentInstance.project = this.project;
    modalRef.componentInstance.distribution = false;
    modalRef.result.then(
      () => {
        this.initDistributions().subscribe();
      },
      () => { }
    );
  }

  createAlignmentsDistribution() {
    const modalRef: NgbModalRef = this.modalService.open(CreateAlignmentDownloadModalComponent, new ModalOptions("lg"));
    modalRef.componentInstance.project = this.project;
    modalRef.result.then(
      () => {
        this.initDistributions().subscribe();
      },
      () => { }
    );
  }

  deleteDistribution(download: DownloadInfo, distribution: boolean) {
    this.basicModals.confirm({ key: "COMMONS.STATUS.WARNING" }, { key: distribution ? "MESSAGES.DELETE_DISTRIBUTION_CONFIRM_WARN" : "MESSAGES.DELETE_FILE_CONFIRM_WARN" }, ModalType.warning).then(
      () => {
        let deleteFn: Observable<void>;
        if (download.type == SingleDownloadType.external) {
          deleteFn = this.downloadService.removeExternalDistribution(download.fileName);
        } else { //local
          if (download.legacy) {
            deleteFn = this.storageService.deleteFile(this.PROJ_DOWNLOAD_DIR_PATH + "/" + download.fileName);
          } else {
            deleteFn = this.downloadService.removeDownload(download.fileName);
          }
        }
        deleteFn.subscribe(
          () => {
            this.initAll();
          }
        );
      },
      () => { }
    );
  }

  downloadDistribution(download: DownloadInfo) {
    if (download.legacy) {
      this.storageService.getFile(this.PROJ_DOWNLOAD_DIR_PATH + "/" + download.fileName).subscribe(
        blob => {
          let exportLink = window.URL.createObjectURL(blob);
          let aElement: HTMLAnchorElement = document.createElement("a");
          aElement.download = download.fileName;
          aElement.href = exportLink;
          aElement.click();
        }
      );
    } else {
      this.downloadService.getFile(download.fileName).subscribe(
        blob => {
          let exportLink = window.URL.createObjectURL(blob);
          let aElement: HTMLAnchorElement = document.createElement("a");
          aElement.download = download.fileName;
          aElement.href = exportLink;
          aElement.click();
        }
      );
    }
  }

  editDistributionLabels(download: DownloadInfo) {
    let localizeMap: LocalizedMap = new Map();
    for (let lang in download.langToLocalizedMap) {
      localizeMap.set(lang, download.langToLocalizedMap[lang]);
    }
    this.sharedModals.localizedEditor({ key: "METADATA.DISTRIBUTIONS.DISTRIBUTION_LABELS" }, localizeMap, false).then(
      (map: LocalizedMap) => {
        let newMap: { [lang: string]: string } = {};
        map.forEach((label, lang) => {
          newMap[lang] = label;
        });
        this.downloadService.updateLocalizedMap(download.fileName, newMap).subscribe(
          () => {
            this.initDistributions().subscribe();
          }
        );
      },
      () => { }
    );
  }

  uploadFile(distribution: boolean) {
    const modalRef: NgbModalRef = this.modalService.open(LoadDownloadModalComponent, new ModalOptions("lg"));
    modalRef.componentInstance.project = this.project;
    modalRef.componentInstance.distribution = distribution;
    modalRef.result.then(
      () => {
        this.initDistributions().subscribe();
      },
      () => { }
    );
  }

}

interface DownloadInfo {
  fileName: string;
  localizedLabel: string;
  langToLocalizedMap: { [lang: string]: string };
  date: Date;
  dateLocal: string;
  format: string;
  type: SingleDownloadType;
  /*
   used internally for distinguishing between 
  - legacy files managed with storage manager
  - new files/dist managed with DownloadSettingsManager
  */
  legacy?: boolean;
}
