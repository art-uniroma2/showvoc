import { Directive, Input, SimpleChanges } from '@angular/core';
import { Project } from 'src/app/models/Project';
import { Multimap } from 'src/app/models/Shared';
import { ProjectsServices } from 'src/app/services/projects.service';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { DatasetsFilter } from './datasets-page.component';

@Directive()
export abstract class AbstractDatasetsView {

    @Input() filters: DatasetsFilter;
    @Input() projectBags: Multimap<Project>;

    protected projectService: ProjectsServices;
    protected settingsMgr: SettingsManager;
    constructor(projectService: ProjectsServices, settingsMgr: SettingsManager) {
        this.projectService = projectService;
        this.settingsMgr = settingsMgr;
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['projectBags'] && changes['projectBags'].currentValue) {
            this.initDatasets();
        }
    }

    abstract initDatasets(): void;

    /**
     * Return true if the provided dataset satisfies the filters.
     * The checks here are:
     * - string filter (looks in name, label, baseuri and description)
     * - only favorites
     * @param dataset 
     * @returns 
     */
    isDatasetVisible(dataset: Project): boolean {
        let stringFilterOk: boolean = true;
        if (this.filters.stringFilter != null && this.filters.stringFilter.trim() != "") {
            let stringFilter: string = this.filters.stringFilter.toUpperCase();
            stringFilterOk = dataset.getName().toUpperCase().includes(stringFilter) || //check match in name
                dataset.getLabel().toUpperCase().includes(stringFilter) || //check match in name
                dataset.getBaseURI().toUpperCase().includes(stringFilter) || //check match in baseuri
                (dataset.getDescription() != null && dataset.getDescription().toUpperCase().includes(stringFilter)); //check match in description
        }
        let favoriteFilterOk: boolean = !this.filters.favorite || this.settingsMgr.getFavoriteDatasets().includes(dataset.getName()); //check only open facet
        return stringFilterOk && favoriteFilterOk;
    }


}