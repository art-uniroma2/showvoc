import { ChangeDetectorRef, Component, ElementRef, HostListener, Input, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Settings } from 'src/app/models/Plugins';
import { QueryParamService } from 'src/app/utils/QueryParamService';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { Project, ProjectUtils } from '../../models/Project';

@Component({
    selector: 'dataset-card',
    templateUrl: './dataset-card.component.html',
    host: { class: "d-block" },
    standalone: false
})
export class DatasetCardComponent {

    @Input() project: Project;

    @ViewChild("modelBadge") modelBadge: ElementRef;
    @ViewChild("cardTitle") cardTitle: ElementRef;

    isFavorite: boolean = false;

    descriptionWithLink: boolean = false;
    splitDescr: string[];

    titleMaxWidth: string = "800px";

    facets: { [key: string]: any };

    constructor(private settingsMgr: SettingsManager,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private changeDetectorRef: ChangeDetectorRef,
        private queryParamService: QueryParamService
    ) {}

    ngOnInit() {
        this.initFacets();
    }

    ngAfterViewInit() {
        this.isFavorite = this.settingsMgr.getFavoriteDatasets().includes(this.project.getName());
        this.initDescription();
        this.updateTitleMaxWidth();
    }

    goToProject(project: Project) {
        let queryParams = this.queryParamService.getPreservedQueryParams();
        this.router.navigate(["/datasets/" + project.getName()], { relativeTo: this.activatedRoute, queryParams: queryParams });
    }

    private initFacets() {
        let pFacets: Settings = this.project.getFacets();
        let facetsMap = pFacets.getPropertiesAsMap();
        if (facetsMap['customFacets'] == null) {
            delete facetsMap['customFacets'];
        }
        this.facets = ProjectUtils.flattenizeFacetsMap(facetsMap);
        //clear empty facetsO
        for (let n in this.facets) {
            if (!this.facets[n]) {
                delete this.facets[n];
            }
        }
        //set facets to null in empty
        if (Object.keys(this.facets).length == 0) {
            this.facets = null;
        }
    }

    initDescription() {
        let description = this.project.getDescription();

        let regexToken = /(((ftp|https?):\/\/)[-\w@:%_+.~#?,&//=]+)|((mailto:)?[_.\w-][_.\w-!#$%&'*+-/=?^_`.{|}~]+@([\w][\w-]+\.)+[a-zA-Z]{2,3})/g;
        let urlArray: string[] = [];

        let matchArray: RegExpExecArray;
        while ((matchArray = regexToken.exec(description)) !== null) {
            urlArray.push(matchArray[0]);
        }

        if (urlArray.length > 0) {
            this.descriptionWithLink = true;
            this.splitDescr = [];
            for (let i = 0; i < urlArray.length; i++) {
                let idx: number = 0;
                let urlStartIdx: number = description.indexOf(urlArray[i]);
                let urlEndIdx: number = description.indexOf(urlArray[i]) + urlArray[i].length;
                this.splitDescr.push(description.substring(idx, urlStartIdx)); //what there is before url
                this.splitDescr.push(description.substring(urlStartIdx, urlEndIdx)); //url
                idx = urlEndIdx;
                description = description.substring(idx);
                //what there is between url and the end of the string
                if (urlArray[i + 1] == null && idx != description.length) { //if there is no further links but there is text after last url
                    this.splitDescr.push(description); //push value, namely the rest of the value string
                }
            }
        }
    }

    toggleFavorite() {
        this.isFavorite = !this.isFavorite;
        let favorites = this.settingsMgr.getFavoriteDatasets();
        if (this.isFavorite) {
            favorites.push(this.project.getName());
        } else {
            favorites = favorites.filter(f => f != this.project.getName());
        }
        this.settingsMgr.setFavoriteDatasets(favorites).subscribe();
    }

    @HostListener('window:resize', ['$event'])
    updateTitleMaxWidth() {
        let modelBadgeWidth = this.modelBadge.nativeElement.offsetWidth;
        let cardTitleWidth = this.cardTitle.nativeElement.offsetWidth - 10; //subtract few px as right margin
        this.titleMaxWidth = cardTitleWidth - modelBadgeWidth + "px";
        this.changeDetectorRef.detectChanges();
    }

    isArray(value: any): boolean {
        return Array.isArray(value);
    }



}