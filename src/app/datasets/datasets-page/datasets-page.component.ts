import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of, Subscription } from 'rxjs';
import { finalize, map } from 'rxjs/operators';
import { Scope, STProperties } from 'src/app/models/Plugins';
import { ProjectVisualization } from 'src/app/models/Properties';
import { Multimap } from 'src/app/models/Shared';
import { ProjectsServices } from 'src/app/services/projects.service';
import { LocalStorageManager } from 'src/app/utils/LocalStorageManager';
import { SettingsManager, SettingsManagerID } from 'src/app/utils/SettingsManager';
import { SVEventHandler } from 'src/app/utils/SVEventHandler';
import { ModalOptions } from '../../modal-dialogs/Modals';
import { FacetsValueCountMap, Project, ProjectFacets, ProjectUtils, ProjectVisibility } from '../../models/Project';
import { DatasetsDirsComponent } from './datasets-dirs.component';
import { DatasetsListComponent } from './datasets-list.component';
import { DatasetsSettingsModalComponent } from './datasets-settings-modal.component';

@Component({
  selector: 'datasets-page-component',
  templateUrl: './datasets-page.component.html',
  host: { class: "pageComponent" },
  standalone: false
})
export class DatasetsPageComponent implements OnInit {

  @ViewChild(DatasetsListComponent) datasetListChild: DatasetsListComponent;
  @ViewChild(DatasetsDirsComponent) datasetDirsChild: DatasetsDirsComponent;

  projVisualizationPref: ProjectVisualization;

  projectBags: Multimap<Project>;

  rendering: boolean;

  loading: boolean = false;

  //client side filters
  filters: DatasetsFilter = { favorite: false, open: false, stringFilter: null };

  //facets
  facetsFilters: FacetStruct[] = [];
  facets: { [key: string]: string[] } = {};
  customFacets: STProperties[];

  instanceName: string;
  translationParam: { instanceName: string };

  private eventSubscriptions: Subscription[] = [];

  constructor(
    private projectsServices: ProjectsServices,
    private settingsMgr: SettingsManager,
    private translateService: TranslateService,
    private modalService: NgbModal,
    private eventHandler: SVEventHandler
  ) {
    this.eventSubscriptions.push(this.eventHandler.projectUpdatedEvent.subscribe(
      () => this.initProjectsAndFacets())
    );
  }

  ngOnInit() {
    this.projVisualizationPref = this.settingsMgr.getSetting(Scope.USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.projectVisualization, null, new ProjectVisualization());

    //init filters
    this.restoreStoredFilters();

    this.instanceName = window['showvoc_instance_name'];
    if (this.instanceName == null) {
      this.instanceName = "ShowVoc";
    }
    this.translationParam = { instanceName: this.instanceName };

    this.rendering = this.settingsMgr.getProjectRendering();

    this.projectsServices.getFacetsAndValue().subscribe(
      (facetsAndValues) => {
        this.restoreStoredActiveFacets(facetsAndValues);
        this.initProjectsAndFacets();
      }
    );
  }

  ngOnDestroy() {
    this.eventSubscriptions.forEach(s => s.unsubscribe());
  }

  private initProjectsAndFacets() {
    this.projVisualizationPref = this.settingsMgr.getSetting(Scope.USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.projectVisualization, null, new ProjectVisualization());
    let bagOfFacet = this.projVisualizationPref.facetBagOf;

    this.initCustomFacetsSchema().subscribe(
      () => {
        this.loading = true;
        this.projectsServices.searchAndRetrieveProjectsAndFacets(this.facets, bagOfFacet, this.filters.open, [ProjectVisibility.PUBLIC]).pipe(
          finalize(() => { this.loading = false; })
        ).subscribe(
          projAndFacets => {
            // projAndFacets.
            this.initFacetsFilters(projAndFacets.facetsValueCountMap);
            this.projectBags = projAndFacets.bagOfProjects;

          }
        );
      }
    );

  }

  refresh() {
    this.initProjectsAndFacets();
  }

  /* FILTERS - start */

  toggleOnlyOpen() {
    this.filters.open = !this.filters.open;
    this.updateStoredFilters();
    this.initProjectsAndFacets();
  }

  toggleOnlyFavorites() {
    this.filters.favorite = !this.filters.favorite;
    this.updateStoredFilters();
  }

  private restoreStoredFilters() {
    this.filters.open = LocalStorageManager.getItem(LocalStorageManager.DATASETS_FILTER_ONLY_OPEN_PROJECTS) != false;
    this.filters.favorite = LocalStorageManager.getItem(LocalStorageManager.DATASETS_FILTER_ONLY_FAVORITES);
  }

  private updateStoredFilters() {
    LocalStorageManager.setItem(LocalStorageManager.DATASETS_FILTER_ONLY_OPEN_PROJECTS, this.filters.open);
    LocalStorageManager.setItem(LocalStorageManager.DATASETS_FILTER_ONLY_FAVORITES, this.filters.favorite);
  }

  /* FILTERS - end */

  /* FACETS - start */

  private initCustomFacetsSchema(): Observable<void> {
    //init facets schema only if not yet done
    if (this.customFacets) {
      return of(null);
    } else {
      return this.projectsServices.getProjectFacetsForm().pipe(
        map(facetsSchema => {
          this.customFacets = facetsSchema.properties;
        })
      );
    }
  }

  /**
   * Convert the FacetsValueCountMap to the facets filters structure, namely a structure like
   * facetName -> facetValue -> count
   * to a list of FacetStruct (which contains further info like displayName of the facets, values, and so on)
   * @param facetsValueCountMap
   */
  private initFacetsFilters(facetsValueCountMap: FacetsValueCountMap) {
    this.facetsFilters = [];
    for (let facetName in facetsValueCountMap) {
      //ignore predefined facets unhandled in SV 
      if (facetName == ProjectFacets.prjHistoryEnabled || facetName == ProjectFacets.prjValidationEnabled) {
        continue;
      }

      let facetValues: FacetValueStruct[] = [];

      //compute display name
      let facetDisplayName = facetName; //by default, display name is the same name of the facet
      let facetTranslationKey = ProjectUtils.projectFacetsTranslationStruct[facetName]; //if facet is a built-in one, the translation is hard-coded
      if (facetTranslationKey) {
        facetDisplayName = this.translateService.instant(ProjectUtils.projectFacetsTranslationStruct[facetName]);
      } else { //look for display name in custom facets
        let customFacet = this.customFacets.find(cf => cf.name == facetName);
        if (customFacet) {
          let displayName = customFacet.displayName;
          if (displayName) {
            facetDisplayName = displayName;
          }
        }
      }
      this.facetsFilters.push({
        open: true,
        name: facetName,
        displayName: facetDisplayName,
        values: facetValues
      });
      for (let fValue in facetsValueCountMap[facetName]) {
        let facetValueDisplay = fValue;
        if (facetName == ProjectFacets.prjModel || facetName == ProjectFacets.prjLexModel) {
          facetValueDisplay = Project.getPrettyPrintModelType(fValue);
        }
        facetValues.push({
          value: fValue,
          displayValue: facetValueDisplay,
          active: this.facets[facetName]?.includes(fValue),
          count: facetsValueCountMap[facetName][fValue]
        });
      }
    }
    //sort facets => Model, Lexicalization, built-in, custom
    this.facetsFilters.sort((f1, f2) => {
      // Ensure "Model" comes first
      if (f1.name == ProjectFacets.prjModel) return -1;
      if (f2.name == ProjectFacets.prjModel) return 1;
      // Ensure "Lexicalization" comes second
      if (f1.name == ProjectFacets.prjLexModel) return -1;
      if (f2.name == ProjectFacets.prjLexModel) return 1;
      //built-in features come before the custom ones
      if (f1.name in ProjectFacets) return -1;
      if (f2.name in ProjectFacets) return 1;
      // Maintain original order for other cases
      return 0;
    });
    //sort values by count and alphabetically
    this.facetsFilters.forEach(f => {
      f.values.sort((v1, v2) => {
        if (v1.count == v2.count) {
          return v1.displayValue.toLocaleLowerCase().localeCompare(v2.displayValue.toLocaleLowerCase());
        }
        return v2.count - v1.count;
      });
    });
  }

  onFacetToggled() {
    this.facets = {};
    this.facetsFilters.forEach(f => {
      f.values.forEach(v => {
        if (v.active) {
          if (!(f.name in this.facets)) {
            this.facets[f.name] = [];
          }
          this.facets[f.name].push(v.value);
        }
      });
    });
    this.updateStoredFacets();
    this.initProjectsAndFacets();
  }

  /**
   * Restore the active facets stored in the LocalStorage also checking/validating them 
   * (e.g. if the LocalStorage contains a facet no more existing, it is ignored, the same for unexisting values)
   * @param facetsAndValues 
   */
  private restoreStoredActiveFacets(facetsAndValues: Multimap<string>) {
    try { //wrap everyting it try-catch so that in case of invalid value in LocalStorage, the initialization doesn't break anything
      let storedFacets: { [key: string]: string[] } = LocalStorageManager.getItem(LocalStorageManager.DATASETS_FACETS);
      if (storedFacets) { //facets found in local storage => restore it
        for (let fName in storedFacets) { //iterate over facet names (keys of the stored object)
          if (facetsAndValues[fName] != null) { //facet name found in the existing ones (those from ST)
            let storedValues: string[] = storedFacets[fName];
            for (let i = storedValues.length - 1; i >= 0; i--) { //iterate over values stored in LocalStorage
              if (!facetsAndValues[fName].includes(storedFacets[fName][i])) { //if value doesn't exist => remove it
                storedValues.splice(i, 1);
              }
            }
          } else { //facet name not existing => remove it
            delete storedFacets[fName];
          }
        }
        for (let fName in storedFacets) { //clear empty facets
          if (storedFacets[fName].length == 0) {
            delete storedFacets[fName];
          }
        }
        this.facets = storedFacets;
      }
    } catch {
      console.warn("Couldn't restore stored facets");
    }
  }

  private updateStoredFacets() {
    let facetsToStore: { [key: string]: string[] };
    if (Object.keys(this.facets).length != 0) {
      facetsToStore = this.facets;
    }
    LocalStorageManager.setItem(LocalStorageManager.DATASETS_FACETS, facetsToStore);
  }

  /* FACETS - end */

  settings() {
    this.modalService.open(DatasetsSettingsModalComponent, new ModalOptions()).result.then(
      () => {
        this.refresh();
      },
      () => { }
    );
  }

  switchRendering() {
    this.rendering = !this.rendering;
    this.settingsMgr.setProjectRendering(this.rendering).subscribe();
  }

  expandAll() {
    this.datasetDirsChild.expandAll();
  }
  collapseAll() {
    this.datasetDirsChild.collapseAll();
  }

}

export interface DatasetsFilter {
  stringFilter: string;
  open: boolean;
  favorite: boolean;
}

interface FacetStruct {
  open: boolean;
  name: string;
  displayName: string;
  values: FacetValueStruct[];
}

interface FacetValueStruct {
  value: string;
  active: boolean;
  displayValue: string;
  count: number;
}