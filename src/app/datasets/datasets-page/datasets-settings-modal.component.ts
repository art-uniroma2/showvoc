import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Scope } from 'src/app/models/Plugins';
import { DatasetViewsUtils } from 'src/app/models/Project';
import { ProjectVisualization } from 'src/app/models/Properties';
import { SettingsManager, SettingsManagerID } from 'src/app/utils/SettingsManager';

@Component({
  selector: "datasets-settings-modal",
  templateUrl: "./datasets-settings-modal.component.html",
  standalone: false
})
export class DatasetsSettingsModalComponent {

  projVisualization: ProjectVisualization;

  constructor(public activeModal: NgbActiveModal, private settingsMgr: SettingsManager) { }

  ngOnInit() {
    this.projVisualization = this.settingsMgr.getSetting(Scope.USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.projectVisualization, null, new ProjectVisualization());
  }

  ok() {
    this.settingsMgr.setSetting(Scope.USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.projectVisualization, this.projVisualization).subscribe();
    DatasetViewsUtils.storeCollpasedDirectoriesCookie([]); //mode or facet changed => reset collapsed dir in LocalStorage
    this.activeModal.close();
  }

  close() {
    this.activeModal.dismiss();
  }

}