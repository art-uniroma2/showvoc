import { Component } from '@angular/core';
import { Scope } from 'src/app/models/Plugins';
import { SettingsManager, SettingsManagerID } from 'src/app/utils/SettingsManager';
import { DatasetDirEntryI, DatasetViewsUtils, Project, ProjectFacets } from '../../models/Project';
import { ProjectsServices } from '../../services/projects.service';
import { AbstractDatasetsView } from './abstract-datasets-view.component';

@Component({
    selector: 'datasets-dirs',
    templateUrl: './datasets-dirs.component.html',
    standalone: false
})
export class DatasetsDirsComponent extends AbstractDatasetsView {

    datasetDirs: DatasetDirEntry[];

    constructor(projectService: ProjectsServices, settingsMgr: SettingsManager) {
        super(projectService, settingsMgr);
    }

    initDatasets() {
        let projVisualization = this.settingsMgr.getSetting(Scope.USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.projectVisualization);
        //if this component is rendered, for sure the mode is facet and a facetBagOf has been provided, so no need to do further checks (e.g. projVisualization != null)
        let bagOfFacet = projVisualization.facetBagOf;

        this.datasetDirs = [];
        Object.keys(this.projectBags).forEach(bag => {
            let dirEntry = new DatasetDirEntry(bag);
            dirEntry.datasets = this.projectBags[bag];
            this.datasetDirs.push(dirEntry);
        });
        this.datasetDirs.sort((d1: DatasetDirEntry, d2: DatasetDirEntry) => {
            if (d1.dirName == null || d1.dirName == "") return 1;
            else if (d2.dirName == null || d2.dirName == "") return -1;
            else return d1.dirName.localeCompare(d2.dirName);
        });
        //init open/close directory according the stored cookie
        let collapsedDirs: string[] = DatasetViewsUtils.retrieveCollapsedDirectoriesCookie();
        this.datasetDirs.forEach(pd => {
            pd.open = !collapsedDirs.includes(pd.dirName);
        });
        //init dir displayName (e.g.: prjLexModel and prjModel have values that can be written as RDFS, OWL, SKOS...)
        this.datasetDirs.forEach(pd => { pd.dirDisplayName = pd.dirName; }); //init with the same dir as default
        if (bagOfFacet == ProjectFacets.prjLexModel || bagOfFacet == ProjectFacets.prjModel) {
            this.datasetDirs.forEach(pd => {
                pd.dirDisplayName = Project.getPrettyPrintModelType(pd.dirName);
            });
        }
    }

    toggleDirectory(dir: DatasetDirEntry, open?: boolean) {
        if (open == undefined) {
            dir.open = !dir.open;
        } else {
            dir.open = open;
        }
        //update collapsed directories cookie
        DatasetViewsUtils.storeCollpasedDirectoriesCookie(this.datasetDirs);
    }

    expandAll() {
        this.datasetDirs.forEach(dir => {
            this.toggleDirectory(dir, true);
        });
    }
    collapseAll() {
        this.datasetDirs.forEach(dir => {
            this.toggleDirectory(dir, false);
        });
    }

}

class DatasetDirEntry implements DatasetDirEntryI {
    dirName: string;
    dirDisplayName: string;
    open: boolean;
    datasets: Project[];
    constructor(dirName: string) {
        this.dirName = dirName;
        this.open = true;
        this.datasets = [];
    }
}
