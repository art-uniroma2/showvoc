import { Component, Input } from '@angular/core';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { Project } from '../../models/Project';
import { ProjectsServices } from '../../services/projects.service';
import { AbstractDatasetsView } from './abstract-datasets-view.component';

@Component({
    selector: 'datasets-list',
    templateUrl: './datasets-list.component.html',
    standalone: false
})
export class DatasetsListComponent extends AbstractDatasetsView {

    @Input() facets: { [key: string]: string[] } = {}; //facet name -> values

    datasets: Project[] = [];

    constructor(projectService: ProjectsServices, settingsMgr: SettingsManager) {
        super(projectService, settingsMgr);
    }

    initDatasets() {
        this.datasets = [];
        Object.keys(this.projectBags).forEach(bag => {
            this.projectBags[bag].forEach(p => {
                this.datasets.push(p);
            });
        });
        this.datasets.sort((p1, p2) => {
            return p1.getLabel().toLocaleLowerCase().localeCompare(p2.getLabel().toLocaleLowerCase());
        });
    }


}