import { Component, forwardRef, Input } from "@angular/core";
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { RDFResourceRolesEnum } from 'src/app/models/Resources';
import { TranslationUtils } from "src/app/utils/TranslationUtils";
import { ResViewTemplate, SectionFilterPreference } from "../../models/Properties";
import { ResViewSection } from "../../models/ResourceView";
import { ResourceUtils } from "../../utils/ResourceUtils";

@Component({
  selector: "section-filter",
  templateUrl: "./section-filter.component.html",
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => SectionFilterComponent), multi: true,
  }],
  standalone: false
})
export class SectionFilterComponent {

  @Input() template: ResViewTemplate;
  @Input() readonly: boolean;

  private sectionFilterSetting: SectionFilterPreference;

  roleSectionsStructs: RoleSectionsStruct[];
  selectedRoleSectionsStruct: RoleSectionsStruct;
  selectedSection: SectionStruct;

  constructor(private basicModals: BasicModalsServices) { }

  private initSectionFilter() {
    if (!this.template || !this.sectionFilterSetting) {
      /*
      Initialization of the internal structures needs
      - template: on which to base the editor (provided as @Input)
      - setting: actual value of setting, namely the hidden sections for each role/template (bound with ngModel)
      In on of these is still not provided (e.g. async initialization from parent component), do nothing
      */
      return;
    }
    /*
    Convert the sectionFilterSetting to a RoleSectionStruct list.
    SectionFilterSetting is an object that lists the hidden sections for each roles.
    RoleSectionStruct contains also other info, such as the role or section "show" and the "checked" boolean when a section is visible
    */
    this.roleSectionsStructs = [];
    for (let role in this.template) {
      let sectionsStructs: SectionStruct[] = [];
      let sections: ResViewSection[] = this.template[role];
      sections.forEach(p => {
        //section visible if the role has no filtered-section listed, or the section for the role is not present among the filtered
        let showSection: boolean = this.sectionFilterSetting[role] == null || this.sectionFilterSetting[role].indexOf(p) == -1;
        sectionsStructs.push({
          id: p,
          showTranslationKey: TranslationUtils.getResViewSectionTranslationKey(p),
          checked: showSection
        });
      });
      this.roleSectionsStructs.push({
        role: { id: role as RDFResourceRolesEnum, show: ResourceUtils.getResourceRoleLabel(role as RDFResourceRolesEnum) },
        sections: sectionsStructs
      });
    }
    this.roleSectionsStructs.sort((p1, p2) => p1.role.show.localeCompare(p2.role.show));
    //init the selection on the first role
    this.selectRoleSectionsStruct(this.roleSectionsStructs[0]);
  }

  selectRoleSectionsStruct(rps: RoleSectionsStruct) {
    this.selectedRoleSectionsStruct = rps;
    this.selectedSection = null;
  }

  selectSection(section: SectionStruct) {
    if (this.selectedSection == section) {
      this.selectedSection = null;
    } else {
      this.selectedSection = section;
    }
  }

  /**
   * Checks/Unchecks (according the check parameter) all the sections of the struct provided
   * @param roleSectionsStruct 
   * @param check 
   */
  checkAll(roleSectionsStruct: RoleSectionsStruct, check: boolean) {
    roleSectionsStruct.sections.forEach(p => {
      p.checked = check;
    });
    this.updatePref();
  }

  areAllUnchecked(roleSectionsStruct: RoleSectionsStruct) {
    if (roleSectionsStruct == null) return false;
    let allUnchecked: boolean = true;
    roleSectionsStruct.sections.forEach(p => {
      if (p.checked) {
        allUnchecked = false;
      }
    });
    return allUnchecked;
  }

  /**
   * Checks/Unchecks (according the check parameter) the same section (the selected one) for all the roles.
   * The button that invokes this method should be enable only if there is a selectedSection.
   * @param check 
   */
  checkInAllRoles(check: boolean) {
    let section: ResViewSection = this.selectedSection.id;
    this.roleSectionsStructs.forEach(rps => {
      rps.sections.forEach(p => {
        if (section == p.id) {
          p.checked = check;
        }
      });
    });
    this.updatePref();
  }

  /**
   * Restore to visible all the sections for all the roles
   */
  reset() {
    this.basicModals.confirm({ key: "COMMONS.ACTIONS.RESET" }, { key: "MESSAGES.RESET_SECTION_FILTER_CONFIRM" }, ModalType.warning).then(
      () => {
        this.roleSectionsStructs.forEach(rps => {
          rps.sections.forEach(p => {
            p.checked = true;
          });
        });
        this.propagateChange(null); //this in place of updatePref() because the latter set {} instead of null (which prevents fallback mechanism)
      },
      () => { }
    );
  }

  updatePref() {
    let pref: SectionFilterPreference = {};
    this.roleSectionsStructs.forEach(rps => {
      let sectionsPref: ResViewSection[] = [];
      rps.sections.forEach(p => {
        if (!p.checked) {
          sectionsPref.push(p.id);
        }
      });
      if (sectionsPref.length > 0) {
        pref[rps.role.id + ""] = sectionsPref;
      }
    });
    this.propagateChange(pref);
  }



  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(obj: SectionFilterPreference) {
    if (obj != null) {
      this.sectionFilterSetting = obj;
    } else {
      this.sectionFilterSetting = {};
    }
    this.initSectionFilter();
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };

}

class RoleSectionsStruct {
  role: RoleStruct;
  sections: SectionStruct[];
}
class RoleStruct {
  id: RDFResourceRolesEnum;
  show: string;
}
class SectionStruct {
  id: ResViewSection;
  showTranslationKey: string;
  checked: boolean;
}