import { Component, forwardRef, Input } from "@angular/core";
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Language } from 'src/app/models/LanguagesCountries';
import { Project } from 'src/app/models/Project';
import { ValueFilterLanguages } from 'src/app/models/Properties';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';


@Component({
  selector: "lang-value-filter-pref",
  templateUrl: "./language-value-filter.component.html",
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => LanguageValueFilterComponent), multi: true,
  }],
  standalone: false
})
export class LanguageValueFilterComponent {

  @Input() project: Project; //if provided, this component manages the setting for such project

  private projectLanguages: Language[];

  sortOrder: SortOrder = SortOrder.ISO_CODE_ASCENDING;

  filterLanguages: LanguageItem[] = [];
  filterEnabled: boolean = false;

  pristineFilterLanguages: string[];
  languagesChanged: boolean = false;

  constructor(private settingsMgr: SettingsManager) { }

  ngOnInit() {
    this.projectLanguages = this.settingsMgr.getProjectLanguages(SVContext.getProject(this.project));
  }

  private init(valueFilterLanguage: ValueFilterLanguages) {
    this.filterEnabled = valueFilterLanguage.enabled;
    let preferenceLangs: string[] = valueFilterLanguage.languages;
    this.pristineFilterLanguages = preferenceLangs.slice();

    if (preferenceLangs.length == 1 && preferenceLangs[0] == "*") { //"*" stands for all languages
      //set as non active all the available langs
      this.filterLanguages = this.projectLanguages.map(pl => { return { lang: pl, active: false }; });
    } else {
      //set as active only the listed by the preference
      this.filterLanguages = this.projectLanguages.map(pl => {
        return { lang: pl, active: (preferenceLangs.indexOf(pl.tag) != -1) };
      });
    }

    this.filterEnabled = valueFilterLanguage.enabled;
  }

  toggleFilter() {
    this.filterEnabled = !this.filterEnabled;
    this.updatePref();
  }

  toggleLang(langItem: LanguageItem) {
    langItem.active = !langItem.active;
    this.updatePref();
  }

  changeAllLangStatus(checked: boolean) {
    this.filterLanguages.forEach(l => {
      l.active = checked;
    });
    this.updatePref();
  }

  private updatePref() {
    //collect the active languages to set in the preference
    let preferenceLangs: string[] = [];
    let activeLangs: LanguageItem[] = this.getActiveLanguageItems();
    activeLangs.forEach(l => {
      preferenceLangs.push(l.lang.tag);
    });

    //changed if size of original list and edited is different or if exists an element of the pristine not in the edited
    this.languagesChanged = this.pristineFilterLanguages.length != preferenceLangs.length || this.pristineFilterLanguages.some(l => !preferenceLangs.includes(l));

    let newPref: ValueFilterLanguages = {
      languages: preferenceLangs,
      enabled: this.filterEnabled
    };
    this.propagateChange(newPref);
  }

  changeIsocodeOrder() {
    if (this.sortOrder == SortOrder.ISO_CODE_ASCENDING) {
      this.sortOrder = SortOrder.ISO_CODE_DESCENDING;
      this.filterLanguages.sort((l1: LanguageItem, l2: LanguageItem) => {
        return -l1.lang.tag.localeCompare(l2.lang.tag);
      });
    } else { //in case is isocodeDescending or any other order active
      this.sortOrder = SortOrder.ISO_CODE_ASCENDING;
      this.filterLanguages.sort((l1: LanguageItem, l2: LanguageItem) => {
        return l1.lang.tag.localeCompare(l2.lang.tag);
      });
    }
  }
  changeLanguageOrder() {
    if (this.sortOrder == SortOrder.LANGUAGE_ASCENDING) {
      this.sortOrder = SortOrder.LANGUAGE_DESCENDING;
      this.filterLanguages.sort((l1: LanguageItem, l2: LanguageItem) => {
        return -l1.lang.name.localeCompare(l2.lang.name);
      });
    } else { //in case is positionDescending or any other order active
      this.sortOrder = SortOrder.LANGUAGE_ASCENDING;
      this.filterLanguages.sort((l1: LanguageItem, l2: LanguageItem) => {
        return l1.lang.name.localeCompare(l2.lang.name);
      });
    }
  }

  //Utils 

  getActiveLanguageItems(): LanguageItem[] {
    let activeLangs: LanguageItem[] = [];
    for (const l of this.filterLanguages) {
      if (l.active) {
        activeLangs.push(l);
      }
    }
    return activeLangs;
  }

  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(obj: ValueFilterLanguages) {
    if (obj == null) {
      obj = new ValueFilterLanguages();
    }
    this.init(obj);
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };

}

/**
 * Support class that represent a list item of the languages preference
 */
class LanguageItem {
  public lang: Language;
  public active: boolean;
}

class SortOrder {
  public static POSITION_DESCENDING: string = "position_descending";
  public static POSITION_ASCENDING: string = "position_ascending";
  public static ISO_CODE_DESCENDING: string = "isocode_descending";
  public static ISO_CODE_ASCENDING: string = "isocode_ascending";
  public static LANGUAGE_DESCENDING: string = "language_descending";
  public static LANGUAGE_ASCENDING: string = "language_ascending";
}