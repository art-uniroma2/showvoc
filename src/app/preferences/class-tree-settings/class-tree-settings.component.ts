import { ChangeDetectorRef, Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { BrowsingModalsServices } from 'src/app/modal-dialogs/browsing-modals/browsing-modals.service';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { ClassTreePreference } from 'src/app/models/Properties';
import { AnnotatedValue, IRI, RDFResourceRolesEnum, ResAttribute } from 'src/app/models/Resources';
import { OWL, RDFS } from 'src/app/models/Vocabulary';
import { ClassesServices } from 'src/app/services/classes.service';
import { ResourcesServices } from 'src/app/services/resources.service';
import { LocalStorageManager } from 'src/app/utils/LocalStorageManager';
import { NTriplesUtil, ResourceUtils, SortAttribute } from 'src/app/utils/ResourceUtils';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';

@Component({
  selector: "class-tree-settings",
  templateUrl: "./class-tree-settings.component.html",
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => ClassTreeSettingsComponent), multi: true,
  }],
  standalone: false
})
export class ClassTreeSettingsComponent implements ControlValueAccessor {
  @Input() administration: boolean; //if true, tells that the component is used in an administration context, so allows further settings

  rootClass: AnnotatedValue<IRI>;
  filterEnabled: boolean;

  // filterMapRes: FilterMapEntry[] = [];
  filterMapRes: { [cls: string]: FilterMapEntry } = {};
  selectedFilteredClass: AnnotatedValue<IRI>;

  renderingClasses: boolean = false;
  renderingFilter: boolean = false;

  showInstances: boolean;

  constructor(private clsService: ClassesServices, private resourceService: ResourcesServices, private settingsMgr: SettingsManager,
    private basicModals: BasicModalsServices, private browsingModals: BrowsingModalsServices,
    private changeDetectorRef: ChangeDetectorRef) { }

  onModelChange() {
    //convert filterMapRes to a map string: string[]
    let filterMap: { [key: string]: string[] } = {};
    Object.values(this.filterMapRes).forEach(entry => {
      let filteredSubClasses: string[] = [];
      if (entry.subClasses == null) {
        //subClasses in filterMapRes not yet initialized => get it from the preference
        let clsTreePref: ClassTreePreference = this.settingsMgr.getClassTreeSettings(SVContext.getProjectCtx().getProject());
        filteredSubClasses = clsTreePref.filter.map[entry.cls.getValue().getIRI()];
      } else {
        for (const sub of entry.subClasses) {
          if (!sub.checked) {
            filteredSubClasses.push(sub.resource.getValue().getIRI());
          }
        }
      }
      filterMap[entry.cls.getValue().getIRI()] = filteredSubClasses;
    });
    let pref: ClassTreePreference = {
      rootClass: this.rootClass.getValue().toNT(),
      filter: { map: filterMap, enabled: this.filterEnabled },
      showInstancesNumber: this.showInstances
    };
    this.propagateChange(pref);
  }

  /**
   * ROOT CLASS HANDLERS
   */

  changeClass() {
    this.browsingModals.browseClassTree({ key: "DATA.ACTIONS.SELECT_ROOT_CLASS" }, [RDFS.resource]).then(
      (cls: AnnotatedValue<IRI>) => {
        let model: string = SVContext.getWorkingProject().getModelType();
        if (
          (model == RDFS.uri && !cls.getValue().equals(RDFS.resource)) || //root different from rdfs:Resource in RDFS model
          (!cls.getValue().equals(RDFS.resource) && !cls.getValue().equals(OWL.thing)) //root different from rdfs:Resource and owl:Thing in other models
        ) {
          this.basicModals.confirmCheckCookie({ key: "COMMONS.STATUS.WARNING" }, { key: "MESSAGES.CUSTOM_ROOT_WARN" }, LocalStorageManager.WARNING_CUSTOM_ROOT, ModalType.warning).then(
            () => {
              this.rootClass = cls;
              this.onModelChange();
            },
            () => { }
          );
        } else {
          this.rootClass = cls;
          this.onModelChange();
        }
      },
      () => { }
    );
  }

  onManualRootClassEdit(clsURI: string) {
    let cls: AnnotatedValue<IRI> = new AnnotatedValue<IRI>(new IRI(clsURI), { [ResAttribute.ROLE]: RDFResourceRolesEnum.cls });
    //check if clsURI exist
    this.resourceService.getResourcePosition(cls.getValue()).subscribe(
      position => {
        if (position.isLocal()) {
          this.rootClass = cls;
          this.onModelChange();
        } else {
          this.basicModals.alert({ key: "COMMONS.STATUS.ERROR" }, { key: "MESSAGES.UNEXISTING_URI_IN_PROJECT", params: { resUri: cls.getValue().toNT() } }, ModalType.warning);
          //temporarly reset the root class and the restore it (in order to trigger the change detection editable-input)
          let oldRootClass = this.rootClass;
          this.rootClass = null;
          this.changeDetectorRef.detectChanges();
          this.rootClass = oldRootClass;
        }
      }
    );
  }

  /**
   * FILTER MAP HANDLERS
   */

  selectFilteredClass(cls: AnnotatedValue<IRI>) {
    this.selectedFilteredClass = cls;

    let filterMapEntry: FilterMapEntry = this.getFilterMapEntry(this.selectedFilteredClass);
    if (filterMapEntry.subClasses == null) { //subclasses not yet initialized for the given class
      this.clsService.getSubClasses(this.selectedFilteredClass.getValue(), false).subscribe(
        classes => {
          ResourceUtils.sortResources(classes, SortAttribute.show);
          let clsTreePref: ClassTreePreference = this.settingsMgr.getClassTreeSettings(SVContext.getProjectCtx().getProject());
          let filteredSubClssPref = clsTreePref.filter.map[this.selectedFilteredClass.getValue().getIRI()];

          filterMapEntry.subClasses = [];

          classes.forEach(c => {
            if (filteredSubClssPref != null) { //exists a subclasses filter for the selected class
              filterMapEntry.subClasses.push({
                checked: filteredSubClssPref.indexOf(c.getValue().getIRI()) == -1, //subClass not in the filter, so checked (visible)
                disabled: c.getValue().equals(OWL.thing), //owl:Thing cannot be filtered out
                resource: c
              });
            } else { //doesn't exist a subclasses filter for the selected class => every subclasses is checked
              filterMapEntry.subClasses.push({ checked: true, disabled: c.getValue().equals(OWL.thing), resource: c });
            }
          });
        }
      );
    }
  }

  getFilterSubClasses(): SubClassFilterItem[] {
    if (this.selectedFilteredClass != null) {
      return this.getFilterMapEntry(this.selectedFilteredClass).subClasses;
    } else {
      return [];
    }
  }

  addFilter() {
    this.browsingModals.browseClassTree({ key: "DATA.ACTIONS.SELECT_CLASS" }, [RDFS.resource]).then(
      (cls: AnnotatedValue<IRI>) => {
        if (this.getFilterMapEntry(cls) == null) {
          this.filterMapRes[cls.getValue().getIRI()] = { cls: cls, subClasses: null };
          this.onModelChange();
        } else {
          this.basicModals.alert({ key: "COMMONS.STATUS.ERROR" }, { key: "MESSAGES.CLASS_FILTER_ALREADY_EXIST", params: { cls: cls.getShow() } }, ModalType.warning);
        }
      },
      () => { }
    );
  }

  removeFilter() {
    delete this.filterMapRes[this.selectedFilteredClass.getValue().getIRI()];
    this.onModelChange();
  }

  checkAllClasses(checked: boolean) {
    this.getFilterMapEntry(this.selectedFilteredClass).subClasses.forEach((c: SubClassFilterItem) => {
      if (!c.disabled) {
        c.checked = checked;
      }
    });
    this.onModelChange();
  }

  private getFilterMapEntry(cls: AnnotatedValue<IRI>): FilterMapEntry {
    return this.filterMapRes[cls.getValue().getIRI()];
  }


  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(pref: ClassTreePreference) {
    if (pref) {
      this.filterEnabled = pref.filter.enabled;

      let filteredClss: IRI[] = [];
      for (let key in pref.filter.map) {
        filteredClss.push(new IRI(key));
      }
      if (filteredClss.length > 0) {
        this.resourceService.getResourcesInfo(filteredClss).subscribe(
          resources => {
            resources.forEach(r => {
              this.filterMapRes[r.getValue().getIRI()] = { cls: r, subClasses: null };
            });
          }
        );
      }

      this.rootClass = new AnnotatedValue(NTriplesUtil.parseIRI(pref.rootClass));
      this.showInstances = pref.showInstancesNumber;

    }
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };

}

class FilterMapEntry {
  cls: AnnotatedValue<IRI>;
  subClasses: SubClassFilterItem[];
}

class SubClassFilterItem {
  checked: boolean;
  resource: AnnotatedValue<IRI>;
  disabled?: boolean;
}