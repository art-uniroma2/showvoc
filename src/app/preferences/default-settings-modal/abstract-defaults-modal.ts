import { Directive } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { forkJoin, Observable, of } from "rxjs";
import { map } from "rxjs/operators";
import { Scope, Settings } from "src/app/models/Plugins";
import { SettingsServices } from "src/app/services/settings.service";
import { SettingsManager, SettingsManagerID } from "src/app/utils/SettingsManager";
import { SVContext } from "src/app/utils/SVContext";

@Directive()
export abstract class AbstractDefaultsModal {

    abstract settingName: string;

    userDefault: any;
    projectDefault: any;

    scopes: Scope[];
    activeScope: Scope;

    restoreProjDefaultOpt: string = "COMMONS.ACTIONS.RESTORE_PROJ_DEFAULT";
    applyUserDefaultOpt: string = "COMMONS.ACTIONS.APPLY_USER_DEFAULT";
    multiActionOpts: string[] = [this.restoreProjDefaultOpt, this.applyUserDefaultOpt];
    selectedMultiActionOpt: string = this.multiActionOpts[0];

    protected activeModal: NgbActiveModal;
    protected settingsService: SettingsServices;
    protected settingsMgr: SettingsManager;
    constructor(
        activeModal: NgbActiveModal,
        settingsService: SettingsServices,
        settingsMgr: SettingsManager
    ) { 
        this.activeModal = activeModal;
        this.settingsService = settingsService;
        this.settingsMgr = settingsMgr;
    }

    ngOnInit() {
        this.init();
    }

    init() {
        /*
        Depending on user, USER default needs to be initialized differently:
        - client side using SettingsManager, in case of anonymous (non-logged) user, this is because USER defaults are stored on LocalStorage
        - server side using SettingsServices invocation, in case of logged-in user, because each user has his own defaults stored on ST and defaults are not initialized at user access
        */
        let initUserDefaults: Observable<void> = of(null);
        if (SVContext.getLoggedUser() == null) {
            this.userDefault = this.settingsMgr.getDefault(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.settingName, Scope.USER);
        } else {
            initUserDefaults = this.settingsService.getSettingsDefault(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.PROJECT_USER, Scope.USER).pipe(
                map((setting: Settings) => {
                    this.userDefault = setting.getPropertyValue(this.settingName);
                })
            );
        }

        /**
         * Note that, in case of anonymous user, it might also be possible to get the PROJECT defaults from SettingsManager cache, 
         * because PROJECT defaults are initialized at project access, anyway, to align the behavior, get it always from ST
         */
        let initProjDefaults = this.settingsService.getSettingsDefault(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.PROJECT_USER, Scope.PROJECT).pipe(
            map((setting: Settings) => {
                this.projectDefault = setting.getPropertyValue(this.settingName);
            })
        );

        forkJoin([initProjDefaults, initUserDefaults]).subscribe(
            () => {
                this.scopes = [];
                if (this.projectDefault) {
                    this.scopes.push(Scope.PROJECT);
                }
                if (this.userDefault) {
                    this.scopes.push(Scope.USER);
                }
                if (this.scopes.length > 0) {
                    this.activeScope = this.scopes[0];
                }
            }
        );
    }

    /**
     * Action perfomed when only one default is available
     */
    restoreDefault() {
        if (this.scopes[0] == Scope.PROJECT) {
            //restore project default
            this.restoreProjDefault();
        } else { //USER
            //apply user default
            this.applyUserDefault();
        }
    }

    applyAction() {
        if (this.selectedMultiActionOpt == this.restoreProjDefaultOpt) {
            this.restoreProjDefault();
        } else if (this.selectedMultiActionOpt == this.applyUserDefaultOpt) {
            this.applyUserDefault();
        }
    }

    abstract restoreProjDefaultImpl(): Observable<void>
    abstract applyUserDefaultImpl(): Observable<void>

    restoreProjDefault() {
        this.restoreProjDefaultImpl().subscribe(
            () => {
                this.ok();
            }
        );
    }

    applyUserDefault() {
        this.applyUserDefaultImpl().subscribe(
            () => {
                this.ok();
            }
        );
    }


    ok() {
        this.activeModal.close();
    }

    cancel() {
        this.activeModal.dismiss();
    }


}