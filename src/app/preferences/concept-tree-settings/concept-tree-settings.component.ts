import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Project } from 'src/app/models/Project';
import { ConceptTreePreference, ConceptTreeVisualizationMode } from 'src/app/models/Properties';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { TranslationUtils } from 'src/app/utils/TranslationUtils';

@Component({
  selector: "concept-tree-settings",
  templateUrl: "./concept-tree-settings.component.html",
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => ConceptTreeSettingsComponent), multi: true,
  }],
  standalone: false
})
export class ConceptTreeSettingsComponent implements ControlValueAccessor {
  @Input() project: Project; //if not explicitly provided, get the current accessed project
  @Input() administration: boolean; //if true, tells that the component is used in an administration context, so allows further settings

  visualization: ConceptTreeVisualizationMode;
  visualizationModes: { value: ConceptTreeVisualizationMode, labelTranslationKey: string }[] = [
    { value: ConceptTreeVisualizationMode.hierarchyBased, labelTranslationKey: TranslationUtils.visualizationModeTranslationMap[ConceptTreeVisualizationMode.hierarchyBased] },
    { value: ConceptTreeVisualizationMode.searchBased, labelTranslationKey: TranslationUtils.visualizationModeTranslationMap[ConceptTreeVisualizationMode.searchBased] }
  ];
  allowVisualizationChange: boolean;

  safeToGoLimit: number;

  constructor(private settingsMgr: SettingsManager) { }

  ngOnInit() {
    this.project = SVContext.getProject(this.project);
    this.allowVisualizationChange = this.settingsMgr.getAllowConceptTreeVisualizationChange(this.project);
  }

  onModelChange() {
    let pref: ConceptTreePreference = {
      visualization: this.visualization,
      safeToGoLimit: this.safeToGoLimit,
    };
    this.propagateChange(pref);
  }

  toggleAllowVisualizationChange() {
    this.allowVisualizationChange = !this.allowVisualizationChange;
    this.settingsMgr.setAllowConceptTreeVisualizationChange(this.project, this.allowVisualizationChange).subscribe();
  }


  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(pref: ConceptTreePreference) {
    if (pref) {
      this.visualization = pref.visualization;
      this.safeToGoLimit = pref.safeToGoLimit;
    }
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };

}