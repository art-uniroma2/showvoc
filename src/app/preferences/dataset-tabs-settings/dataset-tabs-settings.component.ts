import { Component, forwardRef, Input } from "@angular/core";
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { DatasetTab } from 'src/app/models/Properties';
import { AuthorizationEvaluator, STActionsEnum } from "src/app/utils/AuthorizationEvaluator";

@Component({
  selector: "dataset-tabs-settings",
  templateUrl: "./dataset-tabs-settings.component.html",
  styles: [`
        .nav li a {
            padding: 10px;
            font-size: 15px;
            color: #17a2b8;
        }
    `],
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => DatasetsTabsSettingsComponent), multi: true,
  }],
  standalone: false
})
export class DatasetsTabsSettingsComponent {

  @Input() readonly: boolean;

  DatasetTab = DatasetTab; //for using the enum in the html

  hiddenTabs: DatasetTab[] = [];

  tabsStruct: TabEntry[] = [
    // { tab: DatasetTab.data, visible: true, translationKey: "DATA.DATA" },
    { tab: DatasetTab.sparql, visible: true, translationKey: "Sparql" },
    { tab: DatasetTab.metadata, visible: true, translationKey: "Metadata" },
    { tab: DatasetTab.downloads, visible: true, translationKey: "METADATA.DISTRIBUTIONS.DOWNLOADS" },
    { tab: DatasetTab.tools, visible: true, translationKey: "COMMONS.TOOLS" }
  ];

  toolsAuthorized: boolean = false;

  previewHideTabSelector: boolean = false; //tells if the tab selector is hidden (in case only 1 tab is visible)
  previewActiveTab: DatasetTab = DatasetTab.data;

  constructor() { }

  ngOnInit() {
    //custom services and invokable reporters page
    let isCustomServiceAuthorized = AuthorizationEvaluator.isAuthorized(STActionsEnum.customServiceRead) &&
      AuthorizationEvaluator.isAuthorized(STActionsEnum.invokableReporterRead);
    //custom view page (uses the same auth of cform services)
    let isCustomViewsAuthorized = AuthorizationEvaluator.isAuthorized(STActionsEnum.customFormGetCollections) &&
      AuthorizationEvaluator.isAuthorized(STActionsEnum.customFormGetFormMappings) &&
      AuthorizationEvaluator.isAuthorized(STActionsEnum.customFormGetForms);
    this.toolsAuthorized = isCustomServiceAuthorized || isCustomViewsAuthorized;
    if (!this.toolsAuthorized) {
      //if user has no access to any page under Tools tab, remove it from the struct
      this.tabsStruct = this.tabsStruct.filter(t => t.tab != DatasetTab.tools);
    }
  }


  isTabVisible(tab: DatasetTab): boolean {
    return !this.hiddenTabs.includes(tab);
  }

  private updateInternalStruct() {
    this.hiddenTabs = this.tabsStruct.filter(tab => !tab.visible).map(t => t.tab);
    //update preview staff
    this.previewHideTabSelector = this.tabsStruct.filter(t => t.visible).length == 0;
    if (this.hiddenTabs.includes(this.previewActiveTab)) {
      //if the active tab in preview is hidden, activate data
      this.previewActiveTab = DatasetTab.data;
    }
  }

  toggleTab(tab: TabEntry) {
    //cannot toggle tab if it is the last one visible
    if (this.readonly) {
      return;
    }
    tab.visible = !tab.visible;
    this.onModelChange();
  }

  onModelChange() {
    this.updateInternalStruct();
    this.propagateChange(this.hiddenTabs);
  }

  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(hiddens: DatasetTab[]) {
    if (hiddens) {
      for (let t of this.tabsStruct) {
        t.visible = !hiddens.includes(t.tab);
      }
      this.updateInternalStruct();
    }
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };


}

interface TabEntry {
  tab: DatasetTab;
  visible: boolean;
  translationKey: string;
}