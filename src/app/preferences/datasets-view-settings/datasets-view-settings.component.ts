import { Component, forwardRef } from "@angular/core";
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { STProperties } from 'src/app/models/Plugins';
import { ProjectFacets, ProjectUtils, ProjectViewMode } from 'src/app/models/Project';
import { ProjectVisualization } from 'src/app/models/Properties';
import { SVContext } from 'src/app/utils/SVContext';
import { ProjectsServices } from '../../services/projects.service';

@Component({
  selector: "datasets-view-settings",
  templateUrl: "./datasets-view-settings.component.html",
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => DatasetsViewSettingsComponent), multi: true,
  }],
  standalone: false
})
export class DatasetsViewSettingsComponent {

  projVisualization: ProjectVisualization;

  isAdmin: boolean;

  visualizationModes: { translationKey: string, mode: ProjectViewMode }[] = [
    { translationKey: "DATASETS.SETTINGS.VIEW_MODE_FLAT", mode: ProjectViewMode.list },
    { translationKey: "DATASETS.SETTINGS.VIEW_MODE_FOLDED", mode: ProjectViewMode.facet }
  ];
  selectedVisualizationMode: { translationKey: string, mode: ProjectViewMode };

  facets: FacetStruct[] = [];
  selectedFacet: FacetStruct; //name of the facet

  constructor(private projectService: ProjectsServices, private basicModals: BasicModalsServices, private translateService: TranslateService) { }

  ngOnInit() {
    this.isAdmin = SVContext.getLoggedUser()?.isAdmin();
  }

  private restoreSelectedOptions() {
    this.ensureFacetsInitialized().subscribe(() => {
      this.selectedVisualizationMode = this.visualizationModes.find(m => m.mode == this.projVisualization.mode);
      if (this.selectedVisualizationMode == null) {
        this.selectedVisualizationMode = this.visualizationModes[0];
      }

      //init selected facet (for facet-based visualization) with the stored cookie, or if none (or not valid) with the first facet available
      let selectedFacetStored: string = this.projVisualization.facetBagOf;
      if (selectedFacetStored != null && this.facets.some(f => f.name == selectedFacetStored)) {
        this.selectedFacet = this.facets.find(f => f.name == selectedFacetStored);
      } else {
        this.selectedFacet = this.facets[0];
      }
    });
  }

  private ensureFacetsInitialized(): Observable<void> {
    //- collect the custom facets
    return this.projectService.getProjectFacetsForm().pipe(
      mergeMap(facetsForm => {
        facetsForm.properties.forEach(p => {
          if (p.name == "customFacets") {
            let customFacetsProps: STProperties[] = p.type.schema.properties;
            customFacetsProps.forEach(cf => {
              this.facets.push({ name: cf.name, displayName: cf.displayName, set: cf.type.name == "Set" });
            });
          } else {
            this.facets.push({ name: p.name, displayName: p.displayName, set: p.type.name == "Set" });
          }
        });
        //now the built-in (e.g. lex model, history, ...)
        return this.projectService.getFacetsAndValue().pipe(
          map(facetsAndValues => {
            Object.keys(facetsAndValues).forEach(facetName => {
              if (facetName == ProjectFacets.prjHistoryEnabled || facetName == ProjectFacets.prjValidationEnabled) {
                return; //history and validation are not foreseen in SV
              }
              //check if the facets is not yet added (getFacetsAndValue returns all the facets which have at least a value in the projects)
              if (!this.facets.some(f => f.name == facetName)) {
                //retrieve and translate the display name
                let displayName = facetName; //as fallback the displayName is the same facet name
                let facetTranslationKey = ProjectUtils.projectFacetsTranslationStruct[facetName];
                if (facetTranslationKey != null) {
                  displayName = this.translateService.instant(facetTranslationKey);
                }
                this.facets.push({ name: facetName, displayName: displayName, set: false }); //built-in facets are all non-Set
              }
            });

            this.facets.sort((f1, f2) => f1.displayName.localeCompare(f2.displayName));

          })
        );
      })
    );
  }

  recreateFacetsIndex() {
    this.projectService.createFacetIndex().subscribe(
      () => {
        this.basicModals.alert({ key: "COMMONS.STATUS.OPERATION_DONE" }, { key: "MESSAGES.FACETS_INDEX_RECREATED" });
      }
    );
  }


  onModelChange() {
    let conf: ProjectVisualization = {
      mode: this.selectedVisualizationMode.mode,
      facetBagOf: this.selectedFacet.name
    };
    this.propagateChange(conf);
  }

  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(obj: ProjectVisualization) {
    if (obj) {
      this.projVisualization = obj;
      this.restoreSelectedOptions();
    }
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };



}


interface FacetStruct {
  name: string;
  displayName: string;
  set: boolean;
}