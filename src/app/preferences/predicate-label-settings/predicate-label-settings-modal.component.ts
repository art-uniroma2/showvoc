import { Component, ElementRef, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Project } from 'src/app/models/Project';
import { PredLabelMapping } from "src/app/models/Properties";
import { UIUtils } from "src/app/utils/UIUtils";

@Component({
  selector: "pred-label-settings-modal",
  templateUrl: "./predicate-label-settings-modal.component.html",
  standalone: false
})
export class PredicateLabelSettingsModalComponent {

  @Input() project: Project; //if provided, this component manages the setting for such project
  @Input() predLabelMappings: { [lang: string]: PredLabelMapping };

  constructor(public activeModal: NgbActiveModal, private elementRef: ElementRef) { }

  ngOnInit() {
    UIUtils.setFullSizeModal(this.elementRef);
  }

  ok() {
    this.activeModal.close(this.predLabelMappings);
  }

  close() {
    this.activeModal.dismiss();
  }

}