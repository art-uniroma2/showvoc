import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Project } from 'src/app/models/Project';
import { IRI } from "src/app/models/Resources";
import { ExportServices } from "src/app/services/export.service";
import { STRequestOptions } from "src/app/utils/HttpManager";
import { ResourceUtils, SortAttribute } from "src/app/utils/ResourceUtils";

@Component({
  selector: "select-vocabularies-modal",
  templateUrl: "./select-vocabularies-modal.component.html",
  standalone: false
})
export class SelectVocabulariesModalComponent {

  @Input() project: Project; //if provided, this component manages the setting for such project

  graphs: VocabEntry[] = [];

  constructor(public activeModal: NgbActiveModal, private exportService: ExportServices) { }

  ngOnInit() {
    this.exportService.getNamedGraphs(STRequestOptions.getRequestOptions(this.project)).subscribe(
      graphs => {
        ResourceUtils.sortResources(graphs, SortAttribute.value);
        this.graphs = graphs.map(g => {
          return {
            graph: g.getValue(),
            checked: true
          };
        });
      }
    );
  }

  checkAll(check: boolean) {
    this.graphs.forEach(g => {
      g.checked = check;
    });
  }

  isOkEnabled(): boolean {
    return this.graphs.some(g => g.checked);
  }

  ok() {
    let selectedGraphs: IRI[] = this.graphs.filter(g => g.checked).map(g => g.graph);
    this.activeModal.close(selectedGraphs);
  }

  close() {
    this.activeModal.dismiss();
  }

}


interface VocabEntry {
  checked: boolean;
  graph: IRI;
}