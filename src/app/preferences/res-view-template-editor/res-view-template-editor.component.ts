import { Component, forwardRef, Input } from "@angular/core";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import { ResViewTemplate } from "src/app/models/Properties";
import { RDFResourceRolesEnum } from "src/app/models/Resources";
import { ResViewSection } from "src/app/models/ResourceView";
import { TranslationUtils } from "src/app/utils/TranslationUtils";

@Component({
  selector: "res-view-template-editor",
  templateUrl: "./res-view-template-editor.component.html",
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => ResViewTemplateEditorComponent), multi: true,
  }],
  standalone: false
})
export class ResViewTemplateEditorComponent {

  @Input() customSections: string[]; //ID of custom sections

  //templates
  templates: { [role: string]: TemplateStruct } = {}; //for each role list of enabled and disabled sections

  roles: RDFResourceRolesEnum[];
  selectedRole: RDFResourceRolesEnum;

  selectedIncludedSection: SectionStruct;
  selectedAvailableSection: SectionStruct;

  constructor() { }

  /**
   * Convert input template setting struct (map role->section[]) to internal struct
   * @param template
   */
  private init(setting: ResViewTemplate) {
    //collects all the sections
    let allSections: ResViewSection[] = [];
    for (let p in ResViewSection) { //the built-in
      allSections.push(p as ResViewSection);
    }
    if (this.customSections) {
      this.customSections.forEach(cs => allSections.push(cs as ResViewSection)); //the custom ones
      allSections.sort();
    }

    for (let role in setting) {
      let activeSections: ResViewSection[] = setting[role];
      let notActiveSections: ResViewSection[] = allSections.filter(p => !activeSections.includes(p));
      this.templates[role] = {
        included: activeSections.map(s => {
          return { section: s, translationKey: TranslationUtils.getResViewSectionTranslationKey(s) };
        }),
        available: notActiveSections.map(s => {
          return { section: s, translationKey: TranslationUtils.getResViewSectionTranslationKey(s) };
        })
      };
    }
    this.roles = Object.keys(this.templates).sort() as RDFResourceRolesEnum[];
  }

  selectRole(role: RDFResourceRolesEnum) {
    this.selectedRole = role;
    this.selectedIncludedSection = null;
    this.selectedAvailableSection = null;
  }

  selectIncluded(section: SectionStruct) {
    if (this.selectedIncludedSection == section) {
      this.selectedIncludedSection = null;
    } else {
      this.selectedIncludedSection = section;
    }
  }

  selectAvailable(section: SectionStruct) {
    if (this.selectedAvailableSection == section) {
      this.selectedAvailableSection = null;
    } else {
      this.selectedAvailableSection = section;
    }
  }

  addSection(role: RDFResourceRolesEnum, section: SectionStruct) {
    let template: TemplateStruct = this.templates[role];
    //remove section from available ones
    if (template.available.includes(section)) {
      template.available.splice(template.available.indexOf(section), 1);
    }
    //add section to included
    if (!template.included.includes(section)) {
      //add it before other properties section (if in the template) or at the end
      if (template.included.some(s => s.section == ResViewSection.properties)) {
        template.included.splice(template.included.findIndex(s => s.section == ResViewSection.properties), 0, section);
      } else {
        template.included.push(section);
      }
    }
    this.selectedAvailableSection = null;
    this.onModelChange();
  }

  addSectionToAll() {
    let sectionToAdd = this.selectedAvailableSection;
    for (let r of this.roles) {
      this.addSection(r, sectionToAdd);
    }
    this.onModelChange();
  }

  removeSection(role: RDFResourceRolesEnum, section: SectionStruct) {
    let template: TemplateStruct = this.templates[role];
    //remove section from included ones
    if (template.included.includes(section)) {
      template.included.splice(template.included.indexOf(section), 1);
    }
    //add section to available ones
    if (!template.available.includes(section)) {
      template.available.push(section);
      template.available.sort((s1, s2) => s1.section.localeCompare(s2.section));
    }
    this.selectedIncludedSection = null;
    this.onModelChange();
  }

  removeSectionFromAll() {
    let sectionToRemove = this.selectedIncludedSection;
    for (let r of this.roles) {
      this.removeSection(r, sectionToRemove);
    }
    this.onModelChange();
  }

  moveSectionUp() {
    let template: TemplateStruct = this.templates[this.selectedRole];
    let idx = template.included.indexOf(this.selectedIncludedSection);
    if (idx > 0) {
      let oldP = template.included[idx - 1];
      template.included[idx - 1] = this.selectedIncludedSection;
      template.included[idx] = oldP;
    }
    this.onModelChange();
  }
  moveSectionDown() {
    let template: TemplateStruct = this.templates[this.selectedRole];
    let idx = template.included.indexOf(this.selectedIncludedSection);
    if (idx < template.included.length - 1) {
      let oldP = template.included[idx + 1];
      template.included[idx + 1] = this.selectedIncludedSection;
      template.included[idx] = oldP;
    }
    this.onModelChange();
  }



  /* CUSTOM SECTIONS SYNC UTILS === START */
  /*
   * Methods invokable from outside, useful to keep template in sync with changes on Custom Sections
   */

  onCustomSectionRenamed(oldSection: string, newSection: string) {
    for (let role in this.templates) {
      //check if it is among the included
      this.templates[role].included.forEach((section, index, list) => {
        if (section.section == oldSection) {
          list[index] = { section: newSection as ResViewSection, translationKey: newSection };
        }
      });
      //check if it is among the available
      this.templates[role].available.forEach((section, index, list) => {
        if (section.section == oldSection) {
          list[index] = { section: newSection as ResViewSection, translationKey: newSection };
        }
      });
    }
    //if custom section was selected, update the selections
    if (this.selectedIncludedSection?.section == oldSection) {
      this.selectedIncludedSection = this.templates[this.selectedRole].included.find(s => s.section == newSection);
    } else if (this.selectedAvailableSection?.section == oldSection) {
      this.selectedAvailableSection = this.templates[this.selectedRole].available.find(s => s.section == newSection);
    }
    this.onModelChange();
  }

  onCustomSectionDeleted(customSection: string) {
    for (let role in this.templates) {
      //check if it is among the enabled
      if (this.templates[role].included.some(s => s.section == customSection)) {
        this.templates[role].included.splice(this.templates[role].included.findIndex(s => s.section == customSection as ResViewSection), 1);
        if (this.selectedIncludedSection?.section == customSection) {
          this.selectedIncludedSection = null;
        }
      }
      //check if it is among the available
      if (this.templates[role].available.some(s => s.section == customSection)) {
        this.templates[role].available.splice(this.templates[role].available.findIndex(s => s.section == customSection), 1);
        if (this.selectedAvailableSection?.section == customSection) {
          this.selectedAvailableSection = null;
        }
      }
    }
    this.onModelChange();
  }

  onCustomSectionCreated(customSection: string) {
    //add the new section to those disabled in the templates
    for (let role in this.templates) {
      this.templates[role].available.push({ section: customSection as ResViewSection, translationKey: customSection });
      this.templates[role].available.sort((s1, s2) => s1.section.localeCompare(s2.section));
    }
    this.onModelChange();
  }

  /* CUSTOM SECTIONS SYNC UTILS === END */



  onModelChange() {
    let setting: ResViewTemplate = {};
    for (let role in this.templates) {
      setting[role] = this.templates[role].included.map(s => s.section);
    }
    this.propagateChange(setting);
  }


  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(obj: ResViewTemplate) {
    if (obj) {
      this.init(obj);
    }
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };
}


interface TemplateStruct {
  included: SectionStruct[];
  available: SectionStruct[];
}

interface SectionStruct {
  section: ResViewSection;
  translationKey: string;
}