import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { DataPanel, DataPanelLabelMap, DataPanelLabelSetting, DataStructureUtils } from 'src/app/models/DataStructure';
import { Project } from 'src/app/models/Project';
import { TranslationUtils } from 'src/app/utils/TranslationUtils';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';

@Component({
  selector: 'data-panel-labels-editor-modal',
  templateUrl: './data-panels-labels-editor-modal.component.html',
  standalone: false
})
export class DataPanelLabelsEditorModalComponent {

  @Input() project: Project;

  panels: DataPanel[];

  translateLangs: string[];

  langs: string[]; //langs for which exists a mapping
  additionalLangs: string[];

  langMappings: LangMappingsTab[] = [];
  activeTab: LangMappingsTab;

  activeTabLang: string;

  constructor(
    public activeModal: NgbActiveModal,
    private settingsMgr: SettingsManager,
    private basicModals: BasicModalsServices,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.translateLangs = this.translate.getLangs();
    this.additionalLangs = this.translateLangs;

    let model = this.project.getModelType();
    this.panels = [DataPanel.cls, DataPanel.concept, DataPanel.skosCollection, DataPanel.conceptScheme,
    DataPanel.limeLexicon, DataPanel.ontolexLexicalEntry, DataPanel.property];
    //filter only tabs compliant with model
    this.panels = this.panels.filter(t => DataStructureUtils.modelPanelsMap[model].includes(t));

    this.panels.push(DataPanel.alignments);
    if (this.settingsMgr.getXkosTabEnabled(this.project)) {
      this.panels.push(DataPanel.correspondence);
    }

    //if a setting is provided, fill the struct
    let setting = this.settingsMgr.getDataPanelsLabels(this.project);
    if (setting) {
      for (let lang in setting) {
        if (this.translateLangs.includes(lang)) {
          this.addLangTab(lang);
          let langMap = this.langMappings.find(m => m.lang == lang); //gets the newly created mapping
          let panelLabelMappings = setting[lang]; //get the lang mapping for the adding lang
          for (let panel in panelLabelMappings) {
            let pm = langMap.panelMappings.find(pm => pm.panel == panel);
            if (pm) {
              pm.label = panelLabelMappings[panel];
            }
          }
        }
      }
      //once restored tabs, activate the first one
      this.activateTabLang(this.langMappings[0].lang);
    }
  }

  private updateLangs() {
    this.langs = this.langMappings.map(m => m.lang);
    this.additionalLangs = this.translateLangs.filter(l => !this.langs.includes(l));
  }

  /* =====
  TABS
  * ====== */

  activateTabLang(lang: string) {
    this.activeTabLang = lang;
    // this.activeMappingTab.labelMappings = this.langMappings.find(m => m.lang == lang).labelMappings;
    this.activeTab = this.langMappings.find(m => m.lang == lang);
  }

  closeTab(lang: string, event: Event) {
    this.basicModals.confirm({ key: "COMMONS.STATUS.WARNING" }, { key: "PREFERENCES.DATA_PANEL_LABELS.DELETE_MAPPINGS_WARN", params: { language: lang } }, ModalType.warning).then(
      () => {
        event.preventDefault(); //prevent the refresh of the page
        //remove the tab to close
        this.langMappings = this.langMappings.filter(m => m.lang != lang);
        //update active tab
        if (this.langMappings.length > 0) {
          this.activateTabLang(this.langMappings[0].lang);
        } else {
          this.activeTabLang = null;
          this.activeTab = null;
        }
        this.updateLangs();
      },
      () => { }
    );
  }

  addLangTab(lang: string) {
    let panelMappings: PanelMappingEntry[] = this.panels.map(p => {
      return {
        panel: p,
        translationKey: TranslationUtils.dataPanelTranslationMap[p],
        label: null
      };
    });
    this.langMappings.push({
      lang: lang,
      panelMappings: panelMappings
    });
    this.updateLangs();
    this.activateTabLang(lang);
  }

  ok() {
    //convert internal structure to the setting value 
    let setting: DataPanelLabelSetting = {};

    this.langMappings.forEach(langMap => {
      let mappings: DataPanelLabelMap = {};
      langMap.panelMappings.filter(m => {
        return m.label && m.label.trim() != "";
      }).forEach(m => {
        mappings[m.panel] = m.label;
      });
      setting[langMap.lang] = mappings;
    });
    //clean empty langs
    for (let lang in setting) {
      //empty map in lang? => delete it
      if (Object.keys(setting[lang]).length == 0) {
        delete setting[lang];
      }
    }

    this.settingsMgr.setDataPanelsLabels(this.project, setting).subscribe(
      () => {
        //if the change has been done on current project, emit onLangChange to force Data page to reinit the labels
        if (SVContext.getProject()?.getName() == this.project.getName()) {
          let event: LangChangeEvent = { lang: this.translate.currentLang, translations: this.translate.translations };
          this.translate.onLangChange.emit(event);
        }
        this.activeModal.close();
      }
    );
  }

  close() {
    this.activeModal.dismiss();
  }

}

interface PanelMappingEntry {
  panel: DataPanel;
  translationKey: string;
  label: string;
}

interface LangMappingsTab {
  lang: string;
  panelMappings: PanelMappingEntry[];
}