import { Component, forwardRef, Input } from "@angular/core";
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { TranslateService } from "@ngx-translate/core";
import { DataPanel, DataPanelLabelSetting, DataStructureUtils } from "src/app/models/DataStructure";
import { Project } from "src/app/models/Project";
import { SettingsManager } from "src/app/utils/SettingsManager";
import { SVContext } from "src/app/utils/SVContext";
import { TranslationUtils } from "src/app/utils/TranslationUtils";

@Component({
  selector: "data-panels-settings",
  templateUrl: "./data-panels-settings.component.html",
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => DataPanelsSettingsComponent), multi: true,
  }],
  standalone: false
})
export class DataPanelsSettingsComponent {

  @Input() readonly: boolean;
  @Input() project: Project;

  DataPanel = DataPanel; //for using the enum in the html

  hiddenTabs: DataPanel[] = [];

  tabsStruct: TabEntry[];

  previewShowTabSelector: boolean = true; //tells if the tab selector is shown (in case 2+ tabs are visible)
  previewActiveTab: DataPanel;

  constructor(
    private settingsMgr: SettingsManager,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.project = SVContext.getProject(this.project);

    this.initTabStruct();
  }

  initTabStruct() {

    let panels = [DataPanel.cls, DataPanel.concept, DataPanel.skosCollection, DataPanel.conceptScheme,
    DataPanel.limeLexicon, DataPanel.ontolexLexicalEntry, DataPanel.property];
    //filter according model compatibility
    let model = this.project.getModelType();
    panels = panels.filter(p => DataStructureUtils.modelPanelsMap[model].includes(p));

    this.tabsStruct = panels.map(p => {
      return {
        panel: p,
        visible: true,
        label: this.translate.instant(TranslationUtils.dataPanelTranslationMap[p]),
        customLabel: ""
      };
    });

    //add additional panels
    // - alignments
    this.tabsStruct.push({
      panel: DataPanel.alignments,
      visible: true,
      label: this.translate.instant(TranslationUtils.dataPanelTranslationMap[DataPanel.alignments]),
      customLabel: "",
    });
    // - correspondences, if enabled
    if (this.settingsMgr.getXkosTabEnabled(this.project)) {
      this.tabsStruct.push({
        panel: DataPanel.correspondence,
        visible: true,
        label: this.translate.instant(TranslationUtils.dataPanelTranslationMap[DataPanel.correspondence]),
        customLabel: "",
      });
    }

    this.initTabsLabels();
  }

  initTabsLabels() {
    let currentLang = this.translate.currentLang;
    let panelLabelsMap: DataPanelLabelSetting = this.settingsMgr.getDataPanelsLabels(this.project);
    this.tabsStruct.forEach(t => {
      if (panelLabelsMap?.[currentLang]?.[t.panel] != null) {
        t.customLabel = panelLabelsMap[currentLang][t.panel];
      } else {
        t.customLabel = t.label;
      }
    });
  }

  private updateInternalStruct() {
    this.hiddenTabs = this.tabsStruct.filter(tab => !tab.visible).map(t => t.panel);
    //update preview staff
    this.previewShowTabSelector = this.tabsStruct.filter(t => t.visible).length > 1;
    //activate first visible tab
    this.previewActiveTab = this.tabsStruct.find(t => t.visible).panel;
  }

  toggleTab(tab: TabEntry) {
    //cannot toggle tab if it is the last one visible
    if ((tab.visible && this.tabsStruct.filter(t => t.visible).length == 1) || this.readonly) {
      return;
    }
    tab.visible = !tab.visible;
    this.onModelChange();
  }

  onModelChange() {
    this.updateInternalStruct();
    this.propagateChange(this.hiddenTabs);
  }

  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(hiddens: DataPanel[]) {
    if (hiddens) {
      for (let t of this.tabsStruct) {
        t.visible = !hiddens.includes(t.panel);
      }
      this.updateInternalStruct();
    }
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };


}

interface TabEntry {
  panel: DataPanel;
  visible: boolean;
  label?: string;
  customLabel?: string;
}