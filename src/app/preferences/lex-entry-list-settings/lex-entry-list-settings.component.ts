import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Project } from 'src/app/models/Project';
import { LexEntryVisualizationMode, LexicalEntryListPreference } from 'src/app/models/Properties';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { TranslationUtils } from 'src/app/utils/TranslationUtils';

@Component({
  selector: "lex-entry-list-settings",
  templateUrl: "./lex-entry-list-settings.component.html",
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => LexEntryListSettingsComponent), multi: true,
  }],
  standalone: false
})
export class LexEntryListSettingsComponent implements ControlValueAccessor {
  @Input() project: Project; //if not explicitly provided, get the current accessed project
  @Input() administration: boolean; //if true, tells that the component is used in an administration context, so allows further settings

  lexEntryPref: LexicalEntryListPreference;

  visualization: LexEntryVisualizationMode;
  visualizationModes: { value: LexEntryVisualizationMode, labelTranslationKey: string }[] = [
    { value: LexEntryVisualizationMode.indexBased, labelTranslationKey: TranslationUtils.visualizationModeTranslationMap[LexEntryVisualizationMode.indexBased] },
    { value: LexEntryVisualizationMode.searchBased, labelTranslationKey: TranslationUtils.visualizationModeTranslationMap[LexEntryVisualizationMode.searchBased] }
  ];
  allowVisualizationChange: boolean;

  safeToGoLimit: number;

  indexLength: number;
  lengthChoices: number[] = [1, 2];
  allowIndexLengthChange: boolean;

  constructor(private settingsMgr: SettingsManager) { }

  ngOnInit() {
    this.project = SVContext.getProject(this.project);
    this.allowVisualizationChange = this.settingsMgr.getAllowLexEntryListVisualizationChange(this.project);
    this.allowIndexLengthChange = this.settingsMgr.getAllowLexEntryListIndexLengthChange(this.project);
  }

  onModelChange() {
    let pref: LexicalEntryListPreference = {
      visualization: this.visualization,
      safeToGoLimit: this.safeToGoLimit,
      indexLength: this.indexLength,
    };
    this.propagateChange(pref);
  }

  toggleAllowVisualizationModeChange() {
    this.allowVisualizationChange = !this.allowVisualizationChange;
    this.settingsMgr.setAllowLexEntryListVisualizationChange(this.project, this.allowVisualizationChange).subscribe();
  }

  toggleAllowIndexChange() {
    this.allowIndexLengthChange = !this.allowIndexLengthChange;
    this.settingsMgr.setAllowLexEntryListIndexLengthChange(this.project, this.allowIndexLengthChange).subscribe();
  }

  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(pref: LexicalEntryListPreference) {
    if (pref) {
      this.visualization = pref.visualization;
      this.safeToGoLimit = pref.safeToGoLimit;
      this.indexLength = pref.indexLength;
    }
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };

}