import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Project } from 'src/app/models/Project';
import { InstanceListPreference, InstanceListVisualizationMode } from 'src/app/models/Properties';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { TranslationUtils } from 'src/app/utils/TranslationUtils';

@Component({
  selector: "instance-list-settings",
  templateUrl: "./instance-list-settings.component.html",
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => InstanceListSettingsComponent), multi: true,
  }],
  standalone: false
})
export class InstanceListSettingsComponent implements ControlValueAccessor {
  @Input() project: Project; //if not explicitly provided, get the current accessed project
  @Input() administration: boolean; //if true, tells that the component is used in an administration context, so allows further settings

  visualization: InstanceListVisualizationMode;
  visualizationModes: { value: InstanceListVisualizationMode, labelTranslationKey: string }[] = [
    { value: InstanceListVisualizationMode.standard, labelTranslationKey: TranslationUtils.visualizationModeTranslationMap[InstanceListVisualizationMode.standard] },
    { value: InstanceListVisualizationMode.searchBased, labelTranslationKey: TranslationUtils.visualizationModeTranslationMap[InstanceListVisualizationMode.searchBased] }
  ];
  allowVisualizationChange: boolean;

  safeToGoLimit: number;

  constructor(private settingsMgr: SettingsManager) { }

  ngOnInit() {
    this.project = SVContext.getProject(this.project);
    this.allowVisualizationChange = this.settingsMgr.getAllowInstanceListVisualizationChange(this.project);
  }

  onModelChange() {
    let pref: InstanceListPreference = {
      visualization: this.visualization,
      safeToGoLimit: this.safeToGoLimit,
    };
    this.propagateChange(pref);
  }

  toggleAllowVisualizationChange() {
    this.allowVisualizationChange = !this.allowVisualizationChange;
    this.settingsMgr.setAllowInstanceListVisualizationChange(this.project, this.allowVisualizationChange).subscribe();
  }


  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(pref: InstanceListPreference) {
    if (pref) {
      this.visualization = pref.visualization;
      this.safeToGoLimit = pref.safeToGoLimit;
    }
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };

}