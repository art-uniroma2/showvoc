import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { WidgetModule } from '../widget/widget.module';
import { ClassTreeSettingsComponent } from './class-tree-settings/class-tree-settings.component';
import { ClickableValueStyleEditorComponent } from './clickable-value-style/clickable-value-style-editor.component';
import { ConceptTreeSettingsComponent } from './concept-tree-settings/concept-tree-settings.component';
import { DataPanelLabelsEditorModalComponent } from './data-panels-settings/data-panels-labels-editor-modal.component';
import { DataPanelsSettingsComponent } from './data-panels-settings/data-panels-settings.component';
import { DatasetsTabsSettingsComponent } from './dataset-tabs-settings/dataset-tabs-settings.component';
import { DatasetsViewSettingsComponent } from './datasets-view-settings/datasets-view-settings.component';
import { InstanceListSettingsComponent } from './instance-list-settings/instance-list-settings.component';
import { LanguageValueFilterComponent } from './languages/language-value-filter.component';
import { LexEntryListSettingsComponent } from './lex-entry-list-settings/lex-entry-list-settings.component';
import { PredicateLabelSettingsModalComponent } from './predicate-label-settings/predicate-label-settings-modal.component';
import { PredicateLabelSettingsComponent } from './predicate-label-settings/predicate-label-settings.component';
import { SelectVocabulariesModalComponent } from './predicate-label-settings/select-vocabularies-modal.component';
import { ResViewSectionCustomizationModalComponent } from './res-view-section-customization/res-view-section-customization-modal.component';
import { ResViewTemplateEditorComponent } from './res-view-template-editor/res-view-template-editor.component';
import { SectionFilterComponent } from './section-filter/section-filter.component';


@NgModule({
    declarations: [
        DataPanelsSettingsComponent,
        ClassTreeSettingsComponent,
        ClickableValueStyleEditorComponent,
        ConceptTreeSettingsComponent,
        DataPanelLabelsEditorModalComponent,
        DatasetsTabsSettingsComponent,
        DatasetsViewSettingsComponent,
        InstanceListSettingsComponent,
        LanguageValueFilterComponent,
        LexEntryListSettingsComponent,
        PredicateLabelSettingsComponent,
        PredicateLabelSettingsModalComponent,
        ResViewSectionCustomizationModalComponent,
        ResViewTemplateEditorComponent,
        SectionFilterComponent,
        SelectVocabulariesModalComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        NgbDropdownModule,
        NgbTooltipModule,
        TranslateModule,
        WidgetModule,
    ],
    exports: [
        ClassTreeSettingsComponent,
        ClickableValueStyleEditorComponent,
        ConceptTreeSettingsComponent,
        DataPanelsSettingsComponent,
        DatasetsTabsSettingsComponent,
        DatasetsViewSettingsComponent,
        InstanceListSettingsComponent,
        LanguageValueFilterComponent,
        LexEntryListSettingsComponent,
        PredicateLabelSettingsModalComponent,
        ResViewTemplateEditorComponent,
        SectionFilterComponent
    ],
    providers: []
})
export class PreferencesModule { }