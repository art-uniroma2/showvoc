import { Component, ElementRef, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { Project } from 'src/app/models/Project';
import { ResViewSectionDisplayInfo, ResViewSectionInfoMapping, ResViewSectionsCustomization, RoleSectionsMap } from 'src/app/models/Properties';
import { RDFResourceRolesEnum } from 'src/app/models/Resources';
import { ResViewSection, ResViewUtils } from 'src/app/models/ResourceView';
import { TranslationUtils } from 'src/app/utils/TranslationUtils';
import { OntoLex, SKOS } from 'src/app/models/Vocabulary';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { UIUtils } from 'src/app/utils/UIUtils';

@Component({
  selector: 'res-view-section-customization-modal',
  templateUrl: './res-view-section-customization-modal.component.html',
  standalone: false
})
export class ResViewSectionCustomizationModalComponent {

  @Input() project: Project;

  sections: ResViewSection[];

  translateLangs: string[];

  langs: string[]; //langs for which exists a mapping
  additionalLangs: string[];

  langMappings: LangMappingsTab[] = [];
  activeTab: LangMappingsTab;

  activeTabLang: string;

  //filter
  roles: RDFResourceRolesEnum[]; //roles selectable in sections filter based on template
  defaultFilteringRole: RDFResourceRolesEnum; //role automatically selected in the filter (chosen according the project model)
  templates: RoleSectionsMap;

  constructor(public activeModal: NgbActiveModal,
    private basicModals: BasicModalsServices,
    private settingsMgr: SettingsManager,
    private translate: TranslateService,
    private elementRef: ElementRef) { }

  ngOnInit() {
    UIUtils.setFullSizeModal(this.elementRef);

    this.translateLangs = this.translate.getLangs();
    this.additionalLangs = this.translateLangs;

    this.sections = ResViewUtils.orderedResourceViewSections.slice();
    let customSections = this.settingsMgr.getResourceViewProjectSettings(this.project).customSections;
    if (customSections) {
      this.sections = this.sections.concat(Object.keys(customSections) as ResViewSection[]);
    }

    //init templates for sections filter
    this.templates = this.settingsMgr.getResourceViewProjectSettings(this.project).templates;

    //init roles for filtering by templates
    this.roles = Object.keys(this.templates) as RDFResourceRolesEnum[];

    //init default choice for filtering role, based on project model
    let model = this.project.getModelType();
    if (model == SKOS.uri) {
      this.defaultFilteringRole = RDFResourceRolesEnum.concept;
    } else if (model == OntoLex.uri) {
      this.defaultFilteringRole = RDFResourceRolesEnum.ontolexLexicalEntry;
    } else { //RDFS/OWL
      this.defaultFilteringRole = RDFResourceRolesEnum.cls;
    }

    //retrieve setting, if available restore the UI
    let setting: ResViewSectionsCustomization = this.settingsMgr.getResViewSectionsCustomization(this.project);
    if (setting) {
      for (let lang in setting) {
        this.addLangTab(lang);
        let m = this.langMappings.find(m => m.lang == lang);

        let sectionInfo: ResViewSectionInfoMapping = setting[lang];
        for (let section in sectionInfo) {
          let sc = m.sectionsCustomization.find(c => c.section == section);
          if (sc) {
            sc.label = sectionInfo[section].label;
            sc.description = sectionInfo[section].description;
          }
        }
        this.applyFilters();
      }
      //once restored tabs, activate the first one
      this.activateTabLang(this.langMappings[0].lang);
    }
  }

  applyFilters() {
    if (this.activeTab) {
      this.activeTab.sectionsCustomization.forEach(c => {
        if (!this.activeTab.filter.enabled) {
          c.visible = true;
        } else {
          c.visible = this.templates[this.activeTab.filter.role].includes(c.section);
        }
      });
    }
  }

  private updateLangs() {
    this.langs = this.langMappings.map(m => m.lang);
    this.additionalLangs = this.translateLangs.filter(l => !this.langs.includes(l));
  }

  /* =====
  TABS
  * ====== */

  activateTabLang(lang: string) {
    this.activeTabLang = lang;
    // this.activeMappingTab.labelMappings = this.langMappings.find(m => m.lang == lang).labelMappings;
    this.activeTab = this.langMappings.find(m => m.lang == lang);
  }

  closeTab(lang: string, event: Event) {
    this.basicModals.confirm({ key: "COMMONS.STATUS.WARNING" }, { key: "PREFERENCES.PRED_LABEL_MAPPING.DELETE_MAPPINGS_WARN", params: { language: lang } }, ModalType.warning).then(
      () => {
        event.preventDefault(); //prevent the refresh of the page
        //remove the tab to close
        this.langMappings = this.langMappings.filter(m => m.lang != lang);
        //update active tab
        if (this.langMappings.length > 0) {
          this.activateTabLang(this.langMappings[0].lang);
        } else {
          this.activeTabLang = null;
          this.activeTab = null;
        }
        this.updateLangs();
      },
      () => { }
    );
  }

  addLangTab(lang: string) {
    let sectionsCustomization: SectionCustomizationEntry[] = this.sections.map(s => {
      return {
        section: s,
        translationKey: TranslationUtils.getResViewSectionTranslationKey(s),
        label: null,
        description: null,
        visible: true
      };
    });
    this.langMappings.push({
      lang: lang,
      sectionsCustomization: sectionsCustomization,
      filter: {
        enabled: true,
        role: this.defaultFilteringRole
      }
    });
    this.updateLangs();
    this.activateTabLang(lang);
    this.applyFilters();
  }


  ok() {

    //convert internal structure to the setting
    let setting: ResViewSectionsCustomization = {};

    this.langMappings.forEach(langMap => {
      let mappings: ResViewSectionInfoMapping = {};
      langMap.sectionsCustomization.filter(c => {
        return c.label && c.label.trim() != "" || c.description && c.description.trim() != "";
      }).forEach(c => {
        let info: ResViewSectionDisplayInfo = {
          label: (c.label && c.label.trim() != "") ? c.label : null,
          description: (c.description && c.description.trim() != "") ? c.description : null,
        };
        mappings[c.section] = info;
      });
      setting[langMap.lang] = mappings;
    });
    //clean empty langs
    for (let lang in setting) {
      //empty map in lang? => delete it
      if (Object.keys(setting[lang]).length == 0) {
        delete setting[lang];
      }
    }

    this.settingsMgr.setResViewSectionsCustomization(this.project, setting).subscribe(
      () => {
        //if the change has been done on current project, emit onLangChange to force Data page to reinit the labels
        if (SVContext.getProject()?.getName() == this.project.getName()) {
          let event: LangChangeEvent = { lang: this.translate.currentLang, translations: this.translate.translations };
          this.translate.onLangChange.emit(event);
        }
        this.activeModal.close();
      }
    );
  }

  close() {
    this.activeModal.dismiss();
  }

}


interface SectionCustomizationEntry {
  section: ResViewSection;
  translationKey: string;
  label: string;
  description: string;
  visible: boolean; //support for filter
}

interface LangMappingsTab {
  lang: string;
  sectionsCustomization: SectionCustomizationEntry[];
  filter: {
    enabled: boolean;
    role: RDFResourceRolesEnum; //role on which to filter sections (based on the template)
  }
}