import { ChangeDetectorRef, Component, forwardRef } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ClickableValueStyle } from 'src/app/models/Properties';
import { AnnotatedValue, IRI, RDFResourceRolesEnum, ResAttribute } from 'src/app/models/Resources';
import { SKOS } from 'src/app/models/Vocabulary';


@Component({
  selector: "clickable-value-style-editor",
  templateUrl: "./clickable-value-style-editor.component.html",
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => ClickableValueStyleEditorComponent), multi: true,
  }],
  standalone: false
})
export class ClickableValueStyleEditorComponent implements ControlValueAccessor {

  style: ClickableValueStyle = new ClickableValueStyle();

  previewRes: AnnotatedValue<IRI>;
  private skosConcept: AnnotatedValue<IRI> = new AnnotatedValue<IRI>(SKOS.concept,
    {
      [ResAttribute.QNAME]: SKOS.prefix + ":" + SKOS.concept.getLocalName(),
      [ResAttribute.SHOW]: SKOS.prefix + ":" + SKOS.concept.getLocalName(),
      [ResAttribute.ROLE]: RDFResourceRolesEnum.cls,
    }
  );

  constructor(private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.previewRes = this.skosConcept;
  }


  changeStyleColor(color?: string) {
    this.style.color = color;
    this.onClickableStyleChanged();
  }

  onClickableStyleChanged() {
    this.previewRes = null;
    this.cdRef.detectChanges();
    this.previewRes = this.skosConcept;
    this.propagateChange(this.style);
  }

  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(obj: ClickableValueStyle) {
    if (obj) {
      this.style = obj;
    }
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };

}