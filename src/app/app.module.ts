import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { inject, NgModule, provideAppInitializer } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { OAuthModule } from "angular-oauth2-oidc";
import { AdministrationModule } from './administration/administration.module';
import { AlignmentsModule } from './alignments/alignments.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContributionModule } from './contribution/contribution.module';
import { CustomServicesModule } from './custom-services/custom-services.module';
import { CustomViewsModule } from './custom-views/custom-views.module';
import { CustomReuseStrategy } from './CustomReuseStrategy';
import { DatasetsModule } from './datasets/datasets.module';
import { GraphModule } from './graph/graph.module';
import { HomeComponent } from './home/home.component';
import { MetadataRegistryModule } from './metadata-registry/metadata-registry.module';
import { ModalsModule } from './modal-dialogs/modals.module';
import { ShowVocUrlParams } from './models/ShowVoc';
import { MultiverseModule } from './multiverse/multiverse.module';
import { NotFoundComponent } from './not-found.component';
import { PreferencesModule } from './preferences/preferences.module';
import { SearchModule } from './search/search.module';
import { AppConfigService } from "./services/app-config.service";
import { Oauth2Service } from "./services/oauth2.service";
import { STServicesModule } from './services/st-services.module';
import { TranslationModule } from './translation/translation.module';
import { UserModule } from './user/user.module';
import { DatatypeValidator } from './utils/DatatypeValidator';
import { DpopInterceptor } from "./utils/interceptors/dpop.interceptor";
import { QueryParamService } from './utils/QueryParamService';
import { SettingsManager } from './utils/SettingsManager';
import { SVContext } from "./utils/SVContext";
import { SVEventHandler } from './utils/SVEventHandler';
import { SVProperties } from './utils/SVProperties';
import { UserResolver } from './utils/UserResolver';
import { WidgetModule } from './widget/widget.module';

let HttpLoaderFactory = (http: HttpClient) => {
  return new TranslateHttpLoader(http, "./assets/l10n/");
};

let initAppParams = (queryParamService: QueryParamService) => {
  return () => new Promise<void>((resolve) => {
    const fullUrl = window.location.href;
    const currentParams = new URLSearchParams(fullUrl.split('?')[1] || '');
    let ctx_world = currentParams.get(ShowVocUrlParams.ctx_world);
    let hideNav = currentParams.get(ShowVocUrlParams.hideNav);
    let hideDatasetName = currentParams.get(ShowVocUrlParams.hideDatasetName);
    if (ctx_world) {
      queryParamService.setCtxWorld(ctx_world);
    }
    if (hideNav) {
      queryParamService.setHideTab(hideNav);
    }
    if (hideDatasetName) {
      queryParamService.setHideDatasetName(hideDatasetName);
    }
    resolve();
  });
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent
  ],
  imports: [
    AdministrationModule,
    AlignmentsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    ContributionModule,
    CustomServicesModule,
    CustomViewsModule,
    DatasetsModule,
    GraphModule,
    MetadataRegistryModule,
    MultiverseModule,
    ModalsModule,
    NgbModule,
    PreferencesModule,
    SearchModule,
    TranslationModule,
    STServicesModule,
    TranslateModule.forRoot({
      defaultLanguage: 'en',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    OAuthModule.forRoot(),
    UserModule,
    WidgetModule
  ],
  providers: [
    AppConfigService,
    Oauth2Service,
    provideAppInitializer(() => { // The functions contained here are executed before bootstrapping the Angular application, that is, before rendering the components.
      // Here it is used to ensure that the app and OAuth2 configuration is complete before the app is fully launched
      const appConfigService = inject(AppConfigService); // It's an Angular function that provides access to services within a context without the need to use the class constructor.
      const oauth2Service = inject(Oauth2Service); // It's an Angular function that provides access to services within a context without the need to use the class constructor.
      return appConfigService.loadConfig().then(isReady => {
        // It's called to load the system settings
        if (isReady && SVContext.getSystemSettings().authService === 'OAuth2') {
          // It's used to configure the OAuth2 system and start the process of authentication.
          return oauth2Service.configure().then(() => {
            // Go on
          }).catch(err => {
            console.error('Error in Oauth2 configuration:', err);
            // Warn the user there is an issue with loading of OAUTH2
            //const notificationService = inject(BasicModalServices);
            //notificationService.alert({ key: "STATUS.WARNING" }, "OAuth2 configuration failed.", ModalType.error);

            // DOES NOT interrupt the bootstrap of the application
            return Promise.resolve();
          });

        }
        return Promise.resolve(); // configuration ended without loading Oauth2 case in which the application starts in Default or SAMl mode
      });
    }),
    DatatypeValidator,
    SettingsManager,
    SVProperties,
    SVEventHandler,
    UserResolver,
    { provide: RouteReuseStrategy, useClass: CustomReuseStrategy },
    /** Uses the HashLocationStrategy instead of the default "HTML 5 pushState" PathLocationStrategy.
     * This solves the 404 error problem when reloading a page in a production server
     */
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: DpopInterceptor,
      multi: true,
    },
    provideAppInitializer(() => {
      const initializerFn = (initAppParams)(inject(QueryParamService));
      return initializerFn();
    })
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
