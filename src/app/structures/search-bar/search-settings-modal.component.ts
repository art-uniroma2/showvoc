import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SharedModalsServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { Project } from 'src/app/models/Project';
import { ClassIndividualPanelSearchMode, SearchMode, SearchSettings } from 'src/app/models/Properties';
import { RDFResourceRolesEnum } from 'src/app/models/Resources';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';

@Component({
  selector: 'search-settings-modal',
  templateUrl: './search-settings-modal.component.html',
  standalone: false
})
export class SearchSettingsModalComponent implements OnInit {

  @Input() roles: RDFResourceRolesEnum[];
  @Input() project: Project;

  private settings: SearchSettings;

  stringMatchModes: { show: string, value: SearchMode }[] = [
    { show: "Starts with", value: SearchMode.startsWith },
    { show: "Contains", value: SearchMode.contains },
    { show: "Ends with", value: SearchMode.endsWith },
    { show: "Exact", value: SearchMode.exact },
    { show: "Fuzzy", value: SearchMode.fuzzy }
  ];
  activeStringMatchMode: SearchMode;

  settingsForConceptPanel: boolean = false;
  settingsForClassInstancePanel: boolean = false;
  showAutocompletionSetting: boolean = false;

  //search mode use URI/LocalName
  useURI: boolean = true;
  useLocalName: boolean = true;
  useNotes: boolean = true;

  restrictLang: boolean = false;
  includeLocales: boolean = false;
  languages: string[];

  useAutocompletion: boolean = false;

  //concept search restriction
  restrictConceptSchemes: boolean = true;

  //class-instance panel search
  clsIndSearchMode: { show: string, value: ClassIndividualPanelSearchMode }[] = [
    { show: "Only classes", value: ClassIndividualPanelSearchMode.onlyClasses },
    { show: "Only instances", value: ClassIndividualPanelSearchMode.onlyInstances },
    { show: "Both classes and instances", value: ClassIndividualPanelSearchMode.all }
  ];
  activeClsIndSearchMode: ClassIndividualPanelSearchMode;

  constructor(public activeModal: NgbActiveModal, private sharedModals: SharedModalsServices, private settingsMgr: SettingsManager) { }

  ngOnInit() {
    this.settingsForConceptPanel = this.roles.length == 1 && this.roles[0] == RDFResourceRolesEnum.concept;
    this.settingsForClassInstancePanel = this.roles.indexOf(RDFResourceRolesEnum.cls) != -1 && this.roles.indexOf(RDFResourceRolesEnum.individual) != -1;
    //autocompletion is supported only in the several data panels, so show its setting only if role includes one related to these panels
    this.showAutocompletionSetting = this.roles.includes(RDFResourceRolesEnum.cls) ||
      this.roles.includes(RDFResourceRolesEnum.concept) ||
      this.roles.includes(RDFResourceRolesEnum.conceptScheme) ||
      this.roles.includes(RDFResourceRolesEnum.individual) ||
      this.roles.includes(RDFResourceRolesEnum.limeLexicon) ||
      this.roles.includes(RDFResourceRolesEnum.ontolexLexicalEntry) ||
      this.roles.includes(RDFResourceRolesEnum.property) ||
      this.roles.includes(RDFResourceRolesEnum.skosCollection);

    this.settings = this.settingsMgr.getSearchSettings(SVContext.getProject(this.project));
    this.activeStringMatchMode = this.settings.stringMatchMode;
    this.useURI = this.settings.useURI;
    this.useLocalName = this.settings.useLocalName;
    this.useNotes = this.settings.useNotes;
    this.restrictLang = this.settings.restrictLang;
    this.includeLocales = this.settings.includeLocales;
    this.languages = this.settings.languages;
    this.useAutocompletion = this.settings.useAutocompletion;
    this.restrictConceptSchemes = this.settings.restrictActiveScheme;
    this.activeClsIndSearchMode = this.settings.classIndividualSearchMode;
  }

  selectRestrictionLanguages() {
    this.sharedModals.selectLanguages({ key: "COMMONS.ACTIONS.SELECT_LANGUAGES" }, this.languages, false, true, this.project).then(
      (langs: string[]) => {
        this.languages = langs;
        this.updateSettings();
      },
      () => { }
    );
  }

  updateSettings() {
    let newSettings: SearchSettings = {
      stringMatchMode: this.activeStringMatchMode,
      useURI: this.useURI,
      useLocalName: this.useLocalName,
      useNotes: this.useNotes,
      restrictLang: this.restrictLang,
      includeLocales: this.includeLocales,
      languages: this.languages,
      useAutocompletion: this.useAutocompletion,
      restrictActiveScheme: this.restrictConceptSchemes,
      classIndividualSearchMode: this.activeClsIndSearchMode
    };
    this.settingsMgr.setSearchSettings(SVContext.getProject(this.project), newSettings).subscribe();
  }

  ok() {
    this.activeModal.close();
  }

  close() {
    this.activeModal.dismiss();
  }

}
