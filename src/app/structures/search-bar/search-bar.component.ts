import { Component, EventEmitter, Input, Output } from "@angular/core";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject, Subscription } from 'rxjs';
import { debounceTime, finalize } from 'rxjs/operators';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { Project } from 'src/app/models/Project';
import { ClassIndividualPanelSearchMode, SearchMode, SearchSettings, SearchUtils } from 'src/app/models/Properties';
import { AnnotatedValue, IRI, RDFResourceRolesEnum, Resource } from 'src/app/models/Resources';
import { SearchServices } from 'src/app/services/search.service';
import { STRequestOptions } from 'src/app/utils/HttpManager';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { SVEventHandler } from 'src/app/utils/SVEventHandler';
import { TreeListContext } from 'src/app/utils/UIUtils';
import { AdvancedSearchModalComponent } from './advanced-search-modal.component';
import { CustomSearchModalComponent } from './custom-search-modal.component';
import { LoadCustomSearchModalComponent } from './load-custom-search-modal.component';
import { SearchSettingsModalComponent } from './search-settings-modal.component';

@Component({
  selector: "search-bar",
  templateUrl: "./search-bar.component.html",
  styleUrls: ['./search-bar.component.css'],
  standalone: false
})
export class SearchBarComponent {

  @Input() roles: RDFResourceRolesEnum[]; //tells the role of the panel where the search bar is placed (usefull for customizing the settings)
  @Input() disabled: boolean = false;
  @Input() schemes: IRI[]; //if search-bar is in the conceptTreePanel
  @Input() lexicon: IRI; //if search-bar is in the lexicalEntryListPanel
  @Input() cls: Resource; //if search-bar is in the instanceListPanel
  @Input() context: TreeListContext;
  @Input() project: Project;
  @Output() searchResults: EventEmitter<AnnotatedValue<Resource>[]> = new EventEmitter<AnnotatedValue<Resource>[]>();
  @Output() advancedSearchResult: EventEmitter<AnnotatedValue<Resource>> = new EventEmitter<AnnotatedValue<Resource>>();

  loading: boolean = false;

  //search mode startsWith/contains/endsWith
  stringMatchModesMap = SearchUtils.stringMatchModesMap;

  searchSettings: SearchSettings;

  searchStr: string;
  private lastSearch: string;

  private eventSubscriptions: Subscription[] = [];

  constructor(private searchService: SearchServices, private settingsMgr: SettingsManager, private eventHandler: SVEventHandler,
    private basicModals: BasicModalsServices, private modalService: NgbModal) {

    this.eventSubscriptions.push(eventHandler.searchPrefsUpdatedEvent.subscribe(
      () => this.updateSearchSettings()));

    this.eventSubscriptions.push(
      this.inputChangeSubject.pipe(debounceTime(500)).subscribe(() => {
        this.getAutocompleterSuggestions();
      })
    );
  }

  ngOnInit() {
    this.searchSettings = this.settingsMgr.getSearchSettings(SVContext.getProject(this.project));
  }

  ngOnDestroy() {
    this.eventHandler.unsubscribeAll(this.eventSubscriptions);
  }

  doSearch() {
    if (this.searchStr != undefined && this.searchStr.trim() != "") {
      this.lastSearch = this.searchStr;
      this.doSearchImpl();
    } else {
      this.basicModals.alert({ key: "COMMONS.ACTIONS.SEARCH" }, { key: "MESSAGES.INVALID_SEARCH_STRING" }, ModalType.warning);
    }
  }

  public doSearchImpl() {
    if (this.lastSearch == null) return; //prevent error in case search is forced from a parent panel (e.g. concept tree in search based mode)

    let searchLangs: string[];
    let includeLocales: boolean;
    if (this.searchSettings.restrictLang) {
      searchLangs = this.searchSettings.languages;
      includeLocales = this.searchSettings.includeLocales;
    }

    let searchingScheme: IRI[] = [];
    if (this.roles.length == 1 && this.roles[0] == RDFResourceRolesEnum.concept && this.searchSettings.restrictActiveScheme) {
      searchingScheme = this.schemes;
    }

    let searchFn: Observable<AnnotatedValue<Resource>[]>;
    let reqOpts: STRequestOptions = STRequestOptions.getRequestOptions(SVContext.getProject(this.project));
    if (this.roles.length == 1 && this.roles[0] == RDFResourceRolesEnum.ontolexLexicalEntry) { //bar in lexical entry panel
      searchFn = this.searchService.searchLexicalEntry(this.lastSearch, this.searchSettings.useLocalName, this.searchSettings.useURI,
        this.searchSettings.useNotes, this.searchSettings.stringMatchMode, [this.lexicon], searchLangs, includeLocales, reqOpts);
    } else if (this.roles.length == 1 && this.roles[0] == RDFResourceRolesEnum.individual) { //bar in instances panel
      searchFn = this.searchService.searchInstancesOfClass(this.cls, this.lastSearch, this.searchSettings.useLocalName, this.searchSettings.useURI,
        this.searchSettings.useNotes, this.searchSettings.stringMatchMode, searchLangs, includeLocales, reqOpts);
    } else if (this.roles.length == 2 && this.roles.indexOf(RDFResourceRolesEnum.cls) != -1 && this.roles.indexOf(RDFResourceRolesEnum.individual) != -1) { //panel in cls-instance panel
      let searchRoles: RDFResourceRolesEnum[] = [RDFResourceRolesEnum.individual, RDFResourceRolesEnum.cls]; //by default search both
      if (this.searchSettings.classIndividualSearchMode == ClassIndividualPanelSearchMode.onlyInstances) {
        searchRoles = [RDFResourceRolesEnum.individual];
      } else if (this.searchSettings.classIndividualSearchMode == ClassIndividualPanelSearchMode.onlyClasses) {
        searchRoles = [RDFResourceRolesEnum.cls];
      }
      searchFn = this.searchService.searchResource(this.lastSearch, searchRoles, this.searchSettings.useLocalName,
        this.searchSettings.useURI, this.searchSettings.useNotes, this.searchSettings.stringMatchMode, searchLangs,
        includeLocales, null, reqOpts);
    } else { //concept
      searchFn = this.searchService.searchResource(this.lastSearch, this.roles, this.searchSettings.useLocalName,
        this.searchSettings.useURI, this.searchSettings.useNotes, this.searchSettings.stringMatchMode, searchLangs,
        includeLocales, searchingScheme, reqOpts);
    }

    this.loading = true;
    searchFn.pipe(
      finalize(() => { this.loading = false; })
    ).subscribe(
      searchResult => {
        if (searchResult.length == 0) {
          this.basicModals.alert({ key: "COMMONS.ACTIONS.SEARCH" }, { key: "MESSAGES.NO_RESULTS_FOUND_FOR", params: { searchedString: this.lastSearch } }, ModalType.warning);
        } else {
          this.searchResults.emit(searchResult);
        }
      }
    );
  }

  advancedSearch() {
    const modalRef: NgbModalRef = this.modalService.open(AdvancedSearchModalComponent, new ModalOptions('xl'));
    modalRef.result.then(
      (resource: AnnotatedValue<Resource>) => {
        this.advancedSearchResult.emit(resource);
      },
      () => { }
    );
  }

  customSearch() {
    const modalRef: NgbModalRef = this.modalService.open(LoadCustomSearchModalComponent, new ModalOptions());
    modalRef.result.then(
      customSearchRef => {
        const modalRef: NgbModalRef = this.modalService.open(CustomSearchModalComponent, new ModalOptions());
        modalRef.componentInstance.searchParameterizationReference = customSearchRef;
        modalRef.result.then(
          (resource: AnnotatedValue<Resource>) => {
            //exploit the same event (and related handler) of advanced search
            this.advancedSearchResult.emit(resource);
          },
          () => { }
        );
      },
      () => { }
    );
  }

  settings() {
    const modalRef: NgbModalRef = this.modalService.open(SearchSettingsModalComponent, new ModalOptions());
    modalRef.componentInstance.roles = this.roles;
    modalRef.componentInstance.project = this.project;
    return modalRef.result.then(
      () => {
        this.searchSettings = this.settingsMgr.getSearchSettings(SVContext.getProject(this.project));
      },
      () => { }
    );
  }

  updateSearchMode(mode: string) {
    this.searchSettings.stringMatchMode = mode as SearchMode;
    this.settingsMgr.setSearchSettings(SVContext.getProject(this.project), this.searchSettings).subscribe();
  }

  /**
   * When the search settings is updated, updates the setting of the bar and the settings for the autocompleter
   */
  private updateSearchSettings() {
    this.searchSettings = this.settingsMgr.getSearchSettings(SVContext.getProject(this.project));
  }



  /**
   * AUTOCOMPLETER STUFF
   */
  private inputChangeSubject: Subject<void> = new Subject<void>();

  completerData: string[];
  completerLimit: number = 30;
  completerLimitExceeded: boolean = false;
  isCompleterLoading: boolean;

  private lastCompleterKey: string;

  onAutocompleteChange() {
    this.inputChangeSubject.next();
  }

  getAutocompleterSuggestions() {
    if (this.searchStr.trim() == "" || this.searchStr.length < 3) {
      this.completerData = [];
      return;
    }
    /* ng-autocomplete component emit a inputChanged event even when user press any other key (e.g. move the caret) and the string doesn't change.
    I need to perform this check in order to prevent search repetition */
    if (this.lastCompleterKey == this.searchStr) {
      return;
    }
    this.lastCompleterKey = this.searchStr;

    this.isCompleterLoading = true;
    let langsParam: string[];
    let includeLocales: boolean;
    if (this.searchSettings.restrictLang) {
      langsParam = this.searchSettings.languages;
      includeLocales = this.searchSettings.includeLocales;
    }
    let schemesParam: IRI[];
    if (this.searchSettings.restrictActiveScheme) {
      schemesParam = this.schemes;
    }
    let clsParam: Resource;
    if (this.roles.includes(RDFResourceRolesEnum.individual)) {
      clsParam = this.cls;
    }
    this.searchService.searchStringList(this.searchStr, this.roles, this.searchSettings.useLocalName, this.searchSettings.stringMatchMode,
      langsParam, includeLocales, schemesParam, clsParam, STRequestOptions.getRequestOptions(this.project)).subscribe(
        (results: string[]) => {
          results.sort();
          this.completerLimitExceeded = results.length > this.completerLimit;
          if (this.completerLimitExceeded) {
            this.completerData = results.slice(0, this.completerLimit);
          } else {
            this.completerData = results;
          }
          this.isCompleterLoading = false;
        }
      );
  }

}