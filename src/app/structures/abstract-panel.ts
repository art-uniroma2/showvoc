import { Directive, EventEmitter, Input, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { BasicModalsServices } from '../modal-dialogs/basic-modals/basic-modals.service';
import { SharedModalsServices } from '../modal-dialogs/shared-modals/shared-modals.service';
import { Project } from '../models/Project';
import { AnnotatedValue, IRI, RDFResourceRolesEnum, Resource } from '../models/Resources';
import { LocalStorageManager } from '../utils/LocalStorageManager';
import { SortAttribute } from '../utils/ResourceUtils';
import { SettingsManager } from '../utils/SettingsManager';
import { SVEventHandler } from '../utils/SVEventHandler';
import { TreeListContext } from '../utils/UIUtils';
import { NodeSelectEvent } from './abstract-node';
import { SVContext } from '../utils/SVContext';

@Directive()
export abstract class AbstractPanel<T extends Resource> {

  /**
   * VIEWCHILD, INPUTS / OUTPUTS
   */

  @Input() context: TreeListContext;
  @Input() hideSearch: boolean = false; //if true hide the search bar
  @Input() project: Project; //useful to make the panel (the underlying tree or list) work with another project, different from the open one

  @Output() nodeSelected: EventEmitter<NodeSelectEvent> = new EventEmitter<NodeSelectEvent>();
  @Output() advancedSearchResult: EventEmitter<AnnotatedValue<Resource>> = new EventEmitter<AnnotatedValue<Resource>>();

  /**
   * ATTRIBUTES
   */

  abstract panelRole: RDFResourceRolesEnum; //declare the type of resources in the panel

  rendering: boolean = true; //if true the nodes in the tree should be rendered with the show, with the qname otherwise
  sortBy: SortAttribute = SortAttribute.value;
  showDeprecated: boolean = true;
  eventSubscriptions: Subscription[] = [];
  selectedNode: AnnotatedValue<T> = null;

  /**
   * CONSTRUCTOR
   */
  protected basicModals: BasicModalsServices;
  protected sharedModals: SharedModalsServices;
  protected eventHandler: SVEventHandler;
  protected settingsMgr: SettingsManager;
  constructor(basicModals: BasicModalsServices, sharedModals: SharedModalsServices, eventHandler: SVEventHandler, settingsMgr: SettingsManager) {
    this.basicModals = basicModals;
    this.sharedModals = sharedModals;
    this.eventHandler = eventHandler;
    this.settingsMgr = settingsMgr;

    this.eventSubscriptions.push(eventHandler.showDeprecatedChangedEvent.subscribe(
      (showDeprecated: boolean) => { this.showDeprecated = showDeprecated; }));
  }

  /**
   * METHODS
   */

  ngOnInit() {
    this.showDeprecated = LocalStorageManager.getItem(LocalStorageManager.SHOW_DEPRECATED) != false;
    this.rendering = this.settingsMgr.getStructureRendering(this.panelRole, SVContext.getProject(this.project));

    let sortByStored = LocalStorageManager.getItem(this.getItemKey(LocalStorageManager.STRUCTURE_SORT));
    if (sortByStored in SortAttribute) {
      this.sortBy = sortByStored as SortAttribute;
    }
  }

  ngOnDestroy() {
    this.eventHandler.unsubscribeAll(this.eventSubscriptions);
  }

  switchRendering() {
    this.rendering = !this.rendering;
    this.settingsMgr.setStructureRendering(this.panelRole, SVContext.getProject(this.project), this.rendering).subscribe();
  }

  sortByRendering() {
    this.sortBy = SortAttribute.show;
    LocalStorageManager.setItem(this.getItemKey(LocalStorageManager.STRUCTURE_SORT), this.sortBy);
  }
  sortByUri() {
    this.sortBy = SortAttribute.value;
    LocalStorageManager.setItem(this.getItemKey(LocalStorageManager.STRUCTURE_SORT), this.sortBy);
  }

  abstract handleSearchResults(results: AnnotatedValue<Resource>[]): void;

  abstract openAt(node: AnnotatedValue<IRI>): void;

  handleAdvSearchResult(resource: AnnotatedValue<Resource>) {
    this.advancedSearchResult.emit(resource);
  }

  //actions
  abstract refresh(): void;

  onNodeSelected(event?: NodeSelectEvent) {
    this.selectedNode = event?.value as AnnotatedValue<T>;
    this.nodeSelected.emit(event);
  }

  private getItemKey(item: string) {
    return item + "#" + this.panelRole;
  }

}