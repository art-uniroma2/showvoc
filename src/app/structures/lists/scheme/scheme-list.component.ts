import { ChangeDetectorRef, Component } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { AnnotatedValue, IRI, RDFResourceRolesEnum } from 'src/app/models/Resources';
import { SkosServices } from 'src/app/services/skos.service';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { SVEventHandler } from 'src/app/utils/SVEventHandler';
import { AbstractList } from '../abstract-list';

@Component({
    selector: 'scheme-list',
    templateUrl: './scheme-list.component.html',
    host: { class: "structureComponent" },
    standalone: false
})
export class SchemeListComponent extends AbstractList<IRI> {

    structRole: RDFResourceRolesEnum = RDFResourceRolesEnum.conceptScheme;

    constructor(private skosService: SkosServices, private settingsMgr: SettingsManager, eventHandler: SVEventHandler, changeDetectorRef: ChangeDetectorRef) {
        super(eventHandler, changeDetectorRef);
        //handler when active schemes is changed programmatically when a searched concept belong to a non active scheme
        this.eventSubscriptions.push(eventHandler.schemeChangedEvent.subscribe(
            (schemes: IRI[]) => {
                this.nodes.forEach((s: AnnotatedValue<IRI>) => { s['checked'] = schemes.some(sc => sc.equals(s.getValue())); });
            }
        ));
    }

    initImpl() {
        this.loading = true;
        this.skosService.getAllSchemes().pipe(
            finalize(() => { this.loading = false; })
        ).subscribe(
            schemes => {
                let activeSchemes: IRI[] = this.settingsMgr.getActiveSchemes(SVContext.getProjectCtx().getProject());
                schemes.forEach(s => {
                    if (activeSchemes.some(as => as.equals(s.getValue()))) {
                        s['checked'] = true;
                    }
                });
                this.nodes = schemes;
                this.sortNodes();

                if (this.pendingSearchRes) {
                    this.openListAt(this.pendingSearchRes);
                }
            }
        );
    }

    updateActiveSchemesPref() {
        this.settingsMgr.setActiveSchemes(SVContext.getProjectCtx().getProject(), this.collectCheckedSchemes()).subscribe();
    }

    collectCheckedSchemes(): IRI[] {
        //collect all the active scheme
        let activeSchemes: IRI[] = [];
        this.nodes.forEach(n => {
            if (n['checked']) {
                activeSchemes.push(n.getValue());
            }
        });
        return activeSchemes;
    }

    public activateAllScheme() {
        this.nodes.forEach(n => { n['checked'] = true; });
        this.updateActiveSchemesPref();
    }

    public deactivateAllScheme() {
        this.nodes.forEach(n => { n['checked'] = false; });
        this.updateActiveSchemesPref();
    }

}
