import { Component, ViewChild } from "@angular/core";
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { SharedModalsServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { Scope } from 'src/app/models/Plugins';
import { AnnotatedValue, IRI, RDFResourceRolesEnum, Resource } from 'src/app/models/Resources';
import { SettingsServices } from 'src/app/services/settings.service';
import { AuthorizationEvaluator } from "src/app/utils/AuthorizationEvaluator";
import { ResourceUtils, SortAttribute } from 'src/app/utils/ResourceUtils';
import { SettingsManager, SettingsManagerID } from 'src/app/utils/SettingsManager';
import { SVEventHandler } from 'src/app/utils/SVEventHandler';
import { ToastService } from 'src/app/widget/toast/toast-service';
import { AbstractListPanel } from '../abstract-list-panel';
import { SchemeListComponent } from './scheme-list.component';

@Component({
    selector: "scheme-list-panel",
    templateUrl: "./scheme-list-panel.component.html",
    host: { class: "vbox" },
    standalone: false
})
export class SchemeListPanelComponent extends AbstractListPanel<IRI> {
    @ViewChild(SchemeListComponent) viewChildList: SchemeListComponent;

    panelRole: RDFResourceRolesEnum = RDFResourceRolesEnum.conceptScheme;

    isSetDefaultSchemeAuthorized: boolean = false;

    constructor(basicModals: BasicModalsServices, sharedModals: SharedModalsServices, eventHandler: SVEventHandler, settingsMgr: SettingsManager, 
        private settingsService: SettingsServices, private toastService: ToastService) {
        super(basicModals, sharedModals, eventHandler, settingsMgr);
    }

    ngOnInit() {
        super.ngOnInit();
        this.isSetDefaultSchemeAuthorized = AuthorizationEvaluator.isDefaultSettingsActionAuthorized(Scope.PROJECT_USER, Scope.PROJECT, "U");
    }

    handleSearchResults(results: AnnotatedValue<Resource>[]) {
        if (results.length == 1) {
            this.openAt(results[0].asAnnotatedIRI());
        } else { //multiple results, ask the user which one select
            ResourceUtils.sortResources(results, this.rendering ? SortAttribute.show : SortAttribute.value);
            this.sharedModals.selectResource({ key: "SEARCH.SEARCH_RESULTS" }, { key: "MESSAGES.X_SEARCH_RESOURCES_FOUND", params: { results: results.length } }, results as AnnotatedValue<IRI>[], this.rendering).then(
                (selectedResources: AnnotatedValue<IRI>[]) => {
                    this.openAt(selectedResources[0]);
                },
                () => {}
            );
        }
    }

    public openAt(node: AnnotatedValue<IRI>) {
        this.viewChildList.openListAt(node);
    }

    activateAllScheme() {
        this.viewChildList.activateAllScheme();
    }
    deactivateAllScheme() {
        this.viewChildList.deactivateAllScheme();
    }

    setAsProjectDefault() {
        let activeSchemes = this.viewChildList.collectCheckedSchemes().map(s => s.toNT());
        this.settingsService.storeSettingDefault(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.PROJECT_USER, Scope.PROJECT, SettingsManager.PROP_NAME.activeSchemes, activeSchemes).subscribe(
            () => {
                this.toastService.show({ key: "COMMONS.STATUS.OPERATION_DONE"}, { key: "MESSAGES.DEFAULT_UPDATED" }, { toastClass: "bg-success" });
            }
        );
    }

    refresh() {
        this.viewChildList.init();
    }

}