import { Component, ViewChild } from "@angular/core";
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { SharedModalsServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { AnnotatedValue, IRI, RDFResourceRolesEnum, Resource } from 'src/app/models/Resources';
import { ResourceUtils, SortAttribute } from 'src/app/utils/ResourceUtils';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVEventHandler } from 'src/app/utils/SVEventHandler';
import { AbstractListPanel } from '../abstract-list-panel';
import { LexiconListComponent } from './lexicon-list.component';

@Component({
  selector: "lexicon-list-panel",
  templateUrl: "./lexicon-list-panel.component.html",
  host: { class: "vbox" },
  standalone: false
})
export class LexiconListPanelComponent extends AbstractListPanel<IRI> {
  @ViewChild(LexiconListComponent) viewChildList: LexiconListComponent;

  panelRole: RDFResourceRolesEnum = RDFResourceRolesEnum.limeLexicon;

  constructor(basicModals: BasicModalsServices, sharedModals: SharedModalsServices, eventHandler: SVEventHandler, settingsMgr: SettingsManager) {
    super(basicModals, sharedModals, eventHandler, settingsMgr);
  }

  handleSearchResults(results: AnnotatedValue<Resource>[]) {
    if (results.length == 1) {
      this.openAt(results[0].asAnnotatedIRI());
    } else { //multiple results, ask the user which one select
      ResourceUtils.sortResources(results, this.rendering ? SortAttribute.show : SortAttribute.value);
      this.sharedModals.selectResource({ key: "SEARCH.SEARCH_RESULTS" }, { key: "MESSAGES.X_SEARCH_RESOURCES_FOUND", params: { results: results.length } }, results as AnnotatedValue<IRI>[], this.rendering).then(
        (selectedResources: AnnotatedValue<IRI>[]) => {
          this.openAt(selectedResources[0]);
        },
        () => { }
      );
    }
  }

  public openAt(node: AnnotatedValue<IRI>) {
    this.viewChildList.openListAt(node);
  }

  refresh() {
    this.viewChildList.init();
  }

}