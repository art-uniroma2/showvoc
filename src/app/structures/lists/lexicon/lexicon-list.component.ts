import { ChangeDetectorRef, Component } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { AnnotatedValue, IRI, RDFResourceRolesEnum } from 'src/app/models/Resources';
import { OntoLexLemonServices } from 'src/app/services/ontolex-lemon.service';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { SVEventHandler } from 'src/app/utils/SVEventHandler';
import { AbstractList } from '../abstract-list';

@Component({
  selector: 'lexicon-list',
  templateUrl: './lexicon-list.component.html',
  host: { class: "structureComponent" },
  standalone: false
})
export class LexiconListComponent extends AbstractList<IRI> {

  structRole: RDFResourceRolesEnum = RDFResourceRolesEnum.limeLexicon;

  activeLexicon: AnnotatedValue<IRI>;

  constructor(private ontolexService: OntoLexLemonServices, private settingsMgr: SettingsManager, eventHandler: SVEventHandler, changeDetectorRef: ChangeDetectorRef) {
    super(eventHandler, changeDetectorRef);
    //handler when active lexicon is changed programmatically when a searched entry belong to a non active lexicon
    this.eventSubscriptions.push(eventHandler.lexiconChangedEvent.subscribe(
      (lexicon: IRI) => {
        if (lexicon != null) {
          this.activeLexicon = this.nodes.find(n => n.getValue().equals(lexicon));
        } else {
          this.activeLexicon = null;
        }
      })
    );
  }

  initImpl() {
    this.loading = true;
    this.ontolexService.getLexicons().pipe(
      finalize(() => { this.loading = false; })
    ).subscribe(lexicons => {
      let activeLexicon: IRI = this.settingsMgr.getActiveLexicon(SVContext.getProjectCtx().getProject());
      for (let l of lexicons) {
        if (activeLexicon != null && activeLexicon.equals(l.getValue())) {
          this.activeLexicon = l;
          break;
        }
      }
      this.nodes = lexicons;
      this.sortNodes();

      if (this.pendingSearchRes) {
        this.openListAt(this.pendingSearchRes);
      }
    });
  }

  toggleLexicon(lexicon: AnnotatedValue<IRI>) {
    if (this.activeLexicon == lexicon) {
      this.activeLexicon = null;
    } else {
      this.activeLexicon = lexicon;
    }
    let activeLexiconIRI: IRI = this.activeLexicon != null ? this.activeLexicon.getValue() : null;
    this.settingsMgr.setActiveLexicon(SVContext.getProject(this.project), activeLexiconIRI).subscribe();
  }

}
