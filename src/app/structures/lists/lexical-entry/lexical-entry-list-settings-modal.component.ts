import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LexicalEntryListPreference } from 'src/app/models/Properties';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';

@Component({
  selector: 'lexical-entry-list-settings-modal',
  templateUrl: './lexical-entry-list-settings-modal.component.html',
  standalone: false
})
export class LexicalEntryListSettingsModalComponent implements OnInit {

  lexEntryListPref: LexicalEntryListPreference;

  constructor(public activeModal: NgbActiveModal, private settingsMgr: SettingsManager) { }

  ngOnInit() {
    let lexEntryPref: LexicalEntryListPreference = this.settingsMgr.getLexEntryListSettings(SVContext.getProjectCtx().getProject());
    this.lexEntryListPref = JSON.parse(JSON.stringify(lexEntryPref));
  }

  ok() {
    this.settingsMgr.setLexEntryListSettings(SVContext.getProjectCtx().getProject(), this.lexEntryListPref).subscribe();
    this.activeModal.close();
  }

  close() {
    this.activeModal.dismiss();
  }

}
