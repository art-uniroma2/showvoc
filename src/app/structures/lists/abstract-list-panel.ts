import { Directive } from '@angular/core';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { SharedModalsServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { Resource } from 'src/app/models/Resources';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVEventHandler } from 'src/app/utils/SVEventHandler';
import { AbstractPanel } from '../abstract-panel';

@Directive()
export abstract class AbstractListPanel<T extends Resource> extends AbstractPanel<T> {

    /**
     * VIEWCHILD, INPUTS / OUTPUTS
     */

    /**
     * ATTRIBUTES
     */

    /**
     * CONSTRUCTOR
     */
    constructor(basicModals: BasicModalsServices, sharedModals: SharedModalsServices, eventHandler: SVEventHandler, settingsMgr: SettingsManager) {
        super(basicModals, sharedModals, eventHandler, settingsMgr);
    }

    /**
     * METHODS
     */

}