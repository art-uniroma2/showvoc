import { Component, Input, ViewChild } from "@angular/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalOptions } from "src/app/modal-dialogs/Modals";
import { SharedModalsServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { InstanceListVisualizationMode } from "src/app/models/Properties";
import { AnnotatedValue, RDFResourceRolesEnum, Resource } from 'src/app/models/Resources';
import { ResourceUtils, SortAttribute } from 'src/app/utils/ResourceUtils';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { SVEventHandler } from 'src/app/utils/SVEventHandler';
import { SearchBarComponent } from "../../search-bar/search-bar.component";
import { AbstractListPanel } from '../abstract-list-panel';
import { InstanceListSettingsModalComponent } from "./instance-list-settings-modal.component";
import { InstanceListComponent } from './instance-list.component';

@Component({
  selector: "instance-list-panel",
  templateUrl: "./instance-list-panel.component.html",
  host: { class: "vbox" },
  standalone: false
})
export class InstanceListPanelComponent extends AbstractListPanel<Resource> {
  @Input() cls: AnnotatedValue<Resource>; //class of the instances

  @ViewChild(InstanceListComponent) viewChildList: InstanceListComponent;
  @ViewChild(SearchBarComponent) searchBar: SearchBarComponent;

  panelRole: RDFResourceRolesEnum = RDFResourceRolesEnum.individual;
  rendering: boolean = false; //override the value in AbstractPanel
  sortBy: SortAttribute = SortAttribute.value; //override the value in AbstractPanel

  visualizationMode: InstanceListVisualizationMode;

  constructor(basicModals: BasicModalsServices, sharedModals: SharedModalsServices, eventHandler: SVEventHandler, settingsMgr: SettingsManager, private modalService: NgbModal) {
    super(basicModals, sharedModals, eventHandler, settingsMgr);
  }

  ngOnInit() {
    super.ngOnInit();
    this.visualizationMode = this.settingsMgr.getInstanceListSettings(SVContext.getProject(this.project)).visualization;
  }

  handleSearchResults(results: AnnotatedValue<Resource>[]) {
    this.visualizationMode = this.settingsMgr.getInstanceListSettings(SVContext.getProject(this.project)).visualization;
    if (this.visualizationMode == InstanceListVisualizationMode.standard) {
      if (results.length == 1) {
        this.openAt(results[0]);
      } else { //multiple results, ask the user which one select
        ResourceUtils.sortResources(results, this.rendering ? SortAttribute.show : SortAttribute.value);
        this.sharedModals.selectResource({ key: "SEARCH.SEARCH_RESULTS" }, { key: "MESSAGES.X_SEARCH_RESOURCES_FOUND", params: { results: results.length } }, results, this.rendering).then(
          (selectedResources: AnnotatedValue<Resource>[]) => {
            this.openAt(selectedResources[0]);
          },
          () => { }
        );
      }
    } else { //search based
      ResourceUtils.sortResources(results, this.rendering ? SortAttribute.show : SortAttribute.value);
      this.viewChildList.forceList(results);
    }
  }

  settings() {
    const modalRef: NgbModalRef = this.modalService.open(InstanceListSettingsModalComponent, new ModalOptions());
    return modalRef.result.then(
      () => {
        this.visualizationMode = this.settingsMgr.getInstanceListSettings(SVContext.getProject(this.project)).visualization;
        if (this.visualizationMode == InstanceListVisualizationMode.searchBased) {
          this.viewChildList.forceList([]);
        }
        this.refresh();
      },
      () => { }
    );
  }

  refresh() {
    this.visualizationMode = this.settingsMgr.getInstanceListSettings(SVContext.getProject(this.project)).visualization;
    //reinit the list
    this.viewChildList.init();
    if (this.visualizationMode == InstanceListVisualizationMode.searchBased) {
      //in search based visualization repeat the search
      this.searchBar.doSearchImpl();
    }
  }

  openAt(node: AnnotatedValue<Resource>) {
    this.viewChildList.openListAt(node);
  }

  isSearchEnabled() {
    /*
    search enabled if 
    - input class is provided
    AND 
    - visualization is search based OR instance list is safetly initialized (if hierarchy based)
    */
    return this.cls != null &&
      (this.visualizationMode == InstanceListVisualizationMode.searchBased || this.viewChildList?.safeToGo.safe);
  }

}