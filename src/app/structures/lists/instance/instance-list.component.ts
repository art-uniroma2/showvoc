import { ChangeDetectorRef, Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { Observable, of } from 'rxjs';
import { finalize, mergeMap } from 'rxjs/operators';
import { InstanceListPreference, InstanceListVisualizationMode, SafeToGo } from 'src/app/models/Properties';
import { AnnotatedValue, IRI, RDFResourceRolesEnum, ResAttribute, Resource } from 'src/app/models/Resources';
import { ClassesServices } from 'src/app/services/classes.service';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { SVEventHandler } from 'src/app/utils/SVEventHandler';
import { AbstractList } from '../abstract-list';

@Component({
  selector: 'instance-list',
  templateUrl: './instance-list.component.html',
  host: { class: "structureComponent" },
  standalone: false
})
export class InstanceListComponent extends AbstractList<Resource> {

  @Input() cls: AnnotatedValue<Resource>;
  @Output() requireSettings = new EventEmitter<void>(); //requires to the parent panel to open/change settings

  structRole: RDFResourceRolesEnum = RDFResourceRolesEnum.individual;

  private pendingSearchCls: IRI; //class of a searched instance that is waiting to be selected once the list is initialized

  visualizationMode: InstanceListVisualizationMode;//this could be changed dynamically, so each time it is used, get it again from preferences

  translationParam: { count: number, safeToGoLimit: number };

  constructor(private clsService: ClassesServices, private settingsMgr: SettingsManager, eventHandler: SVEventHandler, changeDetectorRef: ChangeDetectorRef) {
    super(eventHandler, changeDetectorRef);
  }

  ngOnChanges(changes: SimpleChanges) {
    super.ngOnChanges(changes);
    if (changes['cls'] && changes['cls'].currentValue) {
      this.init();
    }
  }

  initImpl() {
    this.visualizationMode = this.settingsMgr.getInstanceListSettings(SVContext.getProjectCtx().getProject()).visualization;
    if (this.cls != null) { //class provided => init list
      if (this.visualizationMode == InstanceListVisualizationMode.standard) {
        this.checkInitializationSafe().subscribe(
          () => {
            if (this.safeToGo.safe) {
              this.loading = true;
              this.clsService.getInstances(this.cls.getValue()).pipe(
                finalize(() => { this.loading = false; })
              ).subscribe(
                instances => {
                  this.nodes = instances;
                  this.sortNodes();
                  this.resumePendingSearch();
                }
              );
            }
          }
        );
      } else { //search based
        //don't do nothing, just check for pending search
        this.forceList(null);
        this.resumePendingSearch();
      }
    } else { //class not provided, reset the instance list
      this.setInitialStatus();
      //prevent ExpressionChangedAfterItHasBeenCheckedError on isOpenGraphEnabled('dataOriented') in the parent panel
      this.changeDetectorRef.detectChanges();
    }
  }

  private resumePendingSearch() {
    // if there is some pending search where the class is same class which instance are currently described
    if (
      this.pendingSearchRes &&
      (
        (this.pendingSearchCls && this.cls.getValue() && this.pendingSearchCls.equals(this.cls.getValue())) ||
        !this.pendingSearchCls //null if already checked that the pendingSearchCls is the current (see selectSearchedInstance)
      )
    ) {
      if (this.settingsMgr.getInstanceListSettings(SVContext.getProjectCtx().getProject()).visualization == InstanceListVisualizationMode.standard) {
        this.openListAt(this.pendingSearchRes); //standard mode => simply open list (focus searched res)
      } else { //search mode => set the pending searched resource as only element of the list and then focus it
        this.forceList([this.pendingSearchRes]);
        this.changeDetectorRef.detectChanges();
        this.openListAt(this.pendingSearchRes);
      }
    }
  }

  public forceList(list: AnnotatedValue<Resource>[]) {
    this.safeToGo = { safe: true }; //prevent the list not showing if a previous standard initialization set the safeToGo to false
    this.setInitialStatus();
    this.nodes = list;
  }

  /**
   * Perform a check in order to prevent the initialization of the structure with too many elements
   * Return true if the initialization is safe or if the user agreed to init the structure anyway
   */
  private checkInitializationSafe(): Observable<void> {
    let instListPreference: InstanceListPreference = this.settingsMgr.getInstanceListSettings(SVContext.getProjectCtx().getProject());
    if (this.safeToGoLimit != instListPreference.safeToGoLimit) {
      this.safeToGoMap = {}; //limit changed, safetiness checks invalidated => reset the map
    }
    this.safeToGoLimit = instListPreference.safeToGoLimit;

    let checksum = this.getInitRequestChecksum();

    let safeness: SafeToGo = this.safeToGoMap[checksum];
    if (safeness != null) { //found safeness in cache
      this.safeToGo = safeness;
      this.translationParam = { count: this.safeToGo.count, safeToGoLimit: this.safeToGoLimit };
      return of(null);
    } else { //never initialized => count
      return this.getNumberOfInstances(this.cls.getValue()).pipe(
        mergeMap(count => {
          safeness = { safe: count < this.safeToGoLimit, count: count };
          this.safeToGoMap[checksum] = safeness; //cache the safeness
          this.safeToGo = safeness;
          this.translationParam = { count: this.safeToGo.count, safeToGoLimit: this.safeToGoLimit };
          return of(null);
        })
      );
    }
  }

  private getInitRequestChecksum() {
    let checksum = "cls:" + this.cls.getValue().toNT();
    return checksum;
  }

  /**
   * Forces the safeness of the structure even if it was reported as not safe, then re initialize it
   */
  forceSafeness() {
    this.safeToGo = { safe: true };
    let checksum = this.getInitRequestChecksum();
    this.safeToGoMap[checksum] = this.safeToGo;
    this.initImpl();
  }

  /**
   * Returns the number of instances of the given class. Useful when the user select a class in order to check if there 
   * are too many instances.
   * @param cls 
   */
  private getNumberOfInstances(cls: Resource): Observable<number> {
    if (this.settingsMgr.getClassTreeSettings(SVContext.getProjectCtx().getProject()).showInstancesNumber) { //if num inst are already computed when building the tree...
      return of(this.cls.getAttribute(ResAttribute.NUM_INST));
    } else { //otherwise call a service
      return this.clsService.getNumberOfInstances(cls);
    }
  }

}
