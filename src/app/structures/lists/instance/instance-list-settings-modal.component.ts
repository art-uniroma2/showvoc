import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from "src/app/utils/SVContext";
import { InstanceListPreference } from "../../../models/Properties";

@Component({
  selector: "instance-list-settings-modal",
  templateUrl: "./instance-list-settings-modal.component.html",
  standalone: false
})
export class InstanceListSettingsModalComponent {

  instListPref: InstanceListPreference;

  constructor(public activeModal: NgbActiveModal, private settingsMgr: SettingsManager) { }

  ngOnInit() {
    let instanceListPref: InstanceListPreference = this.settingsMgr.getInstanceListSettings(SVContext.getProjectCtx().getProject());
    this.instListPref = JSON.parse(JSON.stringify(instanceListPref));
  }

  ok() {
    this.settingsMgr.setInstanceListSettings(SVContext.getProjectCtx().getProject(), this.instListPref).subscribe();
    this.activeModal.close();
  }

  close() {
    this.activeModal.dismiss();
  }

}