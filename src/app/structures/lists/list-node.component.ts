import { Component, ElementRef, ViewChild } from '@angular/core';
import { Resource } from 'src/app/models/Resources';
import { AbstractNode } from '../abstract-node';

@Component({
  selector: 'list-node',
  templateUrl: './list-node.component.html',
  standalone: false
})
export class ListNodeComponent<T extends Resource> extends AbstractNode<T> {

  //get an element in the view referenced with #listNodeElement (useful to apply scrollIntoView in the search function)
  @ViewChild('listNodeElement') listNodeElement: ElementRef;

  ensureVisible() {
    this.listNodeElement.nativeElement.scrollIntoView({ block: 'end', behavior: 'smooth' });
  }

}
