import { ChangeDetectorRef, Directive, QueryList, ViewChildren } from '@angular/core';
import { SVEventHandler } from 'src/app/utils/SVEventHandler';
import { AnnotatedValue, ResAttribute, Resource } from '../../models/Resources';
import { NodeSelectEvent } from '../abstract-node';
import { AbstractStruct } from '../abstract-structure';
import { ListNodeComponent } from './list-node.component';

@Directive()
export abstract class AbstractList<T extends Resource> extends AbstractStruct<T> {

  @ViewChildren(ListNodeComponent) viewChildrenNode: QueryList<ListNodeComponent<T>>;

  /**
   * ATTRIBUTES
   */

  protected pendingSearchRes: AnnotatedValue<Resource>; //searched resource that is waiting to be selected once the list is initialized

  /**
   * CONSTRUCTOR
   */
  protected changeDetectorRef: ChangeDetectorRef;
  constructor(eventHandler: SVEventHandler, changeDetectorRef: ChangeDetectorRef) {
    super(eventHandler);
    this.changeDetectorRef = changeDetectorRef;
  }

  /**
   * METHODS
   */

  init() {
    this.setInitialStatus();
    this.initImpl();
  }

  abstract initImpl(): void;

  // selectNode(event?: NodeSelectEvent) {
  //   if (this.selectedNode != undefined) {
  //     this.selectedNode.deleteAttribute(ResAttribute.SELECTED);
  //   }
  //   this.selectedNode = event?.value as AnnotatedValue<T>;
  //   this.selectedNode.setAttribute(ResAttribute.SELECTED, true);
  //   this.nodeSelected.emit(event);
  // }

  openListAt(node: AnnotatedValue<Resource>) {
    this.ensureNodeVisibility(node);
    this.changeDetectorRef.detectChanges(); //wait that the children node is rendered (in case the openPages has been increased)
    let childrenNodeComponent = this.viewChildrenNode.toArray();

    for (const child of childrenNodeComponent) {
      if (child.node.getValue().equals(node.getValue())) {
        if (!child.node.getAttribute(ResAttribute.SELECTED)) {
          child.selectNode();
        }
        setTimeout(() => {
          child.ensureVisible();
        });
        break;
      }
    }
  }

  ensureNodeVisibility(resource: AnnotatedValue<Resource>) {
    for (let i = 0; i < this.nodes.length; i++) {
      if (this.nodes[i].getValue().equals(resource.getValue())) {
        if (i >= this.nodesLimit) {
          //update nodeLimit so that node at index i is within the range
          let scrollStep: number = ((i - this.nodesLimit) / this.increaseRate) + 1;
          this.nodesLimit += this.increaseRate * scrollStep;
        }
        this.pendingSearchRes = null; //if there was any pending search, reset it
        return; //node found and visible
      }
    }
    //if this code is reached, the node is not found (so probably it is waiting that the list is initialized)
    this.pendingSearchRes = resource;
  }

}