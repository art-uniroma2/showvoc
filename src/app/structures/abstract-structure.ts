import { Directive, ElementRef, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from "@angular/core";
import { Subscription } from 'rxjs';
import { Project } from '../models/Project';
import { SafeToGo, SafeToGoMap } from "../models/Properties";
import { AnnotatedValue, RDFResourceRolesEnum, ResAttribute, Resource } from '../models/Resources';
import { ResourceUtils, SortAttribute } from '../utils/ResourceUtils';
import { SVEventHandler } from '../utils/SVEventHandler';
import { TreeListContext } from '../utils/UIUtils';
import { NodeSelectEvent } from './abstract-node';

@Directive()
export abstract class AbstractStruct<T extends Resource> implements OnInit {

  @Input() rendering: boolean = true; //if true the nodes in the list should be rendered with the show, with the qname otherwise
  @Input() sortBy: SortAttribute = SortAttribute.show;
  @Input() showDeprecated: boolean = true;
  @Input() context: TreeListContext;
  @Input() project: Project;
  @Output() nodeSelected = new EventEmitter<NodeSelectEvent>();

  @ViewChild('scrollableContainer') scrollableElement: ElementRef;

  /**
   * ATTRIBUTES
   */

  abstract structRole: RDFResourceRolesEnum; //declare the type of resources in the panel, used for search service

  nodes: AnnotatedValue<T>[];
  selectedNode: AnnotatedValue<T>;

  //safe to go (used by concept tree, instance list, lex-entry list)
  safeToGoLimit: number;
  safeToGo: SafeToGo = { safe: true };
  safeToGoMap: SafeToGoMap = {};

  loading: boolean = false;

  eventSubscriptions: Subscription[] = [];

  /**
   * CONSTRUCTOR
   */
  protected eventHandler: SVEventHandler;
  constructor(eventHandler: SVEventHandler) {
    this.eventHandler = eventHandler;
  }

  /**
   * METHODS
   */

  ngOnInit() {
    this.init();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['sortBy'] && !changes['sortBy'].isFirstChange()) {
      this.sortNodes();
    }
  }

  ngOnDestroy() {
    this.eventHandler.unsubscribeAll(this.eventSubscriptions);
  }


  abstract init(): void;

  setInitialStatus() {
    this.nodes = [];
    this.selectedNode = null;
    this.nodeSelected.emit(null);
    this.nodesLimit = this.initialNodes;
  }

  sortNodes() {
    ResourceUtils.sortResources(this.nodes, this.sortBy);
  }

  onNodeSelected(event?: NodeSelectEvent) {
    if (this.selectedNode != undefined) {
      this.selectedNode.deleteAttribute(ResAttribute.SELECTED);
    }
    this.selectedNode = event?.value as AnnotatedValue<T>;
    this.selectedNode.setAttribute(ResAttribute.SELECTED, true);
    this.nodeSelected.emit(event);
  }

  //Root limitation management
  initialNodes: number = 100;
  nodesLimit: number = this.initialNodes;
  increaseRate: number = this.nodesLimit / 5;
  onScroll() {
    let scrollElement: HTMLElement = this.scrollableElement.nativeElement;
    if (Math.abs(scrollElement.scrollHeight - scrollElement.offsetHeight - scrollElement.scrollTop) < 2) {
      //bottom reached => increase max range if there are more roots to show
      if (this.nodesLimit < this.nodes.length) {
        this.nodesLimit += this.increaseRate;
      }
    }
  }

}