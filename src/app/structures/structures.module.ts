import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { AlignmentsModule } from '../alignments/alignments.module';
import { CorrespondencesModule } from '../correspondences/correspondences.module';
import { PreferencesModule } from '../preferences/preferences.module';
import { WidgetModule } from '../widget/widget.module';
import { InstanceListPanelComponent } from './lists/instance/instance-list-panel.component';
import { InstanceListSettingsModalComponent } from './lists/instance/instance-list-settings-modal.component';
import { InstanceListComponent } from './lists/instance/instance-list.component';
import { LexicalEntryListPanelComponent } from './lists/lexical-entry/lexical-entry-list-panel.component';
import { LexicalEntryListSettingsModalComponent } from './lists/lexical-entry/lexical-entry-list-settings-modal.component';
import { LexicalEntryListComponent } from './lists/lexical-entry/lexical-entry-list.component';
import { LexiconListPanelComponent } from './lists/lexicon/lexicon-list-panel.component';
import { LexiconListComponent } from './lists/lexicon/lexicon-list.component';
import { ListNodeComponent } from './lists/list-node.component';
import { SchemeListPanelComponent } from './lists/scheme/scheme-list-panel.component';
import { SchemeListComponent } from './lists/scheme/scheme-list.component';
import { AdvancedSearchModalComponent } from './search-bar/advanced-search-modal.component';
import { CustomSearchModalComponent } from './search-bar/custom-search-modal.component';
import { LoadCustomSearchModalComponent } from './search-bar/load-custom-search-modal.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { SearchSettingsModalComponent } from './search-bar/search-settings-modal.component';
import { StructureTabsetSettingsModalComponent } from './structure-tabset/structure-tabset-settings-modal.component';
import { StructureTabsetComponent } from './structure-tabset/structure-tabset.component';
import { ClassIndividualTreeComponent } from './trees/class/class-individual-tree.component';
import { ClassInstancePanelComponent } from './trees/class/class-instance-panel.component';
import { ClassTreeNodeComponent } from './trees/class/class-tree-node.component';
import { ClassTreePanelComponent } from './trees/class/class-tree-panel.component';
import { ClassTreeSettingsModalComponent } from './trees/class/class-tree-settings-modal.component';
import { ClassTreeComponent } from './trees/class/class-tree.component';
import { CollectionTreeNodeComponent } from './trees/collection/collection-tree-node.component';
import { CollectionTreePanelComponent } from './trees/collection/collection-tree-panel.component';
import { CollectionTreeComponent } from './trees/collection/collection-tree.component';
import { ConceptTreeNodeComponent } from './trees/concept/concept-tree-node.component';
import { ConceptTreePanelComponent } from './trees/concept/concept-tree-panel.component';
import { ConceptTreeSettingsModalComponent } from './trees/concept/concept-tree-settings-modal.component';
import { ConceptTreeComponent } from './trees/concept/concept-tree.component';
import { PropertyTreeNodeComponent } from './trees/property/property-tree-node.component';
import { PropertyTreePanelComponent } from './trees/property/property-tree-panel.component';
import { PropertyTreeComponent } from './trees/property/property-tree.component';
import { StructureTabsetDefaultsModalComponent } from './structure-tabset/structure-tabset-defaults-modal.component';

@NgModule({
    declarations: [
        AdvancedSearchModalComponent,
        ClassIndividualTreeComponent,
        ClassInstancePanelComponent,
        ClassTreeComponent,
        ClassTreeNodeComponent,
        ClassTreePanelComponent,
        ClassTreeSettingsModalComponent,
        CollectionTreeComponent,
        CollectionTreeNodeComponent,
        CollectionTreePanelComponent,
        ConceptTreeComponent,
        ConceptTreeNodeComponent,
        ConceptTreePanelComponent,
        ConceptTreeSettingsModalComponent,
        CustomSearchModalComponent,
        InstanceListComponent,
        InstanceListPanelComponent,
        InstanceListSettingsModalComponent,
        LexiconListComponent,
        LexiconListPanelComponent,
        LexicalEntryListComponent,
        LexicalEntryListPanelComponent,
        LexicalEntryListSettingsModalComponent,
        ListNodeComponent,
        LoadCustomSearchModalComponent,
        PropertyTreeComponent,
        PropertyTreeNodeComponent,
        PropertyTreePanelComponent,
        SchemeListComponent,
        SchemeListPanelComponent,
        SearchBarComponent,
        SearchSettingsModalComponent,
        StructureTabsetComponent,
        StructureTabsetDefaultsModalComponent,
        StructureTabsetSettingsModalComponent
    ],
    imports: [
        AlignmentsModule,
        AutocompleteLibModule,
        DragDropModule,
        CommonModule,
        CorrespondencesModule,
        FormsModule,
        NgbDropdownModule,
        PreferencesModule,
        TranslateModule,
        WidgetModule
    ],
    exports: [
        StructureTabsetComponent,
        ClassIndividualTreeComponent,
        ClassInstancePanelComponent,
        ClassTreePanelComponent,
        CollectionTreePanelComponent,
        ConceptTreePanelComponent,
        LexicalEntryListPanelComponent,
        LexiconListPanelComponent,
        PropertyTreePanelComponent,
        SchemeListPanelComponent,
    ]
})
export class StructuresModule { }
