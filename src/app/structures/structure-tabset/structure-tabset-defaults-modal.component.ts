import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DataPanel } from "src/app/models/DataStructure";
import { AbstractDefaultsModal } from "src/app/preferences/default-settings-modal/abstract-defaults-modal";
import { SettingsServices } from "src/app/services/settings.service";
import { SettingsManager, SettingsMgrUtils } from "src/app/utils/SettingsManager";
import { SVContext } from "src/app/utils/SVContext";

@Component({
  selector: "structure-tabset-defaults-modal",
  templateUrl: "./structure-tabset-defaults-modal.component.html",
  standalone: false
})
export class StructureTabsetDefaultsModalComponent extends AbstractDefaultsModal {

  settingName = SettingsMgrUtils.modelToHiddenDataPanelSetting[SVContext.getProject().getModelType()];

  userDefault: DataPanel[];
  projectDefault: DataPanel[];

  constructor(
    activeModal: NgbActiveModal,
    settingsService: SettingsServices,
    settingsMgr: SettingsManager
  ) {
    super(activeModal, settingsService, settingsMgr);
  }

  restoreProjDefaultImpl() {
    return this.settingsMgr.setPanelFilter(SVContext.getWorkingProject(), null);
  }

  applyUserDefaultImpl() {
    return this.settingsMgr.setPanelFilter(SVContext.getWorkingProject(), this.userDefault);
  }

}