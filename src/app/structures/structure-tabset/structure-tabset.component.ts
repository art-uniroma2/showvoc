import { ChangeDetectorRef, Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { CheckOptions, ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { SharedModalsServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { DataPanel, DataPanelLabelSetting, DataStructureUtils } from 'src/app/models/DataStructure';
import { LinksetMetadata } from 'src/app/models/Metadata';
import { AnnotatedValue, IRI, RDFResourceRolesEnum, Resource } from 'src/app/models/Resources';
import { LocalStorageManager } from 'src/app/utils/LocalStorageManager';
import { ResourceUtils } from 'src/app/utils/ResourceUtils';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { TranslationUtils } from 'src/app/utils/TranslationUtils';
import { TreeListContext } from 'src/app/utils/UIUtils';
import { RenderingEditorModalComponent } from 'src/app/widget/rendering-editor/rendering-editor-modal.component';
import { NodeSelectEvent } from '../abstract-node';
import { LexicalEntryListPanelComponent } from '../lists/lexical-entry/lexical-entry-list-panel.component';
import { LexiconListPanelComponent } from '../lists/lexicon/lexicon-list-panel.component';
import { SchemeListPanelComponent } from '../lists/scheme/scheme-list-panel.component';
import { ClassInstancePanelComponent } from '../trees/class/class-instance-panel.component';
import { CollectionTreePanelComponent } from '../trees/collection/collection-tree-panel.component';
import { ConceptTreePanelComponent } from '../trees/concept/concept-tree-panel.component';
import { PropertyTreePanelComponent } from '../trees/property/property-tree-panel.component';
import { StructureTabsetSettingsModalComponent } from './structure-tabset-settings-modal.component';

@Component({
  selector: 'structure-tabset',
  templateUrl: './structure-tabset.component.html',
  host: { class: "vbox" },
  standalone: false
})
export class StructureTabsetComponent implements OnInit {
  @Output() nodeSelected = new EventEmitter<NodeSelectEvent>();
  @Output() linksetSelected = new EventEmitter<LinksetMetadata>();
  @Output() correspondenceSelected = new EventEmitter<NodeSelectEvent>();

  @ViewChild(ClassInstancePanelComponent) viewChildClassInstancePanel: ClassInstancePanelComponent;
  @ViewChild(ConceptTreePanelComponent) viewChildConceptPanel: ConceptTreePanelComponent;
  @ViewChild(CollectionTreePanelComponent) viewChildCollectionPanel: CollectionTreePanelComponent;
  @ViewChild(SchemeListPanelComponent) viewChildSchemePanel: SchemeListPanelComponent;
  @ViewChild(PropertyTreePanelComponent) viewChildPropertyPanel: PropertyTreePanelComponent;
  @ViewChild(LexiconListPanelComponent) viewChildLexiconPanel: LexiconListPanelComponent;
  @ViewChild(LexicalEntryListPanelComponent) viewChildLexialEntryPanel: LexicalEntryListPanelComponent;

  context: TreeListContext = TreeListContext.dataPanel;

  availablePanels: DataPanel[]; //list of data panels available (useful to optimize check if it needs to render specific panels)
  activeTab: DataPanel;
  tabs: DataPanelTab[] = [];
  hidePanelSelector: boolean = false; //tells if the panel selector is hidden (in case only 1 panel is visible)

  private model: string;

  DataPanel = DataPanel; //to use enum in template

  constructor(private basicModals: BasicModalsServices, private sharedModals: SharedModalsServices, private modalService: NgbModal,
    private settingsMgr: SettingsManager, private translate: TranslateService, private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.model = SVContext.getWorkingProject().getModelType(false);
    this.translate.onLangChange.subscribe(() => this.initTabsLabels());
    this.initTabs();

    this.translate.onLangChange.subscribe(() => this.initTabsLabels());
  }

  /**
   * Initialize available tabs according to the model and to the (eventual) filters.
   * Init also the active tab.
   */
  private initTabs() {
    //init all data panels

    let allPanels = [DataPanel.cls, DataPanel.concept, DataPanel.skosCollection, DataPanel.conceptScheme,
    DataPanel.limeLexicon, DataPanel.ontolexLexicalEntry, DataPanel.property];

    let panelFilter: DataPanel[] = this.settingsMgr.getPanelFilter(SVContext.getWorkingProject());

    this.availablePanels = allPanels.filter(p => {
      //check if tab is compliant with model
      let modelCompliant = DataStructureUtils.modelPanelsMap[this.model].includes(p);
      //check if tab is not hidden by the setting
      let notHidden = !panelFilter.includes(p);
      return modelCompliant && notHidden;
    });

    //Add additional panels
    //- alignments, if not filtered
    if (!panelFilter.includes(DataPanel.alignments)) {
      this.availablePanels.push(DataPanel.alignments);
    }
    //- XKOS correspondences if explicitly enabled with dedicated project setting AND not filtered
    if (this.settingsMgr.getXkosTabEnabled(SVContext.getProject()) && !panelFilter.includes(DataPanel.correspondence)) {
      this.availablePanels.push(DataPanel.correspondence);
    }

    //init tabs
    this.tabs = this.availablePanels.map(p => {
      return { panel: p, label: "" };
    });

    this.initTabsLabels();

    this.hidePanelSelector = this.tabs.length == 1;

    /* 
    Init active tab. The default choice is based on common convention (e.g. SKOS -> concept, OWL/RDFS -> cls, ...)
    If the default tab is filtered in the setting, automatically activate the first available tab
    */
    if (this.activeTab != null) { //active tab already initialized before
      //if active tab is not more among those available, reset it
      if (!this.tabs.some(t => t.panel == this.activeTab)) {
        this.activeTab = null;
      }
    }
    if (this.activeTab == null) { //active tab not initialized
      //init according the priority list
      let tabsPriority: DataPanel[] = DataStructureUtils.panelsPriority[this.model];
      for (let t of tabsPriority) {
        if (this.tabs.some(tab => tab.panel == t)) { //tab in priority list available => set it
          this.activeTab = t;
          break;
        }
      }
      //no tab in priority list is available => set the first one available
      if (this.activeTab == null && this.tabs.length > 0) {
        this.activeTab = this.tabs[0].panel;
      }
    }
  }

  private initTabsLabels() {
    let currentLang = this.translate.currentLang;
    let panelLabelsMap: DataPanelLabelSetting = this.settingsMgr.getDataPanelsLabels(SVContext.getProject());
    this.tabs.forEach(t => {
      if (panelLabelsMap?.[currentLang]?.[t.panel] != null) {
        t.label = panelLabelsMap[currentLang][t.panel];
      } else {
        t.label = this.translate.instant(TranslationUtils.dataPanelTranslationMap[t.panel]);
      }
    });
  }

  isPanelAvailable(panel: DataPanel) {
    return this.availablePanels.includes(panel);
  }


  onNodeSelected(event?: NodeSelectEvent) {
    this.nodeSelected.emit(event);
  }

  handleAdvSearchResult(resource: AnnotatedValue<Resource>) {
    this.selectResource(resource);
  }

  /**
   * Allows to force the selection of a resource from outside this component.
   * This is useful when a search result is selected and it has to be show in the tree/list view or when an object is dblclicked from a RV
   */
  selectResource(resource: AnnotatedValue<Resource>) {
    if (resource.getValue() instanceof IRI) {
      let annotatedIRI: AnnotatedValue<IRI> = resource.asAnnotatedIRI();
      let role: RDFResourceRolesEnum = resource.getRole();
      let tabToActivate: DataPanel;
      if (ResourceUtils.roleSubsumes(RDFResourceRolesEnum.property, role)) {
        tabToActivate = DataPanel.property;
      } else if (ResourceUtils.roleSubsumes(RDFResourceRolesEnum.skosCollection, role)) {
        tabToActivate = DataPanel.skosCollection;
      } else if (role == RDFResourceRolesEnum.individual) {
        tabToActivate = DataPanel.cls;
      } else if (
        role == RDFResourceRolesEnum.cls || role == RDFResourceRolesEnum.concept || role == RDFResourceRolesEnum.conceptScheme ||
        role == RDFResourceRolesEnum.limeLexicon || role == RDFResourceRolesEnum.ontolexLexicalEntry
      ) {
        tabToActivate = role as unknown as DataPanel; //here I can force cast since the roles in the if are overlapped with DataPanel
      }

      if (tabToActivate != null && this.tabs.some(t => t.panel == tabToActivate)) {
        this.activeTab = tabToActivate;
        // this.viewChildNavbar.select(tabToActivate);
        this.changeDetectorRef.detectChanges(); //wait for the tab to be activate
        if (tabToActivate == DataPanel.cls) {
          this.viewChildClassInstancePanel.selectSearchedResource(annotatedIRI);
        } else if (tabToActivate == DataPanel.concept) {
          this.viewChildConceptPanel.selectSearchedResource(annotatedIRI);
        } else if (tabToActivate == DataPanel.conceptScheme) {
          this.viewChildSchemePanel.openAt(annotatedIRI);
        } else if (tabToActivate == DataPanel.limeLexicon) {
          this.viewChildLexiconPanel.openAt(annotatedIRI);
        } else if (tabToActivate == DataPanel.ontolexLexicalEntry) {
          this.viewChildLexialEntryPanel.selectSearchedResource(annotatedIRI);
        } else if (tabToActivate == DataPanel.property) {
          this.viewChildPropertyPanel.openAt(annotatedIRI);
        } else if (tabToActivate == DataPanel.skosCollection) {
          this.viewChildCollectionPanel.openAt(annotatedIRI);
        }
      } else { //tabToActivate null means that the resource doesn't belong to any kind handled by the tabset
        let hideWarning: boolean = LocalStorageManager.getItem(LocalStorageManager.EXPLORE_HIDE_WARNING_MODAL_RES_VIEW);
        if (hideWarning) {
          this.sharedModals.openResourceView(resource.getValue());
        } else {
          let checkOpt: CheckOptions = {
            label: { key: "COMMONS.DONT_SHOW_AGAIN" },
            value: false
          };
          this.basicModals.alert({ key: "SEARCH.RESOURCE_NOT_REACHABLE" }, { key: "MESSAGES.RESOURCE_NOT_REACHABLE", params: { resource: annotatedIRI.getValue().getIRI() } }, ModalType.warning, null, checkOpt).then(
            (checkOptRes: CheckOptions) => {
              if (checkOptRes.value) {
                LocalStorageManager.setItem(LocalStorageManager.EXPLORE_HIDE_WARNING_MODAL_RES_VIEW, true);
              }
              this.sharedModals.openResourceView(resource.getValue());
            },
            () => { }
          );
        }
      }
    } else { //Bnode
      this.sharedModals.openResourceView(resource.getValue());
    }
  }

  onLinksetSelected(linkset: LinksetMetadata) {
    this.linksetSelected.emit(linkset);
  }

  onCorrespondenceSelected(event?: NodeSelectEvent) {
    this.correspondenceSelected.emit(event);
  }

  tabsSettings() {
    const modalRef: NgbModalRef = this.modalService.open(StructureTabsetSettingsModalComponent, new ModalOptions('lg'));
    modalRef.componentInstance.project = SVContext.getWorkingProject();
    modalRef.result.then(
      () => {
        this.initTabs();
      },
      () => { }
    );
  }

  changeRendering() {
    this.modalService.open(RenderingEditorModalComponent, new ModalOptions());
  }

}

interface DataPanelTab {
  panel: DataPanel;
  label: string;
}