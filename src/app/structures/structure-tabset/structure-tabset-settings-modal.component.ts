import { Component, Input } from "@angular/core";
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { BasicModalsServices } from "src/app/modal-dialogs/basic-modals/basic-modals.service";
import { ModalOptions, ModalType } from "src/app/modal-dialogs/Modals";
import { DataPanel } from "src/app/models/DataStructure";
import { Scope } from "src/app/models/Plugins";
import { Project } from 'src/app/models/Project';
import { SettingsManager, SettingsManagerID } from 'src/app/utils/SettingsManager';
import { StructureTabsetDefaultsModalComponent } from "./structure-tabset-defaults-modal.component";

@Component({
  selector: "structure-tabset-settings-modal",
  templateUrl: "./structure-tabset-settings-modal.component.html",
  standalone: false
})
export class StructureTabsetSettingsModalComponent {

  @Input() project: Project;

  hiddenTabsPref: DataPanel[];

  constructor(
    public activeModal: NgbActiveModal,
    private settingsMgr: SettingsManager,
    private basicModals: BasicModalsServices,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.init();
  }

  private init() {
    this.hiddenTabsPref = this.settingsMgr.getPanelFilter(this.project);
  }

  setAsDefault() {
    this.basicModals.confirm({ key: "COMMONS.ACTIONS.SET_AS_DEFAULT" }, { key: "MESSAGES.SET_USER_DEFAULT_CONFIRM" }, ModalType.warning).then(
      () => {
        this.settingsMgr.setDefaultHiddenDataPanels(this.hiddenTabsPref, this.project.getModelType(), Scope.USER, this.project).subscribe();
      },
      () => { }
    );
  }

  showDefaults() {
    const modalRef: NgbModalRef = this.modalService.open(StructureTabsetDefaultsModalComponent, new ModalOptions('lg'));
    modalRef.result.then(
      () => {
        this.settingsMgr.initSettings(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project).subscribe(
          () => {
            this.init();
          }
        );
      },
      () => { }
    );
  }

  ok() {
    this.settingsMgr.setPanelFilter(this.project, this.hiddenTabsPref).subscribe(
      () => {
        this.activeModal.close();
      }
    );
  }


  cancel() {
    this.activeModal.dismiss();
  }

}