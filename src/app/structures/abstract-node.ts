import { Directive, EventEmitter, Input, Output } from "@angular/core";
import { Subscription } from 'rxjs';
import { Project } from '../models/Project';
import { AnnotatedValue, Resource } from '../models/Resources';
import { SortAttribute } from "../utils/ResourceUtils";
import { TreeListContext } from '../utils/UIUtils';

@Directive()
export abstract class AbstractNode<T extends Resource> {

  /**
   * VIEWCHILD, INPUTS / OUTPUTS
   */

  @Input() node: AnnotatedValue<T>;
  @Input() rendering: boolean; //if true the node be rendered with the show, with the qname otherwise
  @Input() showDeprecated: boolean;
  @Input() context: TreeListContext;
  @Input() project: Project;
  @Input() sortBy: SortAttribute = SortAttribute.show;

  @Output() nodeSelected = new EventEmitter<NodeSelectEvent>();

  eventSubscriptions: Subscription[] = [];

  /**
   * ATTRIBUTES
   */

  /**
   * CONSTRUCTOR
   */

  constructor() { }

  /**
   * METHODS
   */

  selectNode(event?: MouseEvent) {
    this.nodeSelected.emit(new NodeSelectEvent(this.node, event));
    if (event?.shiftKey) {
      event.preventDefault();
    }
  }


}

export class NodeSelectEvent {
  ctrl?: boolean;
  shift?: boolean;
  value: AnnotatedValue<Resource>;

  constructor(value: AnnotatedValue<Resource>, event?: MouseEvent) {
    this.value = value;
    this.ctrl = event?.metaKey || event?.ctrlKey;
    this.shift = event?.shiftKey;
  }
}