import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ClassTreePreference } from 'src/app/models/Properties';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';

@Component({
  selector: 'class-tree-settings-modal',
  templateUrl: './class-tree-settings-modal.component.html',
  standalone: false
})
export class ClassTreeSettingsModalComponent implements OnInit {

  clsTreePref: ClassTreePreference;


  constructor(public activeModal: NgbActiveModal, private settingsMgr: SettingsManager) { }

  ngOnInit() {
    let classTreePref: ClassTreePreference = this.settingsMgr.getClassTreeSettings(SVContext.getProjectCtx().getProject());
    this.clsTreePref = JSON.parse(JSON.stringify(classTreePref));
  }


  ok() {
    //update the settings only if changed
    let pristine = this.settingsMgr.getClassTreeSettings(SVContext.getProjectCtx().getProject());
    if (JSON.stringify(this.clsTreePref) != JSON.stringify(pristine)) { //changed
      //if something changed close the dialog (so that the class tree refresh)
      this.settingsMgr.setClassTreeSettings(SVContext.getProjectCtx().getProject(), this.clsTreePref).subscribe();
      this.activeModal.close();
    } else { //otherwise simply dismiss the modal
      this.close();
    }
  }

  close() {
    this.activeModal.dismiss();
  }

}