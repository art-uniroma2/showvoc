import { ChangeDetectorRef, Component, EventEmitter, Input, Output, SimpleChanges, ViewChild } from "@angular/core";
import { AnnotatedValue, IRI, Resource } from 'src/app/models/Resources';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { SKOS } from '../../../models/Vocabulary';
import { TreeListContext } from "../../../utils/UIUtils";
import { NodeSelectEvent } from '../../abstract-node';
import { ClassTreePanelComponent } from './class-tree-panel.component';

@Component({
  selector: "class-individual-tree",
  templateUrl: "./class-individual-tree.component.html",
  host: { class: "vbox" },
  standalone: false
})
export class ClassIndividualTreeComponent {

  @Input() context: TreeListContext;
  @Input() roots: Resource[]; //roots of the class three
  @Input() schemes: IRI[]; //scheme to use in case the class selected is skos:Concept
  @Output() nodeSelected = new EventEmitter<NodeSelectEvent>();//when an instance or a concept is selected
  /*in the future I might need an Output for selected class. In case, change nodeSelected in instanceSelected and
  create classSelected Output. (Memo: nodeSelected is to maintain the same Output of the other tree components)*/

  @ViewChild(ClassTreePanelComponent, { static: true }) classTreePanelChild: ClassTreePanelComponent;

  selectedClass: AnnotatedValue<Resource> = null; //the class selected from class tree
  currentSchemes: IRI[];//the scheme selecte in the concept tree (only if selected class is skos:Concept)
  selectedInstance: AnnotatedValue<Resource>; //the instance (or concept) selected in the instance list (or concept tree)

  constructor(private settingsMgr: SettingsManager, private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit() {
    if (this.schemes === undefined) { //if @Input scheme is not provided at all, get it from project preference
      this.currentSchemes = this.settingsMgr.getActiveSchemes(SVContext.getProjectCtx().getProject());
    } else { //if @Input scheme is provided (it could be null => no scheme-mode), initialize the tree with this scheme
      this.currentSchemes = this.schemes;
    }
    if (this.context == undefined) { //if not overwritten from a parent component (e.g. addPropertyValueModal), set its default
      this.context = TreeListContext.clsIndTree;
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['roots']) { //when roots changes, deselect eventals class and instance selected
      this.selectedClass = null;
      this.selectedInstance = null;
      //when there is only one class, select it automatically
      if (this.roots && this.roots.length == 1) {
        this.changeDetectorRef.detectChanges(); //give time to initialize child component (without this, openTreeAt in class tree generates error)
        this.classTreePanelChild.openAt(new AnnotatedValue(this.roots[0]));
      }
    }
  }

  /**
   * Listener to the event nodeSelected thrown by the class-tree. Updates the selectedClass
   */
  onTreeClassSelected(event?: NodeSelectEvent) {
    if (this.selectedClass == undefined || !this.selectedClass.getValue().equals(event?.value.getValue())) {
      this.selectedInstance = null; //reset the instance only if selected class changes
      this.nodeSelected.emit(null);
    }
    this.selectedClass = event?.value;
  }

  /**
   * Listener to click on element in the instance list. Updates the selectedInstance
   */
  onInstanceSelected(event?: NodeSelectEvent) {
    this.selectedInstance = event?.value;
    this.nodeSelected.emit(event);
  }

  /**
   * Listener to schemeChanged event emitted by concept-tree when range class is skos:Concept.
   */
  onConceptTreeSchemeChange() {
    this.selectedInstance = null;
    this.nodeSelected.emit(null);
  }

  /**
   * Listener to lexiconChanged event emitted by lexical-entry-list when range class is ontolex:LexicalEntry.
   */
  onLexEntryLexiconChange() {
    this.selectedInstance = null;
    this.nodeSelected.emit(null);
  }

  /**
   * Tells if the current selected range class is skos:Concept. It's useful to show concept tree
   * instead of instance list in the modal
   */
  isRangeConcept(): boolean {
    return (this.selectedClass != undefined && this.selectedClass.getValue().equals(SKOS.concept));
  }

}