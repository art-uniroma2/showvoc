import { ChangeDetectorRef, Component, Input, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { SharedModalsServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { RDFResourceRolesEnum, Resource } from 'src/app/models/Resources';
import { ClassesServices } from 'src/app/services/classes.service';
import { SearchServices } from 'src/app/services/search.service';
import { NTriplesUtil } from 'src/app/utils/ResourceUtils';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { SVEventHandler } from 'src/app/utils/SVEventHandler';
import { AbstractTree } from '../abstract-tree';
import { ClassTreeNodeComponent } from './class-tree-node.component';

@Component({
    selector: 'class-tree',
    templateUrl: './class-tree.component.html',
    host: { class: "structureComponent" },
    standalone: false
})
export class ClassTreeComponent extends AbstractTree<Resource> {
    @Input() filterEnabled: boolean = false;
    @Input() roots: Resource[];

    @ViewChildren(ClassTreeNodeComponent) viewChildrenNode: QueryList<ClassTreeNodeComponent>;

    structRole: RDFResourceRolesEnum = RDFResourceRolesEnum.cls;

    constructor(private clsService: ClassesServices, private settingsMgr: SettingsManager, searchService: SearchServices, basicModals: BasicModalsServices, sharedModals: SharedModalsServices, 
        eventHandler: SVEventHandler, changeDetectorRef: ChangeDetectorRef) {
        super(eventHandler, searchService, basicModals, sharedModals, changeDetectorRef);
    }

    ngOnChanges(changes: SimpleChanges) {
        super.ngOnChanges(changes);
        if (changes['roots'] && !changes['roots'].firstChange) {
            this.init();
        }
    }

    initImpl() {
        let clsTreeRoots: Resource[] = this.roots;
        if (clsTreeRoots == undefined || clsTreeRoots.length == 0) {
            let classTreePref = this.settingsMgr.getClassTreeSettings(SVContext.getProjectCtx().getProject());
            clsTreeRoots = [NTriplesUtil.parseIRI(classTreePref.rootClass)];
        }

        this.loading = true;
        this.clsService.getClassesInfo(clsTreeRoots).pipe(
            finalize(() => { this.loading = false; })
        ).subscribe(
            classes => {
                this.nodes = classes;
                this.sortNodes();
            }
        );
    }

}
