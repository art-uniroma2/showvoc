import { Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { AnnotatedValue, IRI, RDFResourceRolesEnum, ResAttribute, Resource } from 'src/app/models/Resources';
import { IndividualsServices } from 'src/app/services/individuals.service';
import { TreeListContext } from 'src/app/utils/UIUtils';
import { NodeSelectEvent } from '../../abstract-node';
import { InstanceListPanelComponent } from '../../lists/instance/instance-list-panel.component';
import { ClassTreePanelComponent } from './class-tree-panel.component';

/**
 * While classTreeComponent has as @Input rootClasses this componente cannot
 * because if it allows multiple roots, when the user wants to add a class (not a sublcass)
 * I don't know wich class consider as superClass of the new added class
 */

@Component({
  selector: "class-instance-panel",
  templateUrl: "./class-instance-panel.component.html",
  host: { class: "vbox" },
  standalone: false
})
export class ClassInstancePanelComponent {
  @Input() context: TreeListContext;

  @Output() nodeSelected = new EventEmitter<NodeSelectEvent>();
  @Output() advancedSearchResult: EventEmitter<AnnotatedValue<Resource>> = new EventEmitter<AnnotatedValue<Resource>>();

  @ViewChild(ClassTreePanelComponent) viewChildClassTree: ClassTreePanelComponent;
  @ViewChild(InstanceListPanelComponent) viewChildInstanceList: InstanceListPanelComponent;

  private rendering: boolean = false; //if true the nodes in the tree should be rendered with the show, with the qname otherwise

  selectedClass: AnnotatedValue<Resource> = null;
  selectedInstance: AnnotatedValue<Resource>;

  panelRole: RDFResourceRolesEnum[] = [RDFResourceRolesEnum.cls, RDFResourceRolesEnum.individual];

  constructor(private individualService: IndividualsServices) { }

  /**
   * If resource is a class expands the class tree and select the resource,
   * otherwise (resource is an instance) expands the class tree to the class of the instance and
   * select the instance in the instance list
   */
  public selectSearchedResource(resource: AnnotatedValue<IRI>) {
    if (resource.getRole() == RDFResourceRolesEnum.cls) {
      this.viewChildClassTree.openAt(resource);
    } else { // resource is an instance
      //get type of instance, then open the tree to that class
      this.individualService.getNamedTypes(resource.getValue()).subscribe(
        types => {
          this.viewChildClassTree.openAt(types[0]);
          //center instanceList to the individual
          this.viewChildInstanceList.openAt(resource);
        }
      );
    }
  }

  //EVENT LISTENERS
  onClassSelected(event?: NodeSelectEvent) {
    this.selectedClass = event?.value;
    if (this.selectedInstance != null) {
      this.selectedInstance.setAttribute(ResAttribute.SELECTED, false);
      this.selectedInstance = null;
    }
    if (event != null) { //cls could be null if the underlaying classTree has been refreshed
      // this.classSelected.emit(cls);
      this.nodeSelected.emit(event);
    }
  }

  onInstanceSelected(event?: NodeSelectEvent) {
    this.selectedInstance = event?.value;
    //event could be fired after a refresh on the list, in that case, instance is null
    if (event != null) { //forward the event only if instance is not null
      // this.instanceSelected.emit(instance);
      this.nodeSelected.emit(event);
    }
  }

  handleAdvSearchResult(resource: AnnotatedValue<Resource>) {
    this.advancedSearchResult.emit(resource);
  }

}