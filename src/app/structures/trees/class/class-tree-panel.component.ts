import { Component, Input, ViewChild } from "@angular/core";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { GraphModalServices } from 'src/app/graph/modals/graph-modal.service';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalOptions } from 'src/app/modal-dialogs/Modals';
import { SharedModalsServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { AnnotatedValue, RDFResourceRolesEnum, ResAttribute, Resource } from 'src/app/models/Resources';
import { ResourceUtils, SortAttribute } from 'src/app/utils/ResourceUtils';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { SVEventHandler } from 'src/app/utils/SVEventHandler';
import { AbstractTreePanel } from '../abstract-tree-panel';
import { ClassTreeSettingsModalComponent } from './class-tree-settings-modal.component';
import { ClassTreeComponent } from './class-tree.component';

@Component({
  selector: "class-tree-panel",
  templateUrl: "./class-tree-panel.component.html",
  host: { class: "vbox" },
  standalone: false
})
export class ClassTreePanelComponent extends AbstractTreePanel<Resource> {
  @ViewChild(ClassTreeComponent) viewChildTree: ClassTreeComponent;

  @Input() roots: Resource[];

  panelRole: RDFResourceRolesEnum = RDFResourceRolesEnum.cls;
  rendering: boolean = false; //override the value in AbstractPanel
  sortBy: SortAttribute = SortAttribute.value; //override the value in AbstractPanel

  filterEnabled: boolean;

  constructor(basicModals: BasicModalsServices, sharedModals: SharedModalsServices, eventHandler: SVEventHandler, settingsMgr: SettingsManager, private graphModals: GraphModalServices, private modalService: NgbModal) {
    super(basicModals, sharedModals, eventHandler, settingsMgr);
  }

  ngOnInit() {
    super.ngOnInit();
    this.filterEnabled = this.settingsMgr.getClassTreeSettings(SVContext.getProjectCtx().getProject()).filter.enabled;
  }

  //top bar commands handlers

  refresh() {
    this.viewChildTree.init();
  }

  openModelGraph() {
    this.graphModals.openModelGraph(null, this.rendering);
  }

  openUmlGraph() {
    this.graphModals.openUmlGraph(this.rendering);
  }

  isOpenIncrementalModelGraphEnabled() {
    return this.selectedNode != null && this.selectedNode.getAttribute(ResAttribute.EXPLICIT);
  }

  openIncrementalGraph() {
    this.graphModals.openModelGraph(this.selectedNode, this.rendering);
  }

  //search handlers

  handleSearchResults(results: AnnotatedValue<Resource>[]) {
    if (results.length == 1) {
      this.openAt(results[0]);
    } else { //multiple results, ask the user which one select
      ResourceUtils.sortResources(results, this.rendering ? SortAttribute.show : SortAttribute.value);
      this.sharedModals.selectResource({ key: "SEARCH.SEARCH_RESULTS" }, { key: "MESSAGES.X_SEARCH_RESOURCES_FOUND", params: { results: results.length } }, results, this.rendering).then(
        (selectedResources: AnnotatedValue<Resource>[]) => {
          this.openAt(selectedResources[0]);
        },
        () => { }
      );
    }
  }

  openAt(node: AnnotatedValue<Resource>) {
    let root: Resource;
    if (this.roots != null && this.roots.length == 1) {
      root = this.roots[0];
    }
    this.viewChildTree.openTreeAt(node, null, root);
  }

  settings() {
    const modalRef: NgbModalRef = this.modalService.open(ClassTreeSettingsModalComponent, new ModalOptions());
    modalRef.result.then(
      () => { //changes done
        this.filterEnabled = this.settingsMgr.getClassTreeSettings(SVContext.getProjectCtx().getProject()).filter.enabled;
        this.refresh();
      },
      () => {
        //not done
        this.filterEnabled = this.settingsMgr.getClassTreeSettings(SVContext.getProjectCtx().getProject()).filter.enabled;
      }
    );
  }

}