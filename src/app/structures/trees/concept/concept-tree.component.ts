import { ChangeDetectorRef, Component, EventEmitter, Input, Output, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { Observable, of } from 'rxjs';
import { finalize, mergeMap } from 'rxjs/operators';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { SharedModalsServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { ConceptTreePreference, ConceptTreeVisualizationMode, SafeToGo } from 'src/app/models/Properties';
import { AnnotatedValue, IRI, RDFResourceRolesEnum } from 'src/app/models/Resources';
import { SearchServices } from 'src/app/services/search.service';
import { SkosServices } from 'src/app/services/skos.service';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { SVEventHandler } from 'src/app/utils/SVEventHandler';
import { AbstractTree } from '../abstract-tree';
import { ConceptTreeNodeComponent } from './concept-tree-node.component';

@Component({
  selector: 'concept-tree',
  templateUrl: './concept-tree.component.html',
  host: { class: "structureComponent" },
  standalone: false
})
export class ConceptTreeComponent extends AbstractTree<IRI> {

  @Input() schemes: IRI[];
  @Output() requireSettings = new EventEmitter<void>(); //requires to the parent panel to open/change settings

  @ViewChildren(ConceptTreeNodeComponent) viewChildrenNode: QueryList<ConceptTreeNodeComponent>;

  structRole: RDFResourceRolesEnum = RDFResourceRolesEnum.concept;

  private lastTimeInit: number;

  visualizationMode: ConceptTreeVisualizationMode;//this could be changed dynamically, so each time it is used, get it again from preferences

  translationParam: { count: number, safeToGoLimit: number };

  constructor(private skosService: SkosServices, private settingsMgr: SettingsManager, searchService: SearchServices, basicModals: BasicModalsServices, sharedModals: SharedModalsServices,
    eventHandler: SVEventHandler, changeDetectorRef: ChangeDetectorRef) {
    super(eventHandler, searchService, basicModals, sharedModals, changeDetectorRef);
  }

  /**
   * Listener on changes of @Input scheme. When it changes, update the tree
   */
  ngOnChanges(changes: SimpleChanges) {
    super.ngOnChanges(changes);
    /**
     * Initialized the tree only if not the first change. Avoid multiple initialization.
     * The first initialization was already fired in ngOnInit of AbstractStructure
     */
    if (changes['schemes'] && !changes['schemes'].isFirstChange()) {
      this.init();
    }
  }

  initImpl() {
    this.visualizationMode = this.settingsMgr.getConceptTreeSettings(SVContext.getProject(this.project)).visualization;
    if (this.visualizationMode == ConceptTreeVisualizationMode.hierarchyBased) {
      this.checkInitializationSafe().subscribe(
        () => {
          if (this.safeToGo.safe) {
            this.lastTimeInit = new Date().getTime();
            this.loading = true;
            this.skosService.getTopConcepts(this.lastTimeInit, this.schemes).pipe(
              finalize(() => { this.loading = false; })
            ).subscribe(
              data => {
                if (data.timestamp != this.lastTimeInit) return;
                let topConcepts = data.concepts;
                this.nodes = topConcepts;
                this.sortNodes();

                if (this.pendingSearchPath) {
                  this.expandPath(this.pendingSearchPath);
                }
              }
            );
          }
        }
      );
    } else if (this.visualizationMode == ConceptTreeVisualizationMode.searchBased) {
      //reset list to empty
      this.forceList(null);
    }
  }

  /**
   * Forces the safeness of the structure even if it was reported as not safe, then re initialize it
   */
  forceSafeness() {
    this.safeToGo.safe = true;
    let checksum = this.getInitRequestChecksum();
    this.safeToGoMap[checksum] = this.safeToGo;
    this.initImpl();
  }

  /**
   * Perform a check in order to prevent the initialization of the structure with too many elements.
   * Return true if the initialization is safe or if the user agreed to init the structure anyway
   */
  private checkInitializationSafe(): Observable<void> {
    let conceptTreePreference: ConceptTreePreference = this.settingsMgr.getConceptTreeSettings(SVContext.getProject(this.project));
    if (this.safeToGoLimit != conceptTreePreference.safeToGoLimit) {
      this.safeToGoMap = {}; //limit changed, safetiness checks invalidated => reset the map
    }
    this.safeToGoLimit = conceptTreePreference.safeToGoLimit;

    let checksum = this.getInitRequestChecksum();

    let safeness: SafeToGo = this.safeToGoMap[checksum];
    if (safeness != null) { //found safeness in cache
      this.safeToGo = safeness;
      this.translationParam = { count: this.safeToGo.count, safeToGoLimit: this.safeToGoLimit };
      return of(null);
    } else { //never initialized => count
      this.loading = true;
      return this.skosService.countTopConcepts(this.schemes).pipe(
        mergeMap(count => {
          this.loading = false;
          safeness = { safe: count < this.safeToGoLimit, count: count };
          this.safeToGoMap[checksum] = safeness; //cache the safetyness
          this.safeToGo = safeness;
          this.translationParam = { count: this.safeToGo.count, safeToGoLimit: this.safeToGoLimit };
          return of(null);
        })
      );
    }
  }

  private getInitRequestChecksum() {
    let checksum = "schemes:" + this.schemes.map(s => s.toNT()).join(",");
    return checksum;
  }

  public forceList(list: AnnotatedValue<IRI>[]) {
    this.safeToGo = { safe: true }; //prevent the list not showing if a previous hierarchy initialization set the safeToGo to false
    this.setInitialStatus();
    this.nodes = list;
  }

}
