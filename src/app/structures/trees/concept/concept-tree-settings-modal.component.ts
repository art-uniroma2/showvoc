import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ConceptTreePreference } from 'src/app/models/Properties';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';

@Component({
  selector: 'concept-tree-settings-modal',
  templateUrl: './concept-tree-settings-modal.component.html',
  standalone: false
})
export class ConceptTreeSettingsModalComponent implements OnInit {

  concTreePref: ConceptTreePreference;

  constructor(public activeModal: NgbActiveModal, private settingsMgr: SettingsManager) { }

  ngOnInit() {
    let conceptTreePref: ConceptTreePreference = this.settingsMgr.getConceptTreeSettings(SVContext.getProjectCtx().getProject());
    this.concTreePref = JSON.parse(JSON.stringify(conceptTreePref));
  }

  ok() {
    this.settingsMgr.setConceptTreeSettings(SVContext.getProjectCtx().getProject(), this.concTreePref).subscribe();
    this.activeModal.close();
  }

  close() {
    this.activeModal.dismiss();
  }

}
