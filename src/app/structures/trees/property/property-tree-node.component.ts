import { ChangeDetectorRef, Component, QueryList, ViewChildren } from '@angular/core';
import { map } from 'rxjs/operators';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { SharedModalsServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { IRI } from 'src/app/models/Resources';
import { PropertiesServices } from 'src/app/services/properties.service';
import { STRequestOptions } from 'src/app/utils/HttpManager';
import { SVContext } from 'src/app/utils/SVContext';
import { AbstractTreeNode } from '../abstract-tree-node';

@Component({
  selector: 'property-tree-node',
  templateUrl: './property-tree-node.component.html',
  standalone: false
})
export class PropertyTreeNodeComponent extends AbstractTreeNode<IRI> {

  @ViewChildren(PropertyTreeNodeComponent) viewChildrenNode: QueryList<PropertyTreeNodeComponent>;

  constructor(private propertiesService: PropertiesServices, basicModals: BasicModalsServices, sharedModals: SharedModalsServices, changeDetectorRef: ChangeDetectorRef) {
    super(basicModals, sharedModals, changeDetectorRef);
  }

  /**
   * Implementation of the expansion. It calls the  service for getting the child of a node in the given tree
   */
  expandNodeImpl() {
    return this.propertiesService.getSubProperties(this.node.getValue(), STRequestOptions.getRequestOptions(SVContext.getProject(this.project))).pipe(
      map(properties => {
        return properties;
      })
    );
  }

}
