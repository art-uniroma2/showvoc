import { ChangeDetectorRef, Component, Input, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { SharedModalsServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { AnnotatedValue, IRI, RDFResourceRolesEnum } from 'src/app/models/Resources';
import { PropertiesServices } from 'src/app/services/properties.service';
import { SearchServices } from 'src/app/services/search.service';
import { STRequestOptions } from 'src/app/utils/HttpManager';
import { SVContext } from 'src/app/utils/SVContext';
import { SVEventHandler } from 'src/app/utils/SVEventHandler';
import { AbstractTree } from '../abstract-tree';
import { PropertyTreeNodeComponent } from './property-tree-node.component';

@Component({
  selector: 'property-tree',
  templateUrl: './property-tree.component.html',
  host: { class: "structureComponent" },
  standalone: false
})
export class PropertyTreeComponent extends AbstractTree<IRI> {

  @Input() resource: IRI;//provided to show just the properties with domain the type of the resource
  @Input() type: RDFResourceRolesEnum; //tells the type of the property to show in the tree
  @Input() roots: IRI[]; //in case the roots are provided to the component instead of being retrieved from server

  @ViewChildren(PropertyTreeNodeComponent) viewChildrenNode: QueryList<PropertyTreeNodeComponent>;

  structRole: RDFResourceRolesEnum = RDFResourceRolesEnum.property;

  constructor(private propertyService: PropertiesServices, searchService: SearchServices, basicModals: BasicModalsServices, sharedModals: SharedModalsServices,
    eventHandler: SVEventHandler, changeDetectorRef: ChangeDetectorRef) {
    super(eventHandler, searchService, basicModals, sharedModals, changeDetectorRef);
  }

  /**
   * Called when @Input resource changes, reinitialize the tree
   */
  ngOnChanges(changes: SimpleChanges) {
    /**
     * Initialized the tree only if not the first change. Avoid multiple initialization.
     * The first initialization was already fired in ngOnInit of AbstractStructure
     */
    if ((changes['resource'] && !changes['resource'].isFirstChange()) || (changes['roots'] && !changes['roots'].isFirstChange())) {
      this.init();
    }
  }

  initImpl() {
    /* different cases:
     * - roots provided as Input: tree is build rooted on these properties
     * - roots not provided, Input resource provided: tree roots are those properties that has types of this resource as domain
     * - type provided: initialize tree just for the given property type 
     * - no Input provided: tree roots retrieved from server without restrinction
     */
    if (this.roots) {
      this.loading = true;
      this.propertyService.getPropertiesInfo(this.roots, STRequestOptions.getRequestOptions(SVContext.getProject(this.project))).pipe(
        finalize(() => { this.loading = false; })
      ).subscribe(
        props => {
          this.nodes = props;
          this.sortNodes();

          if (this.pendingSearchPath) {
            this.expandPath(this.pendingSearchPath);
          }
        }
      );
    } else if (this.resource) {
      //at the moment I don't implement this part since it should not be necessary in ShowVoc since it is in readonly
    } else {
      let getPropertiesFn: Observable<AnnotatedValue<IRI>[]>;
      if (this.type == RDFResourceRolesEnum.objectProperty) {
        getPropertiesFn = this.propertyService.getTopObjectProperties(STRequestOptions.getRequestOptions(SVContext.getProject(this.project)));
      } else if (this.type == RDFResourceRolesEnum.annotationProperty) {
        getPropertiesFn = this.propertyService.getTopAnnotationProperties(STRequestOptions.getRequestOptions(SVContext.getProject(this.project)));
      } else if (this.type == RDFResourceRolesEnum.datatypeProperty) {
        getPropertiesFn = this.propertyService.getTopDatatypeProperties(STRequestOptions.getRequestOptions(SVContext.getProject(this.project)));
      } else if (this.type == RDFResourceRolesEnum.ontologyProperty) {
        getPropertiesFn = this.propertyService.getTopOntologyProperties(STRequestOptions.getRequestOptions(SVContext.getProject(this.project)));
      } else if (this.type == RDFResourceRolesEnum.property) {
        getPropertiesFn = this.propertyService.getTopRDFProperties(STRequestOptions.getRequestOptions(SVContext.getProject(this.project)));
      } else {
        getPropertiesFn = this.propertyService.getTopProperties(STRequestOptions.getRequestOptions(SVContext.getProject(this.project)));
      }
      this.loading = true;
      getPropertiesFn.pipe(
        finalize(() => { this.loading = false; })
      ).subscribe(
        props => {
          this.nodes = props;
          this.sortNodes();

          if (this.pendingSearchPath) {
            this.expandPath(this.pendingSearchPath);
          }
        }
      );
    }
  }

}
