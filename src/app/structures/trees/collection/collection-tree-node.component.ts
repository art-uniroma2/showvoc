import { ChangeDetectorRef, Component, QueryList, ViewChildren } from '@angular/core';
import { map } from 'rxjs/operators';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { SharedModalsServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { Resource } from 'src/app/models/Resources';
import { SkosServices } from 'src/app/services/skos.service';
import { AbstractTreeNode } from '../abstract-tree-node';

@Component({
  selector: 'collection-tree-node',
  templateUrl: './collection-tree-node.component.html',
  standalone: false
})
export class CollectionTreeNodeComponent extends AbstractTreeNode<Resource> {

  @ViewChildren(CollectionTreeNodeComponent) viewChildrenNode: QueryList<CollectionTreeNodeComponent>;

  constructor(private skosService: SkosServices, basicModals: BasicModalsServices, sharedModals: SharedModalsServices, changeDetectorRef: ChangeDetectorRef) {
    super(basicModals, sharedModals, changeDetectorRef);
  }

  /**
   * Implementation of the expansion. It calls the  service for getting the child of a node in the given tree
   */
  expandNodeImpl() {
    return this.skosService.getNestedCollections(this.node.getValue()).pipe(
      map(collections => {
        return collections;
      })
    );
  }

}
