import { ChangeDetectorRef, Component, QueryList, ViewChildren } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { SharedModalsServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { RDFResourceRolesEnum, Resource } from 'src/app/models/Resources';
import { SearchServices } from 'src/app/services/search.service';
import { SkosServices } from 'src/app/services/skos.service';
import { SVEventHandler } from 'src/app/utils/SVEventHandler';
import { AbstractTree } from '../abstract-tree';
import { CollectionTreeNodeComponent } from './collection-tree-node.component';

@Component({
    selector: 'collection-tree',
    templateUrl: './collection-tree.component.html',
    host: { class: "structureComponent" },
    standalone: false
})
export class CollectionTreeComponent extends AbstractTree<Resource> {

    @ViewChildren(CollectionTreeNodeComponent) viewChildrenNode: QueryList<CollectionTreeNodeComponent>;

    structRole: RDFResourceRolesEnum = RDFResourceRolesEnum.skosCollection;

    constructor(private skosService: SkosServices, searchService: SearchServices, basicModals: BasicModalsServices, sharedModals: SharedModalsServices, 
        eventHandler: SVEventHandler, changeDetectorRef: ChangeDetectorRef) {
        super(eventHandler, searchService, basicModals, sharedModals, changeDetectorRef);
    }

    initImpl() {
        this.loading = true;
        this.skosService.getRootCollections().pipe(
            finalize(() => { this.loading = false; })
        ).subscribe(
            collections => {
                this.nodes = collections;
                this.sortNodes();

                if (this.pendingSearchPath) {
                    this.expandPath(this.pendingSearchPath);
                }
            }
        );
    }

}
