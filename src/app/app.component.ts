import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import packageJson from '../../package.json';
import { ProjectLabelCtx } from './models/Project';
import { ShowVocUrlParams } from './models/ShowVoc';
import { User } from './models/User';
import { Cookie } from './utils/Cookie';
import { HttpManager } from './utils/HttpManager';
import { LocalStorageManager } from './utils/LocalStorageManager';
import { SVContext } from './utils/SVContext';
import { ToastService } from './widget/toast/toast-service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  host: {
    class: 'd-flex flex-column',
  },
  standalone: false
})
export class AppComponent {

  appVersion = packageJson.version;

  navbarCollapsed: boolean = true;

  currentUser: User;

  translateLangs: string[];
  translateLang: string;

  constructor(
    private translate: TranslateService,
    private toastService: ToastService
  ) {

    LocalStorageManager.updateRoutine(this.appVersion);

    //set the available factory-provided l10n languages
    translate.addLangs(['de', 'en', 'es', 'fr', 'it']);
    //add additional supported l10n languages
    let additionalLangs: string[] = window['additional_l10n_langs'];
    if (additionalLangs.length > 0) {
      translate.addLangs(additionalLangs);
    }
    //fallback when a translation isn't found in the current language
    translate.setDefaultLang('en');
    //restore the lang to use, check first the cookies, if not found, set english by default
    let transLang: string = Cookie.getTranslationLangCookie();
    if (transLang == null || !translate.getLangs().includes(transLang)) {
      transLang = "en";
    }
    this.changeLang(transLang);

  }

  ngOnInit() {
    this.translateLangs = this.translate.getLangs();
    this.translateLang = this.translate.currentLang;
  }

  isNavVisible(): boolean {
    /**
     * Adopted this solution because (I don't know why) after this fix
     * https://bitbucket.org/art-uniroma2/showvoc/commits/78ec4b68001acabac54b30dde66846d8054ce8bd
     * activatedRoute.queryParams.subscribe is not fired anymore for hideNav parameter 
     * when user accesses a nested route (e.g. http://localhost:4200/#/datasets/Teseo/data?hideNav=true)
     * This is the only solution I found without breaking the CustomReuseStrategy logic
     */

    if (this.isLogged()) {
      return true;
    } else {
      let hideNav = false;
      const queryString = window.location.href.split("?")[1]; //split the URL
      if (queryString) {
        hideNav = new URLSearchParams(queryString).get(ShowVocUrlParams.hideNav) == "true";
      }
      return !hideNav;
    }
  }

  /**
   * Determines if the items in the navbar are available: they are available only if the admin or the visitor user is logged
   */
  isLogged(): boolean {
    this.currentUser = SVContext.getLoggedUser();
    return this.currentUser != null;
  }

  isAnonymousAllowed() {
    return SVContext.getSystemSettings()?.allowAnonymous;
  }

  changeLang(lang: string) {
    this.translateLang = lang;
    this.translate.use(this.translateLang);
    Cookie.setTranslationLangCookie(this.translateLang, { path: "/" });

    ProjectLabelCtx.language = this.translateLang; //init lang for datasets rendering
  }

  copyWebApiUrl() {
    let baseUrl = HttpManager.getServerHost() + "/" + HttpManager.serverpath + "/" + HttpManager.groupId + "/" + HttpManager.artifactId + "/";
    navigator.clipboard.writeText(baseUrl).then(
      () => {
        this.toastService.show(null, { key: "APP.FOOTER.WEB_API_COPIED" }, { toastClass: "bg-dark", textClass: "text-white" });
      },
      () => { }
    );
  }

}