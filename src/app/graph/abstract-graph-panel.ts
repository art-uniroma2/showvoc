import { Directive, Input } from "@angular/core";
import FileSaver from 'file-saver';
import { BasicModalsServices } from '../modal-dialogs/basic-modals/basic-modals.service';
import { BrowsingModalsServices } from '../modal-dialogs/browsing-modals/browsing-modals.service';
import { AbstractGraph } from './abstract-graph';
import { ForceDirectedGraph, GraphForces } from "./model/ForceDirectedGraph";
import { Link } from "./model/Link";
import { Node } from "./model/Node";

@Directive()
export abstract class AbstractGraphPanel<N extends Node, L extends Link<N>> {
  @Input() graph: ForceDirectedGraph<N, L>;
  @Input() rendering: boolean = true;

  abstract viewChildGraph: AbstractGraph<N, L>;

  selectedNode: N;
  selectedLink: L;
  isLock: boolean = false;
  forces: GraphForces;

  protected basicModals: BasicModalsServices;
  protected browsingModals: BrowsingModalsServices;
  constructor(basicModals: BasicModalsServices, browsingModals: BrowsingModalsServices) {
    this.basicModals = basicModals;
    this.browsingModals = browsingModals;
    this.forces = new GraphForces();
  }

  abstract addNode(): void;

  onForceChange() {
    this.viewChildGraph.updateForces(this.forces);
  }

  fixNode() {
    this.selectedNode.fixed = !this.selectedNode.fixed;
    if (!this.selectedNode.fixed) {
      this.selectedNode.fx = null;
      this.selectedNode.fy = null;
    }
  }

  fixAll() {
    this.graph.getNodes().forEach(n => {
      n.fixed = true;
      n.fx = n.x;
      n.fy = n.y;
    });
    this.isLock = true;
  }

  unfixAll() {
    this.graph.getNodes().forEach(n => {
      n.fixed = false;
      n.fx = null;
      n.fy = null;
    });
    this.isLock = false;
  }

  onNodeSelected(node: N) {
    this.selectedLink = null;
    this.selectedNode = node;
  }

  onLinkSelected(link: L) {
    this.selectedNode = null;
    this.selectedLink = link;
  }

  snapshot() {
    let exportUrl = this.viewChildGraph.getExportUrl();
    FileSaver.saveAs(exportUrl, "graph.svg");
  }

}