import { ChangeDetectorRef, Directive, ElementRef, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { BasicModalsServices } from '../modal-dialogs/basic-modals/basic-modals.service';
import { AnnotatedValue, IRI } from '../models/Resources';
import { CommonsUtils } from '../utils/CommonsUtils';
import { D3Service } from './d3/d3.services';
import { ForceDirectedGraph, GraphForces, GraphOptions } from "./model/ForceDirectedGraph";
import { Link } from "./model/Link";
import { Node } from "./model/Node";

@Directive()
export abstract class AbstractGraph<N extends Node, L extends Link<N>> {
  @Input() graph: ForceDirectedGraph<N, L>;
  @Input() fixed: boolean;
  @Input() rendering: boolean;
  @Output() nodeSelected = new EventEmitter<N>();
  @Output() linkSelected = new EventEmitter<L>();

  @ViewChild('svg') public svgElement: ElementRef<SVGSVGElement>;

  selectedNode: N;
  selectedLink: L;
  protected linkAhead: L; //link selected to bring ahead the other

  initialized: boolean = false; //true once the graph will be initialized (useful in order to not render the graph view until then)

  protected abstract mode: GraphMode;
  protected options: GraphOptions = new GraphOptions(800, 400); //initial w & h (will be updated later)

  constructor(protected d3Service: D3Service, protected elementRef: ElementRef, protected ref: ChangeDetectorRef, protected basicModals: BasicModalsServices) { }

  //In ngAfterViewInit instead of ngOnInit since I need offsetWidth and offsetHeight that are fixed only once the view is initialized
  ngAfterViewInit() {
    this.initSimulation();
  }

  private initSimulation() {
    this.options.width = this.elementRef.nativeElement.offsetWidth;
    this.options.height = this.elementRef.nativeElement.offsetHeight;

    this.graph.ticker.subscribe((_: any) => {
      this.ref.markForCheck();
    });
    this.graph.initSimulation(this.options);
    this.initialized = true;
  }

  /**
   * Useful to re-init the simulation of the graph when the Input ForceDirectedGraph changes
   */
  public forceInitSimulation() {
    this.initSimulation();
  }

  protected onNodeClicked(node: N) {
    this.selectedLink = null;
    this.linkAhead = null;
    if (node == this.selectedNode) {
      this.selectedNode = null;
    } else {
      this.selectedNode = node;
    }
    // this.elementSelected.emit(this.selectedNode);
    this.nodeSelected.emit(this.selectedNode);
  }

  protected onLinkClicked(link: L) {
    this.selectedNode = null;
    if (link == this.selectedLink) {
      this.selectedLink = null;
    } else {
      this.selectedLink = link;
    }
    this.linkAhead = this.selectedLink;
    // this.elementSelected.emit(this.selectedLink);
    this.linkSelected.emit(this.selectedLink);
  }

  protected abstract onNodeDblClicked(node: N): void;

  protected abstract addNode(res: AnnotatedValue<IRI>): void;

  protected abstract expandNode(node: N, selectOnComplete?: boolean): void;

  protected abstract closeNode(node: N): void;



  onBackgroundClicked() {
    this.selectedNode = null;
    this.selectedLink = null;
    this.linkAhead = null;
    // this.elementSelected.emit(null);
    this.nodeSelected.emit(null);
    this.linkSelected.emit(null);
  }

  public updateForces(forces: GraphForces) {
    this.graph.options.forces = forces;
    this.graph.updateForces();
  }

  public getExportUrl(): string {
    let svgDom: SVGSVGElement = this.svgElement.nativeElement;
    let clonedSvg: SVGSVGElement = svgDom.cloneNode(true) as SVGSVGElement;

    ExportGraphUtils.processDom(clonedSvg, svgDom);

    let serializer = new XMLSerializer();
    let source = serializer.serializeToString(clonedSvg);
    source = '<?xml version="1.0" standalone="no"?>\r\n' + source;//add xml declaration
    source = source.replace(/<!--[\s\S]*?-->/g, "");//remove comments

    let url = "data:image/svg+xml;charset=utf-8," + encodeURIComponent(source); //convert svg source to URI data scheme.
    return url;
  }

  protected initNodePosition(node: N, seedNode?: N) {
    if (this.fixed) {
      node.fixed = true;
      /**
       * Currently the following approach is adopted: 
       * it is considered the seedNode (if provided) or the first node of the graph (whatever it is) and if exists,
       * set the same fixed coordinates slightly shifted.
       */
      if (!seedNode) {
        seedNode = this.graph.getNodes()[0];
      }
      if (seedNode) {
        let distance = 150;
        const angle = CommonsUtils.cryptoRandom() * 2 * Math.PI; // Random angle between 0 and 2*pi
        const xOffset = Math.cos(angle) * distance; // Calculate x offset based on distance and angle
        const yOffset = Math.sin(angle) * distance; // Calculate y offset based on distance and angle
        const newX = seedNode.x + xOffset; // Calculate new x coordinate
        const newY = seedNode.y + yOffset; // Calculate new y coordinate
        node.fx = newX;
        node.fy = newY;
      } else { //no seed provider, neither exists a node in the graph => place the node in a random position
        let min = 40;
        let max = 120;
        let xShift = CommonsUtils.getCryptoStrongRandomInt(min, max);
        let yShift = CommonsUtils.getCryptoStrongRandomInt(min, max);
        node.fx = xShift;
        node.fy = yShift;
      }
    }
  }

}

export enum GraphMode {
  dataOriented = "dataOriented",
  modelOriented = "modelOriented",
  umlOriented = "umlOriented"
}


class ExportGraphUtils {

  private static svgElementToProcess: string[] = ["svg", "path", "rect", "circle", "stop", "text"];
  private static relevantStyleForElement: { [tag: string]: string[] } = {
    svg: ["background-color", "width", "height"],
    path: ["fill", "stroke", "stroke-width"],
    rect: ["fill", "stroke", "stroke-width"],
    circle: ["fill", "stroke", "stroke-width"],
    stop: ["stop-color"],
    text: ["font", "stroke", "stroke-width"]
  };

  public static processDom(targetEl: SVGSVGElement, sourceEl: SVGSVGElement) {
    if (this.svgElementToProcess.indexOf(targetEl.tagName) != -1) { //if the element is one of the processingTagNames
      this.copyStyleAsInline(targetEl, sourceEl); //compy the computed style of the source as inline style in the target
    }
    this.removeNgAttributes(targetEl);
    let targetChildren: HTMLCollection = targetEl.children;
    let sourceChildren: HTMLCollection = sourceEl.children;
    for (let i = 0; i < targetChildren.length; i++) {
      let targetChildEl: SVGSVGElement = targetChildren.item(i) as SVGSVGElement;
      let sourceChildEl: SVGSVGElement = sourceChildren.item(i) as SVGSVGElement;
      this.processDom(targetChildEl, sourceChildEl);
    }
  }

  private static copyStyleAsInline(targetEl: SVGSVGElement, sourceEl: SVGSVGElement) {
    let sourceElComputedStyle: CSSStyleDeclaration = getComputedStyle(sourceEl); //for the the computed style
    let stylesForElement = this.relevantStyleForElement[sourceEl.tagName];
    stylesForElement.forEach(stylePropName => {
      let stylePropValue = sourceElComputedStyle.getPropertyValue(stylePropName); //get only the relevant css properties
      if (stylePropValue) {
        targetEl.style[stylePropName] = stylePropValue; //and set inline
      }
    });
  }

  private static removeNgAttributes(el: SVGSVGElement) {
    let attrs: NamedNodeMap = el.attributes;
    for (let i = attrs.length - 1; i >= 0; i--) {
      let attr = attrs.item(i).name;
      if (attr.startsWith("_ng") || attr.startsWith("ng-")) {
        el.removeAttribute(attr);
      }
    }
  }

}