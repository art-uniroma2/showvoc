import { Directive, ElementRef, Input } from '@angular/core';
import { ForceDirectedGraph } from '../model/ForceDirectedGraph';
import { Link } from '../model/Link';
import { Node } from '../model/Node';
import { D3Service } from './d3.services';

@Directive({
  selector: '[draggableNode]',
  standalone: false
})
export class DraggableDirective {
  @Input() draggableNode: Node;
  @Input() draggableInGraph: ForceDirectedGraph<Node, Link<Node>>;

  constructor(private d3Service: D3Service, private _element: ElementRef) { }

  ngOnInit() {
    this.d3Service.applyDraggableBehaviour(this._element.nativeElement, this.draggableNode, this.draggableInGraph);
  }
}