import { Directive, ElementRef, Input } from '@angular/core';
import { D3Service } from './d3.services';

@Directive({
  selector: '[zoomableOf]',
  standalone: false
})
export class ZoomableDirective {
  @Input() zoomableOf: SVGSVGElement;

  constructor(private d3Service: D3Service, private _element: ElementRef) { }

  ngOnInit() {
    this.d3Service.applyZoomableBehaviour(this.zoomableOf, this._element.nativeElement);
  }
}