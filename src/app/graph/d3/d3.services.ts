import { Injectable } from '@angular/core';
import * as d3 from 'd3';
import { ForceDirectedGraph } from '../model/ForceDirectedGraph';
import { Link } from '../model/Link';
import { Node } from '../model/Node';

@Injectable()
export class D3Service {

  constructor() { }

  /** 
   * A method to bind a pan and zoom behaviour to an svg element 
   */
  applyZoomableBehaviour(svgElement: SVGSVGElement, containerElement: any) {
    let svg: d3.Selection<any, any, null, undefined>;
    let container: d3.Selection<any, any, null, undefined>;
    let zoom: d3.ZoomBehavior<SVGSVGElement, any>;

    svg = d3.select(svgElement);
    container = d3.select(containerElement);

    const zoomed = (event: d3.D3ZoomEvent<Element, any>) => {
      const transform = event.transform;
      container.attr("transform", "translate(" + transform.x + "," + transform.y + ") scale(" + transform.k + ")");
    };

    zoom = d3.zoom<SVGSVGElement, unknown>()
      .on("zoom", zoomed);
    svg.call(zoom);

    svg.on("dblclick.zoom", null); //disable zoom on double click
  }

  /**
   * Gets an instance of the ForceDirectedGraph with the given nodes and links
   * @param nodes 
   * @param links 
   * @param dynamic tells if the graph is dynamic, if double click expands/collapses nodes
   */
  getForceDirectedGraph(nodes: Node[], links: Link<Node>[], dynamic?: boolean): ForceDirectedGraph<Node, Link<Node>> {
    if (dynamic == null) dynamic = true;
    let graph = new ForceDirectedGraph(nodes, links, dynamic);
    return graph;
  }

  /**
   * A method to bind a draggable behaviour to an svg element 
   */
  applyDraggableBehaviour(element: HTMLElement, node: Node, graph: ForceDirectedGraph<Node, Link<Node>>) {
    const d3element = d3.select(element);

    const dragged = (event: d3.D3DragEvent<HTMLElement, any, any>) => {
      node['fx'] = event.x;
      node['fy'] = event.y;
    };

    const ended = (event: d3.D3DragEvent<HTMLElement, any, any>) => {
      if (!event.active) {
        graph.simulation.alphaTarget(0);
      }
      if (!node.fixed) {
        node['fx'] = null;
        node['fy'] = null;
      }
    };

    const started = (event: d3.D3DragEvent<HTMLElement, any, any>) => {
      /** Preventing propagation of dragstart to parent elements */
      event.sourceEvent.stopPropagation();

      if (!event.active) {
        graph.simulation.alphaTarget(0.3).restart();
      }

      event.on("drag", dragged).on("end", ended);
    };

    d3element.call(d3.drag<HTMLElement, any>()
      .on("start", started));
  }

}