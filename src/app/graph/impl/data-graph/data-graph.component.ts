import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Output } from "@angular/core";
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { GraphModelRecord } from 'src/app/models/Graphs';
import { ValueFilterLanguages } from 'src/app/models/Properties';
import { AnnotatedValue, IRI, Literal, PredicateObjects, ResAttribute, Resource, Value } from 'src/app/models/Resources';
import { ResViewSection, ResViewUtils } from 'src/app/models/ResourceView';
import { GraphServices } from 'src/app/services/graph.service';
import { ResourceViewServices } from 'src/app/services/resource-view.service';
import { ResourceDeserializer } from 'src/app/utils/ResourceUtils';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { AbstractGraph, GraphMode } from '../../abstract-graph';
import { D3Service } from '../../d3/d3.services';
import { GraphModalServices } from '../../modals/graph-modal.service';
import { DataLink } from '../../model/DataLink';
import { DataNode } from '../../model/DataNode';

@Component({
  selector: 'data-graph',
  templateUrl: "./data-graph.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['../../graph.css'],
  standalone: false
})
export class DataGraphComponent extends AbstractGraph<DataNode, DataLink> {

  @Output() elementSelected = new EventEmitter<DataNode | DataLink>();

  protected mode = GraphMode.dataOriented;

  private linkLimit: number = 50;

  private rvSections: ResViewSection[] = ResViewUtils.orderedResourceViewSections;

  constructor(protected d3Service: D3Service, protected elementRef: ElementRef, protected ref: ChangeDetectorRef, protected basicModals: BasicModalsServices,
    private resViewService: ResourceViewServices, private graphService: GraphServices, private graphModals: GraphModalServices, private settingsMgr: SettingsManager) {
    super(d3Service, elementRef, ref, basicModals);
  }

  ngOnInit() {
    this.expandNode(this.graph.getNodes()[0], true);
  }

  addNode(res: AnnotatedValue<IRI>) {
    if (this.graph.getNode(res.getValue())) {
      this.basicModals.alert({ key: "GRAPHS.ACTIONS.ADD_NODE" }, { key: "MESSAGES.RESOURCE_ALREADY_IN_GRAPH" }, ModalType.warning);
      return;
    }

    let node: DataNode = new DataNode(res);
    node.root = true;
    this.initNodePosition(node);
    this.graph.addNode(node);
    this.expandNode(node, true);
  }

  expandSub() {
    let nodeRes: AnnotatedValue<Value> = this.selectedNode.res;
    this.graphService.expandSubResources(nodeRes.getValue() as IRI, nodeRes.getRole()).subscribe(
      (graphModel: GraphModelRecord[]) => {
        let links: DataLink[] = this.convertModelToLinks(graphModel);
        this.appendLinks(this.selectedNode, links);
      }
    );
  }

  expandSuper() {
    let nodeRes: AnnotatedValue<Value> = this.selectedNode.res;
    this.graphService.expandSuperResources(nodeRes.getValue() as IRI, nodeRes.getRole()).subscribe(
      (graphModel: GraphModelRecord[]) => {
        let links: DataLink[] = this.convertModelToLinks(graphModel);
        this.appendLinks(this.selectedNode, links);
      }
    );
  }

  protected expandNode(node: DataNode, selectOnComplete?: boolean) {
    let value: Value = node.res.getValue();
    if (value instanceof Resource) {
      this.resViewService.getResourceView(value).subscribe(
        rv => {
          let filteredSections: ResViewSection[] = this.settingsMgr.getGraphSectionFilter(SVContext.getProject())[(node.res).getRole()];
          //create the predicate-object lists for each section (skip the filtered section)
          let predObjListMap: { [section: string]: PredicateObjects[] } = {};
          this.rvSections.forEach(section => {
            if (filteredSections != null && filteredSections.indexOf(section) != -1) {
              return; //if the current section is among the sections filtered for the resource role => skip the parsing
            }
            //parse section
            let sectionJson = rv[section];
            if (section == ResViewSection.facets && sectionJson != null) { //dedicated handler for facets section
              sectionJson = sectionJson.inverseOf;
            }
            if (sectionJson != null) {
              let poList: PredicateObjects[] = ResourceDeserializer.createPredicateObjectsList(sectionJson);
              predObjListMap[section] = poList;
            }
          });
          //filter the objects according the filter-value languages
          for (let section in predObjListMap) {
            this.filterValueLanguageFromPrefObjList(predObjListMap[section]);
          }
          //count number of objects
          let linkCount: number = 0;
          for (let section in predObjListMap) {
            predObjListMap[section].forEach(pol => { //for each pol of a section
              linkCount += pol.getObjects().length; //a link for each object (subject ---predicate---> object)
            });
          }
          //check if the relations that it is going to add are too much
          if (linkCount > this.linkLimit) {
            this.graphModals.filterLinks(predObjListMap).then(
              (predicatesToHide: IRI[]) => {
                let links: DataLink[] = this.convertPredObjListMapToLinks(node, predObjListMap, predicatesToHide);
                this.appendLinks(node, links);
                if (selectOnComplete) {
                  this.onNodeClicked(node);
                }
              },
              () => { }
            );
          } else {
            let links: DataLink[] = this.convertPredObjListMapToLinks(node, predObjListMap, []);
            this.appendLinks(node, links);
            if (selectOnComplete) {
              this.onNodeClicked(node);
            }
          }
        }
      );
      node.open = true;
    }
  }

  protected closeNode(node: DataNode) {
    this.deleteSubtree(node);

    let sourceDataNode = node;
    if (sourceDataNode.isPending()) {
      this.graph.removeNode(sourceDataNode); //remove the node from the graph
    }

    this.graph.update();
    node.open = false;
  }


  private appendLinks(expandedNode: DataNode, links: DataLink[]) {
    links.forEach(l => {
      if (this.retrieveLink(l.source.res.getValue(), l.target.res.getValue(), l.res.getValue())) {
        return;
      }

      let sourceNode = this.retrieveNode(l.source);
      l.source = sourceNode;
      let targetNode = this.retrieveNode(l.target);
      l.target = targetNode;

      if (expandedNode == sourceNode) {
        targetNode.openBy.push(expandedNode);
      }

      this.graph.addLink(l);
    });

    this.graph.update();
  }


  /**
   * Delete the subtree rooted on the given node. Useful when closing a node.
   * @param node 
   */
  private deleteSubtree(node: DataNode) {
    let recursivelyClosingNodes: DataNode[] = []; //nodes target in the removed links that needs to be closed in turn
    let linksFromNode: DataLink[] = this.graph.getLinksFrom(node);
    if (linksFromNode.length > 0) {
      //removes links with the node as source
      linksFromNode.forEach(l => {
        //remove the source node from the openBy nodes of the target
        let targetDataNode = l.target;
        targetDataNode.removeOpenByNode(l.source);
        //remove the link
        this.graph.removeLink(l);
        //if now the openBy list of the target is empty, it means that the node would be detached from the graph
        if (targetDataNode.isPending()) {
          this.graph.removeNode(targetDataNode); //remove the node from the graph
          recursivelyClosingNodes.push(targetDataNode); //add to the list of nodes to recursively close
        }
      });
      //call recursively the deletion of the subtree for the deleted node)
      recursivelyClosingNodes.forEach(n => {
        this.deleteSubtree(n);
      });
    }
  }

  /* ================== UTILS ================== */

  /**
   * Converts the map between sections and predicate-objects lists to a list of links.
   * Filters the convertion according the list of predicates to hide
   * @param sourceNode source node of the links
   * @param predObjListMap 
   * @param predicatesToHide 
   */
  private convertPredObjListMapToLinks(sourceNode: DataNode, predObjListMap: { [section: string]: PredicateObjects[] }, predicatesToHide: IRI[]): DataLink[] {
    let hideLiteralNodes: boolean = this.settingsMgr.getHideLiteralGraphNodes(SVContext.getProject());
    let links: DataLink[] = [];
    for (let section in predObjListMap) {
      predObjListMap[section].forEach(pol => { //for each pol of a section
        let pred: AnnotatedValue<IRI> = pol.getPredicate();
        if (predicatesToHide.find(p => pred.getValue().equals(p)) == null) {
          let objs: AnnotatedValue<Value>[] = pol.getObjects();
          objs.forEach(o => { //for each object/value
            if (hideLiteralNodes && o.getValue() instanceof Literal) {
              return; //if the literal should be hidden and the object is literal, skip the link
            }
            links.push(new DataLink(sourceNode, new DataNode(o), pred));
          });
        }
      });
    }
    return links;
  }

  /**
   * Converts the GraphModelRecord(s) (returned by getGraphModel() and expandGraphModelNode() services) into a list of nodes and links
   */
  private convertModelToLinks(graphModel: GraphModelRecord[]): DataLink[] {
    let links: DataLink[] = [];
    //set the nodes and the links according the model
    graphModel.forEach(record => {
      let nodeSource: DataNode = new DataNode(record.source);
      let nodeTarget: DataNode = new DataNode(record.target);
      links.push(new DataLink(nodeSource, nodeTarget, record.link));
    });
    return links;
  }

  /**
   * Filters the objects list according the value-filter languages
   * @param predObjList 
   */
  private filterValueLanguageFromPrefObjList(predObjList: PredicateObjects[]) {
    let valueFilterLanguage: ValueFilterLanguages = this.settingsMgr.getValueFilterLanguages(SVContext.getProject());
    if (valueFilterLanguage.enabled) {
      let valueFilterLanguages = valueFilterLanguage.languages;
      for (let i = 0; i < predObjList.length; i++) {
        let objList: AnnotatedValue<Value>[] = predObjList[i].getObjects();
        for (let j = 0; j < objList.length; j++) {
          let lang = objList[j].getAttribute(ResAttribute.LANG);
          //remove the object if it has a language not in the languages list of the filter
          if (lang != null && valueFilterLanguages.indexOf(lang) == -1) {
            objList.splice(j, 1);
            j--;
          }
        }
        //after filtering the objects list, if the predicate has no more objects, remove it from predObjList
        if (objList.length == 0) {
          predObjList.splice(i, 1);
          i--;
        }
      }
    }
  }

  private retrieveNode(node: DataNode): DataNode {
    let graphNode: DataNode;
    graphNode = this.graph.getNode(node.res.getValue());
    if (graphNode == null) { //node for the given resource not yet created => create it and add to the graph
      graphNode = node;
      this.initNodePosition(node);
      this.graph.addNode(graphNode);
    }
    return graphNode;
  }

  private retrieveLink(source: Value, target: Value, pred: IRI): DataLink {
    let links: DataLink[] = this.graph.getLinks();
    for (const l of links) {
      if (l.source.res.getValue().equals(source) && l.target.res.getValue().equals(target) && l.res.getValue().equals(pred)) {
        return l;
      }
    }
    return null;
  }

  /* ================== EVENT HANDLER ================== */

  protected onNodeDblClicked(node: DataNode) {
    if (!this.graph.dynamic) return; //if graph is not dynamic, do nothing
    if (node.open) {
      this.closeNode(node);
    } else {
      this.expandNode(node);
    }
  }

}