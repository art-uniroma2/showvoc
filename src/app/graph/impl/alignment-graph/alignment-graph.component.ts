import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, SimpleChanges } from "@angular/core";
import { from, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { LinksetMetadata } from 'src/app/models/Metadata';
import { Project } from "src/app/models/Project";
import { AnnotatedValue, IRI, Value } from 'src/app/models/Resources';
import { MetadataRegistryServices } from 'src/app/services/metadata-registry.service';
import { AbstractGraph, GraphMode } from '../../abstract-graph';
import { D3Service } from '../../d3/d3.services';
import { AlignmentLink } from '../../model/AlignmentLink';
import { AlignmentNode } from '../../model/AlignmentNode';

@Component({
  selector: 'alignment-graph',
  templateUrl: "./alignment-graph.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['../../graph.css'],
  standalone: false
})
export class AlignmentGraphComponent extends AbstractGraph<AlignmentNode, AlignmentLink> {

  @Input() showPercentage: boolean;

  protected mode = GraphMode.dataOriented;

  private linkLimit: number = 50;

  constructor(protected d3Service: D3Service, protected elementRef: ElementRef, protected ref: ChangeDetectorRef, protected basicModals: BasicModalsServices,
    private metadataRegistryService: MetadataRegistryServices) {
    super(d3Service, elementRef, ref, basicModals);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['graph']) {
      this.expandNode(this.graph.getNodes()[0], true); //immediately expand the root node (source project selected in the alignment page)
      //if the graph changes (not for the first time), reinitialize the simulation. The first time it is initialized in ngAfterViewInit of the graph
      if (!changes['graph'].firstChange) {
        this.forceInitSimulation();
      }
    }
  }

  //Required by the extended class, but not implemented in this kind of graph
  addNode(_res: AnnotatedValue<IRI>) { }


  protected expandNode(node: AlignmentNode, selectOnComplete?: boolean) {
    this.getLinksets(node, null).subscribe(
      linksets => {
        this.addtLinksetToGraph(node, linksets);
        if (selectOnComplete) {
          this.onNodeClicked(node);
        }
        node.open = true;
      }
    );
  }

  private getLinksets(node: AlignmentNode, treshold?: number): Observable<LinksetMetadata[]> {
    let sourceProj: Project;
    let projName = node.target.projectName;
    if (projName) {
      sourceProj = new Project(projName);
    }
    return this.metadataRegistryService.getEmbeddedLinksets(node.res.getValue(), sourceProj, treshold, true).pipe(
      mergeMap(linksets => {
        if (linksets.length > this.linkLimit) {
          return from(
            this.basicModals.promptNumber({ key: "GRAPHS.ACTIONS.EXPAND_LINKSET" }, { key: "MESSAGES.TOO_MUCH_LINKSETS_FILTER", params: { linksetCount: linksets.length } },
              treshold, 0, null, 1, ModalType.warning).then(
                numb => { return numb; },
                () => { return null; } //user cancel the expanding action
              )
          ).pipe(
            mergeMap(treshold => {
              if (treshold) {
                return this.getLinksets(node, treshold);
              } else { //treshold null => in the preview dialog, user canceled action, return empty linksets
                return of([]);
              }
            })
          );
        } else {
          return of(linksets);
        }
      })
    );
  }

  private addtLinksetToGraph(node: AlignmentNode, linksets: LinksetMetadata[]) {
    let links: AlignmentLink[] = []; //links to open when double clicking on the given node
    linksets.forEach(l => {
      let targetNodeValue: AnnotatedValue<IRI> = this.getTargetDatasetAnnotatedIRI(l);
      let targetNode: AlignmentNode = new AlignmentNode(targetNodeValue, l.getRelevantTargetDataset(), l.registeredTargets.length > 0);
      let link: AlignmentLink = new AlignmentLink(node, targetNode, l);
      links.push(link);
    });
    this.appendLinks(node, links);
  }

  /**
  * Returns an annotated IRI representing the target dataset
  */
  private getTargetDatasetAnnotatedIRI(linkset: LinksetMetadata): AnnotatedValue<IRI> {
    let targetDataset = linkset.getRelevantTargetDataset();
    let annotatedValue: AnnotatedValue<IRI> = new AnnotatedValue<IRI>(targetDataset.dataset);
    return annotatedValue;
  }

  protected closeNode(node: AlignmentNode) {
    this.deleteSubtree(node);

    let sourceNode = node;
    if (sourceNode.isPending()) {
      this.graph.removeNode(sourceNode); //remove the node from the graph
    }

    this.graph.update();
    node.open = false;
  }


  private appendLinks(expandedNode: AlignmentNode, links: AlignmentLink[]) {
    links.forEach(l => {
      if (this.retrieveLink(l.source.res.getValue(), l.target.res.getValue()) != null) {
        return;
      }

      let sourceNode = this.retrieveNode(l.source);
      l.source = sourceNode;
      let targetNode = this.retrieveNode(l.target);
      l.target = targetNode;

      if (expandedNode == sourceNode) {
        targetNode.openBy.push(expandedNode);
      }

      this.graph.addLink(l);
    });

    this.graph.update();
  }


  /**
   * Delete the subtree rooted on the given node. Useful when closing a node.
   * @param node 
   */
  private deleteSubtree(node: AlignmentNode) {
    let recursivelyClosingNodes: AlignmentNode[] = []; //nodes target in the removed links that needs to be closed in turn
    let linksFromNode: AlignmentLink[] = this.graph.getLinksFrom(node);
    if (linksFromNode.length > 0) {
      //removes links with the node as source
      linksFromNode.forEach(l => {
        //remove the source node from the openBy nodes of the target
        let targetNode = l.target;
        targetNode.removeOpenByNode(l.source);
        //remove the link
        this.graph.removeLink(l);
        //if now the openBy list of the target is empty, it means that the node would be detached from the graph
        if (targetNode.isPending()) {
          this.graph.removeNode(targetNode); //remove the node from the graph
          recursivelyClosingNodes.push(targetNode); //add to the list of nodes to recursively close
        }
      });
      //call recursively the deletion of the subtree for the deleted node)
      recursivelyClosingNodes.forEach(n => {
        this.deleteSubtree(n);
      });
    }
  }

  /* ================== UTILS ================== */

  private retrieveNode(node: AlignmentNode): AlignmentNode {
    let graphNode: AlignmentNode;
    graphNode = this.graph.getNode(node.res.getValue());
    if (graphNode == null) { //node for the given resource not yet created => create it and add to the graph
      graphNode = node;
      this.graph.addNode(graphNode);
    }
    return graphNode;
  }

  private retrieveLink(source: Value, target: Value): AlignmentLink {
    let links: AlignmentLink[] = this.graph.getLinks();
    for (const l of links) {
      if (l.source.res.getValue().equals(source) && l.target.res.getValue().equals(target)) {
        return l;
      }
    }
    return null;
  }


  /* ================== EVENT HANDLER ================== */

  protected onNodeDblClicked(node: AlignmentNode) {
    if (!this.graph.dynamic) return; //if graph is not dynamic, do nothing
    if (node.open) {
      this.closeNode(node);
    } else {
      this.expandNode(node);
    }
  }

}