import { Component, Input, SimpleChanges, ViewChild } from "@angular/core";
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { BrowsingModalsServices } from 'src/app/modal-dialogs/browsing-modals/browsing-modals.service';
import { Target } from 'src/app/models/Metadata';
import { Project, ProjectLabelCtx } from 'src/app/models/Project';
import { AnnotatedValue, IRI, Literal } from 'src/app/models/Resources';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { AbstractGraphPanel } from '../../abstract-graph-panel';
import { AlignmentLink } from '../../model/AlignmentLink';
import { AlignmentNode } from '../../model/AlignmentNode';
import { ForceDirectedGraph } from '../../model/ForceDirectedGraph';
import { AlignmentGraphComponent } from './alignment-graph.component';

@Component({
  selector: 'alignment-graph-panel',
  templateUrl: "./alignment-graph-panel.component.html",
  host: { class: "vbox" },
  standalone: false
})
export class AlignmentGraphPanelComponent extends AbstractGraphPanel<AlignmentNode, AlignmentLink> {

  @Input() sourceProject: Project;
  @Input() dataset: AnnotatedValue<IRI>;

  @ViewChild(AlignmentGraphComponent) viewChildGraph: AlignmentGraphComponent;

  showPercentage: boolean = false;

  constructor(basicModals: BasicModalsServices, browsingModals: BrowsingModalsServices, private settingsMgr: SettingsManager) {
    super(basicModals, browsingModals);
  }

  ngOnInit() {
    this.rendering = this.settingsMgr.getProjectRendering();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['dataset']) {
      let nodes: AlignmentNode[] = [];
      let links: AlignmentLink[] = [];

      //Target mockup of the source project 
      let target: Target = {
        dataset: this.dataset.getValue(),
        projectName: this.sourceProject.getName(),
        titles: [new Literal(this.sourceProject.getLabel(), ProjectLabelCtx.language)],
        uriSpace: this.sourceProject.getBaseURI()
      };

      //boolean param (isRegisteredDataset) true because, by construction, the graph is rooted on an existing dataset in ST (selected from a dropdown in AlignmentsComponent)
      let sourceNode: AlignmentNode = new AlignmentNode(this.dataset, target, true);
      sourceNode.root = true;
      nodes.push(sourceNode);

      //reinit the graph
      this.graph = new ForceDirectedGraph(nodes, links, true);
    }
  }

  //Required by the extended class but not implemented in this kind of graph
  addNode() { }

}