import { Component, Input } from '@angular/core';
import { AlignmentsModalsServices } from 'src/app/alignments/modals/alignments-modal.service';
import { Project } from 'src/app/models/Project';
import { Literal } from 'src/app/models/Resources';
import { AlignmentLink } from '../../model/AlignmentLink';

@Component({
  selector: 'alignment-details-panel',
  templateUrl: './alignment-details-panel.component.html',
  host: { class: "vbox" },
  standalone: false
})
export class AlignmentDetailsPanelComponent {

  @Input() alignmentLink: AlignmentLink;
  @Input() rendering: boolean;

  sourceProject: Project;

  constructor(private alignmentModals: AlignmentsModalsServices) { }

  ngOnInit() {
    this.sourceProject = new Project(this.alignmentLink.source.target.projectName);
    if (this.alignmentLink.source.target.titles.length > 0) {
      let title: Literal = this.alignmentLink.source.target.titles[0];
      this.sourceProject.setLabels({ [title.getLanguage()]: title.getLabel() });
    }
  }

  showMappings() {
    this.alignmentModals.openAlignments(this.sourceProject, this.alignmentLink.linkset);
  }

}