import { ChangeDetectorRef, Component, Input } from '@angular/core';
import { GraphMode } from '../../abstract-graph';
import { AbstractGraphNode } from '../../abstract-graph-node';
import { AlignmentNode } from '../../model/AlignmentNode';

@Component({
  selector: '[alignmentNode]',
  templateUrl: "./alignment-node.component.html",
  styleUrls: ['../../graph.css'],
  standalone: false
})
export class AlignmentNodeComponent extends AbstractGraphNode<AlignmentNode> {

  @Input('alignmentNode') node: AlignmentNode;

  graphMode = GraphMode.dataOriented;

  constructor(protected changeDetectorRef: ChangeDetectorRef) {
    super(changeDetectorRef);
  }

  ngOnInit() {
    this.initNode();
  }


  /**
   * Override the default show (which is based on the show attr of the AnnotatedValue of the node)
   */
  getNodeShow(): string {
    let nodeShow: string;
    if (this.rendering) {
      //rendering enabled => show is the dataset title, if any, otherwise the project name
      if (this.node.target.titles.length > 0) {
        nodeShow = this.node.target.titles[0].getLabel();
      } else {
        nodeShow = this.node.target.projectName;
      }
    } else { //rendering disabled => show is the project name
      nodeShow = this.node.target.projectName;
    }
    if (!nodeShow) {
      //still null, it means that the dataset has no title neither projName (likely no registered dataset exists) => show is the uriSpace
      nodeShow = this.node.target.uriSpace;
    }
    return nodeShow;
  }
}