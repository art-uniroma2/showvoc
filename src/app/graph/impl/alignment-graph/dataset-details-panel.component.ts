import { Component, Input, SimpleChanges } from '@angular/core';
import { DatasetMetadata } from 'src/app/models/Metadata';
import { IRI } from 'src/app/models/Resources';
import { MdrVoc } from 'src/app/models/Vocabulary';
import { MetadataRegistryServices } from 'src/app/services/metadata-registry.service';

@Component({
  selector: 'dataset-details-panel',
  templateUrl: './dataset-details-panel.component.html',
  host: { class: "vbox" },
  standalone: false
})
export class DatasetDetailsPanelComponent {

  @Input() dataset: IRI;

  datasetMetadata: DatasetMetadata;
  dereferenciationNormalized: string;

  constructor(private metadataRegistryService: MetadataRegistryServices) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['dataset']) {
      this.metadataRegistryService.getDatasetMetadata(this.dataset).subscribe(
        metadata => {
          this.datasetMetadata = metadata;
          //normalize dereferenciation
          if (this.datasetMetadata.dereferenciationSystem == null) {
            this.dereferenciationNormalized = "Unknown";
          } else if (this.datasetMetadata.dereferenciationSystem == MdrVoc.standardDereferenciation.getIRI()) {
            this.dereferenciationNormalized = "Yes";
          } else if (this.datasetMetadata.dereferenciationSystem == MdrVoc.noDereferenciation.getIRI()) {
            this.dereferenciationNormalized = "No";
          } else {
            this.dereferenciationNormalized = this.datasetMetadata.dereferenciationSystem;
          }
        }
      );
    }
  }

}