import { Component, ViewChild } from "@angular/core";
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { BrowsingModalsServices } from 'src/app/modal-dialogs/browsing-modals/browsing-modals.service';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { AnnotatedValue, IRI, ResAttribute, Value } from 'src/app/models/Resources';
import { AbstractGraphPanel } from '../../abstract-graph-panel';
import { UmlLink } from "../../model/UmlLink";
import { UmlNode } from '../../model/UmlNode';
import { UmlGraphComponent } from './uml-graph.component';


@Component({
  selector: 'uml-graph-panel',
  templateUrl: "./uml-graph-panel.component.html",
  host: { class: "vbox" },
  standalone: false
})
export class UmlGraphPanelComponent extends AbstractGraphPanel<UmlNode, UmlLink> {

  @ViewChild(UmlGraphComponent) viewChildGraph: UmlGraphComponent;

  resourceToDescribe: AnnotatedValue<Value>;
  isHideArrows: boolean = false;
  activeRemove: boolean = false;

  constructor(basicModals: BasicModalsServices, browsingModals: BrowsingModalsServices) {
    super(basicModals, browsingModals);
  }

  addNode() {
    this.browsingModals.browseClassTree({ key: "GRAPHS.ACTIONS.ADD_NODE" }).then(
      (cls: AnnotatedValue<IRI>) => {
        if (!cls.getAttribute(ResAttribute.EXPLICIT)) {
          this.basicModals.alert({ key: "COMMONS.STATUS.WARNING" }, { key: "MESSAGES.CANNOT_ADD_GRAPH_NODE_FOR_NOT_LOCALLY_DEFINED_RES", params: { resource: cls.getShow() } },
            ModalType.warning);
          return;
        }
        this.viewChildGraph.addNode(cls);
      },
      () => { }
    );
  }

  removeNode() {
    this.viewChildGraph.removeNode(this.selectedNode);
  }

  onElementSelected(element: any) {
    this.activeRemove = false;
    this.resourceToDescribe = null;
    if (element != null) {
      if (element instanceof UmlNode) {
        this.selectedNode = element;
        this.activeRemove = true;
        this.resourceToDescribe = element.res;
      } else if (element instanceof UmlLink) {
        this.selectedLink = element;
        this.resourceToDescribe = element.res;
      } else {
        this.resourceToDescribe = element.property;
      }
    }
  }

  updateArrows() {
    this.isHideArrows = !this.isHideArrows;
  }

}