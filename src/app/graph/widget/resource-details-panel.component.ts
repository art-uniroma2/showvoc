import { Component, Input } from '@angular/core';
import { SharedModalsServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { AnnotatedValue, Value } from 'src/app/models/Resources';

@Component({
  selector: 'resource-details-panel',
  templateUrl: './resource-details-panel.component.html',
  host: { class: "vbox" },
  standalone: false
})
export class ResourceDetailsPanelComponent {

  @Input() resource: AnnotatedValue<Value>;

  constructor(private sharedModals: SharedModalsServices) { }

  showResourceView() {
    this.sharedModals.openResourceView(this.resource.getValue());
  }

}