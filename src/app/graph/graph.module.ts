import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { PreferencesModule } from '../preferences/preferences.module';
import { WidgetModule } from '../widget/widget.module';
import { D3Service } from './d3/d3.services';
import { DraggableDirective } from './d3/draggable.directive';
import { ZoomableDirective } from './d3/zoomable.directive';
import { AlignmentDetailsPanelComponent } from './impl/alignment-graph/alignment-details-panel.component';
import { AlignmentGraphPanelComponent } from './impl/alignment-graph/alignment-graph-panel.component';
import { AlignmentGraphComponent } from './impl/alignment-graph/alignment-graph.component';
import { AlignmentLinkComponent } from './impl/alignment-graph/alignment-link.component';
import { AlignmentNodeComponent } from './impl/alignment-graph/alignment-node.component';
import { DatasetDetailsPanelComponent } from './impl/alignment-graph/dataset-details-panel.component';
import { DataGraphPanelComponent } from './impl/data-graph/data-graph-panel.component';
import { DataGraphComponent } from './impl/data-graph/data-graph.component';
import { DataLinkComponent } from './impl/data-graph/data-link.component';
import { DataNodeComponent } from './impl/data-graph/data-node.component';
import { ModelGraphPanelComponent } from './impl/model-graph/model-graph-panel.component';
import { ModelGraphComponent } from './impl/model-graph/model-graph.component';
import { ModelLinkComponent } from './impl/model-graph/model-link.component';
import { ModelNodeComponent } from './impl/model-graph/model-node.component';
import { UmlGraphPanelComponent } from './impl/uml-graph/uml-graph-panel.component';
import { UmlGraphComponent } from './impl/uml-graph/uml-graph.component';
import { UmlLinkComponent } from './impl/uml-graph/uml-link.component';
import { UmlNodeComponent } from './impl/uml-graph/uml-node.component';
import { DataGraphSettingsModalComponent } from './modals/data-graph-settings-modal.component';
import { GraphModalComponent } from './modals/graph-modal.component';
import { GraphModalServices } from './modals/graph-modal.service';
import { LinksFilterModalComponent } from './modals/links-filter-modal.component';
import { ForceControlPanelComponent } from './widget/force-control-panel.component';
import { ResourceDetailsPanelComponent } from './widget/resource-details-panel.component';

@NgModule({
  declarations: [
    AlignmentLinkComponent,
    AlignmentNodeComponent,
    AlignmentGraphComponent,
    AlignmentGraphPanelComponent,
    AlignmentDetailsPanelComponent,
    DataGraphComponent,
    DataGraphPanelComponent,
    DataGraphSettingsModalComponent,
    DataLinkComponent,
    DataNodeComponent,
    DatasetDetailsPanelComponent,
    DraggableDirective,
    ForceControlPanelComponent,
    GraphModalComponent,
    LinksFilterModalComponent,
    ModelGraphComponent,
    ModelGraphPanelComponent,
    ModelLinkComponent,
    ModelNodeComponent,
    ResourceDetailsPanelComponent,
    UmlGraphComponent,
    UmlGraphPanelComponent,
    UmlLinkComponent,
    UmlNodeComponent,
    ZoomableDirective,
  ],
  imports: [
    CommonModule,
    DragDropModule,
    FormsModule,
    NgbDropdownModule,
    PreferencesModule,
    TranslateModule,
    WidgetModule
  ],
  exports: [
    AlignmentGraphPanelComponent
  ],
  providers: [D3Service, GraphModalServices]
})
export class GraphModule { }
