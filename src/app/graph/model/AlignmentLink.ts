import { LinksetMetadata } from 'src/app/models/Metadata';
import { AlignmentNode } from './AlignmentNode';
import { Link } from './Link';

export class AlignmentLink extends Link<AlignmentNode> {

    source: AlignmentNode;
    target: AlignmentNode;
    linkset: LinksetMetadata;

    constructor(source: AlignmentNode, target: AlignmentNode, linkset: LinksetMetadata) {
        super(source, target);
        this.linkset = linkset;
    }

}