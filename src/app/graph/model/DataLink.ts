import { AnnotatedValue, IRI } from 'src/app/models/Resources';
import { DataNode } from './DataNode';
import { Link } from './Link';

export class DataLink extends Link<DataNode> {

  res: AnnotatedValue<IRI>;

  // constructor(source: DataNode, target: DataNode, res: AnnotatedValue<IRI>) {
  //   super(source, target, res);
  // }

}