import { AnnotatedValue, IRI } from 'src/app/models/Resources';
import { Link } from './Link';
import { ModelNode } from './ModelNode';

export class ModelLink extends Link<ModelNode> {

    res: AnnotatedValue<IRI>;

    // constructor(source: ModelNode, target: ModelNode, res: AnnotatedValue<IRI>, classAxiom?: boolean) {
    //     super(source, target, res, classAxiom);
    //     this.res = res;
    // }

}