import { Target } from 'src/app/models/Metadata';
import { AnnotatedValue, IRI } from 'src/app/models/Resources';
import { Size } from "./GraphConstants";
import { Node, NodeShape } from "./Node";

export class AlignmentNode extends Node {

  //list of nodes that opened the current. Useful to determine whenever delete the current node from the graph when it is closing other nodes
  openBy: Node[];

  /*
  contains additional details about the dataset represented by the node.

  Note: this may not be an actual a Target dataset. 
  Indeed Target represents the info of a target dataset in a linkset (returned by getEmbeddedLinksets).
  In case of the root project of the alignments graph, there is no Target dataset. 
  As a workaround, the Target is created using the info of the (source) Project with the addition of the dataset IRI (from MetadataRegistry#findDatasetForProjects).
  I preferred to use Target interface anyway, since it has the info needed here and the root node is the only exception where I need to create a mockup for it.
   */
  target: Target;
  /*
  tells if the detaset represented by the node (which details are in "target") is a registered dataset or not.
  This means that, if it is a registered dataset, it exists a corresponding dataset in ST, otherwise no dataset in ST is available.
  This affects the description of a node when selected in a graph, indeed if no registered dataset is available for a target dataset,
  getDatasetMetadata returns an exception 
  (this solves the issue reported in the comment here https://art-uniroma2.atlassian.net/browse/ST-2481)
  */
  isRegisteredDataset: boolean;

  res: AnnotatedValue<IRI>; //res in alignment node is IRI (dataset ref)

  constructor(res: AnnotatedValue<IRI>, target: Target, isRegisteredDataset: boolean) {
    super(res);
    this.openBy = [];
    this.target = target;
    this.isRegisteredDataset = isRegisteredDataset;
  }

  initNodeShape() {
    this.shape = NodeShape.circle;
  }

  initNodeMeasure() {
    this.measures = { radius: Size.Circle.radius };
  }

  isPending(): boolean {
    if (this.root) return false; //root node excluded from check

    if (this.openBy.length == 0) {
      return true;
    } else {
      //check if the openBy nodes create loop(s).
      for (const n of this.openBy) {
        //If there is at least a node in openBy different from the current => return false
        if (n != this) {
          return false;
        }
      }
      return true; //all the nodes in openBy are the current node (loops), the node is then pending
    }
  }

  removeOpenByNode(node: Node) {
    this.openBy.splice(this.openBy.indexOf(node), 1);
  }

}