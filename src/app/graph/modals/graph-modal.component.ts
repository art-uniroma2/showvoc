import { Component, ElementRef, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RDFResourceRolesEnum } from 'src/app/models/Resources';
import { UIUtils } from 'src/app/utils/UIUtils';
import { GraphMode } from '../abstract-graph';
import { DataLink } from "../model/DataLink";
import { DataNode } from "../model/DataNode";
import { ForceDirectedGraph } from "../model/ForceDirectedGraph";
import { Link } from "../model/Link";
import { ModelLink } from "../model/ModelLink";
import { ModelNode } from "../model/ModelNode";
import { Node } from "../model/Node";
import { UmlLink } from "../model/UmlLink";
import { UmlNode } from "../model/UmlNode";

@Component({
  selector: "graph-modal",
  templateUrl: "./graph-modal.component.html",
  standalone: false
})
export class GraphModalComponent {

  @Input() graph: ForceDirectedGraph<Node, Link<Node>>;
  @Input() mode: GraphMode;
  @Input() rendering: boolean;
  @Input() role?: RDFResourceRolesEnum; //needed in data-oriented graph in order to inform the graph panel which role should allow to add

  dataGraph: ForceDirectedGraph<DataNode, DataLink>;
  modelGraph: ForceDirectedGraph<ModelNode, ModelLink>;
  umlGraph: ForceDirectedGraph<UmlNode, UmlLink>;

  constructor(public activeModal: NgbActiveModal, private elementRef: ElementRef) { }

  ngOnInit() {
    if (this.mode == GraphMode.dataOriented) {
      this.dataGraph = this.graph as ForceDirectedGraph<DataNode, DataLink>;
    } else if (this.mode == GraphMode.modelOriented) {
      this.modelGraph = this.graph as ForceDirectedGraph<ModelNode, ModelLink>;
    } else if (this.mode == GraphMode.umlOriented) {
      this.umlGraph = this.graph as ForceDirectedGraph<UmlNode, UmlLink>;
    }
  }

  ngAfterViewInit() {
    UIUtils.setFullSizeModal(this.elementRef);
  }

  ok() {
    this.activeModal.close();
  }

  close() {
    this.activeModal.dismiss();
  }

}
