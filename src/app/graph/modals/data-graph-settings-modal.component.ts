import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ResViewTemplate, SectionFilterPreference, ValueFilterLanguages } from 'src/app/models/Properties';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';

@Component({
  selector: "data-graph-settings-modal",
  templateUrl: "./data-graph-settings-modal.component.html",
  standalone: false
})
export class DataGraphSettingsModalComponent {

  sectionFilter: SectionFilterPreference;
  hideLiteralNodes: boolean;

  //lang-value-filter
  langValueFilter: ValueFilterLanguages;

  template: ResViewTemplate; //template on which base the graph sections filter

  constructor(public activeModal: NgbActiveModal, private settingsMgr: SettingsManager) { }

  ngOnInit() {
    this.sectionFilter = this.settingsMgr.getGraphSectionFilter(SVContext.getProjectCtx().getProject());
    this.langValueFilter = this.settingsMgr.getValueFilterLanguages(SVContext.getProjectCtx().getProject());
    this.hideLiteralNodes = this.settingsMgr.getHideLiteralGraphNodes(SVContext.getProjectCtx().getProject());
    this.template = this.settingsMgr.getResourceViewProjectSettings(SVContext.getProjectCtx().getProject()).templates;
  }

  onSectionFilterChange() {
    this.settingsMgr.setGraphSectionFilter(SVContext.getProjectCtx().getProject(), this.sectionFilter).subscribe();
  }

  onHideLiteralChange() {
    this.settingsMgr.setHideLiteralGraphNodes(SVContext.getProjectCtx().getProject(), this.hideLiteralNodes).subscribe();
  }

  onLangValueFilterChange() {
    this.settingsMgr.setValueFilterLanguages(SVContext.getProject(), this.langValueFilter).subscribe();
  }

  ok() {
    this.activeModal.close();
  }

  close() {
    this.activeModal.dismiss();
  }

}