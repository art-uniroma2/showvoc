import { Component, Input } from '@angular/core';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { Scope } from '../../models/Plugins';
import { Project, ProjectVisibility } from '../../models/Project';
import { GlobalSearchProjectsGroup } from '../../models/Search';
import { ProjectsServices } from '../../services/projects.service';
import { SettingsServices } from '../../services/settings.service';
import { SVContext } from '../../utils/SVContext';
import { SettingsManager, SettingsManagerID } from '../../utils/SettingsManager';
import { DatasetsGroupEditorModalComponent } from './datasets-group-editor-modal.component';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';

@Component({
  selector: 'search-groups-filter-modal',
  templateUrl: './search-groups-filter-modal.html',
  standalone: false
})
export class SearchGroupsFilterModalComponent {

  @Input() activeGroups: string[];

  isAdmin: boolean;

  groups: GroupStruct[] = [];

  private searchProjectsGroup: GlobalSearchProjectsGroup[];
  private projects: Project[];

  constructor(
    public activeModal: NgbActiveModal,
    private projectsService: ProjectsServices,
    private settingsService: SettingsServices,
    private basicModals: BasicModalsServices,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.isAdmin = SVContext.getLoggedUser()?.isAdmin();

    let initGroups = this.projectsService.listSearchProjectsGroups().pipe(
      map(groups => {
        this.searchProjectsGroup = groups ? groups : [];
      })
    );
    let initProjects = this.projectsService.listProjects(null, false, [ProjectVisibility.PUBLIC]).pipe(
      map(projects => {
        this.projects = projects;
      })
    );

    forkJoin([initGroups, initProjects]).subscribe(() => {
      this.groups = this.searchProjectsGroup.map(g => {
        return {
          groupName: g.groupName,
          groupDescription: g.groupDescription,
          checked: this.activeGroups.includes(g.groupName),
          projects: g.projectNames.map(pn => this.projects.find(p => p.getName() == pn)).filter(p => p != null),
          collapsed: true
        };
      });
    });
  }

  createGroup() {
    this.openGroupEditor().then(
      (newGroup: GlobalSearchProjectsGroup) => {
        this.groups.push({
          checked: false,
          collapsed: true,
          groupName: newGroup.groupName,
          groupDescription: newGroup.groupDescription,
          projects: newGroup.projectNames.map(name => this.projects.find(p => p.getName() == name)).filter(p => p != null)
        });

        this.updateGroupsSetting();
      },
      () => { }
    );
  }

  editGroup(group: GroupStruct) {
    this.openGroupEditor(group).then(
      (newGroup: GlobalSearchProjectsGroup) => {
        group.groupName = newGroup.groupName;
        group.groupDescription = newGroup.groupDescription;
        group.projects = newGroup.projectNames.map(name => this.projects.find(p => p.getName() == name)).filter(p => p != null);

        this.updateGroupsSetting();
      },
      () => { }
    );
  }

  private openGroupEditor(group?: GroupStruct): Promise<GlobalSearchProjectsGroup> {
    const modalRef: NgbModalRef = this.modalService.open(DatasetsGroupEditorModalComponent, new ModalOptions("lg"));
    if (group) {
      let gToEdit: GlobalSearchProjectsGroup = {
        groupName: group.groupName,
        groupDescription: group.groupDescription,
        projectNames: group.projects.map(p => p.getName())
      };
      modalRef.componentInstance.group = gToEdit;
    }
    return modalRef.result;
  }

  deleteGroup(group: GroupStruct) {
    this.basicModals.confirm({ key: "COMMONS.STATUS.WARNING" }, { key: "SEARCH.DATASETS_GROUP_EDITOR.DELETE_GROUP_CONFIRM" }, ModalType.warning).then(
      () => {
        this.groups.splice(this.groups.indexOf(group), 1);
        this.updateGroupsSetting();
      },
      () => { }
    );
  }

  private updateGroupsSetting() {
    let setting: GlobalSearchProjectsGroup[] = this.groups.map(g => {
      return {
        groupName: g.groupName,
        groupDescription: g.groupDescription,
        projectNames: g.projects.map(p => p.getName())
      };
    });
    this.settingsService.storeSetting(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.SYSTEM, SettingsManager.PROP_NAME.projectsSearchGroups, setting).subscribe();
  }


  ok() {
    let checkedGroups = this.groups.filter(g => g.checked).map(g => g.groupName);
    this.activeModal.close(checkedGroups);
  }

  close() {
    this.activeModal.dismiss();
  }


}


interface GroupStruct {
  collapsed: boolean;
  checked: boolean;
  groupName: string;
  groupDescription?: string;
  projects: Project[];
}