import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Project } from '../../models/Project';

@Component({
    selector: 'search-datasets-filter-modal',
    templateUrl: './search-datasets-filter-modal.component.html',
    standalone: false
})
export class SearchDatasetsFilterModalComponent {

    @Input() projectNames: string[];

    projects: Project[];

    constructor(public activeModal: NgbActiveModal) { }
    
    ngOnInit() {
        this.projects = this.projectNames.map(n => new Project(n));
    }

    ok() {
        this.activeModal.close(this.projects);
    }

    close() {
        this.activeModal.dismiss();
    }


}
