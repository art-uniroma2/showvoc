import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Project } from "src/app/models/Project";
import { GlobalSearchProjectsGroup } from "src/app/models/Search";

@Component({
  selector: "datasets-group-editor-modal",
  templateUrl: "./datasets-group-editor-modal.component.html",
  standalone: false
})
export class DatasetsGroupEditorModalComponent {

  @Input() group: GlobalSearchProjectsGroup;

  name: string;
  description: string;
  datasets: Project[] = [];

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
    if (this.group) {
      this.name = this.group.groupName;
      this.description = this.group.groupDescription;
      this.datasets = this.group.projectNames.map(p => new Project(p));
    }
  }

  isDataValid(): boolean {
    return this.name && this.name.trim() != "";
  }

  ok() {
    if (!this.isDataValid()) {
      return;
    }

    let g: GlobalSearchProjectsGroup = {
      groupName: this.name,
      groupDescription: this.description,
      projectNames: this.datasets.map(d => d.getName())
    };

    this.activeModal.close(g);
  }

  close() {
    this.activeModal.dismiss();
  }

}