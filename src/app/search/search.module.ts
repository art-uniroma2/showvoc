import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { WidgetModule } from '../widget/widget.module';
import { DatasetsGroupEditorModalComponent } from './filters/datasets-group-editor-modal.component';
import { EditLanguageModalComponent } from './filters/edit-language-modal.component';
import { SearchDatasetsFilterModalComponent } from './filters/search-datasets-filter-modal.component';
import { SearchGroupsFilterModalComponent } from './filters/search-groups-filter-modal';
import { SearchResultsetComponent } from './search-resultset.component';
import { SearchComponent } from './search.component';

@NgModule({
    declarations: [
        DatasetsGroupEditorModalComponent,
        EditLanguageModalComponent,
        SearchComponent,
        SearchDatasetsFilterModalComponent,
        SearchGroupsFilterModalComponent,
        SearchResultsetComponent
    ],
    imports: [
        CommonModule,
        DragDropModule,
        FormsModule,
        NgbDropdownModule,
        RouterModule,
        TranslateModule,
        WidgetModule,
    ]
})
export class SearchModule { }
