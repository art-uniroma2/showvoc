import { Component, QueryList, ViewChildren } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { forkJoin, Observable, of } from 'rxjs';
import { finalize, map, mergeMap } from 'rxjs/operators';
import { BasicModalsServices } from '../modal-dialogs/basic-modals/basic-modals.service';
import { ModalOptions, ModalType } from '../modal-dialogs/Modals';
import { Project } from '../models/Project';
import { SearchMode, SearchUtils } from '../models/Properties';
import { IRI } from '../models/Resources';
import { GlobalSearchProjectsGroup, GlobalSearchResult } from '../models/Search';
import { GlobalSearchServices } from '../services/global-search.service';
import { ProjectsServices } from '../services/projects.service';
import { ResourcesServices } from '../services/resources.service';
import { STRequestOptions } from '../utils/HttpManager';
import { LocalStorageManager } from '../utils/LocalStorageManager';
import { SettingsManager } from '../utils/SettingsManager';
import { SVContext } from '../utils/SVContext';
import { EditLanguageModalComponent } from './filters/edit-language-modal.component';
import { SearchDatasetsFilterModalComponent } from './filters/search-datasets-filter-modal.component';
import { SearchGroupsFilterModalComponent } from './filters/search-groups-filter-modal';
import { SearchResultsetComponent } from './search-resultset.component';

@Component({
  selector: 'search-component',
  templateUrl: './search.component.html',
  host: { class: "pageComponent" },
  styles: [`
      .grouped-search-result + .grouped-search-result {
          margin-top: 1rem;
      }
      .dataset-filter-dropdown {
          font-size: .875rem;
          white-space: nowrap;
      }
      .dataset-filter-dropdown > div {
          /* items in dropdown (without dropdown-item to not add the style of such class) */
          padding: 3px 10px
      }
    `],
  standalone: false
})
export class SearchComponent {

  @ViewChildren(SearchResultsetComponent) viewChildrenSearchResult: QueryList<SearchResultsetComponent>;

  isAdmin: boolean;

  searchString: string;
  lastSearch: string;

  loading: boolean = false;

  stringMatchModesMap = SearchUtils.stringMatchModesMap;
  stringMatchModes: SearchMode[] = [SearchMode.exact, SearchMode.contains, SearchMode.startsWith, SearchMode.endsWith]; //fuzzy not available in global search
  searchMode: SearchMode = SearchMode.contains;
  caseSensitive: boolean = false;
  searchInLocalName: boolean = false;

  groupedSearchResults: { [repId: string]: GlobalSearchResult[] };
  accessibleMap: { [repId: string]: boolean }; //for each dataset tells if it is accessible: repo is open and user authorized (possible auth limitations only for SuperUser)

  openProjectFilter: boolean = true;
  filteredRepoIds: string[]; //id (eventually filtered) of the repositories of the results, useful to iterate over them in the view

  activeDatasetsFilterType: DatasetsFilterType;
  datasetFilterFavorites: string[] = [];
  datasetFilterSelection: string[] = [];
  datasetFilterGroups: string[] = [];

  searchGroups: GlobalSearchProjectsGroup[];

  resultsCount: number;
  excludedResultsCount: number; //results excluded due filters (currently just due the "only open" dataset filter)
  excludedRepoCount: number; //repo excluded due filters

  anyLangFilter: boolean;
  languagesFilter: LanguageFilter[];

  constructor(private globalSearchService: GlobalSearchServices, private resourcesService: ResourcesServices, private projectService: ProjectsServices,
    private settingsMgr: SettingsManager, private basicModals: BasicModalsServices, private modalService: NgbModal) { }

  ngOnInit() {
    this.isAdmin = SVContext.getLoggedUser()?.isAdmin();
    this.initSettings();
    this.initFilters();
    this.initSearchGroups();
  }

  private initSearchGroups() {
    this.projectService.listSearchProjectsGroups().subscribe(
      (groups: GlobalSearchProjectsGroup[]) => {
        this.searchGroups = groups;

        let datasetFilterGroupsValue = LocalStorageManager.getItem(LocalStorageManager.GLOBAL_SEARCH_FILTERS_DATASETS_FILTER_GROUPS);
        this.datasetFilterGroups = datasetFilterGroupsValue ? datasetFilterGroupsValue : [];

        let groupUpdated: boolean = false;
        for (let i = this.datasetFilterGroups.length - 1; i >= 0; i--) {
          if (!this.searchGroups.some(g => g.groupName == this.datasetFilterGroups[i])) {
            //group stored in filter not existing => remove it
            this.datasetFilterGroups.splice(i, 1);
            groupUpdated = true;
          }
        }
        if (groupUpdated) {
          LocalStorageManager.setItem(LocalStorageManager.GLOBAL_SEARCH_FILTERS_DATASETS_FILTER_GROUPS, this.datasetFilterGroups);
        }
      }
    );
  }

  searchKeyHandler() {
    if (this.searchString != null && this.searchString.trim() != "") {
      this.search();
    }
  }

  search() {
    this.lastSearch = this.searchString;
    let langPar: string[] = [];
    if (!this.anyLangFilter) {
      this.languagesFilter.forEach(l => {
        if (l.active) {
          langPar.push(l.lang);
        }
      });
    }

    let datasetFilter: string[];
    if (this.activeDatasetsFilterType == DatasetsFilterType.favorites) {
      datasetFilter = this.settingsMgr.getFavoriteDatasets();
    } else if (this.activeDatasetsFilterType == DatasetsFilterType.groups) {
      datasetFilter = [];
      this.searchGroups.filter(g => this.datasetFilterGroups.includes(g.groupName)).forEach(g => {
        datasetFilter = datasetFilter.concat(g.projectNames);
      });
      //remove duplicates
      datasetFilter = datasetFilter.filter((item: string, pos: number) => datasetFilter.findIndex(el => el == item) == pos);
    } else if (this.activeDatasetsFilterType == DatasetsFilterType.selection) {
      datasetFilter = this.datasetFilterSelection;
    }

    this.loading = true;
    this.globalSearchService.search(this.searchString, langPar, datasetFilter, null, this.searchInLocalName, this.caseSensitive, this.searchMode)
      .pipe(finalize(() => { this.loading = false; }))
      .subscribe(
        (results: GlobalSearchResult[]) => {
          //group the results by repository ID 
          this.groupedSearchResults = {};
          results.forEach(r => {
            let resultsInRepo: GlobalSearchResult[] = this.groupedSearchResults[r.repository.id];
            if (resultsInRepo != null) {
              resultsInRepo.push(r);
            } else {
              this.groupedSearchResults[r.repository.id] = [r];
            }
          });

          //compute auth map
          this.accessibleMap = {};
          Object.keys(this.groupedSearchResults).forEach(repoId => {
            this.accessibleMap[repoId] = this.isResultAccessible(this.groupedSearchResults[repoId][0]);
          });

          //compute show of results
          let computeResultsShowFunctions: Observable<void>[] = [];
          Object.keys(this.groupedSearchResults).forEach(repoId => {
            let results: GlobalSearchResult[] = this.groupedSearchResults[repoId];
            computeResultsShowFunctions.push(this.getComputeResultsShowFn(results));
          });
          forkJoin(computeResultsShowFunctions)
            // .pipe(finalize(() => SVContext.removeTempProject()))
            .subscribe(() => {
              //order results
              Object.keys(this.groupedSearchResults).forEach(repoId => {
                this.groupedSearchResults[repoId].sort((r1: GlobalSearchResult, r2: GlobalSearchResult) => {
                  return r1.show.localeCompare(r2.show);
                });
              });
            });
          this.filterSearchResults();
        },
        (err: Error) => {
          if (err.name.endsWith("IndexNotFoundException")) {
            this.basicModals.alert({ key: "SEARCH.INDEX_NOT_FOUND" }, { key: "MESSAGES.SEARCH_INDEX_NOT_FOUND" }, ModalType.warning);
          } else if (err.name.endsWith("TooManyClauses")) {
            this.basicModals.alert({ key: "COMMONS.STATUS.WARNING" }, { key: "SEARCH.TOO_MANY_RESULTS" }, ModalType.warning);
          }
        }
      );
  }

  private filterSearchResults() {
    this.resultsCount = 0;
    this.excludedResultsCount = 0;
    this.excludedRepoCount = 0;

    //collect the repositories ID according the filter
    this.filteredRepoIds = [];
    Object.keys(this.groupedSearchResults).forEach(repoId => {
      if (this.openProjectFilter && this.groupedSearchResults[repoId][0].repository.open || !this.openProjectFilter) {
        this.filteredRepoIds.push(repoId);
        this.resultsCount += this.groupedSearchResults[repoId].length;
      } else {
        this.excludedRepoCount++;
        this.excludedResultsCount += this.groupedSearchResults[repoId].length;
      }
    });
    this.filteredRepoIds.sort();
  }

  private getComputeResultsShowFn(results: GlobalSearchResult[]): Observable<void> {
    if (this.isResultAccessible(results[0])) { //if the repository is open (and accessible in case of SU), compute the show with a service invokation
      let resources: IRI[] = [];
      results.forEach(r => {
        resources.push(r.resource);
      });

      // init the RenderingEngine settings for the project which is going to resolve resources info (in order to pass ctx_langs)
      let resultsProj = new Project(results[0].repository.id);
      return this.settingsMgr.initRenderingLanguages(resultsProj).pipe(
        mergeMap(() => {
          /*
          TODO: there's a corner case that could cause an "access denied" error to occurs here, even if the result was detected as accessible.
          - User accesses Search page => SV initializes publicProjects (P1, P2, ..., Pn)
          - Admin changes P1 from Public to Staging without deleting the index
          - User perform a new search which returns results belonging to P1.
          - SV tries to resolve rendering of result resources, including those belonging to P1 that was cached in publicProjects
          - P1 is actually no more public, it's staging now, so getResourcesInfo return "Access Denied"
           */
          //force ctx_consumer to SYSTEM, so that if any project in the context is set, it is not set as consumer and thus the ACL is not evaluated for the accessed one 
          return this.resourcesService.getResourcesInfo(resources, new STRequestOptions({ ctxProject: resultsProj, ctxConsumer: new Project("SYSTEM") })).pipe(
            map(annotated => {
              annotated.forEach(a => {
                results.find(r => r.resource.equals(a.getValue())).show = a.getShow();
              });
            })
          );
        })
      );
    } else { //if the repository is closed, the show is the same IRI
      return of(
        results.forEach(r => {
          r.show = r.resource.getIRI();
        })
      );
    }
  }

  /**
   * Tells if a given search result is accessible, namely if the repository/project (which the result belongs to) is open
   * @param result
   */
  private isResultAccessible(result: GlobalSearchResult): boolean {
    return result.repository.open;
  }

  expandAll() {
    this.viewChildrenSearchResult.forEach(r => {
      r.collapsed = false;
    });
  }
  collapseAll() {
    this.viewChildrenSearchResult.forEach(r => {
      r.collapsed = true;
    });
  }


  /**======================
   * Filters and Settings
   * ======================*/

  //Projects

  toggleOpenProjectFilter() {
    this.openProjectFilter = !this.openProjectFilter;
    this.storeFilters();
    if (this.groupedSearchResults) { //if search results are available, filter them
      this.filterSearchResults();
    }
  }

  toggleOnlyInFavorites() {
    this.activeDatasetsFilterType = this.activeDatasetsFilterType != null ? this.activeDatasetsFilterType = null : this.activeDatasetsFilterType = DatasetsFilterType.favorites;
    this.storeFilters();
    if (this.searchString) {
      this.search();
    }
  }

  toggleOnlyInProjects() {
    this.activeDatasetsFilterType = this.activeDatasetsFilterType != null ? this.activeDatasetsFilterType = null : this.activeDatasetsFilterType = DatasetsFilterType.selection;
    this.storeFilters();
    if (this.searchString) {
      this.search();
    }
  }

  toggleOnlyInGroups() {
    this.activeDatasetsFilterType = this.activeDatasetsFilterType != null ? this.activeDatasetsFilterType = null : this.activeDatasetsFilterType = DatasetsFilterType.groups;
    this.storeFilters();
    if (this.searchString) {
      this.search();
    }
  }

  editProjectsFilter() {
    const modalRef: NgbModalRef = this.modalService.open(SearchDatasetsFilterModalComponent, new ModalOptions("lg"));
    modalRef.componentInstance.projectNames = this.datasetFilterSelection;
    modalRef.result.then(
      (projects: Project[]) => {
        this.datasetFilterSelection = projects.map(p => p.getName());
        this.storeFilters();
      },
      () => { }
    );
  }


  editSearchGroups() {
    const modalRef: NgbModalRef = this.modalService.open(SearchGroupsFilterModalComponent, new ModalOptions("lg"));
    modalRef.componentInstance.activeGroups = this.datasetFilterGroups;
    modalRef.result.then(
      (groups: string[]) => {
        this.datasetFilterGroups = groups;
        this.storeFilters();
        this.initSearchGroups();
      },
      () => { }
    );
  }


  onDatasetFilterOpen(open: boolean) {
    if (open) {
      this.datasetFilterFavorites = this.settingsMgr.getFavoriteDatasets();
    }
  }

  //Languages

  setAllLanguagesFilter() {
    this.anyLangFilter = true;
    this.languagesFilter.forEach(l => { l.active = false; });
    this.storeFilters();
  }

  activateLanguageFilter(lf: LanguageFilter) {
    lf.active = !lf.active;
    this.anyLangFilter = !this.languagesFilter.some(l => l.active); //if no language is enabled, set the "all languages" to true
    this.storeFilters();
  }

  editLangList() {
    const modalRef: NgbModalRef = this.modalService.open(EditLanguageModalComponent, new ModalOptions());
    modalRef.componentInstance.languages = this.languagesFilter.map(l => l.lang);
    modalRef.result.then(
      (languages: string[]) => {
        let newFilter: LanguageFilter[] = []; //use a temp list in order to keep the "active" status
        languages.forEach(l => {
          let oldLangFilter = this.languagesFilter.find(lf => lf.lang == l);
          newFilter.push({ lang: l, active: oldLangFilter ? oldLangFilter.active : false });
        });
        this.languagesFilter = newFilter;
        this.storeFilters();
      },
      () => { }
    );
  }


  private initSettings() {
    let searchModePref = LocalStorageManager.getItem(LocalStorageManager.GLOBAL_SEARCH_SETTINGS_MODE);
    if (searchModePref in SearchMode) {
      this.searchMode = searchModePref as SearchMode;
    }

    this.caseSensitive = LocalStorageManager.getItem(LocalStorageManager.GLOBAL_SEARCH_SETTINGS_CASE_SENSITIVE);
    this.searchInLocalName = LocalStorageManager.getItem(LocalStorageManager.GLOBAL_SEARCH_SETTINGS_IN_LOCALNAME);
  }
  storeSettings() {
    LocalStorageManager.setItem(LocalStorageManager.GLOBAL_SEARCH_SETTINGS_MODE, this.searchMode);
    LocalStorageManager.setItem(LocalStorageManager.GLOBAL_SEARCH_SETTINGS_CASE_SENSITIVE, this.caseSensitive);
    LocalStorageManager.setItem(LocalStorageManager.GLOBAL_SEARCH_SETTINGS_IN_LOCALNAME, this.searchInLocalName);
  }


  private initFilters() {
    //Projects: only open
    this.openProjectFilter = LocalStorageManager.getItem(LocalStorageManager.GLOBAL_SEARCH_FILTERS_ONLY_OPEN_PROJECTS) != false;

    this.datasetFilterFavorites = this.settingsMgr.getFavoriteDatasets();

    let datasetFilterSelectionValue = LocalStorageManager.getItem(LocalStorageManager.GLOBAL_SEARCH_FILTERS_DATASETS_FILTER_SELECTION);
    this.datasetFilterSelection = datasetFilterSelectionValue ? datasetFilterSelectionValue : [];

    let datasetFilterTypeValue = LocalStorageManager.getItem(LocalStorageManager.GLOBAL_SEARCH_FILTERS_DATASETS_FILTER_TYPE);
    if (datasetFilterTypeValue in DatasetsFilterType) {
      this.activeDatasetsFilterType = datasetFilterTypeValue;
    }

    //Languages
    let langFilterCookie = LocalStorageManager.getItem(LocalStorageManager.GLOBAL_SEARCH_FILTERS_LANGUAGES);
    if (langFilterCookie != null && Array.isArray(langFilterCookie)) {
      try {
        this.languagesFilter = langFilterCookie;
        this.languagesFilter.sort((l1: LanguageFilter, l2: LanguageFilter) => {
          return l1.lang.localeCompare(l2.lang);
        });
        this.anyLangFilter = !this.languagesFilter.some(l => l.active); //if no language is enabled, set the "all languages" to true
      } catch {
        //ignore if langFilterCookie is not correctly parsed
      }
    } else {
      this.languagesFilter = [
        { lang: "de", active: false },
        { lang: "en", active: false },
        { lang: "es", active: false },
        { lang: "fr", active: false },
        { lang: "it", active: false },
      ];
      this.anyLangFilter = true;
    }
  }
  private storeFilters() {
    LocalStorageManager.setItem(LocalStorageManager.GLOBAL_SEARCH_FILTERS_ONLY_OPEN_PROJECTS, this.openProjectFilter);
    LocalStorageManager.setItem(LocalStorageManager.GLOBAL_SEARCH_FILTERS_LANGUAGES, this.languagesFilter);
    LocalStorageManager.setItem(LocalStorageManager.GLOBAL_SEARCH_FILTERS_DATASETS_FILTER_TYPE, this.activeDatasetsFilterType);
    LocalStorageManager.setItem(LocalStorageManager.GLOBAL_SEARCH_FILTERS_DATASETS_FILTER_SELECTION, this.datasetFilterSelection);
    LocalStorageManager.setItem(LocalStorageManager.GLOBAL_SEARCH_FILTERS_DATASETS_FILTER_GROUPS, this.datasetFilterGroups);
  }
}

class LanguageFilter {
  lang: string;
  active: boolean;
}

enum DatasetsFilterType {
  favorites = "favorites",
  groups = "groups",
  selection = "selection",
}