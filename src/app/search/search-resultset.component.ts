import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectLabelCtx } from '../models/Project';
import { GlobalSearchResult, SearchResultRepo } from '../models/Search';
import { ShowVocUrlParams } from '../models/ShowVoc';
import { QueryParamService } from '../utils/QueryParamService';
import { SettingsManager } from '../utils/SettingsManager';

@Component({
  selector: 'search-resultset',
  templateUrl: './search-resultset.component.html',
  styles: [`
      .search-result + .search-result {
          margin-top: 1rem;
      }
    `],
  standalone: false
})
export class SearchResultsetComponent {

  @Input() results: GlobalSearchResult[];
  @Input() accessible: boolean;

  repo: SearchResultRepo;
  collapsed: boolean = true;

  constructor(private settingsMgr: SettingsManager, private router: Router, private queryParamService: QueryParamService) { }

  ngOnInit() {
    this.repo = this.results[0].repository;
  }

  getDatasetLabel() {
    let projectRendering = this.settingsMgr.getProjectRendering();
    if (projectRendering && this.repo.labels && this.repo.labels[ProjectLabelCtx.language]) {
      return this.repo.labels[ProjectLabelCtx.language];
    } else {
      return this.repo.id;
    }
  }

  goToResource(result: GlobalSearchResult) {
    let queryParams = this.queryParamService.getPreservedQueryParams();
    queryParams[ShowVocUrlParams.resId] = result.resource.getIRI();
    this.router.navigate(["/datasets/" + result.repository.id + "/data"], { queryParams: queryParams });
  }

  goToDataset() {
    let queryParams = this.queryParamService.getPreservedQueryParams();
    this.router.navigate(["/datasets/" + this.repo.id], { queryParams: queryParams });
  }

}