import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { WidgetModule } from '../widget/widget.module';
import { CustomViewsDashboardComponent } from './custom-views-dashboard.component';
import { CustomViewsServices } from './custom-views.services';
import { CustomViewEditorModalComponent } from './editors/custom-view-editor-modal.component';
import { CvAssociationEditorModalComponent } from './editors/cv-association-editor-modal.component';
import { ImportCustomViewModalComponent } from './editors/import-custom-view-modal.component';
import { AdvSingleValueViewEditorComponent } from './editors/views/adv-single-value-view-editor.component';
import { AreaViewEditorComponent } from './editors/views/area-view-editor.component';
import { DynamicVectorViewEditorComponent } from './editors/views/dynamic-vector-view-editor.component';
import { PointViewEditorComponent } from './editors/views/point-view-editor.component';
import { PropertyChainViewEditorComponent } from './editors/views/property-chain-view-editor.component';
import { RouteViewEditorComponent } from './editors/views/route-view-editor.component';
import { SeriesCollectionViewEditorComponent } from './editors/views/series-collection-view-editor.component';
import { SeriesViewEditorComponent } from './editors/views/series-view-editor.component';
import { StaticVectorViewEditorComponent } from './editors/views/static-vector-view-editor.component';


@NgModule({
    imports: [
        CommonModule,
        DragDropModule,
        FormsModule,
        NgbDropdownModule,
        TranslateModule,
        WidgetModule,
    ],
    declarations: [
        CustomViewsDashboardComponent,
        CustomViewEditorModalComponent,
        CvAssociationEditorModalComponent,
        AdvSingleValueViewEditorComponent,
        AreaViewEditorComponent,
        DynamicVectorViewEditorComponent,
        ImportCustomViewModalComponent,
        PointViewEditorComponent,
        PropertyChainViewEditorComponent,
        RouteViewEditorComponent,
        SeriesViewEditorComponent,
        SeriesCollectionViewEditorComponent,
        StaticVectorViewEditorComponent,
    ],
    exports: [
        CustomViewsDashboardComponent
    ],
    providers: [
        CustomViewsServices
    ]
})
export class CustomViewsModule { }