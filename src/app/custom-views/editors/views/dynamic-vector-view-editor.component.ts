import { ChangeDetectorRef, Component, Input, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { YasguiComponent } from 'src/app/datasets/sparql/yasgui.component';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { QueryChangedEvent, QueryMode } from 'src/app/models/Sparql';
import { CustomViewsServices } from '../../custom-views.services';
import { CustomViewModel, CustomViewVariables, CvQueryUtils, CvSparqlEditorStruct, DynamicVectorViewDefinition, UpdateMode } from '../../CustomViews';
import { AbstractCustomViewEditor } from './abstract-custom-view-editor';

@Component({
    selector: 'dynamic-vector-view-editor',
    templateUrl: "dynamic-vector-view-editor.component.html",
    host: { class: "vbox" },
    standalone: false
})
export class DynamicVectorViewEditorComponent extends AbstractCustomViewEditor {

    @Input() cvDef: DynamicVectorViewDefinition;

    @ViewChild(YasguiComponent) yasguiEditor: YasguiComponent;

    model: CustomViewModel = CustomViewModel.dynamic_vector;

    retrieveRequiredPlaceholders: CustomViewVariables[] = [CustomViewVariables.resource, CustomViewVariables.trigprop];
    retrievePlaceholdersInfo: VariableInfoStruct[] = [
        { id: CustomViewVariables.resource, descrTranslationKey: "represents the resource being described in ResourceView where the Custom View is shown" },
        { id: CustomViewVariables.trigprop, descrTranslationKey: "represents the predicate that will be associated to the Custom View" },
    ];

    retrieveEditor: CvSparqlEditorStruct = { mode: QueryMode.query, query: "", valid: true };
    retrieveFields: string[] = []; //list of ?SOMETHING_value matched values

    retrieveQuerySkeleton: string = "SELECT ?field_value ?obj WHERE {\n" +
        "    $resource $trigprop ?obj .\n" +
        "    ...\n" +
        "}";


    constructor(private customViewService: CustomViewsServices, private basicModals: BasicModalsServices, private modalService: NgbModal, 
        private translateService: TranslateService, private changeDetectorRef: ChangeDetectorRef) {
        super();
    }

    ngOnInit() {
        super.ngOnInit();
    }

    ngAfterViewInit() {
        this.refreshYasguiEditors();
    }

    initCustomViewDef() {
        this.cvDef = {
            retrieve: this.retrieveQuerySkeleton,
            update: [],
            suggestedView: this.suggestedView,
        };
    }

    restoreEditor() {
        this.retrieveEditor.query = this.cvDef.retrieve;
        this.suggestedView = this.cvDef.suggestedView;
        this.refreshYasguiEditors();
    }

    // ========== RETRIEVE TAB =========

    onRetrieveChanged(event: QueryChangedEvent) {
        this.retrieveEditor.query = event.query;
        this.retrieveEditor.mode = event.mode;
        this.retrieveEditor.valid = event.valid;
        this.emitChanges();
        this.detectFields();
    }

    private detectFields() {
        this.retrieveFields = CvQueryUtils.listFieldVariables(this.retrieveEditor.query);
    }





    emitChanges(): void {
        this.cvDef.suggestedView = this.suggestedView;
        this.cvDef.retrieve = this.retrieveEditor.query;
        this.cvDef.update = this.retrieveFields.map(f => {
            return { field: f, updateMode: UpdateMode.none };
        });
        this.changed.emit(this.cvDef);
    }

    public isDataValid(): boolean {
        this.emitChanges(); //emit changes to parent CV editor
        return this.isRetrieveOk();
    }

    private isRetrieveOk(): boolean {
        //- syntactic check
        if (!this.retrieveEditor.valid) {
            this.basicModals.alert({ key: "COMMONS.STATUS.ERROR" }, { key: "CUSTOM_VIEWS.MESSAGES.RETRIEVE_QUERY_SYNTAX_ERROR" }, ModalType.warning);
            return false;
        }
        let retrieveQuery = this.retrieveEditor.query;
        //- fields: select must returns at least a <field>_value variable
        if (this.retrieveFields.length == 0) {
            this.basicModals.alert({ key: "COMMONS.STATUS.ERROR" }, { key: "CUSTOM_VIEWS.MESSAGES.NO_FIELD_VARIABLE_IN_RETRIEVE" }, ModalType.warning);
            return false;
        }
        //- object: select must returns the object of the $resource $trigprop ?obj triple
        if (CvQueryUtils.getReturnedObjectVariable(retrieveQuery) == null) {
            this.basicModals.alert({ key: "COMMONS.STATUS.ERROR" }, { key: "CUSTOM_VIEWS.MESSAGES.OBJ_VAR_NOT_DETECTED" }, ModalType.warning);
            return false;
        }
        //- placeholders
        for (let v of this.retrieveRequiredPlaceholders) {
            if (!CvQueryUtils.isPlaceholderInWhere(retrieveQuery, v)) {
                this.basicModals.alert({ key: "COMMONS.STATUS.ERROR" }, { key: "CUSTOM_VIEWS.MESSAGES.MISSING_REQUIRED_PLACEHOLDER", params: { ph: v } }, ModalType.warning);
                return false;
            }
        }
        return true;
    }

    /**
     * This method forces content update of yasgui editor, so that valid attributes is updated by queryChanged event,
     * moreover forces the editor to refresh preventing strange UI issues with yasgui editor (left part of the textarea is covered by a gray vertical stripe)
     */
    private refreshYasguiEditors() {
        this.changeDetectorRef.detectChanges(); //wait yasgui to be initialized the first time
        this.yasguiEditor.forceContentUpdate();
    }

}

export interface VariableInfoStruct {
    id: CustomViewVariables;
    descrTranslationKey: string;
}