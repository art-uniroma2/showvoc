import { ChangeDetectorRef, Directive, Input, ViewChild } from '@angular/core';
import { QueryChangedEvent, QueryMode } from 'src/app/models/Sparql';
import { AbstractCustomViewEditor } from './abstract-custom-view-editor';
import { YasguiComponent } from 'src/app/datasets/sparql/yasgui.component';
import { CustomViewVariables, CvQueryUtils, CvSparqlEditorStruct, SparqlBasedCustomViewDefinition } from '../../CustomViews';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';

/**
 * Base component for the custom views with model based on sparql for both retrieve and update actions, namely:
 * - Geospatial
 *  - point
 *  - area
 *  - route
 * - Statistical series
 *  - series
 *  - series-collection
 */
@Directive()
export abstract class AbstractSparqlBasedViewEditor extends AbstractCustomViewEditor {

    @Input() cvDef: SparqlBasedCustomViewDefinition;
    @ViewChild('retrieveYasgui', { static: false }) retrieveYasgui: YasguiComponent;

    retrieveEditor: CvSparqlEditorStruct = { mode: QueryMode.query, query: "", valid: true };

    abstract retrieveRequiredReturnVariables: CustomViewVariables[];
    abstract retrieveDescrIntro: string;
    abstract retrieveVariablesInfo: VariableInfoStruct[];
    abstract retrieveQuerySkeleton: string;

    retrieveRequiredPlaceholders: CustomViewVariables[] = [CustomViewVariables.resource, CustomViewVariables.trigprop];
    retrievePlaceholdersInfo: VariableInfoStruct[] = [
        { id: CustomViewVariables.resource, descrTranslationKey: "represents the resource being described in ResourceView where the Custom View is shown" },
        { id: CustomViewVariables.trigprop, descrTranslationKey: "represents the predicate that will be associated to the Custom View" },
    ];

    protected basicModals: BasicModalsServices;
    protected changeDetectorRef: ChangeDetectorRef;
    constructor(basicModals: BasicModalsServices, changeDetectorRef: ChangeDetectorRef) {
        super();
        this.basicModals = basicModals;
        this.changeDetectorRef = changeDetectorRef;
    }

    ngOnInit() {
        super.ngOnInit();
    }

    ngAfterViewInit() {
        this.refreshYasguiEditors();
    }

    protected initCustomViewDef(): void {
        this.cvDef = {
            retrieve: this.retrieveQuerySkeleton,
            suggestedView: this.suggestedView,
        };
    }

    protected restoreEditor(): void {
        this.retrieveEditor.query = this.cvDef.retrieve;
        this.suggestedView = this.cvDef.suggestedView;
        this.refreshYasguiEditors();
    }

    onRetrieveChanged(event: QueryChangedEvent) {
        this.retrieveEditor.query = event.query;
        this.retrieveEditor.mode = event.mode;
        this.retrieveEditor.valid = event.valid;
        this.emitChanges();
    }

    emitChanges() {
        this.cvDef.retrieve = this.retrieveEditor.query;
        this.cvDef.suggestedView = this.suggestedView;
        this.changed.emit(this.cvDef);
    }

    public isDataValid(): boolean {
        return this.isRetrieveOk();
    }

    protected isRetrieveOk(): boolean {
        //- syntactic check
        if (!this.retrieveEditor.valid) {
            this.basicModals.alert({ key: "COMMONS.STATUS.ERROR" }, { key: "CUSTOM_VIEWS.MESSAGES.RETRIEVE_QUERY_SYNTAX_ERROR" }, ModalType.warning);
            return false;
        }
        //- variables
        let retrieveQuery = this.retrieveEditor.query;
        for (let v of this.retrieveRequiredReturnVariables) {
            if (!CvQueryUtils.isVariableReturned(retrieveQuery, "?" + v)) {
                this.basicModals.alert({ key: "COMMONS.STATUS.ERROR" }, { key: "CUSTOM_VIEWS.MESSAGES.MISSING_REQUIRED_VARIABLE", params: { var: v } }, ModalType.warning);
                return false;
            }
        }
        //- object: select must returns the object of the $resource $trigprop ?obj triple
        if (CvQueryUtils.getReturnedObjectVariable(retrieveQuery) == null) {
            this.basicModals.alert({ key: "COMMONS.STATUS.ERROR" }, { key: "CUSTOM_VIEWS.MESSAGES.OBJ_VAR_NOT_DETECTED" }, ModalType.warning);
            return false;
        }
        //- placeholders
        for (let v of this.retrieveRequiredPlaceholders) {
            if (!CvQueryUtils.isPlaceholderInWhere(retrieveQuery, v)) {
                this.basicModals.alert({ key: "COMMONS.STATUS.ERROR" }, { key: "CUSTOM_VIEWS.MESSAGES.MISSING_REQUIRED_PLACEHOLDER", params: { ph: v } }, ModalType.warning);
                return false;
            }    
        }
        return true;
    }

    /**
     * This method forces content update of yasgui editor, so that valid attributes is updated by queryChanged event,
     * moreover forces the editor to refresh preventing strange UI issues with yasgui editor (left part of the textarea is covered by a gray vertical stripe)
     */
    protected refreshYasguiEditors() {
        if (this.retrieveYasgui && this.retrieveEditor.query != null) {
            this.changeDetectorRef.detectChanges(); //prevent ExpressionChangedAfterItHasBeenCheckedError
            this.retrieveYasgui.forceContentUpdate();
        }
    }

}

export interface VariableInfoStruct {
    id: CustomViewVariables;
    descrTranslationKey: string;
}