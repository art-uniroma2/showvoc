import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { CustomViewModel } from '../../CustomViews';
import { AbstractPropertiesBasedViewEditor } from './abstract-properties-based-view-editor';

@Component({
    selector: 'static-vector-view-editor',
    templateUrl: "./properties-based-view-editor.component.html",
    standalone: false
})
export class StaticVectorViewEditorComponent extends AbstractPropertiesBasedViewEditor {

    model: CustomViewModel = CustomViewModel.static_vector;

    propListLabelTranslationKey: string = "CUSTOM_VIEWS.MODELS.VECTOR.HEADERS";
    invalidPropListMsgTranslationKey: string = "CUSTOM_VIEWS.MESSAGES.INVALID_HEADERS_LIST";
    allowDuplicates: boolean = false;

    infoHtml: string = `This view can be used to represent tabular data in ResourceView.
    Each row of the table represents the description of a value of the triggering property.
    The values represented in the cells are determined by the list of properties which will be used also as table headers.`;

    constructor(basicModals: BasicModalsServices, sanitizer: DomSanitizer) {
        super(basicModals, sanitizer);
    }

    ngOnInit() {
        super.ngOnInit();
    }

}
