import { ChangeDetectorRef, Component, Input, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { YasguiComponent } from 'src/app/datasets/sparql/yasgui.component';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { QueryChangedEvent, QueryMode } from 'src/app/models/Sparql';
import { CustomViewsServices } from '../../custom-views.services';
import { AdvSingleValueViewDefinition, CustomViewModel, CustomViewVariables, CvQueryUtils, CvSparqlEditorStruct, UpdateMode } from '../../CustomViews';
import { AbstractCustomViewEditor } from './abstract-custom-view-editor';
import { VariableInfoStruct } from './abstract-sparql-based-view-editor';

@Component({
    selector: 'adv-single-value-view-editor',
    templateUrl: "adv-single-value-view-editor.component.html",
    host: { class: "vbox" },
    standalone: false
})
export class AdvSingleValueViewEditorComponent extends AbstractCustomViewEditor {

    @Input() cvDef: AdvSingleValueViewDefinition;

    @ViewChild('retrieveYasgui', { static: false }) retrieveYasgui: YasguiComponent;

    model: CustomViewModel = CustomViewModel.adv_single_value;

    retrieveEditor: CvSparqlEditorStruct = { mode: QueryMode.query, query: "", valid: true };

    retrieveRequiredReturnPlaceholders: CustomViewVariables[] = [CustomViewVariables.value];
    retrieveQuerySkeleton: string = "SELECT ?obj $value WHERE {\n" +
        "    $resource $trigprop ?obj .\n" +
        "    ...\n" +
        "}";

    retrieveRequiredPlaceholders: CustomViewVariables[] = [CustomViewVariables.resource, CustomViewVariables.trigprop];
    retrievePlaceholdersInfo: VariableInfoStruct[] = [
        { id: CustomViewVariables.value, descrTranslationKey: "The value to be rendered. It must be returned as well" },
        { id: CustomViewVariables.resource, descrTranslationKey: "represents the resource being described in ResourceView where the Custom View is shown" },
        { id: CustomViewVariables.trigprop, descrTranslationKey: "represents the predicate that will be associated to the Custom View" },
    ];


    constructor(private customViewService: CustomViewsServices, private basicModals: BasicModalsServices, private modalService: NgbModal, private translateService: TranslateService,
        private changeDetectorRef: ChangeDetectorRef) {
        super();
    }

    ngOnInit() {
        super.ngOnInit();
    }

    protected initCustomViewDef(): void {
        this.cvDef = {
            retrieve: this.retrieveQuerySkeleton,
            update: {
                field: "value",
                updateMode: UpdateMode.none
            },
            suggestedView: this.suggestedView,
        };
    }

    protected restoreEditor(): void {
        this.retrieveEditor.query = this.cvDef.retrieve;
        this.suggestedView = this.cvDef.suggestedView;
        this.refreshYasguiEditors();
    }

    onRetrieveChanged(event: QueryChangedEvent) {
        this.retrieveEditor.query = event.query;
        this.retrieveEditor.mode = event.mode;
        this.retrieveEditor.valid = event.valid;
        this.emitChanges();
    }


    private refreshYasguiEditors() {
        this.changeDetectorRef.detectChanges();
        this.retrieveYasgui.forceContentUpdate();
    }

    emitChanges(): void {
        this.cvDef.retrieve = this.retrieveEditor.query;
        this.cvDef.suggestedView = this.suggestedView;
        this.changed.emit(this.cvDef);
    }

    public isDataValid(): boolean {
        return this.isRetrieveOk();
    }

    private isRetrieveOk(): boolean {
        //- syntactic check
        if (!this.retrieveEditor.valid) {
            this.basicModals.alert({ key: "COMMONS.STATUS.ERROR" }, "Retrieve query contains syntactic error(s)", ModalType.warning);
            return false;
        }
        //- variables
        let retrieveQuery = this.retrieveEditor.query;
        for (let v of this.retrieveRequiredReturnPlaceholders) {
            if (!CvQueryUtils.isVariableReturned(retrieveQuery, "$" + v)) {
                this.basicModals.alert({ key: "COMMONS.STATUS.ERROR" }, "Required placeholder $" + v + " is not returned by Retrieve query.", ModalType.warning);
                return false;
            }
        }
        //- object: select must returns the object of the $resource $trigprop ?obj triple
        if (CvQueryUtils.getReturnedObjectVariable(retrieveQuery) == null) {
            this.basicModals.alert({ key: "COMMONS.STATUS.ERROR" }, "Object variable of pair $resource $trigprop either not detected or not returned in Retrieve query", ModalType.warning);
            return false;
        }
        //- placeholders
        for (let v of this.retrieveRequiredPlaceholders) {
            if (!CvQueryUtils.isPlaceholderInWhere(retrieveQuery, v)) {
                this.basicModals.alert({ key: "COMMONS.STATUS.ERROR" }, "Required placeholder $" + v + " missing in Retrieve query.", ModalType.warning);
                return false;
            }
        }
        return true;
    }

}