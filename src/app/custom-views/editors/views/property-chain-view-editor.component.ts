import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AbstractPropertiesBasedViewEditor } from './abstract-properties-based-view-editor';
import { CustomViewModel } from '../../CustomViews';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';

@Component({
    selector: 'property-chain-view-editor',
    templateUrl: "properties-based-view-editor.component.html",
    standalone: false
})
export class PropertyChainViewEditorComponent extends AbstractPropertiesBasedViewEditor {

    model: CustomViewModel = CustomViewModel.property_chain;

    propListLabelTranslationKey: string = "CUSTOM_VIEWS.MODELS.SINGLE_VALUE.PROPERTY_CHAIN";
    invalidPropListMsgTranslationKey: string = "CUSTOM_VIEWS.MESSAGES.INVALID_PROP_CHAIN";
    allowDuplicates: boolean = true;

    infoHtml: string = `With this view, objects connected to a resource in the ResourceView will be rendered with a single value reached by traversing a defined property chain.<br />
    E.g. in the following case, supposing <code>&lt;resource&gt;</code> is the resource being shown in the resource view, linked through a skos reified definition:
    <code class="my-2" style="display: block;">
        &lt;resource&gt; skos:definition &lt;defURI&gt; .<br/>
        &lt;defURI&gt; rdf:value "this is a definition"@en .
    </code>
    In order to see directly the literal of the definition from the property skos:definition, it suffices to:<br/>
    <ol>
    <li>Create a property chain made of the sole property <code>rdf:value</code></li>
    <li>Associate this view to the property <code>skos:definition</code>, so that this view is triggered whenever the propery <code>skos:definition</code> is found in the description of a resource in the resource view</li>
    </ol>`;

    constructor(basicModals: BasicModalsServices, sanitizer: DomSanitizer) {
        super(basicModals, sanitizer);
    }

    ngOnInit() {
        super.ngOnInit();
    }

}
