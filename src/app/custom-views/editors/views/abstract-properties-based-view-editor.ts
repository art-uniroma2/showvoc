import { Directive, Input } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { AbstractCustomViewEditor } from './abstract-custom-view-editor';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { PropertiesBasedViewDefinition } from '../../CustomViews';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { AnnotatedValue, IRI, RDFResourceRolesEnum } from 'src/app/models/Resources';

@Directive()
export abstract class AbstractPropertiesBasedViewEditor extends AbstractCustomViewEditor {

  @Input() cvDef: PropertiesBasedViewDefinition;

  RDFResourceRolesEnum = RDFResourceRolesEnum;

  properties: AnnotatedValue<IRI>[];

  abstract propListLabelTranslationKey: string;
  abstract invalidPropListMsgTranslationKey: string;
  abstract allowDuplicates: boolean; //tells if property list can contains duplicates (e.g. static-vector doesn't allow duplicated headers, prop-chain allows duplicated props)

  abstract infoHtml: string;

  infoHtmlSafe: SafeHtml;

  basicModals: BasicModalsServices;
  sanitizer: DomSanitizer;
  constructor(basicModals: BasicModalsServices, sanitizer: DomSanitizer) {
    super();
    this.basicModals = basicModals;
    this.sanitizer = sanitizer;
  }


  ngOnInit() {
    super.ngOnInit();
    this.infoHtmlSafe = this.sanitizer.bypassSecurityTrustHtml(this.infoHtml);
  }

  initCustomViewDef(): void {
    this.properties = [];
    this.cvDef = {
      properties: [],
      suggestedView: this.suggestedView
    };
  }

  restoreEditor(): void {
    this.properties = this.cvDef.properties.map(p => new AnnotatedValue(new IRI(p)));
    this.suggestedView = this.cvDef.suggestedView;
  }

  onPropertiesChanged(properties: AnnotatedValue<IRI>[]) {
    this.properties = properties;
    this.emitChanges();
  }

  emitChanges(): void {
    this.cvDef.properties = this.properties.map(p => p.getValue().toNT());
    this.cvDef.suggestedView = this.suggestedView;
    this.changed.emit(this.cvDef);
  }

  public isDataValid(): boolean {
    if (this.properties.length == 0) {
      this.basicModals.alert({ key: "COMMONS.STATUS.ERROR" }, { key: this.invalidPropListMsgTranslationKey }, ModalType.warning);
      return false;
    }
    return true;
  }

}
