import { ChangeDetectorRef, Component } from '@angular/core';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { CustomViewModel, CustomViewVariables } from '../../CustomViews';
import { AbstractSparqlBasedViewEditor, VariableInfoStruct } from './abstract-sparql-based-view-editor';

@Component({
    selector: 'series-view-editor',
    templateUrl: "sparql-based-view-editor.component.html",
    host: { class: "vbox" },
    standalone: false
})
export class SeriesViewEditorComponent extends AbstractSparqlBasedViewEditor {

    model: CustomViewModel = CustomViewModel.series;

    retrieveRequiredReturnVariables: CustomViewVariables[] = [CustomViewVariables.series_id, CustomViewVariables.name, CustomViewVariables.value];

    retrieveDescrIntro: string = "The retrieve query for this kind of view must return the following variables:";
    retrieveVariablesInfo: VariableInfoStruct[] = [
        { id: CustomViewVariables.series_id, descrTranslationKey: "The resource representing the series" },
        { id: CustomViewVariables.series_label, descrTranslationKey: "(optional) Where admitted, this value is used for identifying the X axis in the chart" },
        { id: CustomViewVariables.value_label, descrTranslationKey: "(optional) Where admitted, this value is used for identifying the Y axis in the chart" },
        { id: CustomViewVariables.name, descrTranslationKey: "Name of a single value" },
        { id: CustomViewVariables.value, descrTranslationKey: "Value to be represented on the chart. This is supposed to be bound to a literal numeric value" },
    ];
    retrieveQuerySkeleton: string = "SELECT ?series_id ?series_label ?value_label ?name ?value WHERE {\n" +
        "    $resource $trigprop ?series_id .\n" +
        "    ...\n" +
        "}";

    constructor(basicModals: BasicModalsServices, changeDetectorRef: ChangeDetectorRef) {
        super(basicModals, changeDetectorRef);
    }

    ngOnInit() {
        super.ngOnInit();
    }

}
