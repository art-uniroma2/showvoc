import { ChangeDetectorRef, Component } from '@angular/core';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { CustomViewModel, CustomViewVariables } from '../../CustomViews';
import { AbstractSparqlBasedViewEditor, VariableInfoStruct } from './abstract-sparql-based-view-editor';

@Component({
    selector: 'route-view-editor',
    templateUrl: "sparql-based-view-editor.component.html",
    host: { class: "vbox" },
    standalone: false
})
export class RouteViewEditorComponent extends AbstractSparqlBasedViewEditor {

    model: CustomViewModel = CustomViewModel.route;

    retrieveRequiredReturnVariables: CustomViewVariables[] = [CustomViewVariables.route_id, CustomViewVariables.location, CustomViewVariables.latitude, CustomViewVariables.longitude];

    retrieveDescrIntro: string = "The retrieve query for this kind of view must return the following variables:";
    retrieveVariablesInfo: VariableInfoStruct[] = [
        { id: CustomViewVariables.route_id, descrTranslationKey: "The resource representing the route" },
        { id: CustomViewVariables.location, descrTranslationKey: "A resource representing a single point of the route" },
        { id: CustomViewVariables.latitude, descrTranslationKey: "The latitude of the location. This is supposed to be bound to a literal numeric value (likely an xsd:double)" },
        { id: CustomViewVariables.longitude, descrTranslationKey: "The longitude of the location. This is supposed to be bound to a literal numeric value (likely an xsd:double)" },
    ];
    retrieveQuerySkeleton: string = "SELECT ?route_id ?location ?latitude ?longitude WHERE {\n" +
        "    $resource $trigprop ?route_id .\n" +
        "    ...\n" +
        "}";


    constructor(basicModals: BasicModalsServices, changeDetectorRef: ChangeDetectorRef) {
        super(basicModals, changeDetectorRef);
    }

    ngOnInit() {
        super.ngOnInit();
    }

}
