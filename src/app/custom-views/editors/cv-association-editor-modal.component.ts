import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalType } from "src/app/modal-dialogs/Modals";
import { BasicModalsServices } from "src/app/modal-dialogs/basic-modals/basic-modals.service";
import { AnnotatedValue, IRI, RDFResourceRolesEnum } from "src/app/models/Resources";
import { CustomViewAssociation, CustomViewConfiguration, CustomViewConst, CustomViewDefinitionKeys, CustomViewReference, ViewsEnum } from "../CustomViews";
import { CustomViewsServices } from "../custom-views.services";

@Component({
  selector: "cv-assosiaction-editor-modal",
  templateUrl: "./cv-association-editor-modal.component.html",
  standalone: false
})
export class CvAssociationEditorModalComponent {
  @Input() title: string;
  @Input() existingAssociations: CustomViewAssociation[];

  selectedProperty: AnnotatedValue<IRI>;

  customViews: CustomViewReference[];
  selectedCustomView: CustomViewReference;

  availableViews: { id: ViewsEnum, translationKey: string }[];
  defaultView: ViewsEnum;

  RDFResourceRolesEnum = RDFResourceRolesEnum;

  constructor(public activeModal: NgbActiveModal, private customViewsService: CustomViewsServices,
    private basicModals: BasicModalsServices) {
  }

  ngOnInit() {
    this.customViewsService.getViewsIdentifiers().subscribe(
      refs => {
        this.customViews = refs.map(ref => CustomViewReference.parseCustomViewReference(ref));
      }
    );
  }

  onPropertySelected(property: AnnotatedValue<IRI>) {
    //check if an association for the given predicate already exists
    if (this.existingAssociations.some(a => a.property.equals(property.getValue()))) {
      this.basicModals.alert({ key: "COMMONS.STATUS.WARNING" }, { key: "CUSTOM_VIEWS.MESSAGES.ALREADY_ASSOCIATED_PROPERTY" }, ModalType.warning);
      this.selectedProperty = null;
    } else {
      this.selectedProperty = property;
    }
  }

  selectCustomView(ref: CustomViewReference) {
    this.selectedCustomView = ref;
    this.customViewsService.getCustomView(ref.reference).subscribe(
      (cv: CustomViewConfiguration) => {
        this.availableViews = CustomViewConst.modelToViewMap[CustomViewConst.classNameToModelMap[cv.type]];
        this.defaultView = cv.getPropertyValue(CustomViewDefinitionKeys.suggestedView);
      }
    );
  }

  isDataValid(): boolean {
    return this.selectedCustomView != null && this.selectedProperty != null;
  }

  ok() {
    this.customViewsService.addAssociation(this.selectedProperty.getValue(), this.selectedCustomView.reference, this.defaultView).subscribe(
      () => {
        this.activeModal.close();
      }
    );
  }

  cancel() {
    this.activeModal.dismiss();
  }
}