import { Component } from "@angular/core";

@Component({
    selector: "custom-services-router",
    templateUrl: "./custom-service-router.component.html",
    host: { class: "pageComponent" },
    standalone: false
})
export class CustomServiceRouterComponent {

    currentRoute: "customService" | "reporter" = "customService";


    constructor() { }

}