import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BasicModalsServices } from "src/app/modal-dialogs/basic-modals/basic-modals.service";
import { ModalType } from "src/app/modal-dialogs/Modals";
import { ConfigurationComponents } from "src/app/models/Configuration";
import { Scope } from "src/app/models/Plugins";
import { ConfigurationsServices } from "src/app/services/configurations.service";
import { InvokableReportersServices } from "src/app/services/invokable-reporters.service";
import { STError } from "src/app/utils/STServicesUtils";

@Component({
  selector: "import-invokable-reporter-modal",
  templateUrl: "./import-invokable-reporter-modal.component.html",
  standalone: false
})
export class ImportInvokableReporterModalComponent {

  file: File;
  id: string;
  useInputId: boolean = true;
  scopes: Scope[];
  selectedScope: Scope;

  constructor(
    public activeModal: NgbActiveModal,
    private invokableReporterService: InvokableReportersServices,
    private configurationServices: ConfigurationsServices,
    private basicModals: BasicModalsServices
  ) { }

  ngOnInit() {
    this.configurationServices.getConfigurationManager(ConfigurationComponents.INVOKABLE_REPORER_STORE).subscribe(
      cfgMgr => {
        this.scopes = cfgMgr.configurationScopes;
        this.selectedScope = cfgMgr.scope;
      }
    );
  }

  fileChangeEvent(file: File) {
    this.file = file;
    if (this.useInputId) { //initializes the ID with the input file name
      this.id = file.name;
      if (this.id.endsWith(".cfg")) {
        this.id = this.id.substring(0, this.id.lastIndexOf("."));
      }
    }
  }

  isInputValid(): boolean {
    return this.file != null && (this.id != null && this.id.trim() != "");
  }

  ok() {
    let idParam: string;
    if (!this.useInputId) {
      idParam = this.id;
    }
    this.invokableReporterService.importInvokableReporter(this.file, this.selectedScope, idParam).subscribe(
      () => {
        this.activeModal.close();
      },
      (err: STError) => {
        let reporterErrors: ReporterErrorDetail[] = err.additionalInfo.reporterErrors;
        let reportErrorsMsg: string[] = [];
        for (let e of reporterErrors) {
          //at the moment, in ST, MissingOperationError is the only impl of ReporterErrorDetail, so handle only this
          if (e.type == MissingOperationError.errType) {
            reportErrorsMsg.push(e.type + ": " + (e as MissingOperationError).service + "/" + (e as MissingOperationError).name);
          }
        }
        let msg: string = err.message + ":\n" + reportErrorsMsg.map(e => "- " + e).join(";\n");
        this.basicModals.alert({ key: "COMMONS.STATUS.WARNING" }, msg, ModalType.warning);
      }
    );
  }

  cancel() {
    this.activeModal.dismiss();
  }

}

interface ReporterErrorDetail {
  type: string
}

class MissingOperationError implements ReporterErrorDetail {
  static errType = "missing-operation";
  extensionPath: string;
  name: string;
  service: string;
  type: string = MissingOperationError.errType;
}