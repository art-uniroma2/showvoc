import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { ModalOptions } from 'src/app/modal-dialogs/Modals';
import { TextOrTranslation, TranslationUtils } from 'src/app/utils/TranslationUtils';
import { Reference } from '../../../models/Configuration';
import { Report, ServiceInvocationDefinition } from '../../../models/InvokableReporter';
import { InvokableReporterEditorModalComponent } from './invokable-reporter-editor-modal.component';
import { ReportResultModalComponent } from './report-result-modal.component';
import { ServiceInvocationEditorModalComponent } from './service-invocation-editor-modal.component';


@Injectable()
export class InvokableReporterModalServices {

    constructor(private modalService: NgbModal, private translateService: TranslateService) { }

    public openInvokableReporterEditor(title: TextOrTranslation, existingReporters: Reference[], reporterRef?: Reference): Promise<void> {
        const modalRef: NgbModalRef = this.modalService.open(InvokableReporterEditorModalComponent, new ModalOptions('xl'));
        modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
        modalRef.componentInstance.existingReporters = existingReporters;
        if (reporterRef != null) modalRef.componentInstance.reporterRef = reporterRef;
        return modalRef.result;
    }

    public openServiceInvocationEditor(title: TextOrTranslation, invokableReporterRef: Reference, serviceInvocation?: { def: ServiceInvocationDefinition, idx: number }): Promise<void> {
        const modalRef: NgbModalRef = this.modalService.open(ServiceInvocationEditorModalComponent, new ModalOptions('full'));
        modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
        modalRef.componentInstance.invokableReporterRef = invokableReporterRef;
        if (serviceInvocation != null) modalRef.componentInstance.serviceInvocation = serviceInvocation;
        return modalRef.result;
    }

    public showReport(report: Report): Promise<void> {
        const modalRef: NgbModalRef = this.modalService.open(ReportResultModalComponent, new ModalOptions('lg'));
        modalRef.componentInstance.report = report;
        return modalRef.result;
    }

}