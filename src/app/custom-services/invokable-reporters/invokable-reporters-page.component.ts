import { Component } from "@angular/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import FileSaver from "file-saver";
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { InvokableReportersServices } from 'src/app/services/invokable-reporters.service';
import { Reference } from "../../models/Configuration";
import { ImportInvokableReporterModalComponent } from "./modals/import-invokable-reporter-modal.component";
import { InvokableReporterModalServices } from "./modals/invokable-reporter-modal.service";
import { TranslateService } from "@ngx-translate/core";
import { InvokableReporter, ServiceInvocationDefinition } from "src/app/models/InvokableReporter";
import { CustomService } from "src/app/models/CustomService";
import { LocalStorageManager } from "src/app/utils/LocalStorageManager";

@Component({
  selector: "invokable-reporters-component",
  templateUrl: "./invokable-reporters-page.component.html",
  host: { class: "hbox" },
  standalone: false
})
export class InvokableReportersPageComponent {

  reporters: Reference[];
  selectedReporter: Reference;

  constructor(
    private invokableReporterService: InvokableReportersServices,
    private invokableReporterModals: InvokableReporterModalServices,
    private basicModals: BasicModalsServices,
    private modalService: NgbModal,
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.initReporters();
  }

  private initReporters() {
    this.invokableReporterService.getInvokableReporterIdentifiers().subscribe(
      references => {
        this.reporters = references;
      }
    );
  }

  selectReporter(reporter: Reference) {
    this.selectedReporter = reporter;
  }

  createReporter() {
    this.invokableReporterModals.openInvokableReporterEditor({ key: "INVOKABLE_REPORTERS.ACTIONS.CREATE_INVOKABLE_REPORT" }, this.reporters).then(
      () => {
        this.initReporters();
      },
      () => { }
    );
  }

  deleteReporter() {
    this.basicModals.confirm({ key: "INVOKABLE_REPORTERS.ACTIONS.DELETE_INVOKABLE_REPORT" }, { key: "INVOKABLE_REPORTERS.MESSAGES.DELETE_INVOKABLE_REPORT_CONFIRM" }, ModalType.warning).then(
      () => {
        this.invokableReporterService.deleteInvokableReporter(this.selectedReporter.relativeReference).subscribe(
          () => {
            this.initReporters();
          }
        );
      }
    );
  }

  exportInvokableReporter() {
    //before exporting, inform user if reporter uses custom services
    this.invokableReporterService.getInvokableReporter(this.selectedReporter.relativeReference).subscribe(
      (reporter: InvokableReporter) => {
        let sections: ServiceInvocationDefinition[] = reporter.getPropertyValue("sections");
        let customServices: string[] = sections.filter(s => s.extensionPath == CustomService.CUSTOM_SERVICE_PATH).map(s => s.service + "/" + s.operation);
        if (customServices.length > 0) {
          let msg = this.translateService.instant("INVOKABLE_REPORTERS.MESSAGES.WARN_EXPORT_WITH_CUSTOM_SERVICES");
          msg += "\n\n" + customServices.map(s => "- " + s).join(";\n");
          this.basicModals.confirmCheckCookie({ key: "COMMONS.STATUS.WARNING" }, msg, LocalStorageManager.WARNING_INVOKABLE_REPORTER_WITH_CUSTOM_SERVICE, ModalType.warning).then(
            () => {
              this.exportImpl();
            },
            () => { }
          );
        } else {
          this.exportImpl();
        }
      }
    );
  }

  private exportImpl() {
    this.invokableReporterService.exportInvokableReporter(this.selectedReporter.relativeReference).subscribe(
      blob => {
        FileSaver.saveAs(blob, this.selectedReporter.identifier + ".cfg");
      }
    );
  }

  importInvokableReporter() {
    const modalRef: NgbModalRef = this.modalService.open(ImportInvokableReporterModalComponent, new ModalOptions());
    return modalRef.result.then(
      () => {
        this.initReporters();
      },
      () => { }
    );
  }

}