import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { WidgetModule } from '../widget/widget.module';
import { CustomServiceRouterComponent } from './custom-service-router.component';
import { CustomOperationComponent } from './custom-services-editor/custom-operation.component';
import { CustomServiceComponent } from './custom-services-editor/custom-service.component';
import { CustomServicesPageComponent } from './custom-services-editor/custom-services-page.component';
import { AuthorizationHelperModalComponent } from './custom-services-editor/modals/authorization-helper-modal.component';
import { CustomOperationEditorModalComponent } from './custom-services-editor/modals/custom-operation-editor-modal.component';
import { CustomOperationModalComponent } from './custom-services-editor/modals/custom-operation-modal.component';
import { CustomServiceEditorModalComponent } from './custom-services-editor/modals/custom-service-editor-modal.component';
import { CustomServiceModalServices } from './custom-services-editor/modals/custom-service-modal.service';
import { OperationTypeEditorComponent } from './custom-services-editor/modals/operation-type-editor.component';
import { InvokableReportersPageComponent } from './invokable-reporters/invokable-reporters-page.component';
import { InvokableReporterComponent } from './invokable-reporters/invokable-reporter.component';
import { InvokableReporterEditorModalComponent } from './invokable-reporters/modals/invokable-reporter-editor-modal.component';
import { InvokableReporterModalServices } from './invokable-reporters/modals/invokable-reporter-modal.service';
import { ReportResultModalComponent } from './invokable-reporters/modals/report-result-modal.component';
import { ServiceInvocationEditorModalComponent } from './invokable-reporters/modals/service-invocation-editor-modal.component';
import { ServiceInvocationComponent } from './invokable-reporters/service-invocation.component';
import { ImportCustomServiceModalComponent } from './custom-services-editor/modals/import-custom-service-modal.component';
import { ImportInvokableReporterModalComponent } from './invokable-reporters/modals/import-invokable-reporter-modal.component';


@NgModule({
    imports: [
        CommonModule,
        DragDropModule,
        FormsModule,
        NgbDropdownModule,
        NgbPopoverModule,
        TranslateModule,
        WidgetModule,
    ],
    declarations: [
        AuthorizationHelperModalComponent,
        CustomOperationComponent,
        CustomOperationEditorModalComponent,
        CustomOperationModalComponent,
        CustomServicesPageComponent,
        CustomServiceRouterComponent,
        CustomServiceComponent,
        CustomServiceEditorModalComponent,
        ImportCustomServiceModalComponent,
        ImportInvokableReporterModalComponent,
        InvokableReportersPageComponent,
        InvokableReporterComponent,
        InvokableReporterEditorModalComponent,
        ReportResultModalComponent,
        ServiceInvocationComponent,
        ServiceInvocationEditorModalComponent,
        OperationTypeEditorComponent,
    ],
    exports: [
        CustomServiceRouterComponent
    ],
    providers: [
        CustomServiceModalServices,
        InvokableReporterModalServices
    ]
})
export class CustomServicesModule { }