import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { ModalOptions } from 'src/app/modal-dialogs/Modals';
import { TextOrTranslation, TranslationUtils } from 'src/app/utils/TranslationUtils';
import { CustomOperationDefinition, CustomService } from '../../../models/CustomService';
import { CustomOperationEditorModalComponent } from './custom-operation-editor-modal.component';
import { CustomOperationModalComponent } from './custom-operation-modal.component';
import { CustomServiceEditorModalComponent } from './custom-service-editor-modal.component';


@Injectable()
export class CustomServiceModalServices {

    constructor(private modalService: NgbModal, private translateService: TranslateService) { }

    public openCustomServiceEditor(title: TextOrTranslation, serviceConf?: CustomService): Promise<void> {
        const modalRef: NgbModalRef = this.modalService.open(CustomServiceEditorModalComponent, new ModalOptions());
        modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
        if (serviceConf != null) modalRef.componentInstance.service = serviceConf;
        return modalRef.result;
    }

    public openCustomOperationEditor(title: TextOrTranslation, customServiceId: string, operation?: CustomOperationDefinition): Promise<void> {
        const modalRef: NgbModalRef = this.modalService.open(CustomOperationEditorModalComponent, new ModalOptions('xl'));
        modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
        modalRef.componentInstance.customServiceId = customServiceId;
        if (operation != null) modalRef.componentInstance.operation = operation;
        return modalRef.result;
    }

    public openCustomOperationView(operation: CustomOperationDefinition): Promise<void> {
        const modalRef: NgbModalRef = this.modalService.open(CustomOperationModalComponent, new ModalOptions('xl'));
        modalRef.componentInstance.operation = operation;
        return modalRef.result;
    }

}