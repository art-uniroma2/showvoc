import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CustomServiceServices } from "src/app/services/custom-service.service";

@Component({
    selector: "import-custom-service-modal",
    templateUrl: "./import-custom-service-modal.component.html",
    standalone: false
})
export class ImportCustomServiceModalComponent {
    
    file: File;
    id: string;
    useInputId: boolean = true;
    overwrite: boolean = false;

    constructor(
        public activeModal: NgbActiveModal, 
        private customServService: CustomServiceServices
    ) {}

    fileChangeEvent(file: File) {
        this.file = file;
        if (this.useInputId) { //initializes the ID with the input file name
            this.id = file.name;
            if (this.id.endsWith(".cfg")) {
                this.id = this.id.substring(0, this.id.lastIndexOf("."));
            }
        }
    }

    isInputValid(): boolean {
        return this.file != null && (this.id != null && this.id.trim() != "");
    }
    
    ok() {
        let idParam: string;
        if (!this.useInputId) {
            idParam = this.id;
        }
        this.customServService.importCustomService(this.file, idParam, this.overwrite).subscribe(
            () => {
                this.activeModal.close();     
            }
        );
    }

    cancel() {
        this.activeModal.dismiss();
    }

}