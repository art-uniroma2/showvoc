import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { Scope, Settings, STProperties } from 'src/app/models/Plugins';
import { RemoteRepositoryAccessConfig } from 'src/app/models/Project';
import { AuthServiceMode, ShowVocSettings, VocBenchConnectionShowVocSettings } from 'src/app/models/Properties';
import { AdministrationServices } from 'src/app/services/administration.service';
import { SettingsServices } from 'src/app/services/settings.service';
import { ShowVocServices } from 'src/app/services/showvoc.service';
import { SettingsManager, SettingsManagerID } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { SVProperties } from 'src/app/utils/SVProperties';
import { ToastService } from 'src/app/widget/toast/toast-service';

@Component({
    selector: 'system-config',
    templateUrl: './system-configuration.component.html',
    host: { class: "vbox" },
    standalone: false
})
export class SystemConfigurationComponent implements OnInit {

    private getSystemCoreSettingsFn: Observable<Settings> = this.settingsService.getSettings(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.SYSTEM);

    /* ST+VB configuration */
    private showVocSettings: ShowVocSettings;
    vbConnectionConfig: VocBenchConnectionShowVocSettings = { adminEmail: null, adminPassword: null, stHost: null, vbURL: null };
    private pristineVbConnConfig: VocBenchConnectionShowVocSettings;

    testVbConfigLoading: boolean;

    /* Remote access configuration */

    private remoteConfigsSetting: RemoteRepositoryAccessConfig[]; //on ST remote config is a list, here in ShowVoc it is shown just the first
    remoteAccessConfig: RemoteRepositoryAccessConfig = { serverURL: null, username: null, password: null };
    private pristineRemoteAccessConf: RemoteRepositoryAccessConfig;

    /* E-mail configuration */

    emailSettings: MailSettings;
    private emailSettingsPristine: MailSettings;
    cryptoProtocol: string;

    testEmailConfigLoading: boolean;

    /* Home content */
    homeContent: string;
    private homeContentPristine: string;
    safeHomeContent: SafeHtml;

    /* Other */

    disableContributions: boolean;

    authServiceModes: AuthServiceMode[] = [AuthServiceMode.Default, AuthServiceMode.SAML];
    selectedAuthServiceMode: AuthServiceMode;

    allowAnonymous: boolean;

    initializing: boolean;

    constructor(private adminService: AdministrationServices, private svService: ShowVocServices, private settingsService: SettingsServices,
        private svProp: SVProperties, private basicModals: BasicModalsServices, private toastService: ToastService,
        private changeDetectorRef: ChangeDetectorRef, private sanitizer: DomSanitizer) { }

    ngOnInit() {
        this.initAll();
    }

    private initAll() {
        this.getSystemCoreSettingsFn.subscribe(
            settings => {
                this.initEmailConfigHandler(settings);
                this.initRemoteConfigHandler(settings);
                this.initVbConfigHandler(settings);
                this.initOtherConfig(settings);
            }
        );
        this.initHomeContent(); //already initialized as system startup setting
    }

    /* ============================
     * E-mail managment
     * ============================ */

    private initEmailConfig() {
        this.getSystemCoreSettingsFn.subscribe(
            settings => {
                this.initEmailConfigHandler(settings);
            }
        );
    }

    private initEmailConfigHandler(settings: Settings) {
        let mailProp: STProperties = settings.getProperty(SettingsManager.PROP_NAME.mail);
        let mailPropCloned = mailProp.clone();
        this.emailSettings = mailProp.value;
        this.emailSettingsPristine = mailPropCloned.value;

        this.cryptoProtocol = "None";
        if (this.emailSettings.smtp.sslEnabled) {
            this.cryptoProtocol = "SSL";
        } else if (this.emailSettings.smtp.starttlsEnabled) {
            this.cryptoProtocol = "TLS";
        }
    }

    updateProtocol() {
        if (this.cryptoProtocol == "SSL") {
            this.emailSettings.smtp.sslEnabled = true;
            this.emailSettings.smtp.starttlsEnabled = false;
        } else if (this.cryptoProtocol == "TLS") {
            this.emailSettings.smtp.sslEnabled = false;
            this.emailSettings.smtp.starttlsEnabled = true;
        } else {
            this.emailSettings.smtp.sslEnabled = false;
            this.emailSettings.smtp.starttlsEnabled = false;
        }
    }

    updateEmailConfig() {
        this.settingsService.storeSetting(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.SYSTEM, SettingsManager.PROP_NAME.mail, this.emailSettings).subscribe(
            () => {
                this.initEmailConfig();
            }
        );
    }

    testEmailConfig() {
        if (this.isEmailConfigChanged()) {
            this.basicModals.alert({ key: "ADMINISTRATION.SYSTEM.EMAIL.EMAIL_CONFIG_TEST" }, { key: "MESSAGES.EMAIL_CONFIG_CHANGED" }, ModalType.warning);
            return;
        }

        this.basicModals.prompt({ key: "ADMINISTRATION.SYSTEM.EMAIL.EMAIL_CONFIG_TEST" }, { value: "Mail to" }, { key: "MESSAGES.EMAIL_CONFIG_TEST_INFO" },
            SVContext.getLoggedUser().getEmail()).then(
                mailTo => {
                    this.testEmailConfigLoading = true;
                    this.adminService.testEmailConfig(mailTo).pipe(
                        finalize(() => { this.testEmailConfigLoading = false; })
                    ).subscribe(
                        () => {
                            this.basicModals.alert({ key: "ADMINISTRATION.SYSTEM.EMAIL.EMAIL_CONFIG_TEST" }, { key: "MESSAGES.EMAIL_CONFIG_TEST_SUCCESS" });
                        }
                    );
                },
                () => { }
            );
    }

    isEmailConfigChanged(): boolean {
        return JSON.stringify(this.emailSettingsPristine) != JSON.stringify(this.emailSettings);
    }

    /* ============================
     * Remote access config managment
     * ============================ */

    private initRemoteConfig() {
        this.getSystemCoreSettingsFn.subscribe(
            settings => {
                this.initRemoteConfigHandler(settings);
            }
        );
    }

    private initRemoteConfigHandler(settings: Settings) {
        this.remoteConfigsSetting = settings.getPropertyValue(SettingsManager.PROP_NAME.remoteConfigs);
        if (this.remoteConfigsSetting == null || this.remoteConfigsSetting.length == 0) {
            this.remoteConfigsSetting = [this.remoteAccessConfig];
        }
        this.remoteAccessConfig = this.remoteConfigsSetting[0];
        this.pristineRemoteAccessConf = Object.assign({}, this.remoteAccessConfig);
    }

    updateRemoteConfig() {
        this.settingsService.storeSetting(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.SYSTEM, SettingsManager.PROP_NAME.remoteConfigs, this.remoteConfigsSetting).subscribe(
            () => {
                this.initRemoteConfig();
            }
        );
    }

    isRemoteConfigChanged() {
        for (let key in this.pristineRemoteAccessConf) {
            if (this.pristineRemoteAccessConf[key] != this.remoteAccessConfig[key]) {
                return true;
            }
        }
        return false;
    }

    /* ============================
     * VB config managment
     * ============================ */

    private initVbConfig() {
        this.getSystemCoreSettingsFn.subscribe(
            settings => {
                this.initVbConfigHandler(settings);
            }
        );
    }

    private initVbConfigHandler(settings: Settings) {
        this.showVocSettings = settings.getPropertyValue(SettingsManager.PROP_NAME.showvoc);
        if (this.showVocSettings == null) {
            this.showVocSettings = new ShowVocSettings();
        }

        if (this.showVocSettings.vbConnectionConfig != null) {
            this.vbConnectionConfig = this.showVocSettings.vbConnectionConfig;
        }
        this.pristineVbConnConfig = Object.assign({}, this.vbConnectionConfig);
    }

    updateVbConfig() {
        this.showVocSettings.vbConnectionConfig = this.vbConnectionConfig;
        this.settingsService.storeSetting(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.SYSTEM, SettingsManager.PROP_NAME.showvoc, this.showVocSettings).subscribe(
            () => {
                this.initVbConfig();
            }
        );
    }

    testVbConnection() {
        if (
            (!this.vbConnectionConfig.adminEmail || this.vbConnectionConfig.adminEmail.trim() == "") ||
            (!this.vbConnectionConfig.adminPassword || this.vbConnectionConfig.adminPassword.trim() == "") ||
            (!this.vbConnectionConfig.stHost || this.vbConnectionConfig.stHost.trim() == "")
        ) {
            this.basicModals.alert({ key: "ADMINISTRATION.SYSTEM.VB_CONFIG.VB_CONFIG_TEST" }, { key: "MESSAGES.VB_CONFIG_INCOMPLETE" }, ModalType.warning);
            return;
        }
        if (this.isVbConfigChanged()) {
            this.basicModals.alert({ key: "ADMINISTRATION.SYSTEM.VB_CONFIG.VB_CONFIG_TEST" }, { key: "MESSAGES.VB_CONFIG_CHANGED" }, ModalType.warning);
            return;
        }
        this.testVbConfigLoading = true;
        this.svService.testVocbenchConfiguration().pipe(
            finalize(() => { this.testVbConfigLoading = false; })
        ).subscribe(
            () => {
                this.basicModals.alert({ key: "ADMINISTRATION.SYSTEM.VB_CONFIG.VB_CONFIG_TEST" }, { key: "MESSAGES.VB_CONFIG_TEST_SUCCESS" });
            },
            (error: Error) => {
                this.basicModals.alert({ key: "ADMINISTRATION.SYSTEM.VB_CONFIG.VB_CONFIG_TEST" }, { key: "MESSAGES.VB_CONFIG_TEST_FAIL" }, ModalType.error, error.message);
            }
        );
    }

    isVbConfigChanged() {
        for (let key in this.pristineVbConnConfig) {
            if (this.pristineVbConnConfig[key] != this.vbConnectionConfig[key]) {
                return true;
            }
        }
        return false;
    }

    /* ============================
     * Home content
     * ============================ */

    private initHomeContent() {
        this.homeContent = SVContext.getSystemSettings().homeContent;
        this.homeContentPristine = this.homeContent;
        if (this.homeContent != null) {
            this.previewHomeContent();
        }
    }

    previewHomeContent() {
        this.safeHomeContent = null;
        if (this.homeContent) {
            this.safeHomeContent = this.sanitizer.bypassSecurityTrustHtml(this.homeContent);
        }
    }

    updateHomeContent() {
        if (this.homeContent.trim() == "") {
            this.homeContent = null;
        }
        this.svProp.setHomeContent(this.homeContent).subscribe(
            () => {
                this.homeContentPristine = this.homeContent;
            }
        );
    }

    isHomeContentChanged(): boolean {
        return this.homeContent != this.homeContentPristine;
    }


    /* ============================
     * Others
     * ============================ */

    private initOtherConfig(settings: Settings) {
        this.disableContributions = this.showVocSettings.disableContributions; //showVocSettings is already initialized in initVbConfigHandler which is invoked before initOtherConfig in initAll

        this.selectedAuthServiceMode = settings.getPropertyValue(SettingsManager.PROP_NAME.authService);
        this.allowAnonymous = settings.getPropertyValue(SettingsManager.PROP_NAME.allowAnonymous);
    }

    updateDisableContributions() {
        SVContext.getSystemSettings().disableContributions = this.disableContributions;
        this.showVocSettings.disableContributions = this.disableContributions;
        this.settingsService.storeSetting(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.SYSTEM, SettingsManager.PROP_NAME.showvoc, this.showVocSettings).subscribe();
    }


    onAuthServiceChanged(newValue: AuthServiceMode) {
        let oldValue = this.selectedAuthServiceMode;
        this.basicModals.confirm({ key: "COMMONS.STATUS.WARNING" }, { key: "MESSAGES.AUTH_MODE_CHANGE_WARN" }, ModalType.warning).then(
            () => {
                this.selectedAuthServiceMode = newValue;
                this.updateAuthServiceMode();
            },
            () => { //change rejected, restore previous value
                //this "hack" is needed in order to force the ngModel to detect the change
                this.selectedAuthServiceMode = null;
                this.changeDetectorRef.detectChanges();
                this.selectedAuthServiceMode = oldValue;
            }
        );
    }

    private updateAuthServiceMode() {
        SVContext.getSystemSettings().authService = this.selectedAuthServiceMode;
        this.settingsService.storeSetting(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.SYSTEM, SettingsManager.PROP_NAME.authService, this.selectedAuthServiceMode).subscribe();
    }

    onAllowAnonymousChange() {
        SVContext.getSystemSettings().allowAnonymous = this.allowAnonymous;
        this.svService.setAllowAnonymous(this.allowAnonymous).subscribe();
    }

}

class MailSettings {
    smtp: {
        auth: boolean,
        host: string,
        port: number,
        sslEnabled: boolean,
        starttlsEnabled: boolean
        sslProtocols: string,
    };
    from: {
        address: string,
        password: string,
        alias: string,
    };
}