import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalOptions } from 'src/app/modal-dialogs/Modals';
import { SharedModalsServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { Scope } from 'src/app/models/Plugins';
import { Project } from 'src/app/models/Project';
import { PredicateLabelSettings, PredLabelMapping } from 'src/app/models/Properties';
import { PredicateLabelSettingsModalComponent } from 'src/app/preferences/predicate-label-settings/predicate-label-settings-modal.component';
import { SettingsServices } from 'src/app/services/settings.service';
import { SettingsManager, SettingsManagerID } from 'src/app/utils/SettingsManager';

@Component({
    selector: 'proj-defaults-editor',
    templateUrl: './proj-defaults-editor.component.html',
    host: { class: "vbox" }
})
export class ProjectDefaultsEditorComponent implements OnInit {

    //Predicate Label Settings
    renderPredicates: boolean = false;
    predLabelMappings: { [lang: string]: PredLabelMapping };

    constructor(private settingsService: SettingsServices, private settingsMgr: SettingsManager,
        private modalService: NgbModal, private sharedModals: SharedModalsServices) { }

    ngOnInit() {
        /**
         * Defaults here are initialized from server and not by taking them from SettingsManager (cached):
         * - for authorized users the defaults are just used for fallback mechanism (in case of changes refresh cached settings, so to re-exploit fallback on defaults) 
         * - only for visitor users the defaults are cached client side but such users are not allowed to this page
         */
        this.settingsService.getSettingsDefault(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.PROJECT, Scope.SYSTEM).subscribe(
            settings => {
                let predLabelSettings: PredicateLabelSettings = settings.getPropertyValue(SettingsManager.PROP_NAME.resViewPredLabelMappings, new PredicateLabelSettings());
                this.renderPredicates = predLabelSettings.enabled;
                this.predLabelMappings = predLabelSettings.mappings;
            }
        );
    }

    /* ================
    * PREDICATE LABELS
    * ================= */

    onRenderPredicateChange() {
        this.updatePredLabelSettings();
    }

    editPredicateLabels() {
        this.sharedModals.selectDataset({ key: "COMMONS.ACTIONS.SELECT_DATASET" }, { key: "PREFERENCES.PRED_LABEL_MAPPING.SELECT_DATASET_MSG" }, true).then(
            (proj: Project) => {
                this.settingsMgr.initSettingsAtProjectAccess(proj).subscribe(
                    () => {
                        const modalRef: NgbModalRef = this.modalService.open(PredicateLabelSettingsModalComponent, new ModalOptions("xl"));
                        modalRef.componentInstance.predLabelMappings = this.predLabelMappings;
                        modalRef.componentInstance.project = proj;
                        modalRef.result.then(
                            (predLabelMappings) => {
                                this.predLabelMappings = predLabelMappings;
                                this.updatePredLabelSettings();
                            },
                            () => { }
                        );
                    }
                );
            },
            () => {}
        );
    }

    private updatePredLabelSettings() {
        let predicateLabelSettings: PredicateLabelSettings = {
            enabled: this.renderPredicates,
            mappings: this.predLabelMappings
        };
        this.settingsMgr.setDefaultPredLabelSettings(predicateLabelSettings).subscribe();
    }

}