import { Component, OnInit } from '@angular/core';
import { Scope } from 'src/app/models/Plugins';
import { ClickableValueStyle, ProjectVisualization } from 'src/app/models/Properties';
import { SettingsServices } from 'src/app/services/settings.service';
import { SettingsManager, SettingsManagerID } from 'src/app/utils/SettingsManager';

@Component({
    selector: 'user-defaults-editor',
    templateUrl: './user-defaults-editor.component.html',
    host: { class: "vbox" },
    standalone: false
})
export class UserDefaultsEditorComponent implements OnInit {

    clickableValueStyle: ClickableValueStyle;

    projVisualization: ProjectVisualization;

    projectRendering: boolean;


    constructor(private settingsService: SettingsServices, private settingsMgr: SettingsManager) { }

    ngOnInit() {
        /**
         * Defaults here are initialized from server and not by taking them from SettingsManager (cached):
         * - for authorized users the defaults are just used for fallback mechanism (in case of changes refresh cached settings, so to re-exploit fallback on defaults) 
         * - only for visitor users the defaults are cached client side but such users are not allowed to this page
         */
        this.settingsService.getSettingsDefault(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.USER, Scope.SYSTEM).subscribe(
            settings => {
                this.clickableValueStyle = settings.getPropertyValue(SettingsManager.PROP_NAME.clickableValueStyle, new ClickableValueStyle());
                this.projVisualization = settings.getPropertyValue(SettingsManager.PROP_NAME.projectVisualization, new ProjectVisualization());
                this.projectRendering = settings.getPropertyValue(SettingsManager.PROP_NAME.projectRendering, false);
            }
        );
    }

    onClickableStyleChanged() {
        this.settingsMgr.setDefaultClickableValueStyle(this.clickableValueStyle).subscribe();
    }

    onProjVisualizationChanged() {
        this.settingsMgr.setDefaultProjectVisualization(this.projVisualization).subscribe();
    }

    onProjRenderingChanged() {
        this.settingsMgr.setDefaultProjectRendering(this.projectRendering).subscribe();
    }

}