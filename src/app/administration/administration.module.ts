import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbDropdownModule, NgbPopoverModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { ContributionModule } from '../contribution/contribution.module';
import { PreferencesModule } from '../preferences/preferences.module';
import { UserModule } from '../user/user.module';
import { WidgetModule } from '../widget/widget.module';
import { AdminDashboardComponent } from './admin-dashboard.component';
import { ContentNegotiationConfigurationModalComponent } from './http-resolution/content-negotiation-config-modal.component';
import { HttpResolutionComponent } from './http-resolution/http-resolution.component';
import { InverseRewritingRulesComponent } from './http-resolution/inverse-rewriting-rules.component';
import { RewritingRulesComponent } from './http-resolution/rewriting-rules.component';
import { CreateAlignmentDownloadModalComponent } from './projects-manager/create-alignment-download-modal.component';
import { CreateDownloadModalComponent } from './projects-manager/create-download-modal.component';
import { CreateExternalDownloadModalComponent } from './projects-manager/create-external-download-modal.component';
import { CreateProjectModalComponent } from './projects-manager/create-project-modal.component';
import { LoadDataModalComponent } from './projects-manager/load-data-modal.component';
import { LoadDownloadModalComponent } from './projects-manager/load-download-modal.component';
import { ProjectSettingsModalComponent } from './projects-manager/project-settings-modal/project-settings-modal.component';
import { ResViewProjectSettingsComponent } from './projects-manager/project-settings-modal/res-view-project-settings.component';
import { ProjectUsersModalComponent } from './projects-manager/project-users-modal/project-users-modal.component';
import { ProjectsManagerComponent } from './projects-manager/projects-manager.component';
import { RemoteAccessConfigModalComponent } from './projects-manager/remote-access-config-modal.component';
import { DeleteRemoteRepoModalComponent } from './projects-manager/remote-repositories/delete-remote-repo-modal.component';
import { DeleteRemoteRepoReportModalComponent } from './projects-manager/remote-repositories/delete-remote-repo-report-modal.component';
import { RemoteRepoEditorModalComponent } from './projects-manager/remote-repositories/remote-repo-editor-modal.component';
import { RemoteRepoSelectionModalComponent } from './projects-manager/remote-repositories/remote-repo-selection-modal.component';
import { RenderingEngineConfigModalComponent } from './projects-manager/rendering-engine-config-modal.component';
import { UserDefaultsEditorComponent } from './settings/user-defaults/user-defaults-editor.component';
import { InitialConfigurationComponent } from './system-configuration/initial-configuration.component';
import { SystemConfigurationComponent } from './system-configuration/system-configuration.component';
import { UsersManagerComponent } from './users-manager/users-manager.component';

@NgModule({
  declarations: [
    AdminDashboardComponent,
    ContentNegotiationConfigurationModalComponent,
    CreateDownloadModalComponent,
    CreateExternalDownloadModalComponent,
    CreateAlignmentDownloadModalComponent,
    CreateProjectModalComponent,
    DeleteRemoteRepoModalComponent,
    DeleteRemoteRepoReportModalComponent,
    LoadDataModalComponent,
    LoadDownloadModalComponent,
    HttpResolutionComponent,
    InitialConfigurationComponent,
    InverseRewritingRulesComponent,
    ProjectsManagerComponent,
    // ProjectDefaultsEditorComponent,
    ProjectSettingsModalComponent,
    ProjectUsersModalComponent,
    RemoteAccessConfigModalComponent,
    RemoteRepoEditorModalComponent,
    RemoteRepoSelectionModalComponent,
    RenderingEngineConfigModalComponent,
    ResViewProjectSettingsComponent,
    RewritingRulesComponent,
    SystemConfigurationComponent,
    UserDefaultsEditorComponent,
    UsersManagerComponent
  ],
  imports: [
    CommonModule,
    ContributionModule,
    DragDropModule,
    FormsModule,
    NgbDropdownModule,
    NgbPopoverModule,
    NgbTooltipModule,
    PreferencesModule,
    RouterModule,
    TranslateModule,
    UserModule,
    WidgetModule
  ],
  exports: [],
  providers: []
})
export class AdministrationModule { }
