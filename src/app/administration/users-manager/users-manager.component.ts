import { ChangeDetectorRef, Component } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { User, UserForm, UserStatusEnum } from 'src/app/models/User';
import { AdministrationServices } from 'src/app/services/administration.service';
import { UserServices } from 'src/app/services/user.service';
import { RegistrationModalComponent } from 'src/app/user/registration-modal.component';
import { LocalStorageManager } from 'src/app/utils/LocalStorageManager';
import { SVContext } from 'src/app/utils/SVContext';

@Component({
  selector: 'users-manager',
  templateUrl: './users-manager.component.html',
  host: { class: "pageComponent" },
  styles: [`
      .online { color: green; font-weight: bold; } 
      .inactive { color: red; font-weight: bold; }
      .offline { color: lightgray }
  `],
  standalone: false
})
export class UsersManagerComponent {

  UserType = UserType;

  private allUsers: User[];

  currentUser: User;

  users: User[];
  selectedUser: User;

  filterString: string;

  constructor(private usersService: UserServices, private adminService: AdministrationServices,
    private modalService: NgbModal, private basicModals: BasicModalsServices, private translateService: TranslateService,
    private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.currentUser = SVContext.getLoggedUser();
    this.initUsers();
  }

  initUsers() {
    this.usersService.listUsers().subscribe(
      users => {
        this.allUsers = users;
        if (this.selectedUser != null) {
          let userToSelect = this.allUsers.find(u => u.getIri().equals(this.selectedUser.getIri()));
          this.selectUser(userToSelect);
        }
        this.applyFilters();
      }
    );
  }

  selectUser(user: User) {
    this.selectedUser = null;
    if (user != this.selectedUser) {
      /*
      do this "deselect+select" to force the reset of editable-input (in user-details),
      otherwise, if user started editing a value, the widget will still show the input field in edit mode
      */
      this.cdRef.detectChanges();
      this.selectedUser = user;
    }
  }

  createUser() {
    let modalRef: NgbModalRef = this.modalService.open(RegistrationModalComponent, new ModalOptions('lg'));
    modalRef.componentInstance.title = this.translateService.instant("ADMINISTRATION.USERS.CREATE_USER");
    modalRef.result.then(
      (userForm: UserForm) => {
        this.usersService.createUser(userForm.email, userForm.password, userForm.givenName, userForm.familyName).subscribe(
          () => {
            this.initUsers();
          }
        );
      },
      () => { }
    );
  }

  applyFilters() {
    this.users = this.allUsers.filter(u => {
      return this.filterString == null ||
        this.filterString.trim() == "" ||
        u.getGivenName().toLocaleLowerCase().includes(this.filterString.toLocaleLowerCase()) ||
        u.getFamilyName().toLocaleLowerCase().includes(this.filterString.toLocaleLowerCase()) ||
        u.getEmail().toLocaleLowerCase().includes(this.filterString.toLocaleLowerCase());
    });
  }

  changeUserType(toType: UserType) {
    let msgTranslationKey = this.getChangeUserTypeWarningMsg(this.selectedUser, toType);
    this.basicModals.confirmCheckCookie({ key: "COMMONS.STATUS.WARNING" }, { key: msgTranslationKey, params: { user: this.selectedUser.getShow() } }, LocalStorageManager.WARNING_ADMIN_CHANGE_USER_TYPE, ModalType.warning).then(
      () => {
        let removeFn: Observable<void> = of(null);
        if (this.selectedUser.isAdmin()) { // from admin
          removeFn = this.adminService.removeAdministrator(this.selectedUser).pipe(tap(() => { this.selectedUser.setAdmin(false); }));
        } else if (this.selectedUser.isSuperUser(true)) { // from superuser
          removeFn = this.adminService.removeSuperUser(this.selectedUser).pipe(tap(() => { this.selectedUser.setSuperUser(false); }));
        }
        let addFn: Observable<void> = of(null);
        if (toType == UserType.admin) {
          addFn = this.adminService.setAdministrator(this.selectedUser).pipe(tap(() => { this.selectedUser.setAdmin(true); }));
        } else if (toType == UserType.su) {
          addFn = this.adminService.setSuperUser(this.selectedUser).pipe(tap(() => { this.selectedUser.setSuperUser(true); }));
        }
        removeFn.subscribe(
          () => {
            addFn.subscribe(
              () => {
                // this.initUsers();
              }
            );
          }
        );
      },
      () => { }
    );
  }

  private getChangeUserTypeWarningMsg(user: User, toType: UserType): string {
    let msg: string;
    if (user.isAdmin()) {
      if (toType == UserType.su) {
        msg = "MESSAGES.CHANGE_USER_TYPE_ADMIN_TO_SUPER_CONFIRM";
      } else if (toType == UserType.user) {
        msg = "MESSAGES.CHANGE_USER_TYPE_ADMIN_TO_USER_CONFIRM";
      }
    } else if (user.isSuperUser(true)) {
      if (toType == UserType.admin) {
        msg = "MESSAGES.CHANGE_USER_TYPE_SUPER_TO_ADMIN_CONFIRM";
      } else if (toType == UserType.user) {
        msg = "MESSAGES.CHANGE_USER_TYPE_SUPER_TO_USER_CONFIRM";
      }
    } else { //nor admin or superuser
      if (toType == UserType.admin) {
        msg = "MESSAGES.CHANGE_USER_TYPE_USER_TO_ADMIN_CONFIRM";
      } else if (toType == UserType.su) {
        msg = "MESSAGES.CHANGE_USER_TYPE_USER_TO_SUPER_CONFIRM";
      }
    }
    return msg;
  }

  deleteUser(user: User) {
    this.basicModals.confirm({ key: "COMMONS.STATUS.WARNING" }, { key: "MESSAGES.DELETE_USER_CONFIRM_WARN", params: { user: user.getShow() } }, ModalType.warning).then(
      () => {
        this.usersService.deleteUser(user.getEmail()).subscribe(
          () => {
            this.selectedUser = null;
            this.initUsers();
          }
        );
      },
      () => { }
    );
  }

  isChangeStatusButtonDisabled() {
    //user cannot change status to himself or to an administrator user
    return SVContext.getLoggedUser().getEmail() == this.selectedUser.getEmail() || this.selectedUser.isAdmin();
  }

  changeUserStatus() {
    if (this.selectedUser.isAdmin()) { //check performed for robustness, this should never happen since it is forbidden by the UI
      this.basicModals.alert({ key: "COMMONS.STATUS.OPERATION_DENIED" }, { key: "MESSAGES.CANNOT_CHANGE_ADMIN_STATUS" }, ModalType.warning);
      return;
    }
    let enabled = this.selectedUser.getStatus() == UserStatusEnum.ACTIVE;
    if (enabled) {
      this.usersService.enableUser(this.selectedUser.getEmail(), false).subscribe(
        () => {
          this.selectedUser.setStatus(UserStatusEnum.INACTIVE);
        }
      );
    } else {
      this.usersService.enableUser(this.selectedUser.getEmail(), true).subscribe(
        () => {
          this.selectedUser.setStatus(UserStatusEnum.ACTIVE);
        }
      );
    }
  }
}

enum UserType {
  admin = "admin",
  su = "su",
  user = "user"
}