import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SVContext } from '../utils/SVContext';

@Component({
  selector: 'admin-dashboard-component',
  templateUrl: './admin-dashboard.component.html',
  host: { class: "pageComponent" },
  standalone: false
})
export class AdminDashboardComponent implements OnInit {

  isAdmin: boolean;

  constructor(private router: Router) { }

  ngOnInit() {
    this.isAdmin = SVContext.getLoggedUser()?.isAdmin();
  }

  isRouteActive(route: string[]): boolean {
    /**
     * The button group item "Defaults" is a dropdown, so I cannot make it active with the usual routerLink+routerLinkActive because:
     * - The active item is the actual dropown item, not the button
     * - If I apply routerLink+routerLinkActive also to the button, when clicked it will redirect to the route
     */
    let url: string = this.router.url.split("?")[0]; //exclude URL args (if any)
    let splittedUrl = url.split("/");
    let active = splittedUrl[splittedUrl.length - 1];
    return route.includes(active);
  }


}