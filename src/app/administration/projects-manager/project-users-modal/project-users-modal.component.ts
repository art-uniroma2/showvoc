import { Component, ElementRef, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Project } from 'src/app/models/Project';
import { STRoles, User } from 'src/app/models/User';
import { AdministrationServices } from 'src/app/services/administration.service';
import { UserServices } from 'src/app/services/user.service';
import { SVContext } from "src/app/utils/SVContext";
import { UIUtils } from 'src/app/utils/UIUtils';

@Component({
  selector: "project-users-modal",
  templateUrl: "./project-users-modal.component.html",
  standalone: false
})
export class ProjectUsersModalComponent {

  @Input() project: Project;

  usersBound: User[];
  allUsers: User[];

  constructor(public activeModal: NgbActiveModal, private adminService: AdministrationServices, private userService: UserServices,
    private elementRef: ElementRef) { }

  ngOnInit() {
    SVContext.setTempProject(this.project);
    this.init();
  }

  ngAfterViewInit() {
    UIUtils.setFullSizeModal(this.elementRef);
  }

  init() {
    this.userService.listUsers().subscribe(
      users => {
        this.allUsers = users;
      }
    );
    this.userService.listUsersBoundToProject(this.project).subscribe(
      users => {
        this.usersBound = users;
      }
    );
  }

  isBound(user: User): boolean {
    return this.usersBound && this.usersBound.some(u => u.getIri().equals(user.getIri()));
  }

  addUser(user: User) {
    this.adminService.addRolesToUser(this.project, user.getEmail(), [STRoles.projectmanager]).subscribe(
      () => {
        this.usersBound.push(user);
        this.usersBound.sort((u1: User, u2: User) => u1.getGivenName().localeCompare(u2.getGivenName()));
      }
    );
  }

  removeUser(user: User) {
    this.adminService.removeUserFromProject(this.project, user.getEmail()).subscribe(
      () => {
        this.usersBound.splice(this.usersBound.indexOf(user), 1);
      }
    );
  }

  close() {
    SVContext.removeTempProject();
    this.activeModal.dismiss();
  }

}