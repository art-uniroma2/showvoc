import { ChangeDetectorRef, Component, ViewChild } from "@angular/core";
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from "@ngx-translate/core";
import { finalize } from 'rxjs/operators';
import { IRI } from 'src/app/models/Resources';
import { ProjectsServices } from 'src/app/services/projects.service';
import { SettingsServices } from "src/app/services/settings.service";
import { SVContext } from "src/app/utils/SVContext";
import { ExtensionConfiguratorComponent } from 'src/app/widget/extension-configurator/extension-configurator.component';
import { BasicModalsServices } from '../../modal-dialogs/basic-modals/basic-modals.service';
import { ModalOptions, ModalType } from '../../modal-dialogs/Modals';
import { ConfigurableExtensionFactory, ExtensionFactory, ExtensionPointID, PluginSpecification, Settings } from '../../models/Plugins';
import { BackendTypesEnum, Repository, RepositoryAccess } from '../../models/Project';
import { ExtensionsServices } from '../../services/extensions.service';
import { AbstractProjectCreationModal } from "./abstract-project-creation-modal";
import { RemoteRepoSelectionModalComponent } from './remote-repositories/remote-repo-selection-modal.component';

@Component({
  selector: "create-project-modal",
  templateUrl: "./create-project-modal.component.html",
  standalone: false
})
export class CreateProjectModalComponent extends AbstractProjectCreationModal {

  @ViewChild("dataRepoConfigurator") dataRepoConfigurator: ExtensionConfiguratorComponent;

  loading: boolean;

  isAdmin: boolean;

  dataRepoId: string;

  //backend types (when accessing an existing remote repository)
  backendTypes: BackendTypesEnum[] = [BackendTypesEnum.openrdf_NativeStore, BackendTypesEnum.openrdf_MemoryStore, BackendTypesEnum.graphdb_FreeSail];
  selectedCoreRepoBackendType: BackendTypesEnum = this.backendTypes[0];

  //not used (since in ShowVoc the project creation doesn't support History nor Validation), but still necessary to the createProject()
  private supportRepoId: string;


  optionalSettingsOpen: boolean = false;

  //RENDERING GENERATOR PLUGIN
  rendEngUseDefaultSetting: boolean = true;
  rendEngExtensions: ConfigurableExtensionFactory[]; //available extensions for rendering engine
  selectedRendEngExtension: ConfigurableExtensionFactory; //chosen extension for rendering engine
  selectedRendEngExtensionConf: Settings; //chosen configuration for the chosen rendering engine extension

  constructor(activeModal: NgbActiveModal, settingsService: SettingsServices, extensionsService: ExtensionsServices, modalService: NgbModal, changeDetectorRef: ChangeDetectorRef,
    private projectService: ProjectsServices, private basicModals: BasicModalsServices, private translateService: TranslateService) {
    super(activeModal, modalService, extensionsService, settingsService, changeDetectorRef);
  }

  ngOnInit() {
    this.isAdmin = SVContext.getLoggedUser()?.isAdmin();
    this.initCoreRepoExtensions();
    this.initRemoteConfigs();
    this.initRenderingEngine();
  }

  initRenderingEngine() {
    //init rendering engine plugin
    this.extensionsService.getExtensions(ExtensionPointID.RENDERING_ENGINE_ID).subscribe(
      (extensions: ExtensionFactory[]) => {
        this.rendEngExtensions = extensions as ConfigurableExtensionFactory[];
      }
    );
  }

  /**
   * If the user is creation a project (not accessing an existing one),
   * the data repository IDs are determined from project's name
   */
  onProjectNameChange() {
    if (this.isRepoAccessCreateMode()) {
      this.dataRepoId = this.projectName.trim().replace(new RegExp(" ", 'g'), "_") + "_core";
      this.supportRepoId = this.projectName.trim().replace(new RegExp(" ", 'g'), "_") + "_support";
    }
  }

  /**
   * Handler of the button to explore remote repositories (when accessing an existing one)
   * @returns
   */
  changeRemoteRepository() {
    if (this.selectedRemoteRepoConfig == null || this.selectedRemoteRepoConfig.serverURL == null) {
      this.basicModals.alert({ key: "COMMONS.CONFIG.MISSING_CONFIGURATION" }, { key: "MESSAGES.REMOTE_REPO_ACCESS_NOT_CONFIGURED" }, ModalType.warning);
      return;
    }

    const modalRef: NgbModalRef = this.modalService.open(RemoteRepoSelectionModalComponent, new ModalOptions("lg"));
    modalRef.componentInstance.title = this.translateService.instant("ADMINISTRATION.DATASETS.REMOTE.SELECT_REMOTE_REPO");
    modalRef.componentInstance.repoConfig = this.selectedRemoteRepoConfig;
    modalRef.result.then(
      (repo: Repository) => {
        this.dataRepoId = repo.id;
      },
      () => { }
    );
  }

  ok() {
    //check project name
    if (!this.projectName || this.projectName.trim() == "") {
      this.basicModals.alert({ key: "DATASETS.ACTIONS.CREATE_DATASET" }, { key: "MESSAGES.DATASET_NAME_MISSING" }, ModalType.warning);
      return;
    }
    //check baseURI
    if (this.baseURI == null || this.baseURI.trim() == "") {
      this.basicModals.alert({ key: "DATASETS.ACTIONS.CREATE_DATASET" }, { key: "MESSAGES.BASEURI_MISSING" }, ModalType.warning);
      return;
    }

    let namespacePar: string;
    if (!this.lockNs) {
      namespacePar = this.namespace;
      if (namespacePar == null || namespacePar.trim() == "") {
        this.basicModals.alert({ key: "DATASETS.ACTIONS.CREATE_DATASET" }, { key: "MESSAGES.NAMESPACE_MISSING" }, ModalType.warning);
        return;
      }
    }

    /**
     * Prepare repositoryAccess parameter
     */
    let repositoryAccess: RepositoryAccess = new RepositoryAccess(this.selectedRepositoryAccess);
    //in case of remote repository access, set the configuration
    if (this.isRepoAccessRemote()) {
      if (this.selectedRemoteRepoConfig == null) {
        this.basicModals.alert({ key: "COMMONS.CONFIG.MISSING_CONFIGURATION" }, { key: "MESSAGES.REMOTE_REPO_ACCESS_NOT_CONFIGURED" }, ModalType.warning);
        return;
      }
      repositoryAccess.setConfiguration(this.selectedRemoteRepoConfig);
    }

    //check if data repository configuration needs to be configured
    if (this.selectedDataRepoConfig.requireConfiguration()) {
      this.basicModals.alert({ key: "COMMONS.CONFIG.MISSING_CONFIGURATION" }, { key: "MESSAGES.DATA_REPO_NOT_CONFIGURED" }, ModalType.warning);
      return;
    }
    let coreRepoSailConfigurerSpecification: PluginSpecification = {
      factoryId: this.selectedDataRepoExtension.id,
      configType: this.selectedDataRepoConfig.type,
      configuration: this.selectedDataRepoConfig.getPropertiesAsMap()
    };

    //backend types
    let coreRepoBackendType: BackendTypesEnum;
    if (!this.isRepoAccessCreateMode()) {
      coreRepoBackendType = this.selectedCoreRepoBackendType;
    }

    //support repo ID even if it's not used it is a mandatory param. Here set a fake id only if not explicitly initialized
    if (this.supportRepoId == null) {
      this.supportRepoId = this.projectName + "_support";
    }

    /**
     * Prepare renderingEngineSpecification parameter
     */
    let renderingEngineSpecification: PluginSpecification;
    if (!this.rendEngUseDefaultSetting) {
      //check if uriGenerator plugin needs to be configured
      if (this.selectedRendEngExtensionConf?.requireConfiguration()) {
        //...and in case if every required configuration parameters are not null
        this.basicModals.alert({ key: "COMMONS.STATUS.WARNING" }, { key: "MESSAGES.MISSING_RENDERING_ENGINE_CONFIG" }, ModalType.warning);
        return;
      }

      renderingEngineSpecification = {
        factoryId: this.selectedRendEngExtension.id,
        configuration: this.selectedRendEngExtensionConf.getPropertiesAsMap(true)
      };
    }

    this.loading = true;
    this.projectService.createProject(this.projectName, this.baseURI, namespacePar, new IRI(this.selectedSemModel), new IRI(this.selectedLexModel),
      false, false, false, repositoryAccess, this.dataRepoId, this.supportRepoId, coreRepoSailConfigurerSpecification, coreRepoBackendType,
      null, null, null, null, null, renderingEngineSpecification)
      .pipe(
        finalize(() => { this.loading = false; })
      ).subscribe(() => {
        this.basicModals.alert({ key: "DATASETS.STATUS.DATASET_CREATED" }, { key: "MESSAGES.DATASET_CREATED" });
        this.activeModal.close();
      });
  }

}