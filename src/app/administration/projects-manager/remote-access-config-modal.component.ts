import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { Scope } from "src/app/models/Plugins";
import { RemoteRepositoryAccessConfig } from 'src/app/models/Project';
import { SettingsServices } from "src/app/services/settings.service";
import { SettingsManager, SettingsManagerID } from "src/app/utils/SettingsManager";

@Component({
  selector: "remote-access-config-modal",
  templateUrl: "./remote-access-config-modal.component.html",
  standalone: false
})
export class RemoteAccessConfigModalComponent {

  savedConfigs: RemoteRepositoryAccessConfig[] = [];

  newConfig: RemoteRepositoryAccessConfig = { serverURL: null, username: null, password: null };


  constructor(public activeModal: NgbActiveModal, private settingsService: SettingsServices, private basicModals: BasicModalsServices) { }

  ngOnInit() {
    this.settingsService.getSettings(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.SYSTEM).subscribe(
      settings => {
        let remoteConfSetting: RemoteRepositoryAccessConfig[] = settings.getPropertyValue(SettingsManager.PROP_NAME.remoteConfigs);
        if (remoteConfSetting != null) {
          this.savedConfigs = remoteConfSetting;
        }
      }
    );
  }

  createConfiguration() {
    //add the new configuration only if another config with the same url doesn't exist
    for (const conf of this.savedConfigs) {
      if (conf.serverURL == this.newConfig.serverURL) {
        this.basicModals.alert({ key: "COMMONS.CONFIG.DUPLICATED_CONFIGURATION" }, { key: "MESSAGES.DUPLICATED_SERVER_URL_CONFIG" }, ModalType.error);
        return;
      }
    }
    this.savedConfigs.push(this.newConfig);
    this.updateConfigurations();
    this.newConfig = { serverURL: null, username: null, password: null }; //reset config
  }

  deleteConfig(c: RemoteRepositoryAccessConfig) {
    this.savedConfigs.splice(this.savedConfigs.indexOf(c), 1);
    this.updateConfigurations();
  }
  updateConfServerURL(conf: RemoteRepositoryAccessConfig, newValue: string) {
    conf.serverURL = newValue;
    this.updateConfigurations();
  }
  updateConfUsername(conf: RemoteRepositoryAccessConfig, newValue: string) {
    conf.username = newValue;
    this.updateConfigurations();
  }
  updateConfPassword(conf: RemoteRepositoryAccessConfig, newValue: string) {
    conf.password = newValue;
    this.updateConfigurations();
  }
  private updateConfigurations() {
    this.settingsService.storeSetting(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.SYSTEM, SettingsManager.PROP_NAME.remoteConfigs, this.savedConfigs).subscribe();
  }

  ok() {
    this.activeModal.close();
  }

  close() {
    this.activeModal.dismiss();
  }

}