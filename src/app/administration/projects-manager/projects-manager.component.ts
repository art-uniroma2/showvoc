import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, from, Observable, Observer } from 'rxjs';
import { defaultIfEmpty, finalize } from 'rxjs/operators';
import { DatasetsSettingsModalComponent } from 'src/app/datasets/datasets-page/datasets-settings-modal.component';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { CheckOptions, ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { PluginSettingsHandler } from 'src/app/modal-dialogs/shared-modals/plugin-config-modal/plugin-config-modal.component';
import { SharedModalsServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { Scope, Settings } from 'src/app/models/Plugins';
import { DatasetDirEntryI, DatasetViewsUtils, ExceptionDAO, Project, ProjectFacets, ProjectViewMode, ProjectVisibility, RemoteRepositorySummary, RepositorySummary } from 'src/app/models/Project';
import { ProjectVisualization } from 'src/app/models/Properties';
import { OntoLex, SKOS } from 'src/app/models/Vocabulary';
import { GlobalSearchServices } from 'src/app/services/global-search.service';
import { InputOutputServices } from 'src/app/services/input-output.service';
import { MapleServices } from 'src/app/services/maple.service';
import { ProjectsServices } from 'src/app/services/projects.service';
import { RepositoriesServices } from 'src/app/services/repositories.service';
import { QueryParamService } from 'src/app/utils/QueryParamService';
import { SettingsManager, SettingsManagerID } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { SVEventHandler } from 'src/app/utils/SVEventHandler';
import { CreateDownloadModalComponent } from './create-download-modal.component';
import { CreateProjectModalComponent } from './create-project-modal.component';
import { LoadDataModalComponent } from './load-data-modal.component';
import { ProjectSettingsModalComponent } from './project-settings-modal/project-settings-modal.component';
import { ProjectUsersModalComponent } from './project-users-modal/project-users-modal.component';
import { DeleteRemoteRepoModalComponent } from './remote-repositories/delete-remote-repo-modal.component';
import { DeleteRemoteRepoReportModalComponent } from './remote-repositories/delete-remote-repo-report-modal.component';
import { RemoteRepoEditorModalComponent } from './remote-repositories/remote-repo-editor-modal.component';
import { RenderingEngineConfigModalComponent } from './rendering-engine-config-modal.component';
import { LocalizedMap } from 'src/app/widget/localized-editor/localized-editor.component';
import { STRoles } from 'src/app/models/User';

@Component({
  selector: 'projects-manager',
  templateUrl: './projects-manager.component.html',
  host: { class: "pageComponent" },
  styleUrls: ['./projects-manager.component.css'],
  standalone: false
})
export class ProjectsManagerComponent {

  projVisualization: ProjectVisualization;
  visualizationMode: ProjectViewMode;

  isAdmin: boolean;
  isCreateProjectAuthorized: boolean;

  rendering: boolean;

  projectList: Project[];
  projectDirs: ProjectDirEntry[];

  filterString: string;

  //usefuld attributes to set in the Project objects
  private readonly clearingIndexAttr: string = "clearingIndex"; //stores a boolean that tells if the system is clearing the index for the project
  private readonly creatingIndexAttr: string = "creatingIndex"; //stores a boolean that tells if the system is creating the index for the project
  private readonly creatingMetadataAttr: string = "creatingMetadata"; //stores a boolean that tells if the system is creating the metadata for the project
  private readonly openingAttr: string = "opening"; //stores a boolean that tells if the system is creating the index for the project
  private readonly clearingDataAttr: string = "clearingData"; //stores a boolean that tells if the system is clearing the data of project
  private readonly changingStatus: string = "changingStatus"; //stores a boolean that tells if the project status changing is in progress

  readonly visibilityStatusMap: { [visibility: string]: string } = {
    [ProjectVisibility.PRISTINE]: "Pristine",
    [ProjectVisibility.AUTHORIZED]: "Staging",
    [ProjectVisibility.PUBLIC]: "Public",
  };

  globalCreatingIndex: boolean = false; //when it's true, all the other "create index" button should be disabled


  constructor(private modalService: NgbModal, private projectService: ProjectsServices, private repositoriesService: RepositoriesServices,
    private inputOutputService: InputOutputServices, private globalSearchService: GlobalSearchServices, private mapleService: MapleServices,
    private basicModals: BasicModalsServices, private sharedModals: SharedModalsServices, private router: Router, private queryParamService: QueryParamService,
    private eventHandler: SVEventHandler, private settingsMgr: SettingsManager, private translateService: TranslateService) { }

  ngOnInit() {
    this.isAdmin = SVContext.getLoggedUser()?.isAdmin();
    this.isCreateProjectAuthorized = SVContext.getLoggedUser()?.isSuperUser(false); //SU and admin can create projects
    this.initProjects();

    this.rendering = this.settingsMgr.getProjectRendering();
  }

  initProjects() {
    this.projVisualization = this.settingsMgr.getSetting(Scope.USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.projectVisualization, null, new ProjectVisualization());
    this.visualizationMode = this.projVisualization.mode;
    //init projects
    if (this.visualizationMode == ProjectViewMode.list) { //as list
      this.initProjectList();
    } else { //as bagOf facets based
      this.initProjectDirectories();
    }
  }

  private initProjectList() {
    this.projectList = [];

    let roleFilter = this.isAdmin ? null : STRoles.projectmanager; //if not admin, filter by PM role
    this.projectService.listProjects(null, null, null, roleFilter).subscribe(
      (projects: Project[]) => {
        projects.sort((p1: Project, p2: Project) => {
          return p1.getLabel().toLocaleLowerCase().localeCompare(p2.getLabel().toLocaleLowerCase());
        });
        this.projectList = projects;
      }
    );
  }

  private initProjectDirectories() {
    this.projectDirs = [];

    let bagOfFacet = this.projVisualization.facetBagOf;

    let roleFilter = this.isAdmin ? null : STRoles.projectmanager; //if not admin, filter by PM role

    this.projectService.retrieveProjects(bagOfFacet, null, null, roleFilter).subscribe(
      projectBags => {
        Object.keys(projectBags).forEach(bag => {
          let dirEntry = this.projectDirs.find(pd => pd.dirName == bag);
          if (dirEntry == null) { //if a dir with such name is not yet available, add a new entry
            dirEntry = new ProjectDirEntry(bag);
            this.projectDirs.push(dirEntry);
          }
          dirEntry.projects = dirEntry.projects.concat(projectBags[bag]);
        });
        //sort dirs by name
        this.projectDirs.sort((d1: ProjectDirEntry, d2: ProjectDirEntry) => {
          if (d1.dirName == null || d1.dirName == "") return 1;
          else if (d2.dirName == null || d2.dirName == "") return -1;
          else return d1.dirName.localeCompare(d2.dirName);
        });
        //sort projects in dir
        this.projectDirs.forEach(pd => {
          pd.projects.sort((p1, p2) => p1.getLabel().toLocaleLowerCase().localeCompare(p2.getLabel().toLocaleLowerCase()));
        });
        //init open/close directory according the stored cookie
        let collapsedDirs: string[] = DatasetViewsUtils.retrieveCollapsedDirectoriesCookie();
        this.projectDirs.forEach(pd => {
          pd.open = !collapsedDirs.includes(pd.dirName);
        });
        //init dir displayName (e.g.: prjLexModel and prjModel have values that can be written as RDFS, OWL, SKOS...)
        this.projectDirs.forEach(pd => { pd.dirDisplayName = pd.dirName; }); //init with the same dir as default
        if (bagOfFacet == ProjectFacets.prjLexModel || bagOfFacet == ProjectFacets.prjModel) {
          this.projectDirs.forEach(pd => {
            pd.dirDisplayName = Project.getPrettyPrintModelType(pd.dirName);
          });
        }
      }
    );
  }

  toggleDirectory(projectDir: ProjectDirEntry) {
    projectDir.open = !projectDir.open;
    //update collapsed directories cookie
    DatasetViewsUtils.storeCollpasedDirectoriesCookie(this.projectDirs);
  }

  isDatasetVisible(project: Project): boolean {
    if (this.filterString && this.filterString.trim() != "") {
      return project.getLabel().toLocaleLowerCase().includes(this.filterString.toLocaleLowerCase());
    } else {
      return true;
    }
  }


  createProject() {
    this.modalService.open(CreateProjectModalComponent, new ModalOptions("lg")).result.then(
      () => {
        /*
        No need to emit projectUpdatedEvent because a new project has Staging status,
        but main Datasets page only lists public datasets
        */
        this.initProjects();
      },
      () => { }
    );
  }

  /** ==========================
   * Actions of project
   * ========================== */

  loadData(project: Project) {
    if (!project.isOpen()) {
      this.basicModals.alert({ key: "ADMINISTRATION.DATASETS.MANAGEMENT.LOAD_DATA" }, { key: "MESSAGES.CANNOT_LOAD_DATA_IN_CLOSED_DATASET" }, ModalType.warning);
      return;
    }
    const modalRef: NgbModalRef = this.modalService.open(LoadDataModalComponent, new ModalOptions("lg"));
    modalRef.componentInstance.project = project;
    modalRef.result.then(
      () => { //load data might update the project status => refresh the projects list
        this.initProjects();
      },
      () => { }
    );
  }

  clearData(project: Project) {
    this.basicModals.confirm({ key: "ADMINISTRATION.DATASETS.MANAGEMENT.CLEAR_DATA" }, { key: "MESSAGES.CLEAR_DATA_CONFIRM", params: { datasetName: project.getName() } }, ModalType.warning).then(
      () => {
        SVContext.setTempProject(project);
        project[this.clearingDataAttr] = true;
        this.inputOutputService.clearData().pipe(
          finalize(() => {
            SVContext.removeTempProject();
            project[this.clearingDataAttr] = false;
          })
        ).subscribe(
          () => {
            //according the model, reset schemes and/or lexicon in order to prevent error when re-init the concept-tree/lexEntry-list
            let clearStoredInfoFn = [];
            if (project.getModelType() == SKOS.uri || project.getModelType() == OntoLex.uri) {
              clearStoredInfoFn.push(this.settingsMgr.setActiveSchemes(project, null));
            }
            if (project.getModelType() == OntoLex.uri) {
              clearStoredInfoFn.push(this.settingsMgr.setActiveLexicon(project, null));
            }
            forkJoin(clearStoredInfoFn)
              .pipe(defaultIfEmpty(null)) //needed since if clearStoredInfoFn is empty (namely no action after clear data), the handler of subscribe is not executed
              .subscribe(
                () => {
                  if (SVContext.getWorkingProject()?.getName() == project.getName()) {
                    //force the destroy of routes in case the clean data has been done on the current active project
                    SVContext.setResetRoutes(true);
                    SVContext.removeWorkingProject();
                  }
                }
              );

            this.basicModals.alert({ key: "ADMINISTRATION.DATASETS.MANAGEMENT.LOAD_DATA" }, { key: "MESSAGES.DATA_CLEARED", params: { datasetName: project.getName() } });
          }
        );
      },
      () => { }
    );
  }

  deleteProject(project: Project) {
    if (project.isOpen()) {
      this.basicModals.alert({ key: "DATASETS.ACTIONS.DELETE_DATASET" }, { key: "MESSAGES.CANNOT_DELETE_OPEN_DATASET" }, ModalType.warning);
      return;
    }
    this.basicModals.confirm({ key: "DATASETS.ACTIONS.DELETE_DATASET" }, { key: "MESSAGES.DELETE_DATASET_CONFIRM_WARN" }, ModalType.warning).then(
      () => {
        //clear the index of the project (NOTE: this MUST be done before deleting the project, otherwise the Auth check in ST would fail (check on a unexisting project))
        this.clearIndexImpl(project).subscribe(
          () => {
            //retrieve the remote repositories referenced by the deleting project (this must be done before the deletion in order to prevent errors)
            this.projectService.getRepositories(project, true).subscribe(
              (repositories: RepositorySummary[]) => {
                this.projectService.deleteProject(project).subscribe( //delete the project
                  () => {
                    if (repositories.length > 0) { //if the deleted project was linked with remote repositories proceed with the deletion
                      this.deleteRemoteRepo(project, repositories);
                    }
                    this.eventHandler.projectUpdatedEvent.emit();
                    //instead of refreshing the whole list, simply remove the deleted project from the list/dirs
                    // this.initProjects();
                    if (this.visualizationMode == ProjectViewMode.list) { //as list
                      for (let i = this.projectList.length - 1; i >= 0; i--) {
                        if (this.projectList[i].getName() == project.getName()) {
                          this.projectList.splice(i, 1);
                        }
                      }
                    } else {
                      this.projectDirs.forEach(dir => {
                        for (let i = dir.projects.length - 1; i >= 0; i--) {
                          if (dir.projects[i].getName() == project.getName()) {
                            dir.projects.splice(i, 1);
                          }
                        }
                      });
                    }
                  }
                );
              }
            );
          }
        );

      },
      () => { }
    );
  }

  private deleteRemoteRepo(deletedProject: Project, repositories: RepositorySummary[]) {
    this.selectRemoteRepoToDelete(deletedProject, repositories).subscribe( //ask to the user which repo delete
      (deletingRepositories: RemoteRepositorySummary[]) => {
        if (deletingRepositories.length > 0) {
          this.repositoriesService.deleteRemoteRepositories(deletingRepositories).subscribe( //delete them
            (exceptions: ExceptionDAO[]) => {
              if (exceptions.some(e => e != null)) { //some deletion has failed => show the report
                this.showDeleteRemoteRepoReport(deletingRepositories, exceptions);
              }
            }
          );
        }
      }
    );
  }

  private selectRemoteRepoToDelete(project: Project, repositories: RepositorySummary[]): Observable<RemoteRepositorySummary[]> {
    const modalRef: NgbModalRef = this.modalService.open(DeleteRemoteRepoModalComponent, new ModalOptions("lg"));
    modalRef.componentInstance.project = project;
    modalRef.componentInstance.repositories = repositories;
    return from(
      modalRef.result.then(
        repos => {
          return repos;
        }
      )
    );
  }

  private showDeleteRemoteRepoReport(deletingRepositories: RemoteRepositorySummary[], exceptions: ExceptionDAO[]) {
    const modalRef: NgbModalRef = this.modalService.open(DeleteRemoteRepoReportModalComponent, new ModalOptions("lg"));
    modalRef.componentInstance.deletingRepositories = deletingRepositories;
    modalRef.componentInstance.exceptions = exceptions;
  }

  createDownload(project: Project) {
    const modalRef: NgbModalRef = this.modalService.open(CreateDownloadModalComponent, new ModalOptions("lg"));
    modalRef.componentInstance.project = project;
  }

  createIndex(project: Project) {
    if (!project.isOpen()) {
      this.basicModals.alert({ key: "ADMINISTRATION.DATASETS.MANAGEMENT.CREATE_INDEX" }, { key: "MESSAGES.CANNOT_CREATE_INDEX_OF_CLOSED_DATASET" }, ModalType.warning);
      return;
    }
    this.clearIndexImpl(project).subscribe(
      () => {
        this.createIndexImpl(project).subscribe();
      }
    );
  }
  deleteIndex(project: Project) {
    this.clearIndexImpl(project).subscribe();
  }

  private createIndexImpl(project: Project): Observable<void> {
    return new Observable((observer: Observer<void>) => {
      SVContext.setTempProject(project);
      project[this.creatingIndexAttr] = true;
      this.globalCreatingIndex = true;
      this.globalSearchService.createIndex().pipe(
        finalize(() => {
          SVContext.removeTempProject();
          project[this.creatingIndexAttr] = false;
          this.globalCreatingIndex = false;
        })
      ).subscribe(
        () => observer.next()
      );
    });
  }
  private clearIndexImpl(project: Project): Observable<void> {
    return new Observable((observer: Observer<void>) => {
      project[this.clearingIndexAttr] = true;
      this.globalSearchService.clearSpecificIndex(project.getName()).pipe(
        finalize(() => {
          project[this.clearingIndexAttr] = false;
        })
      ).subscribe(
        () => observer.next()
      );
    });
  }

  createMapleMetadata(project: Project) {
    if (!project.isOpen()) {
      this.basicModals.alert({ key: "DATASETS.ACTIONS.CREATE_METADATA" }, { key: "MESSAGES.CANNOT_CREATE_METADATA_OF_CLOSED_DATASET" }, ModalType.warning);
      return;
    }
    this.createMapleMetadataImpl(project).subscribe();
  }

  private createMapleMetadataImpl(project: Project): Observable<void> {
    return new Observable((observer: Observer<void>) => {
      SVContext.setTempProject(project);
      project[this.creatingMetadataAttr] = true;
      this.mapleService.profileProject().pipe(
        finalize(() => {
          SVContext.removeTempProject();
          project[this.creatingMetadataAttr] = false;
        })
      ).subscribe(
        () => observer.next()
      );
    });
  }

  isChangeStatusAvailable(project: Project): boolean {
    //Status can be changed if:
    return this.isAdmin && //user is admin
      project.isOpen() && //project is open
      project.getVisibility() != ProjectVisibility.PRISTINE && //current visibility is not pristine (pristine status changes only after an upload data in the contribution flow)
      !project[this.creatingIndexAttr] && //project indexing is in execution
      !project[this.changingStatus]; //project status changing is in execution
  }

  makePublic(project: Project) {
    this.basicModals.confirm({ key: "ADMINISTRATION.DATASETS.MANAGEMENT.CHANGE_STATUS" }, { key: "MESSAGES.MAKE_DATASET_PUBLIC_CONFIRM" }, ModalType.warning).then(
      () => {
        SVContext.setTempProject(project);
        project[this.changingStatus] = true;
        this.projectService.makePublic().pipe(
          finalize(() => {
            SVContext.removeTempProject();
            project[this.changingStatus] = false;
          })
        ).subscribe(() => {
          this.eventHandler.projectUpdatedEvent.emit();
          project.setVisibility(ProjectVisibility.PUBLIC);
        });
      },
      () => { }
    );
  }

  makeStaging(project: Project) {
    let deleteIndexLabel: string = this.translateService.instant("ADMINISTRATION.DATASETS.MANAGEMENT.DELETE_INDEX");
    let confirmActionOpt = [{ label: deleteIndexLabel, value: true }];
    this.basicModals.confirmCheck({ key: "ADMINISTRATION.DATASETS.MANAGEMENT.CHANGE_STATUS" }, { key: "MESSAGES.MAKE_DATASET_STAGING_CONFIRM" }, confirmActionOpt, ModalType.warning).then(
      (checkboxOpts: CheckOptions[]) => {
        let deleteIndexParam = checkboxOpts.find(o => o.label == deleteIndexLabel).value;
        SVContext.setTempProject(project);
        project[this.changingStatus] = true;
        this.projectService.makeStaging(deleteIndexParam).pipe(
          finalize(() => {
            SVContext.removeTempProject();
            project[this.changingStatus] = false;
          })
        ).subscribe(() => {
          this.eventHandler.projectUpdatedEvent.emit();
          project.setVisibility(ProjectVisibility.AUTHORIZED);
        });
      },
      () => { }
    );
  }


  openCloseProject(project: Project) {
    project[this.openingAttr] = true;
    if (project.isOpen()) { //project open => close it
      this.projectService.disconnectFromProject(project).pipe(
        finalize(() => { project[this.openingAttr] = false; })
      ).subscribe(() => {
        this.eventHandler.projectUpdatedEvent.emit();
        project.setOpen(false);
      });
    } else { //project closed => open it
      this.projectService.accessProject(project).pipe(
        finalize(() => { project[this.openingAttr] = false; })
      ).subscribe(() => {
        this.eventHandler.projectUpdatedEvent.emit();
        project.setOpen(true);
      });
    }
  }

  editRemoteRepoCredential(project: Project) {
    if (project.isOpen()) {
      this.basicModals.alert({ key: "COMMONS.STATUS.OPERATION_DENIED" }, { key: "MESSAGES.CANNOT_EDIT_OPEN_PROJECT_CREDENTIALS" }, ModalType.warning);
      return;
    }
    const modalRef: NgbModalRef = this.modalService.open(RemoteRepoEditorModalComponent, new ModalOptions('lg'));
    modalRef.componentInstance.project = project;
  }

  editUsers(project: Project) {
    const modalRef: NgbModalRef = this.modalService.open(ProjectUsersModalComponent, new ModalOptions('xl'));
    modalRef.componentInstance.project = project;
  }

  editSettings(project: Project) {
    const modalRef: NgbModalRef = this.modalService.open(ProjectSettingsModalComponent, new ModalOptions('lg'));
    modalRef.componentInstance.project = project;
  }

  editDescription(project: Project) {
    this.basicModals.prompt({ key: "ADMINISTRATION.DATASETS.MANAGEMENT.EDIT_DESCRIPTION" }, { value: "Description" }, null, project.getDescription(), null, true).then(
      descr => {
        this.projectService.setProjectProperty(project, "description", descr).subscribe(
          () => {
            project.setDescription(descr);
          }
        );
      },
      () => { }
    );
  }

  editLabels(project: Project) {
    let projectLabels = project.getLabels();
    let localizeMap: LocalizedMap = new Map();
    for (let lang in projectLabels) {
      localizeMap.set(lang, projectLabels[lang]);
    }
    this.sharedModals.localizedEditor({ key: "ADMINISTRATION.DATASETS.MANAGEMENT.EDIT_LABELS" }, localizeMap).then(
      (newLocalizedMap: LocalizedMap) => {
        let newProjLabels: { [lang: string]: string } = {};
        newLocalizedMap.forEach((label, lang) => {
          newProjLabels[lang] = label;
        });
        this.projectService.setProjectLabels(project, newProjLabels).subscribe(
          () => {
            project.setLabels(newProjLabels);
          }
        );
      },
      () => { }
    );
  }

  editFacets(project: Project) {
    this.sharedModals.configurePlugin(project.getFacets()).then(
      facets => {
        this.projectService.setProjectFacets(project, facets).subscribe(
          () => {
            if (this.visualizationMode == ProjectViewMode.facet) {
              this.initProjects();
            }
          }
        );
      },
      () => { }
    );
  }

  configureRenderingEngine(project: Project) {
    const modalRef: NgbModalRef = this.modalService.open(RenderingEngineConfigModalComponent, new ModalOptions());
    modalRef.componentInstance.project = project;
  }

  editCustomFacetsSchema() {
    let handler: PluginSettingsHandler = (facets: Settings) => this.projectService.setCustomProjectFacetsSchema(facets);
    this.projectService.getCustomProjectFacetsSchema().subscribe(facetsSchema => {
      this.sharedModals.configurePlugin(facetsSchema, { key: "DATASETS.SETTINGS.FACETS_SCHEMA_DEFINITION" }, handler).then(
        () => { //changed settings
          this.initProjects();
        },
        () => { } //nothing changed
      );
    });
  }

  editViewSettings() {
    this.modalService.open(DatasetsSettingsModalComponent, new ModalOptions()).result.then(
      () => {
        this.initProjects();
      },
      () => { }
    );
  }


  switchRendering() {
    this.rendering = !this.rendering;
    this.settingsMgr.setProjectRendering(this.rendering).subscribe();
  }

  toggleOpenAtStartup(project: Project) {
    let openAtStartup = project.getOpenAtStartup();
    let newValue = !openAtStartup;
    this.projectService.setOpenAtStartup(project, newValue).subscribe(
      () => {
        project.setOpenAtStartup(newValue);
      }
    );
  }

  /* ============================== */

  goToProject(project: Project) {
    let queryParams = this.queryParamService.getPreservedQueryParams();
    this.router.navigate(["/datasets/" + project.getName()], { queryParams: queryParams });
  }

}


class ProjectDirEntry implements DatasetDirEntryI {
  dirName: string;
  dirDisplayName: string;
  open: boolean;
  projects: Project[];
  constructor(dirName: string) {
    this.dirName = dirName;
    this.open = true;
    this.projects = [];
  }
}