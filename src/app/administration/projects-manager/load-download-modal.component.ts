import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { from, Observable, of } from "rxjs";
import { catchError, finalize, map, mergeMap } from "rxjs/operators";
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from "src/app/modal-dialogs/Modals";
import { Project } from 'src/app/models/Project';
import { Literal } from 'src/app/models/Resources';
import { DownloadServices } from 'src/app/services/download.service';
import { ExportServices } from "src/app/services/export.service";
import { SVContext } from "src/app/utils/SVContext";

@Component({
    selector: "load-download-modal",
    templateUrl: "./load-download-modal.component.html",
    standalone: false
})
export class LoadDownloadModalComponent {

    @Input() project: Project;
    @Input() distribution: boolean;

    filePickerAccept: string;
    file: File;
    localized: Literal;

    loading: boolean;

    constructor(public activeModal: NgbActiveModal, private exportService: ExportServices, private downloadService: DownloadServices, private basicModals: BasicModalsServices, private translateService: TranslateService) { }

    ngOnInit() {

        this.localized = new Literal((this.distribution ? "Dist" : "File") + " " + new Date().toLocaleDateString(), this.translateService.currentLang);

        if (this.distribution) {
            this.exportService.getOutputFormats().subscribe(
                formats => {
                    let extList: string[] = []; //collects the extensions of the formats in order to provide them to the file picker
                    formats.forEach(f => {
                        f.fileExtensions.forEach(ext => {
                            extList.push("." + ext);
                        });
                    });
                    //remove duplicated extensions
                    extList = extList.filter((item: string, pos: number) => extList.indexOf(item) == pos);
                    this.filePickerAccept = extList.join(",");
                }
            );
        }
    }

    fileChangeEvent(file: File) {
        this.file = file;
    }

    private createFile(overwrite: boolean = false): Observable<boolean> {
        return this.downloadService.uploadFile(this.file, this.localized, overwrite, this.distribution).pipe(
            map(() => {
                return true;
            }),
            catchError((err: Error) => {
                if (err.name.endsWith("FileAlreadyExistsException")) {
                    return from(
                        this.basicModals.confirm({ key: "COMMONS.ACTIONS.UPLOAD_FILE" }, { key: "MESSAGES.ALREADY_EXISTING_FILE_OVERWRITE_CONFIRM" }, ModalType.warning).then(
                            () => {
                                return this.downloadService.uploadFile(this.file, this.localized, true, this.distribution).pipe(
                                    map(() => {
                                        return true;
                                    })
                                );
                            },
                            () => {
                                return of(false);
                            }
                        )
                    ).pipe(
                        mergeMap(done => done)
                    );
                } else {
                    return of(false);
                }
            })
        );

    }

    ok() {
        this.loading = true;
        SVContext.setTempProject(this.project);
        this.createFile().pipe(
            finalize(() => {
                this.loading = false;
            })
        ).subscribe(
            (success: boolean) => {
                if (success) {
                    this.basicModals.alert({ key: "COMMONS.STATUS.OPERATION_DONE" }, { key: "MESSAGES.FILE_UPLOADED" });
                    SVContext.removeTempProject();
                    this.activeModal.close();
                }
            }
        );
    }

    close() {
        SVContext.removeTempProject();
        this.activeModal.dismiss();
    }

}