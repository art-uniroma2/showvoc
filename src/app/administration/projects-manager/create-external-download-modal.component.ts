import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { from, of } from 'rxjs';
import { catchError, finalize, map, mergeMap } from "rxjs/operators";
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { Project } from 'src/app/models/Project';
import { IRI, Literal } from "src/app/models/Resources";
import { DownloadServices } from "src/app/services/download.service";
import { ResourceUtils } from 'src/app/utils/ResourceUtils';
import { SVContext } from "src/app/utils/SVContext";

@Component({
    selector: "create-external-download-modal",
    templateUrl: "./create-external-download-modal.component.html",
    standalone: false
})
export class CreateExternalDownloadModalComponent {

    @Input() title: string;
    @Input() project: Project;
    @Input() distribution: boolean = true;

    externalLink: string;

    localized: Literal;

    loading: boolean;


    constructor(public activeModal: NgbActiveModal, public downloadService: DownloadServices, public basicModals: BasicModalsServices, public translateService: TranslateService) { }

    ngOnInit() {
        this.localized = new Literal("External " + (this.distribution ? "dist" : "link") + " " + new Date().toLocaleDateString(), this.translateService.currentLang);
    }

    ok() {
        this.externalLink = this.externalLink.trim();
        if (!ResourceUtils.testIRI(this.externalLink)) {
            this.basicModals.alert({ key: "COMMONS.STATUS.INVALID_VALUE" }, { key: "MESSAGES.INVALID_URL", params: { url: this.externalLink } }, ModalType.warning);
            return;
        }

        this.loading = true;
        SVContext.setTempProject(this.project);
        this.downloadService.createExternalDownload(new IRI(this.externalLink), this.localized, this.distribution).pipe(
            finalize(() => {
                this.loading = false;
                SVContext.removeTempProject();
            }),
            map(() => {
                return true;
            }),
            catchError((err: Error) => {
                if (err.name.endsWith("FileAlreadyExistsException")) {
                    return from(
                        this.basicModals.confirm(this.title, { key: "MESSAGES.ALREADY_EXISTING_URL_OVERWRITE_CONFIRM" }, ModalType.warning).then(
                            () => {
                                return this.downloadService.createExternalDownload(new IRI(this.externalLink), this.localized, this.distribution, true).pipe(
                                    map(() => {
                                        return true;
                                    })
                                );
                            },
                            () => {
                                return of(false);
                            }
                        )
                    ).pipe(
                        mergeMap(done => done)
                    );
                } else {
                    return of(false);
                }
            })
        ).subscribe(
            (success: boolean) => {
                if (success) {
                    this.basicModals.alert({ key: "COMMONS.STATUS.OPERATION_DONE" }, { key: "MESSAGES.DOWNLOAD_CREATED" });
                    this.activeModal.close();
                }
            }
        );
    }

    close() {
        SVContext.removeTempProject();
        this.activeModal.dismiss();
    }

}