import { Component, ElementRef, Input, ViewChild } from "@angular/core";
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { forkJoin } from 'rxjs';
import { finalize, map } from 'rxjs/operators';
import { ModalOptions } from "src/app/modal-dialogs/Modals";
import { DataPanel } from "src/app/models/DataStructure";
import { Scope } from 'src/app/models/Plugins';
import { Project } from 'src/app/models/Project';
import { ConceptTreePreference, DatasetTab, InstanceListPreference, LexicalEntryListPreference, PreferencesUtils } from 'src/app/models/Properties';
import { OntoLex, OWL, SKOS } from 'src/app/models/Vocabulary';
import { DataPanelLabelsEditorModalComponent } from "src/app/preferences/data-panels-settings/data-panels-labels-editor-modal.component";
import { DataPanelsSettingsComponent } from "src/app/preferences/data-panels-settings/data-panels-settings.component";
import { SettingsServices } from "src/app/services/settings.service";
import { SettingsManager, SettingsManagerID, SettingsMgrUtils } from 'src/app/utils/SettingsManager';
import { UIUtils } from 'src/app/utils/UIUtils';

@Component({
    selector: "project-settings-modal",
    templateUrl: "./project-settings-modal.component.html",
    standalone: false
})
export class ProjectSettingsModalComponent {

    @ViewChild(DataPanelsSettingsComponent) dataPanelSettingsComponent: DataPanelsSettingsComponent;

    @Input() project: Project;

    activeTab: string = "languages";

    /* Languages */
    projSettingsReady: boolean = false; //rendering editor needs project languages (from proj settings) to be initialized
    renderingLangs: string[];

    /* Other */
    isOntolex: boolean; //useful to determine whether to show the settings about lexical entry list
    isSkos: boolean; //useful to determine whether to show the settings about concept tree
    isOwl: boolean; //useful to determine whether to show the settings about the instance list

    concTreePref: ConceptTreePreference;

    lexEntryListPref: LexicalEntryListPreference;

    instListPref: InstanceListPreference;

    xkosTabEnabled: boolean;

    hiddenDatasetTabsPref: DatasetTab[];
    hiddenDataPanelsPref: DataPanel[];

    initializing: boolean;

    constructor(
        public activeModal: NgbActiveModal, 
        private settingsMgr: SettingsManager, 
        private settingsService: SettingsServices, 
        private modalService: NgbModal,
        private elementRef: ElementRef
    ) { }


    /*
    * Here the preferences are retrieved from the PU-settings (specific settings for the given p-u pair, in this case the user is the admin), 
    * but it will be written as pu_default (default pu-setting at project level) so it will be applied for all the users (both admin and visitor).
    * The getSettings in case the setting has not been set before, will fallback to the pu_default
    */

    ngOnInit() {
        this.isOntolex = this.project.getModelType() == OntoLex.uri;
        this.isSkos = this.project.getModelType() == SKOS.uri;
        this.isOwl = this.project.getModelType() == OWL.uri;

        /**
         * Here I need to init the settings of the project with:
         * 1 - default rendering languages for the current project: RenderingEngine component, PROJECT_USER scope, PROJECT default
         * 2 - default tree/list preferences for the current project: SemanticTurkeyCoreSettingsManager, PROJECT_USER scope, PROJECT default
         * 3 - project settings (SemanticTurkeyCoreSettingsManager, PROJECT scope) for:
         *      - project languages (needed in rendering editor)
         *      - rv settings (needed in res view project settings editor)
         */


        // 1 default rendering languages 
        let initRenderingLanguagesFn = this.settingsService.getPUSettingsProjectDefault(SettingsManagerID.RenderingEngine, this.project).pipe(
            map((settings) => {
                let settingValue: string = settings.getPropertyValue(SettingsManager.PROP_NAME.languages);
                this.renderingLangs = (settingValue != null) ? settingValue.split(",") : [];
            })
        );

        // 2 default tree/list preferences 
        let initStructuresSettingsFn = this.settingsService.getPUSettingsProjectDefault(SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project).pipe(
            map((settings) => {

                this.concTreePref = new ConceptTreePreference();
                let concTreeDefaults = settings.getPropertyValue(SettingsManager.PROP_NAME.conceptTree, new ConceptTreePreference());
                PreferencesUtils.mergePreference(this.concTreePref, concTreeDefaults);

                this.instListPref = new InstanceListPreference();
                let instListDefaults = settings.getPropertyValue(SettingsManager.PROP_NAME.instanceList, new InstanceListPreference());
                PreferencesUtils.mergePreference(this.instListPref, instListDefaults);

                this.lexEntryListPref = new LexicalEntryListPreference();
                let lexEntryDefaults = settings.getPropertyValue(SettingsManager.PROP_NAME.lexEntryList, new LexicalEntryListPreference());
                PreferencesUtils.mergePreference(this.lexEntryListPref, lexEntryDefaults);

                this.hiddenDatasetTabsPref = settings.getPropertyValue(SettingsManager.PROP_NAME.hiddenShowVocTabs, []);

                let settingsName = SettingsMgrUtils.modelToHiddenDataPanelSetting[this.project.getModelType()];
                this.hiddenDataPanelsPref = settings.getPropertyValue(settingsName, []);

            })
        );

        // 3 project settings
        let initProjSettingsFn = [
            //proj core settings, for project languages (needed in rendering editor)
            this.settingsMgr.initProjSettingsForAdministration(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project),
        ];
        if (this.project.isOpen()) { //only if project is open (allows adding property in custom section), init also...
            //PU core settings, for search settings
            initProjSettingsFn.push(this.settingsMgr.initProjSettingsForAdministration(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project));
            //PU rendering settings, for property tree browsing
            initProjSettingsFn.push(this.settingsMgr.initProjSettingsForAdministration(Scope.PROJECT_USER, SettingsManagerID.RenderingEngine, this.project));
        }

        /*
        Some project settings are needed before some other settings can be edited.
        E.g.:
        - project languages in the rendering editor
        - ResourceView project settings in the dedicated editor
        - xkos tab enabled in data panel settings editor
        - allowVisualizationChange (et simila) in ConceptTree/InstanceList/LexEntryList pref editor
        So, here projects settings are initialized before the other
        */
        this.initializing = true;
        forkJoin([...initProjSettingsFn])
            .pipe(
                finalize(() => { this.initializing = false; })
            ).subscribe(
                () => {
                    this.projSettingsReady = true; //required to know when to render res-view-project-settings (only when RV project settings are initialized)
                    this.xkosTabEnabled = this.settingsMgr.getXkosTabEnabled(this.project);
                    forkJoin([initRenderingLanguagesFn, initStructuresSettingsFn]).subscribe();
                }
            );
    }

    ngAfterViewInit() {
        UIUtils.setFullSizeModal(this.elementRef);
    }

    changeTab(tabName: string) {
        this.activeTab = tabName;
    }

    onXkosTabEnabledChanged() {
        this.settingsMgr.setXkosTabEnabled(this.project, this.xkosTabEnabled).subscribe(
            () => {
                this.dataPanelSettingsComponent.initTabStruct();
            }
        );
    }

    onHiddenDatasetTabChanged() {
        this.settingsService.storePUSettingProjectDefault(SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project, SettingsManager.PROP_NAME.hiddenShowVocTabs, this.hiddenDatasetTabsPref).subscribe();
    }

    onHiddenDataPanelChanged() {
        this.settingsMgr.setDefaultHiddenDataPanels(this.hiddenDataPanelsPref, this.project.getModelType(), Scope.PROJECT, this.project).subscribe();
    }

    editDataPanelsNames() {
        const modalRef: NgbModalRef = this.modalService.open(DataPanelLabelsEditorModalComponent, new ModalOptions());
        modalRef.componentInstance.project = this.project;
        modalRef.result.then(
            () => {
                this.dataPanelSettingsComponent.initTabsLabels();
            },
            () => {}
        );
    }

    //======== Rendering settings handlers =======

    onRenderingChange() {
        this.settingsService.storePUSettingProjectDefault(SettingsManagerID.RenderingEngine, this.project, SettingsManager.PROP_NAME.languages, this.renderingLangs.join(",")).subscribe();
    }

    //======== Skos settings handlers ======

    onConceptTreePrefChanged() {
        this.settingsService.storePUSettingProjectDefault(SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project, SettingsManager.PROP_NAME.conceptTree, this.concTreePref).subscribe();
    }

    //======== Owl settings handlers ======

    onInstanceListPrefChanged() {
        this.settingsService.storePUSettingProjectDefault(SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project, SettingsManager.PROP_NAME.instanceList, this.instListPref).subscribe();
    }

    //======== Ontolex settings handlers ======

    onLexEntryListPrefChanged() {
        this.settingsService.storePUSettingProjectDefault(SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project, SettingsManager.PROP_NAME.lexEntryList, this.lexEntryListPref).subscribe();
    }

    //==========================================

    close() {
        this.activeModal.dismiss();
    }

}