import { Component, Input, ViewChild } from "@angular/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { forkJoin, Observable, of } from 'rxjs';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { BrowsingModalsServices } from 'src/app/modal-dialogs/browsing-modals/browsing-modals.service';
import { ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { Scope, Settings } from "src/app/models/Plugins";
import { Project } from 'src/app/models/Project';
import { CustomSection, PredicateLabelSettings, ResourceViewProjectSettings, ResViewTemplate, SectionFilterPreference, ValueFilterLanguages } from 'src/app/models/Properties';
import { AnnotatedValue, IRI } from 'src/app/models/Resources';
import { ResViewSection } from "src/app/models/ResourceView";
import { PredicateLabelSettingsModalComponent } from "src/app/preferences/predicate-label-settings/predicate-label-settings-modal.component";
import { ResViewSectionCustomizationModalComponent } from "src/app/preferences/res-view-section-customization/res-view-section-customization-modal.component";
import { ResViewTemplateEditorComponent } from "src/app/preferences/res-view-template-editor/res-view-template-editor.component";
import { SettingsServices } from 'src/app/services/settings.service';
import { NTriplesUtil, ResourceUtils } from "src/app/utils/ResourceUtils";
import { SettingsManager, SettingsManagerID } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';

@Component({
  selector: "res-view-project-settings",
  templateUrl: "./res-view-project-settings.component.html",
  host: { class: "vbox" },
  standalone: false
})
export class ResViewProjectSettingsComponent {

  @Input() project: Project;

  @ViewChild(ResViewTemplateEditorComponent) rvTemplateEditor: ResViewTemplateEditorComponent;

  rvSettings: ResourceViewProjectSettings;

  isAdmin: boolean; //useful to decide which store/restore defualt action allow

  private rvSettingTimer: number; //in order to do not fire too much close requests to update rv settings

  //custom sections
  customSections: { [key: string]: IRI[] }; //ResViewSection -> properties
  customSectionsIDs: string[];

  selectedCustomSection: string;
  selectedManagedProperty: IRI;

  //Predicate Label Settings
  predLabelSettings: PredicateLabelSettings;

  sectionVisibilityDefault: SectionFilterPreference;

  //PU settings (defaults project level)
  langValueFilter: ValueFilterLanguages;

  hideLiteralLang: boolean;

  showSectionHeader: boolean;

  constructor(private settingsService: SettingsServices, private settingsMgr: SettingsManager, private basicModals: BasicModalsServices, private browsingModals: BrowsingModalsServices, private modalService: NgbModal) { }

  ngOnInit() {
    this.isAdmin = SVContext.getLoggedUser()?.isAdmin();
    this.initResViewSettings();
  }

  private initResViewSettings() {

    this.rvSettings = this.settingsMgr.getResourceViewProjectSettings(this.project);

    this.customSections = {};
    this.customSectionsIDs = [];
    this.selectedCustomSection = null;
    this.selectedManagedProperty = null;

    if (this.rvSettings.customSections != null) {
      for (let sectionId in this.rvSettings.customSections) {
        let cs: CustomSection = this.rvSettings.customSections[sectionId];
        let matchedProperties: IRI[] = cs.matchedProperties.map(p => new IRI(p));
        matchedProperties.sort((p1, p2) => p1.getIRI().localeCompare(p2.getIRI()));
        this.customSections[sectionId] = matchedProperties;
      }
    }
    this.customSectionsIDs = Object.keys(this.customSections);

    this.predLabelSettings = this.settingsMgr.getResourceViewPredLabelSettings(this.project);

    this.settingsService.getPUSettingsProjectDefault(SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project).subscribe(
      (settings: Settings) => {
        this.langValueFilter = settings.getPropertyValue(SettingsManager.PROP_NAME.filterValueLanguages);
        this.hideLiteralLang = settings.getPropertyValue(SettingsManager.PROP_NAME.resViewHideLang);
        this.showSectionHeader = settings.getPropertyValue(SettingsManager.PROP_NAME.resViewShowSectionHeader);
        this.sectionVisibilityDefault = settings.getPropertyValue(SettingsManager.PROP_NAME.resViewPartitionFilter);
      }
    );
  }

  editSectionDetails() {
    const modalRef: NgbModalRef = this.modalService.open(ResViewSectionCustomizationModalComponent, new ModalOptions("lg"));
    modalRef.componentInstance.project = this.project;
  }

  /* ================
  * CUSTOM SECTIONS
  * ================= */

  selectCustomSection(cs: string) {
    this.selectedCustomSection = cs;
    this.selectedManagedProperty = null;
  }

  renameCustomSection() {
    this.basicModals.prompt({ key: "RESOURCE_VIEW.CUSTOM_SECTIONS.RENAME_CUSTOM_SECTION" }, null, null, this.selectedCustomSection).then(
      sectionName => {
        let newSectionName: ResViewSection = sectionName as ResViewSection;
        if (newSectionName == this.selectedCustomSection) { //not changed
          return;
        }
        if (Object.keys(this.rvSettings.templates).includes(newSectionName)) { //changed but section with the same name already exists
          this.basicModals.alert({ key: "COMMONS.STATUS.INVALID_VALUE" }, { key: "MESSAGES.ALREADY_EXISTING_SECTION" }, ModalType.warning);
          return;
        }
        //move the managed properties to the new section, then remove the old one
        let props: IRI[] = this.customSections[this.selectedCustomSection];
        this.customSections[newSectionName] = props;
        delete this.customSections[this.selectedCustomSection];
        this.customSectionsIDs = Object.keys(this.customSections);

        //rename the custom section also in the template
        this.rvTemplateEditor.onCustomSectionRenamed(this.selectedCustomSection, newSectionName);

        this.selectedCustomSection = newSectionName;

        //use timeout because the update is automatically fired also by rvTemplateEditor.onCustomSectionRenamed
        this.updateResViewSettingsWithTimeout();
      },
      () => { }
    );
  }

  addCustomSection() {
    this.basicModals.prompt({ key: "RESOURCE_VIEW.CUSTOM_SECTIONS.CREATE_CUSTOM_SECTION" }).then(
      customSectionName => {
        if (Object.keys(this.rvSettings.templates).includes(customSectionName)) { //section with the same name already exists
          this.basicModals.alert({ key: "COMMONS.STATUS.INVALID_VALUE" }, { key: "MESSAGES.ALREADY_EXISTING_SECTION" }, ModalType.warning);
          return;
        }
        this.customSections[customSectionName] = [];
        this.customSectionsIDs = Object.keys(this.customSections);

        //update templates
        this.rvTemplateEditor.onCustomSectionCreated(customSectionName);

        //use timeout because the update is automatically fired also by rvTemplateEditor.onCustomSectionCreated
        this.updateResViewSettingsWithTimeout();
      },
      () => { }
    );
  }

  deleteCustomSection() {
    delete this.customSections[this.selectedCustomSection];
    this.customSectionsIDs = Object.keys(this.customSections); //update the IDs list

    //remove the custom section from the templates
    this.rvTemplateEditor.onCustomSectionDeleted(this.selectedCustomSection);

    this.selectedCustomSection = null;

    //use timeout because the update is automatically fired also by rvTemplateEditor.onCustomSectionDeleted
    this.updateResViewSettingsWithTimeout();
  }

  selectManagedProperty(prop: IRI) {
    this.selectedManagedProperty = prop;
  }

  addManagedProperty() {
    if (this.project.isOpen()) {
      this.ensureProjectSettingsInitialized().subscribe(
        () => {
          this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }, null, null, null, this.project).then(
            (prop: AnnotatedValue<IRI>) => {
              if (!this.customSections[this.selectedCustomSection].some(p => p.equals(prop.getValue()))) { //if not already in
                this.customSections[this.selectedCustomSection].push(prop.getValue());
                this.updateResViewSettings();
              }
            },
            () => { }
          );
        }
      );
    } else {
      this.basicModals.prompt({ key: "DATA.ACTIONS.ADD_PROPERTY" }, { value: "IRI" }).then(
        valueIRI => {
          let prop: IRI;
          if (ResourceUtils.testIRI(valueIRI)) { //valid iri (e.g. "http://test")
            prop = new IRI(valueIRI);
          } else { //not an IRI, try to parse as NTriples
            try {
              prop = NTriplesUtil.parseIRI(valueIRI);
            } catch { //neither a valid ntriple iri (e.g. "<http://test>")
              this.basicModals.alert({ key: "COMMONS.STATUS.INVALID_VALUE" }, { key: "MESSAGES.INVALID_IRI", params: { iri: valueIRI } }, ModalType.warning);
              return;
            }
          }
          if (!this.customSections[this.selectedCustomSection].some(p => p.equals(prop))) { //if not already in
            this.customSections[this.selectedCustomSection].push(prop);
            this.updateResViewSettings();
          }
        },
        () => { }
      );
    }
  }

  private ensureProjectSettingsInitialized(): Observable<any> {
    //ensure that the search settings are initialized (needed for browsing prop tree)
    if (this.settingsMgr.settingsCache[Scope.PROJECT_USER][this.project.getName()] == null) {
      //search settings are PU settings, so init them with the related defaults
      return forkJoin([
        this.settingsMgr.initSettings(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project),
      ]);
    } else {
      return of(null);
    }
  }

  deleteManagedProperty() {
    this.customSections[this.selectedCustomSection].splice(this.customSections[this.selectedCustomSection].indexOf(this.selectedManagedProperty), 1);
    this.selectedManagedProperty = null;
    this.updateResViewSettings();
  }

  /* ================
  * PREDICATE LABELS
  * ================= */

  onRenderPredicateChange() {
    this.updatePredLabelSettings();
  }

  editPredicateLabels() {
    const modalRef: NgbModalRef = this.modalService.open(PredicateLabelSettingsModalComponent, new ModalOptions("xl"));
    modalRef.componentInstance.project = this.project;
    modalRef.componentInstance.predLabelMappings = this.predLabelSettings.mappings;
    modalRef.result.then(
      (predLabelMappings) => {
        this.predLabelSettings.mappings = predLabelMappings;
        this.updatePredLabelSettings();
      },
      () => { }
    );
  }

  private updatePredLabelSettings() {
    this.settingsService.storeSettingForProjectAdministration(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.PROJECT, SettingsManager.PROP_NAME.resViewPredLabelMappings, this.predLabelSettings, this.project).subscribe(
      () => {
        //in case the edited project is the active one, update/reinit the settings cached
        if (SVContext.getWorkingProject() != null && SVContext.getWorkingProject().getName() == this.project.getName()) {
          this.settingsMgr.initSettings(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project).subscribe();
        }
      }
    );
  }

  //================

  /**
   * Store the settings only after a timeout of 1000 in order to prevent too much service invocation
   * when user is editing the templates
   */
  updateResViewSettingsWithTimeout() {
    clearTimeout(this.rvSettingTimer);
    this.rvSettingTimer = window.setTimeout(() => { this.updateResViewSettings(); }, 1000);
  }


  private updateResViewSettings() {
    let rvSettings: ResourceViewProjectSettings = this.getResViewProjectSettings();
    this.settingsService.storeSettingForProjectAdministration(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.PROJECT, SettingsManager.PROP_NAME.resourceView, rvSettings, this.project).subscribe(
      () => {
        //in case the edited project is the active one, update/reinit the settings cached
        if (SVContext.getWorkingProject() != null && SVContext.getWorkingProject().getName() == this.project.getName()) {
          this.settingsMgr.initSettings(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project).subscribe();
        }
      }
    );
  }

  setAsSystemDefault() {
    this.basicModals.confirm({ key: "COMMONS.ACTIONS.SET_AS_SYSTEM_DEFAULT" }, { key: "MESSAGES.CONFIG_SET_SYSTEM_DEFAULT_CONFIRM" }, ModalType.warning).then(
      () => {
        let rvSettings: ResourceViewProjectSettings = this.getResViewProjectSettings();
        this.settingsService.storeSettingDefault(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.PROJECT, Scope.SYSTEM, SettingsManager.PROP_NAME.resourceView, rvSettings).subscribe(
          () => {
            this.basicModals.alert({ key: "COMMONS.STATUS.OPERATION_DONE" }, { key: "MESSAGES.CONFIG_SYSTEM_DEFAULT_SET" });
          }
        );
      },
      () => { }
    );
  }

  restoreSystemDefault() {
    this.basicModals.confirm({ key: "COMMONS.ACTIONS.RESTORE_SYSTEM_DEFAULT" }, { key: "MESSAGES.CONFIG_RESTORE_SYSTEM_DEFAULT_CONFIRM" }, ModalType.warning).then(
      () => {
        let rvSettings: ResourceViewProjectSettings = null;
        this.settingsService.storeSettingForProjectAdministration(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.PROJECT, SettingsManager.PROP_NAME.resourceView, rvSettings, this.project).subscribe(
          () => {
            this.basicModals.alert({ key: "COMMONS.STATUS.OPERATION_DONE" }, { key: "MESSAGES.CONFIG_SYSTEM_DEFAULT_RESTORED" });
            this.settingsMgr.initSettings(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project).subscribe(
              () => {
                this.initResViewSettings();
              }
            );
          }
        );
      },
      () => { }
    );
  }

  private getResViewProjectSettings(): ResourceViewProjectSettings {
    let customSections: { [key: string]: CustomSection } = {}; //map name -> CustomSection
    for (let csId in this.customSections) {
      customSections[csId] = {
        matchedProperties: this.customSections[csId].map(p => p.toNT())
      };
    }

    let templates: ResViewTemplate = this.rvSettings.templates;

    let rvSettings: ResourceViewProjectSettings = {
      customSections: customSections,
      templates: templates,
    };
    return rvSettings;
  }


  onHideLiteralLangChange() {
    this.settingsService.storePUSettingProjectDefault(SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project, SettingsManager.PROP_NAME.resViewHideLang, this.hideLiteralLang).subscribe(
      () => {
        //in case the edited project is the active one, update/reinit the user settings cached
        if (SVContext.getWorkingProject() != null && SVContext.getWorkingProject().getName() == this.project.getName()) {
          this.settingsMgr.initSettings(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project).subscribe();
        }
      }
    );
  }

  onShowSectionHeaderChange() {
    this.settingsService.storePUSettingProjectDefault(SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project, SettingsManager.PROP_NAME.resViewShowSectionHeader, this.showSectionHeader).subscribe(
      () => {
        //in case the edited project is the active one, update/reinit the PU settings cached
        if (SVContext.getWorkingProject() != null && SVContext.getWorkingProject().getName() == this.project.getName()) {
          this.settingsMgr.initSettings(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project).subscribe();
        }
      }
    );
  }

  /* ================
  * LANG VALUE FILTER
  * ================= */

  onLangValueFilterChange() {
    this.settingsService.storePUSettingProjectDefault(SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project, SettingsManager.PROP_NAME.filterValueLanguages, this.langValueFilter).subscribe(
      () => {
        //in case the edited project is the active one, update/reinit the user settings cached
        if (SVContext.getWorkingProject() != null && SVContext.getWorkingProject().getName() == this.project.getName()) {
          this.settingsMgr.initSettings(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project).subscribe();
        }
      }
    );
  }

  /* ================
  * SECTION VISIBILITY
  * ================= */

  onSectionVisibilityDefaultChanged() {
    this.settingsService.storePUSettingProjectDefault(SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project, SettingsManager.PROP_NAME.resViewPartitionFilter, this.sectionVisibilityDefault).subscribe(
      () => {
        //in case the edited project is the active one, update the settings stored in cache
        if (SVContext.getWorkingProject() != null && SVContext.getWorkingProject().getName() == this.project.getName()) {
          this.settingsMgr.initSettings(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project).subscribe();
        }
      }
    );
  }


}