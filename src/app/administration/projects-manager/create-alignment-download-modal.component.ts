import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { from, Observable, of } from "rxjs";
import { catchError, finalize, map, mergeMap } from "rxjs/operators";
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from "src/app/modal-dialogs/Modals";
import { LinksetMetadata } from 'src/app/models/Metadata';
import { Literal } from 'src/app/models/Resources';
import { DownloadServices } from "src/app/services/download.service";
import { MetadataRegistryServices } from 'src/app/services/metadata-registry.service';
import { SVContext } from "src/app/utils/SVContext";
import { CreateDownloadModalComponent } from './create-download-modal.component';

@Component({
  selector: "create-alignment-download-modal",
  templateUrl: "./create-alignment-download-modal.component.html",
  standalone: false
})
export class CreateAlignmentDownloadModalComponent extends CreateDownloadModalComponent {

  restrictTarget: boolean = false;

  linksets: LinksetMetadata[];
  selectedLinkset: LinksetMetadata;

  constructor(public activeModal: NgbActiveModal, public downloadService: DownloadServices,
    private metadataRegistryService: MetadataRegistryServices,
    public basicModals: BasicModalsServices, public translateService: TranslateService) {
    super(activeModal, downloadService, basicModals, translateService);
  }

  ngOnInit() {
    this.localized = new Literal("Alignments " + new Date().toLocaleDateString(), this.translateService.currentLang);
    this.initDownloadFormats();

    this.metadataRegistryService.findDatasetForProjects([this.project]).subscribe(
      mappings => {
        let datasetIRI = mappings[this.project.getName()];
        this.metadataRegistryService.getEmbeddedLinksets(datasetIRI.getValue(), this.project, null, true).subscribe(
          linksets => {
            this.linksets = linksets;
          }
        );

      }
    );
  }

  onRestrictTargetChange() {
    if (!this.restrictTarget) {
      this.selectedLinkset = null;
    }
  }

  //@override
  createDownload(overwrite: boolean): Observable<boolean> {
    SVContext.setTempProject(this.project);
    let targetUriPrefix = this.selectedLinkset ? this.selectedLinkset.targetDataset.uriSpace : null;
    return this.downloadService.createAlignmentDownload(this.fileName, this.localized, this.format, targetUriPrefix, null, this.zipped, overwrite).pipe(
      map(() => {
        return true;
      }),
      finalize(() => {
        this.loading = false;
        SVContext.removeTempProject();
      }),
      catchError(() => {
        //the only error not automatically handled is FileAlreadyExistsException
        return from(
          this.basicModals.confirm({ key: "COMMONS.STATUS.WARNING" }, { key: "MESSAGES.ALREADY_EXISTING_FILE_OVERWRITE_CONFIRM" }, ModalType.warning).then(
            () => {
              return this.createDownload(true).pipe(
                map(() => {
                  return true;
                })
              );
            },
            () => {
              return of(false);
            }
          )
        ).pipe(
          mergeMap(done => done)
        );
      })
    );
  }

}