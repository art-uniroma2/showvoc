import { Component, Input, ViewChild } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { ConfigurableExtensionFactory, ExtensionFactory, ExtensionPointID, PluginSpecification, Settings } from 'src/app/models/Plugins';
import { Project } from 'src/app/models/Project';
import { ExtensionsServices } from 'src/app/services/extensions.service';
import { ProjectsServices } from 'src/app/services/projects.service';
import { ExtensionConfiguratorComponent } from 'src/app/widget/extension-configurator/extension-configurator.component';

@Component({
  selector: "rendering-engine-config-modal",
  templateUrl: "./rendering-engine-config-modal.component.html",
  standalone: false
})
export class RenderingEngineConfigModalComponent {

  @Input() project: Project;

  loading: boolean;

  //RENDERING ENGINE PLUGIN
  @ViewChild("rendEngConfigurator") rendEngConfigurator: ExtensionConfiguratorComponent;
  rendEngExtensions: ConfigurableExtensionFactory[]; //available extensions for rendering engine (retrieved through getExtensions)
  selectedRendEngExtension: ConfigurableExtensionFactory; //chosen extension for rendering engine (the one selected through a <select> element)
  selectedRendEngExtensionConf: Settings; //chosen configuration for the chosen rendering engine extension (selected through a <select> element)

  ngOnInit() {
    this.initRenderingEngine();
  }

  constructor(public activeModal: NgbActiveModal, private projectsService: ProjectsServices, private extensionService: ExtensionsServices, private basicModals: BasicModalsServices) { }


  private initRenderingEngine() {
    this.extensionService.getExtensions(ExtensionPointID.RENDERING_ENGINE_ID).subscribe((extensions: ExtensionFactory[]) => {
      this.rendEngExtensions = extensions as ConfigurableExtensionFactory[];
      this.projectsService.getRenderingEngineConfiguration(this.project).subscribe(
        config => {
          return this.rendEngConfigurator.forceConfiguration(config.factoryID, config.settings);
        }
      );
    });
  }


  ok() {
    //update rendering engine config

    //check if configuration needs to be configured
    if (this.selectedRendEngExtensionConf.requireConfiguration()) {
      this.basicModals.alert({ key: "COMMONS.STATUS.WARNING" }, { key: "MESSAGES.MISSING_RENDERING_ENGINE_CONFIG" }, ModalType.warning);
      return;
    }
    let pluginSpec: PluginSpecification = {
      factoryId: this.selectedRendEngExtension.id,
      configType: this.selectedRendEngExtensionConf.type,
      configuration: this.selectedRendEngExtensionConf.getPropertiesAsMap(true)
    };
    this.projectsService.updateRenderingEngineConfiguration(this.project, pluginSpec).subscribe(
      () => {
        this.activeModal.close();
      }
    );
  }


  close() {
    this.activeModal.dismiss();
  }

}