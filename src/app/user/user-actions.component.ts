import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { BasicModalsServices } from "../modal-dialogs/basic-modals/basic-modals.service";
import { UserServices } from "../services/user.service";
import { ModalType } from "../modal-dialogs/Modals";

@Component({
    selector: "user-actions-component",
    template: "<div></div>",
    host: { class: "pageComponent" },
    standalone: false
})
export class UserActionsComponent {

    constructor(
        private userService: UserServices, 
        private basicModals: BasicModalsServices,
        private activeRoute: ActivatedRoute, 
        private router: Router
    ) { }

    ngOnInit() {
        let action = this.activeRoute.snapshot.queryParams['action'];
        let email: string = this.activeRoute.snapshot.queryParams['email'];
        let token: string = this.activeRoute.snapshot.queryParams['token'];
        if (action == "verify") {
            this.verifyEmail(email, token);
        } else if (action == "activate") {
            this.activateUser(email, token);
        }
    }

    //even if, at the moment, it is not still possible to enable email verification in SV, this has been ported from VB for simplicity
    private verifyEmail(email: string, token: string) {
        this.userService.verifyUserEmail(email, token).subscribe(
            () => {
                this.basicModals.alert({ key: "COMMONS.STATUS.OPERATION_DONE" }, { key: "USER.MESSAGES.USER_EMAIL_VERIFIED" });
                this.router.navigate(["/home"]);
            },
            (err: Error) => {
                if (err.name.endsWith("EmailVerificationExpiredException")) {
                    this.basicModals.alert({ key: "COMMONS.STATUS.ERROR" }, err.message, ModalType.warning);
                }
            }
        );
    }

    private activateUser(email: string, token: string) {
        this.userService.activateRegisteredUser(email, token).subscribe(
            () => {
                this.basicModals.alert({ key: "COMMONS.STATUS.OPERATION_DONE" }, { key: "USER.MESSAGES.USER_REGISTERED_ACTIVATED", params: { email: email } });
                this.router.navigate(["/home"]);
            },
            (err: Error) => {
                if (err.name.endsWith("UserActivationExpiredException")) {
                    this.basicModals.alert({ key: "COMMONS.STATUS.ERROR" }, err.message, ModalType.warning);
                }
            }
        );
    }

}