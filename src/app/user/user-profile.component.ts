import { Component } from "@angular/core";
import { User } from "../models/User";
import { SVContext } from '../utils/SVContext';

@Component({
    selector: "user-profile",
    templateUrl: "./user-profile.component.html",
    host: { class: "pageComponent" },
    standalone: false
})
export class UserProfileComponent {

    user: User;

    constructor() { }

    ngOnInit() {
        this.user = SVContext.getLoggedUser();
    }


}