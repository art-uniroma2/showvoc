import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BasicModalsServices } from '../modal-dialogs/basic-modals/basic-modals.service';
import { AuthServiceMode } from '../models/Properties';
import { AuthServices } from '../services/auth.service';
import { UserServices } from '../services/user.service';
import { HttpManager } from '../utils/HttpManager';
import { SVContext } from '../utils/SVContext';
import { Oauth2Service } from "../services/oauth2.service";

@Component({
  selector: 'login-component',
  templateUrl: './login.component.html',
  host: { class: "pageComponent" },
  styles: [`
    :host { 
        background-color: #f5f5f5;
    }
    .form-signin {
        width: 100%;
        max-width: 330px;
        padding: 15px;
        margin: auto;
    }
    `],
  standalone: false
})
export class LoginComponent implements OnInit, AfterViewInit {

  @ViewChild("samlForm", { static: true }) samlFormEl: ElementRef;

  email: string;
  password: string;

  authServMode: AuthServiceMode;

  samlAction: string;

  constructor(
    private authService: AuthServices,
    private userService: UserServices,
    private basicModals: BasicModalsServices,
    private oauth2Service: Oauth2Service,
    private router: Router) { }

  ngOnInit() {
    this.authServMode = SVContext.getSystemSettings().authService;

    let serverhost = HttpManager.getServerHost();
    this.samlAction = serverhost + "/semanticturkey/saml2/authenticate/st_saml";
  }

  ngAfterViewInit() {
    //in case of saml authentication, right after the view is init (required in order to let the view init samlForm), submit the form
    if (this.authServMode == AuthServiceMode.SAML) {
      let form: HTMLFormElement = this.samlFormEl.nativeElement;
      form.submit();
    } else if (this.authServMode == AuthServiceMode.OAuth2) {
      this.loginOauth2();
    }
  }

  login() {
    this.authService.login(this.email, this.password).subscribe(
      () => {
        this.router.navigate(["/home"]);
      }
    );
  }

  loginOauth2() {
    this.oauth2Service.login();
  }


  forgotPassword() {
    this.basicModals.prompt({ key: "USER.PASSWORD.FORGOT_PASSWORD" }, { value: "E-mail" }, { key: "MESSAGES.INSERT_EMAIL_FOR_RESET_PASSWORD" }).then(
      (email: string) => {
        this.userService.forgotPassword(email).subscribe(
          () => {
            this.basicModals.alert({ key: "USER.PASSWORD.FORGOT_PASSWORD" }, { key: "MESSAGES.RESET_PASSWORD_EMAIL_SENT" });
          }
        );
      },
      () => { }
    );
  }



}
