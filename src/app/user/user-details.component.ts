import { Component, Input } from "@angular/core";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalOptions } from '../modal-dialogs/Modals';
import { User } from "../models/User";
import { UserServices } from '../services/user.service';
import { SVContext } from '../utils/SVContext';
import { ChangePasswordModalComponent } from './change-password-modal.component';

@Component({
  selector: "user-details",
  templateUrl: "./user-details.component.html",
  standalone: false
})
export class UserDetailsComponent {

  @Input() user: User;

  constructor(private userService: UserServices, private modalService: NgbModal) { }

  updateGivenName(newGivenName: string) {
    this.userService.updateUserGivenName(this.user.getEmail(), newGivenName).subscribe(
      user => {
        if (user.getIri().equals(SVContext.getLoggedUser().getIri())) {
          this.updateLoggedUserIfNeeded(user);
        }
      }
    );
  }

  updateFamilyName(newFamilyName: string) {
    this.userService.updateUserFamilyName(this.user.getEmail(), newFamilyName).subscribe(
      user => {
        this.updateLoggedUserIfNeeded(user);
      }
    );
  }

  updateEmail(newEmail: string) {
    this.userService.updateUserEmail(this.user.getEmail(), newEmail).subscribe(
      user => {
        this.updateLoggedUserIfNeeded(user);
      }
    );
  }

  /**
   * Update logged user in context if the update has been done on it
   */
  private updateLoggedUserIfNeeded(updatedUser: User) {
    if (updatedUser.getIri().equals(SVContext.getLoggedUser().getIri())) {
      SVContext.setLoggedUser(updatedUser);
    }
  }

  changePwd() {
    let _options: ModalOptions = new ModalOptions();
    const modalRef: NgbModalRef = this.modalService.open(ChangePasswordModalComponent, _options);
    return modalRef.result;
  }

}