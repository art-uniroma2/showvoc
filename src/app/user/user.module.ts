import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { ModalsModule } from '../modal-dialogs/modals.module';
import { WidgetModule } from '../widget/widget.module';
import { ChangePasswordModalComponent } from './change-password-modal.component';
import { LoginComponent } from './login.component';
import { RegistrationModalComponent } from './registration-modal.component';
import { RegistrationComponent } from './registration.component';
import { ResetPasswordComponent } from './reset-password.component';
import { UserCreateFormComponent } from './user-create-form.component';
import { UserDetailsComponent } from './user-details.component';
import { UserMenuComponent } from './user-menu.component';
import { UserProfileComponent } from './user-profile.component';

@NgModule({
  declarations: [
    ChangePasswordModalComponent,
    LoginComponent,
    RegistrationComponent,
    RegistrationModalComponent,
    ResetPasswordComponent,
    UserCreateFormComponent,
    UserDetailsComponent,
    UserMenuComponent,
    UserProfileComponent
  ],
  imports: [
    CommonModule,
    DragDropModule,
    FormsModule,
    ModalsModule,
    NgbDropdownModule,
    RouterModule,
    TranslateModule,
    WidgetModule
  ],
  exports: [
    RegistrationModalComponent,
    UserMenuComponent,
    UserDetailsComponent,
  ],
  providers: []
})
export class UserModule { }
