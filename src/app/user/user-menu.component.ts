import { Component } from "@angular/core";
import { STRoles, User } from "../models/User";
import { ProjectsServices } from "../services/projects.service";
import { SVContext } from "../utils/SVContext";
import { AuthServices } from "../services/auth.service";

@Component({
  selector: "ul[user-menu]", //to avoid css style breaking (use <ul user-menu ...></ul>)
  templateUrl: "./user-menu.component.html",
  standalone: false
})
export class UserMenuComponent {

  user: User;

  adminDashboardAuthorized: boolean;

  constructor(
    private authServices: AuthServices,
    private projectService: ProjectsServices
  ) { }

  ngOnInit() {
    this.user = SVContext.getLoggedUser();
    if (this.user.isAdmin() || this.user.isSuperUser(true)) {
      this.adminDashboardAuthorized = true;
    } else {
      this.projectService.listProjects(null, null, null, STRoles.projectmanager).subscribe(
        projects => {
          this.adminDashboardAuthorized = projects.length > 0;
        }
      );
    }
  }

  onOpenChange(open: boolean) {
    if (open) {
      //update user each time menu is open (useful in case user info changed during session)
      this.user = SVContext.getLoggedUser();
    }
  }

  logout() {
    this.authServices.logout().subscribe();
  }


}