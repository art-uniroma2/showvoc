import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { BasicModalsServices } from '../modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from '../modal-dialogs/Modals';
import { AuthServiceMode } from '../models/Properties';
import { UserForm } from '../models/User';
import { AuthServices } from '../services/auth.service';
import { ShowVocServices } from '../services/showvoc.service';
import { UserServices } from '../services/user.service';
import { SVContext } from '../utils/SVContext';
import { Translation } from '../utils/TranslationUtils';
import { UserConstraint } from './user-create-form.component';

@Component({
    selector: 'registration-component',
    templateUrl: './registration.component.html',
    host: { class: "pageComponent" },
    standalone: false
})
export class RegistrationComponent implements OnInit {

    private authServMode: AuthServiceMode;

    firstAccess: boolean = false;

    userForm: UserForm;
    constraintUser: UserConstraint;

    loading: boolean;

    constructor(private userService: UserServices, private authService: AuthServices, private svService: ShowVocServices,
        private basicModals: BasicModalsServices, private router: Router, private activeRoute: ActivatedRoute) { }

    ngOnInit() {
        this.firstAccess = this.activeRoute.snapshot.params['firstAccess'] == "1";

        this.authServMode = SVContext.getSystemSettings().authService;
        if (this.authServMode == AuthServiceMode.SAML) {
            let constraintEmail = this.activeRoute.snapshot.queryParams['email'];
            if (constraintEmail) {
                let constraintGivenName = this.activeRoute.snapshot.queryParams['givenName'];
                let constraintFamilyName = this.activeRoute.snapshot.queryParams['familyName'];
                this.constraintUser = { email: constraintEmail, givenName: constraintGivenName, familyName: constraintFamilyName };
            } else { //data about the SAML user are not provided. Probably the page has been accessed manually. Redirect to Home
                this.router.navigate(["/Home"]);
            }
        }
    }

    private isConfirmPwdOk() {
        return this.userForm.password == this.userForm.confirmedPassword;
    }

    isDataValid(): boolean {
        return (
            this.userForm &&
            (this.userForm.email && this.userForm.email.trim() != "") &&
            (this.userForm.password && this.userForm.password.trim() != "") &&
            (this.userForm.confirmedPassword && this.userForm.confirmedPassword.trim() != "") &&
            (this.userForm.givenName && this.userForm.givenName.trim() != "") &&
            (this.userForm.familyName && this.userForm.familyName.trim() != "")
        );
    }

    submit() {
        if (!this.isDataValid()) return;

        //check email
        if (!UserForm.isValidEmail(this.userForm.email)) {
            this.basicModals.alert({ key: "COMMONS.STATUS.INVALID_DATA" }, { key: "MESSAGES.ENTER_VALID_EMAIL" }, ModalType.warning);
            return;
        }
        //check password
        if (!this.isConfirmPwdOk()) {
            this.basicModals.alert({ key: "COMMONS.STATUS.INVALID_DATA" }, { key: "MESSAGES.DIFFERENT_CONFIRM_PASSWORD" }, ModalType.warning);
            return;
        }

        this.loading = true;
        this.userService.registerUser(this.userForm.email, this.userForm.password, this.userForm.givenName, this.userForm.familyName).pipe(
            finalize(() => { this.loading = false; })
        ).subscribe(
            () => {
                let message: Translation;
                if (this.firstAccess) {
                    message = { key: "USER.MESSAGES.USER_ADMINISTRATOR_CREATED" };
                } else {
                    //inform user that registration is completed and the account waits to be activated
                    message = { key: "USER.MESSAGES.USER_CREATED_WAIT_ACTIVATION" };
                }
                this.basicModals.alert({ key: "COMMONS.STATUS.OPERATION_DONE" }, message).then(
                    () => {
                        if (this.firstAccess) {
                            /*
                            Now, only in case of Default auth service mode, login the just registered admin
                            (SAML registered user is automatically set/login server side)
                            In both cases
                            - initialize ShowVoc stuff (required admin to be logged)
                            - show registration success message
                            - redirect to Sysconfig page
                            */
                            if (this.authServMode == AuthServiceMode.Default) {
                                this.authService.login(this.userForm.email, this.userForm.password).subscribe(
                                    () => {
                                        this.initShowVocAndRedirect();
                                    }
                                );
                            } else { //SAML: simply redirect to sys config (registered user is automatically set server side)
                                this.initShowVocAndRedirect();
                            }
                        } else {
                            this.router.navigate(['/home']);
                        }
                    }
                );
            }
        );
    }

    private initShowVocAndRedirect() {
        this.svService.setAllowAnonymous(true).subscribe(
            () => {
                this.router.navigate(["/sysconfig"]);
            }
        );
    }

}
