import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AnnotatedValue, IRI, Resource } from '../models/Resources';
import { HttpManager, STRequestOptions, STRequestParams } from "../utils/HttpManager";
import { ResourceDeserializer } from '../utils/ResourceUtils';
import { SettingsManager } from '../utils/SettingsManager';
import { SVContext } from '../utils/SVContext';

@Injectable()
export class ClassesServices {

  private serviceName = "Classes";

  constructor(private httpMgr: HttpManager, private settingsMgr: SettingsManager) { }

  /**
   * takes a list of classes and return their description as if they were roots for a tree
   * (so more, role, explicit etc...)
   * @param classList
   */
  getClassesInfo(classList: Resource[], options?: STRequestOptions): Observable<AnnotatedValue<Resource>[]> {
    let params: STRequestParams = {
      classList: classList
    };
    options = new STRequestOptions().merge(options);
    if (!options.ctxLangs) {
      options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
    }
    return this.httpMgr.doGet(this.serviceName, "getClassesInfo", params, options).pipe(
      map(stResp => {
        return ResourceDeserializer.createResourceArray(stResp);
      })
    );
  }

  /**
   * Returns a list of AnnotatedValue subClasses of the given class
   * @param superClass class of which retrieve its subClasses
   */
  getSubClasses(superClass: Resource, numInst: boolean, options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
    let params: STRequestParams = {
      superClass: superClass,
      numInst: numInst
    };
    options = new STRequestOptions().merge(options);
    if (!options.ctxLangs) {
      options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
    }
    return this.httpMgr.doGet(this.serviceName, "getSubClasses", params, options).pipe(
      map(stResp => {
        return ResourceDeserializer.createIRIArray(stResp);
      })
    );
  }

  /**
   * Returns the (explicit) instances of the class cls.
   * @param cls
   */
  getInstances(cls: Resource, options?: STRequestOptions): Observable<AnnotatedValue<Resource>[]> {
    let params: STRequestParams = {
      cls: cls
    };
    options = new STRequestOptions().merge(options);
    if (!options.ctxLangs) {
      options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
    }
    return this.httpMgr.doGet(this.serviceName, "getInstances", params, options).pipe(
      map(stResp => {
        let instances = ResourceDeserializer.createResourceArray(stResp);
        return instances;
      })
    );
  }

  /**
   * 
   * @param cls 
   */
  getNumberOfInstances(cls: Resource, options?: STRequestOptions): Observable<number> {
    let params: STRequestParams = {
      cls: cls
    };
    return this.httpMgr.doGet(this.serviceName, "getNumberOfInstances", params, options);
  }


}