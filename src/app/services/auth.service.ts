import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { User } from '../models/User';
import { AuthorizationEvaluator } from '../utils/AuthorizationEvaluator';
import { HttpManager, STRequestParams } from "../utils/HttpManager";
import { SettingsManager } from '../utils/SettingsManager';
import { SVContext } from '../utils/SVContext';
import { Oauth2Service } from "./oauth2.service";
import { AuthServiceMode } from "../models/Properties";

@Injectable()
export class AuthServices {

  private serviceName = "Auth";

  constructor(private httpMgr: HttpManager, private router: Router, private settingsMgr: SettingsManager, private oauth2Service: Oauth2Service) { }

  /**
   * Logs in and registers the logged user in the Context
   */
  login(email: string, password: string, rememberMe?: boolean): Observable<User> {
    let params: STRequestParams = {
      email: email,
      password: password,
      _spring_security_remember_me: rememberMe
    };
    return this.httpMgr.doPost(this.serviceName, "login", params).pipe(
      mergeMap(stResp => {
        let user: User = User.parse(stResp);
        SVContext.resetContext(); //so that everything is emptied (also context project, which is necessary to force the reinit of Settings after user login and re-select the same project accessed before)
        SVContext.setLoggedUser(user);
        SVContext.setResetRoutes(true); //so that the stored route are dropped
        return this.settingsMgr.initSettingsAtUserAccess().pipe(
          map(() => {
            return user;
          })
        );
      })
    );

  }



  /**
   * Logs out and removes the logged user from the SVContext
   */
  logout() {
    let logoutFn: Observable<any>;
    if (SVContext.getSystemSettings().authService == AuthServiceMode.OAuth2) {
      logoutFn = of(this.oauth2Service.logout());
    } else {
      let params: STRequestParams = {};
      logoutFn = this.httpMgr.doGet(this.serviceName, "logout", params);
    }
    return logoutFn.pipe(
      map(stResp => {
        SVContext.removeLoggedUser();
        SVContext.resetContext();
        this.router.navigate(["/"]);
        AuthorizationEvaluator.reset();
        return stResp;
      })
    );
  }

}