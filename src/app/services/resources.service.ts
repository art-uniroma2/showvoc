import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AnnotatedValue, IRI, Resource, ResourcePosition } from '../models/Resources';
import { HttpManager, STRequestOptions, STRequestParams } from "../utils/HttpManager";
import { ResourceDeserializer } from '../utils/ResourceUtils';
import { SettingsManager } from '../utils/SettingsManager';
import { SVContext } from '../utils/SVContext';

@Injectable()
export class ResourcesServices {

  private serviceName = "Resources";

  constructor(private httpMgr: HttpManager, private settingsMgr: SettingsManager) { }

  /**
   * Returns the description (nature show and qname) of the given resource
   * @param resource 
   */
  getResourceDescription<T extends Resource>(resource: T, options?: STRequestOptions): Observable<AnnotatedValue<T>> {
    let params: STRequestParams = {
      resource: resource
    };
    options = new STRequestOptions().merge(options);
    if (!options.ctxLangs) {
      options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
    }
    return this.httpMgr.doGet(this.serviceName, "getResourceDescription", params, options).pipe(
      map(stResp => {
        return ResourceDeserializer.createResource(stResp) as AnnotatedValue<T>;
      })
    );
  }

  /**
   * Returns the description of a set of resources
   * @param resources 
   */
  getResourcesInfo<T extends Resource>(resources: T[], options?: STRequestOptions): Observable<AnnotatedValue<T>[]> {
    let params: STRequestParams = {
      resources: JSON.stringify(resources.map(r => r.toNT()))
    };
    options = new STRequestOptions().merge(options);
    if (!options.ctxLangs) {
      options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
    }
    return this.httpMgr.doPost(this.serviceName, "getResourcesInfo", params, options).pipe(
      map(stResp => {
        return ResourceDeserializer.createResourceArray(stResp) as AnnotatedValue<T>[];
      })
    );
  }

  /**
   * Returns the position (local/remote/unknown) of the given resource
   * @param resource
   */
  getResourcePosition(resource: IRI): Observable<ResourcePosition> {
    let params: STRequestParams = {
      resource: resource
    };
    return this.httpMgr.doGet(this.serviceName, "getResourcePosition", params).pipe(
      map(stResp => {
        return ResourcePosition.deserialize(stResp);
      })
    );
  }

  /**
   * Returns the position (local/remote/unknown) of the given resources
   * @param resource
   */
  getResourcesPosition(resources: IRI[], options?: STRequestOptions): Observable<{ [key: string]: ResourcePosition }> {
    let resourcesIri: string[] = resources.map(r => r.toNT());
    let params: STRequestParams = {
      resources: JSON.stringify(resourcesIri)
    };
    return this.httpMgr.doPost(this.serviceName, "getResourcesPosition", params, options).pipe(
      map(stResp => {
        let map: { [key: string]: ResourcePosition } = {};
        for (let res in stResp) {
          map[res] = ResourcePosition.deserialize(stResp[res].position);
        }
        return map;
      })
    );
  }

  /**
   * Checks if a IRIs/QNames list (manually entered by the user) is correct.
   * Returns an exception if it is not a valid IRI list
   * 
   * @param iriList
   * @return
   */
  validateIRIList(iriList: string): Observable<AnnotatedValue<IRI>[]> {
    let params = {
      iriList: iriList
    };
    let options: STRequestOptions = new STRequestOptions({
      errorHandlers: [{
        className: "*", action: 'skip'
      }]
    });
    return this.httpMgr.doGet(this.serviceName, "validateIRIList", params, options).pipe(
      map(stResp => {
        return ResourceDeserializer.createIRIArray(stResp);
      })
    );
  }

}