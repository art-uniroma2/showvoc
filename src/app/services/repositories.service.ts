import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Repository, ExceptionDAO, RemoteRepositorySummary } from "../models/Project";
import { HttpManager } from "../utils/HttpManager";

@Injectable()
export class RepositoriesServices {

  private serviceName = "Repositories";

  constructor(private httpMgr: HttpManager) { }

  /**
   * Updates the value of a triple replacing the old value with the new one
   */
  getRemoteRepositories(serverURL: string, username?: string, password?: string): Observable<Repository[]> {
    let params: any = {
      serverURL: serverURL
    };
    if (username != null && password != null) {
      params.username = username;
      params.password = password;
    }
    return this.httpMgr.doPost(this.serviceName, "getRemoteRepositories", params).pipe(
      map(stResp => {
        let repositories: Repository[] = [];
        for (const r of stResp) {
          let repo: Repository = {
            id: r.id,
            location: r.location,
            description: r.description,
            readable: r.readable,
            writable: r.writable
          };
          repositories.push(repo);
        }
        return repositories;
      })
    );
  }

  /**
   * Delete a list of remote repositories.
   * Returns a list of ExceptionDAO. This list has the same length of the param remoteRepositories and contains null if the repository
   * (at the corresponding position) has been deleted successfully, or the exception description if it failed
   * 
   * @param remoteRepositories 
   */
  deleteRemoteRepositories(remoteRepositories: RemoteRepositorySummary[]): Observable<ExceptionDAO[]> {
    let params: any = {
      remoteRepositories: JSON.stringify(remoteRepositories)
    };
    return this.httpMgr.doPost(this.serviceName, "deleteRemoteRepositories", params).pipe(
      map(stResp => {
        let exceptions: ExceptionDAO[] = [];
        for (const r of stResp) {
          let ex: ExceptionDAO;
          if (r != null) {
            ex = {
              message: r.message,
              type: r.type,
              stacktrace: r.stacktrace
            };
          }
          exceptions.push(ex); //in case the repository at position i has been
        }
        return exceptions;
      })
    );
  }


}