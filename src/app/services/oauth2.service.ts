import { Injectable } from '@angular/core';
import { AuthConfig, OAuthService } from "angular-oauth2-oidc";
import { UserServices } from "./user.service";
import { BasicModalsServices } from "../modal-dialogs/basic-modals/basic-modals.service";
import { ModalType } from "../modal-dialogs/Modals";

const authConfig: AuthConfig = {
  issuer: 'https://127.0.0.1:7002/cas/oauth2', // The URL of the OpenID Connect / OAuth2 identity provider.
  redirectUri: window.location.origin, // The URL of this SPA to which users should be redirected after logging in to (or out of) IDP.
  postLogoutRedirectUri: window.location.origin,
  clientId: 'a8ximt03sA8jjxb6n3zdLbU7HqoMnObWEdSkr8mbKlMJXu8Azi3YG7T8PB7YV6UyQEmSwUWpK5RghKPxw2Vm05-wVU4Cr9ce0us6qTKNYmJC4', // The client ID of this SPA registered in the IDP.
  responseType: 'code',
  scope: 'openid profile email', // The list of scopes that this SPA intends to request.
  showDebugInformation: true  // A flag to display debug information on the browser console.
};

@Injectable()
export class Oauth2Service {

  constructor(private oauthService: OAuthService, private userService: UserServices, private basicModalServices: BasicModalsServices) {
  }

  /**
   * Configures the OAuth service with the provided authentication configuration,
   * loads the discovery document, and attempts to log in. Ensures the completion
   * of the configuration process regardless of whether a valid access token is present
   * or if any errors occur during the execution.
   *
   * @return {Promise<void>} A promise that resolves when the configuration process completes.
   */
  configure(): Promise<void> {
    return new Promise((resolve) => {
      this.oauthService.configure(authConfig);

      this.oauthService.loadDiscoveryDocumentAndTryLogin()
        .then(() => {
          if (this.oauthService.hasValidAccessToken()) {
            this.userService.getUser().subscribe({
              next: () => {
                resolve(); // the process ends
              },
              error: (err) => {
                console.error("Error loading user:", err);
                resolve(); // Even if there is an error, complete the Promise to avoid snags
              }
            });
          } else {
            if (this.oauthService.getAccessToken()) {
              this.oauthService.revokeTokenAndLogout();
            }
            console.log("No valid id token found");
            resolve(); // Consider the configuration completed even without a valid token
          }
        })
        .catch(error => {
          console.error("Error loading of the discovery document:", error);
          this.basicModalServices.alert({ key: "STATUS.WARNING" }, "OAuth2 configuration failed. Please try to reload the page or contact the support.", ModalType.warning);
          resolve(); // Complete the Promise even in case of an error
        });
    });
  }


  login() {
    this.oauthService.initCodeFlow();
  }

  logout() {
    this.oauthService.revokeTokenAndLogout();
  }




}
