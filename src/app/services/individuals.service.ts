import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AnnotatedValue, Resource, IRI } from '../models/Resources';
import { HttpManager, STRequestOptions } from "../utils/HttpManager";
import { ResourceDeserializer } from '../utils/ResourceUtils';
import { SettingsManager } from '../utils/SettingsManager';
import { SVContext } from '../utils/SVContext';

@Injectable()
export class IndividualsServices {

    private serviceName = "Individuals";

    constructor(private httpMgr: HttpManager, private settingsMgr: SettingsManager) { }

    /**
     * Returns the (explicit) named types of the given individual
     * @param individual
     */
    getNamedTypes(individual: Resource, options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
        let params: any = {
            individual: individual
        };
        options = new STRequestOptions().merge(options);
        if (!options.ctxLangs) {
            options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
        }
        return this.httpMgr.doGet(this.serviceName, "getNamedTypes", params, options).pipe(
            map(stResp => {
                let types = ResourceDeserializer.createIRIArray(stResp);
                return types;
            })
        );
    }
   

}