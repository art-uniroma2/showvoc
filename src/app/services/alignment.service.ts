import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SearchMode } from '../models/Properties';
import { RDFFormat } from '../models/RDFFormat';
import { IRI, Triple } from '../models/Resources';
import { HttpManager, STRequestOptions, STRequestParams } from "../utils/HttpManager";
import { NTriplesUtil } from '../utils/ResourceUtils';

@Injectable()
export class AlignmentServices {

    private serviceName = "Alignment";

    constructor(private httpMgr: HttpManager) { }

    /**
     * Returns the number of available mappings
     * @param targetUriPrefix 
     * @param mappingProperties 
     * @param expressInPages whether the count should be an absolute number or expressed as the number of pages
     * @param pageSize if less or equal to zero, then everything goes into one page
     */
    getMappingCount(targetUriPrefix: string, mappingProperties?: IRI[], includeSubProperties?: boolean, expressInPages?: boolean, pageSize?: number, options?: STRequestOptions): Observable<number> {
        let params: STRequestParams = {
            targetUriPrefix: targetUriPrefix,
            mappingProperties: mappingProperties,
            includeSubProperties: includeSubProperties,
            expressInPages: expressInPages,
            pageSize: pageSize
        };
        return this.httpMgr.doGet(this.serviceName, "getMappingCount", params, options);
    }

    /**
     * Returns the available mappings (triple of IRI)
     * @param targetUriPrefix 
     * @param page 
     * @param pageSize 
     * @param mappingProperties 
     */
    getMappings(targetUriPrefix: string, page?: number, pageSize?: number, mappingProperties?: IRI[], includeSubProperties?: boolean, sortByLang?: string, options?: STRequestOptions): Observable<Triple<IRI>[]> {
        let params: STRequestParams = {
            targetUriPrefix: targetUriPrefix,
            page: page,
            pageSize: pageSize,
            mappingProperties: mappingProperties,
            includeSubProperties: includeSubProperties,
            sortByLang: sortByLang
        };
        return this.httpMgr.doGet(this.serviceName, "getMappings", params, options).pipe(
            map(stResp => {
                let triples: Triple<IRI>[] = [];
                for (let t of stResp) {
                    triples.push(new Triple(
                        NTriplesUtil.parseIRI(t[0]),
                        NTriplesUtil.parseIRI(t[1]),
                        NTriplesUtil.parseIRI(t[2])
                    ));
                }
                return triples;
            })
        );
    }

    filterMappings(targetUriPrefix: string,
        searchString: string,
        searchMode: SearchMode,
        mappingProperties?: IRI[],
        useLexicalizations?: boolean,
        useLocalName?: boolean,
        useURI?: boolean,
        useNotes?: boolean,
        langs?: string[], 
        sortByLang?: string, 
        includeLocales?: boolean,
        options?: STRequestOptions): Observable<Triple<IRI>[]> {
        let params: STRequestParams = {
            targetUriPrefix: targetUriPrefix,
            searchString: searchString,
            searchMode: searchMode,
            mappingProperties: mappingProperties,
            useLexicalizations: useLexicalizations,
            useLocalName: useLocalName,
            useURI: useURI,
            useNotes: useNotes,
            langs: langs,
            sortByLang: sortByLang,
            includeLocales: includeLocales,
        };
        return this.httpMgr.doGet(this.serviceName, "filterMappings", params, options).pipe(
            map(stResp => {
                let triples: Triple<IRI>[] = [];
                for (let t of stResp) {
                    triples.push(new Triple(
                        NTriplesUtil.parseIRI(t[0]),
                        NTriplesUtil.parseIRI(t[1]),
                        NTriplesUtil.parseIRI(t[2])
                    ));
                }
                return triples;
            })
        );
    }


    exportMappings(targetUriPrefix: string, format: RDFFormat, mappingProperties?: IRI[], options?: STRequestOptions): Observable<Blob> {
        let params: STRequestParams = {
            targetUriPrefix: targetUriPrefix,
            format: format.name,
            mappingProperties: mappingProperties
        };
        return this.httpMgr.downloadFile(this.serviceName, "exportMappings", params, false, options);
    }

}