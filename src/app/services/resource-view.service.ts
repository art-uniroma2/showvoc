import { Injectable } from '@angular/core';
import { Resource } from '../models/Resources';
import { HttpManager, STRequestOptions } from "../utils/HttpManager";
import { SettingsManager } from '../utils/SettingsManager';
import { SVContext } from '../utils/SVContext';

@Injectable()
export class ResourceViewServices {

  private serviceName = "ResourceView";

  constructor(private httpMgr: HttpManager, private settingsMgr: SettingsManager) { }

  /**
   * Returns the resource view of the given resource
   * @param resource
   */
  getResourceView(resource: Resource, includeInferred?: boolean, resourcePosition?: string, options?: STRequestOptions) {
    let params: any = {
      resource: resource,
      includeInferred: includeInferred,
      resourcePosition: resourcePosition,
      computeCvSingleValuePreview: true
    };
    options = new STRequestOptions({
      errorHandlers: [
        { className: "java.net.UnknownHostException", action: 'skip' },
        { className: "org.eclipse.rdf4j.rio.UnsupportedRDFormatException", action: 'warning' },
      ],
    }).merge(options);
    if (!options.ctxLangs) {
      options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
    }
    return this.httpMgr.doGet(this.serviceName, "getResourceView", params, options);
  }

}