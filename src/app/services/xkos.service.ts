import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SearchMode } from '../models/Properties';
import { RDFFormat } from '../models/RDFFormat';
import { AnnotatedValue, IRI } from '../models/Resources';
import { AssociationAttr, CorrespondencesAttr } from '../models/XKOS';
import { HttpManager, STRequestOptions, STRequestParams } from "../utils/HttpManager";
import { ResourceDeserializer } from '../utils/ResourceUtils';
import { SettingsManager } from '../utils/SettingsManager';
import { SVContext } from '../utils/SVContext';

@Injectable()
export class XkosServices {

    private serviceName = "XKOS";

    constructor(private httpMgr: HttpManager, private settingsMgr: SettingsManager) { }

    getCorrespondences(numAss?: boolean, options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
        let params: STRequestParams = {
            numAss: numAss,
        };
        options = new STRequestOptions().merge(options);
        if (!options.ctxLangs) {
            options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
        }
        return this.httpMgr.doGet(this.serviceName, "getCorrespondences", params, options).pipe(
            map(stResp => {
                return ResourceDeserializer.createIRIArray(stResp, [CorrespondencesAttr.numAss, CorrespondencesAttr.comparesA, CorrespondencesAttr.comparesB]);
            })
        );
    }

    getAssociations(correspondence: IRI, assConcepts?: boolean, assConcepsLimit?: number, options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
        let params: STRequestParams = {
            correspondence: correspondence,
            assConcepts: assConcepts,
            assConcepsLimit: assConcepsLimit,
        };
        options = new STRequestOptions().merge(options);
        if (!options.ctxLangs) {
            options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
        }
        return this.httpMgr.doGet(this.serviceName, "getAssociations", params, options).pipe(
            map(stResp => {
                return ResourceDeserializer.createIRIArray(stResp, [AssociationAttr.sourceConcept, AssociationAttr.targetConcept]);
            })
        );
    }

    filterAssociations(correspondence: IRI, searchString: string, searchMode: SearchMode,
        assConcepts?: boolean, assConcepsLimit?: number,
        useLexicalizations?: boolean,
        useLocalName?: boolean,
        useURI?: boolean,
        useNotes?: boolean,
        langs?: string[], 
        sortByLang?: string, 
        includeLocales?: boolean,
        options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
        options = new STRequestOptions().merge(options);
        if (!options.ctxLangs) {
            options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
        }
    
        let params: STRequestParams = {
            correspondence: correspondence,
            assConcepts: assConcepts,
            assConcepsLimit: assConcepsLimit,
            searchString: searchString,
            searchMode: searchMode,
            useLexicalizations: useLexicalizations,
            useLocalName: useLocalName,
            useURI: useURI,
            useNotes: useNotes,
            langs: langs,
            sortByLang: sortByLang,
            includeLocales: includeLocales,
        };
        return this.httpMgr.doGet(this.serviceName, "filterAssociations", params, options).pipe(
            map(stResp => {
                return ResourceDeserializer.createIRIArray(stResp, [AssociationAttr.sourceConcept, AssociationAttr.targetConcept]);
            })
        );
    }

    getSingleAssociation(association: IRI, options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
        let params: STRequestParams = {
            association: association,
        };
        options = new STRequestOptions().merge(options);
        if (!options.ctxLangs) {
            options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
        }
        return this.httpMgr.doGet(this.serviceName, "getSingleAssociation", params, options).pipe(
            map(stResp => {
                return ResourceDeserializer.createIRIArray(stResp, [AssociationAttr.sourceConcept, AssociationAttr.targetConcept]);
            })
        );
    }

    exportAssociations(format: RDFFormat, correspondence?: IRI): Observable<Blob> {
        let params: STRequestParams = {
            format: format.name,
            correspondence: correspondence
        };
        return this.httpMgr.downloadFile(this.serviceName, "exportAssociations", params);
    }

}