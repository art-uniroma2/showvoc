import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AnnotatedValue, IRI } from '../models/Resources';
import { HttpManager, STRequestOptions, STRequestParams } from "../utils/HttpManager";
import { ResourceDeserializer } from '../utils/ResourceUtils';
import { SettingsManager } from '../utils/SettingsManager';
import { SVContext } from '../utils/SVContext';

@Injectable()
export class PropertiesServices {

    private serviceName = "Properties";

    constructor(private httpMgr: HttpManager, private settingsMgr: SettingsManager) { }

    /**
     * Returns a list of top properties (properties which have not a superProperty)
     * @return an array of Properties
     */
    getTopProperties(options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
        let params: STRequestParams = {};
        options = new STRequestOptions().merge(options);
        if (!options.ctxLangs) {
            options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
        }
        return this.httpMgr.doGet(this.serviceName, "getTopProperties", params, options).pipe(
            map(stResp => {
                return ResourceDeserializer.createIRIArray(stResp);
            })
        );
    }

    /**
     * Returns a list of top Rdf Properties (properties which have not a superProperty)
     * @return an array of Properties
     */
    getTopRDFProperties(options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
        let params: STRequestParams = {};
        options = new STRequestOptions().merge(options);
        if (!options.ctxLangs) {
            options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
        }
        return this.httpMgr.doGet(this.serviceName, "getTopRDFProperties", params, options).pipe(
            map(stResp => {
                return ResourceDeserializer.createIRIArray(stResp);
            })
        );
    }

    /**
     * Returns a list of top object properties (properties which have not a superProperty)
     * @return an array of Properties
     */
    getTopObjectProperties(options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
        let params: STRequestParams = {};
        options = new STRequestOptions().merge(options);
        if (!options.ctxLangs) {
            options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
        }
        return this.httpMgr.doGet(this.serviceName, "getTopObjectProperties", params, options).pipe(
            map(stResp => {
                return ResourceDeserializer.createIRIArray(stResp);
            })
        );
    }

    /**
     * Returns a list of top datatype properties (properties which have not a superProperty)
     * @return an array of Properties
     */
    getTopDatatypeProperties(options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
        let params: STRequestParams = {};
        options = new STRequestOptions().merge(options);
        if (!options.ctxLangs) {
            options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
        }
        return this.httpMgr.doGet(this.serviceName, "getTopDatatypeProperties", params, options).pipe(
            map(stResp => {
                return ResourceDeserializer.createIRIArray(stResp);
            })
        );
    }

    /**
     * Returns a list of top annotation properties (properties which have not a superProperty)
     * @return an array of Properties
     */
    getTopAnnotationProperties(options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
        let params: STRequestParams = {};
        options = new STRequestOptions().merge(options);
        if (!options.ctxLangs) {
            options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
        }
        return this.httpMgr.doGet(this.serviceName, "getTopAnnotationProperties", params, options).pipe(
            map(stResp => {
                return ResourceDeserializer.createIRIArray(stResp);
            })
        );
    }

    /**
     * Returns a list of top ontology properties (properties which have not a superProperty)
     * @return an array of Properties
     */
    getTopOntologyProperties(options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
        let params: STRequestParams = {};
        options = new STRequestOptions().merge(options);
        if (!options.ctxLangs) {
            options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
        }
        return this.httpMgr.doGet(this.serviceName, "getTopOntologyProperties", params, options).pipe(
            map(stResp => {
                return ResourceDeserializer.createIRIArray(stResp);
            })
        );
    }

    /**
     * Returns the subProperty of the given property
     * @param property
     * @return an array of subProperties
     */
    getSubProperties(property: IRI, options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
        let params: STRequestParams = {
            superProperty: property
        };
        options = new STRequestOptions().merge(options);
        if (!options.ctxLangs) {
            options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
        }
        return this.httpMgr.doGet(this.serviceName, "getSubProperties", params, options).pipe(
            map(stResp => {
                return ResourceDeserializer.createIRIArray(stResp);
            })
        );
    }

    getFlatProperties(vocabularies?: IRI[], options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
        let params: STRequestParams = {
            vocabularies: vocabularies
        };
        options = new STRequestOptions().merge(options);
        if (!options.ctxLangs) {
            options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
        }
        return this.httpMgr.doGet(this.serviceName, "getFlatProperties", params, options).pipe(
            map(stResp => {
                return ResourceDeserializer.createIRIArray(stResp);
            })
        );
    }

    /**
     * takes a list of Properties and return their description as if they were roots for a tree
     * (so more, role, explicit etc...)
     * @param properties
     */
    getPropertiesInfo(properties: IRI[], options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
        let params: STRequestParams = {
            propList: properties
        };
        options = new STRequestOptions().merge(options);
        if (!options.ctxLangs) {
            options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
        }
        return this.httpMgr.doGet(this.serviceName, "getPropertiesInfo", params, options).pipe(
            map(stResp => {
                return ResourceDeserializer.createIRIArray(stResp);
            })
        );
    }

    getPropertiesLexicalizations(properties: IRI[], lang: string, includeSimpleString?: boolean, options?: STRequestOptions): Observable<{[key: string]: string}> {
        let params: STRequestParams = {
            properties: properties,
            lang: lang,
            includeSimpleString: includeSimpleString
        };
        options = new STRequestOptions().merge(options);
        if (!options.ctxLangs) {
            options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
        }
        return this.httpMgr.doPost(this.serviceName, "getPropertiesLexicalizations", params, options);
    }

}