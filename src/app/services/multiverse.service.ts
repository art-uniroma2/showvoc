import { Injectable } from "@angular/core";
import { HttpManager, STRequestOptions, STRequestParams } from "../utils/HttpManager";
import { Observable } from "rxjs";
import { WorldInfo } from "../models/Multiverse";

@Injectable()
export class MultiverseServices {

    private serviceName = "Multiverse";

    constructor(private httpMgr: HttpManager) { }

    listWorlds(): Observable<string[]> {
        let params: STRequestParams = {};
        return this.httpMgr.doGet(this.serviceName, "listWorlds", params);
    }

    listAlternativeWorldInfos(): Observable<WorldInfo[]> {
        let params: STRequestParams = {};
        return this.httpMgr.doGet(this.serviceName, "listAlternativeWorldInfos", params);
    }

    createWorld(name: string): Observable<void> {
        let params: STRequestParams = {
            name: name
        };
        let options: STRequestOptions = new STRequestOptions({
            errorHandlers: [
                { className: 'it.uniroma2.art.semanticturkey.exceptions.WorldAlreadyExistingException', action: 'warning' },
                { className: 'jakarta.validation.ConstraintViolationException', action: 'warning' },
            ]
        });
        return this.httpMgr.doPost(this.serviceName, "createWorld", params, options);
    }

    destroyWorld(name: string): Observable<void> {
        let params: STRequestParams = {
            name: name
        };
        return this.httpMgr.doPost(this.serviceName, "destroyWorld", params);
    }

}