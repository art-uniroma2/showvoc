import { Injectable } from '@angular/core';
import { HttpManager, STRequestOptions, STRequestParams } from "../utils/HttpManager";
import { Observable } from 'rxjs';
import { IRI, AnnotatedValue } from '../models/Resources';
import { map } from 'rxjs/operators';
import { ResourceDeserializer } from '../utils/ResourceUtils';
import { SVContext } from '../utils/SVContext';
import { SettingsManager } from '../utils/SettingsManager';

@Injectable()
export class OntoLexLemonServices {

    private serviceName = "OntoLexLemon";

    constructor(private httpMgr: HttpManager, private settingsMgr: SettingsManager) { }

    /**
     * Returns lexicons
     */
    getLexicons(options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
        let params: STRequestParams = {};
        options = new STRequestOptions().merge(options);
        if (!options.ctxLangs) {
            options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
        }
        return this.httpMgr.doGet(this.serviceName, "getLexicons", params, options).pipe(
            map(stResp => {
                return ResourceDeserializer.createIRIArray(stResp);
            })
        );
    }


    /**
     * Returns the entries in a given lexicon that starts with the supplied character.
     * @param index 
     * @param lexicon 
     */
    getLexicalEntriesByAlphabeticIndex(index: string, lexicon: IRI, options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
        let params: STRequestParams = {
            index: index,
            lexicon: lexicon
        };
        options = new STRequestOptions().merge(options);
        if (!options.ctxLangs) {
            options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
        }
        return this.httpMgr.doGet(this.serviceName, "getLexicalEntriesByAlphabeticIndex", params, options).pipe(
            map(stResp => {
                return ResourceDeserializer.createIRIArray(stResp);
            })
        );
    }

    countLexicalEntriesByAlphabeticIndex(index: string, lexicon: IRI): Observable<number> {
        let params: STRequestParams = {
            index: index,
            lexicon: lexicon
        };
        return this.httpMgr.doGet(this.serviceName, "countLexicalEntriesByAlphabeticIndex", params);
    }

    /**
     * Returns the lexicon which the lexicalEntry belongs to
     * @param lexicalEntry 
     */
    getLexicalEntryLexicons(lexicalEntry: IRI, options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
        let params: STRequestParams = {
            lexicalEntry: lexicalEntry
        };
        options = new STRequestOptions().merge(options);
        if (!options.ctxLangs) {
            options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
        }
        return this.httpMgr.doGet(this.serviceName, "getLexicalEntryLexicons", params, options).pipe(
            map(stResp => {
                return ResourceDeserializer.createIRIArray(stResp);
            })
        );
    }

    /**
     * Returns the 2-digits index of the given lexicalEntry 
     * @param lexicalEntry 
     */
    getLexicalEntryIndex(lexicalEntry: IRI): Observable<string> {
        let params: STRequestParams = {
            lexicalEntry: lexicalEntry
        };
        return this.httpMgr.doGet(this.serviceName, "getLexicalEntryIndex", params);
    }

    /**
     * 
     * @param lexicalEntry 
     */
    getLexicalEntryLanguage(lexicalEntry: IRI): Observable<string> {
        let params: STRequestParams = {
            lexicalEntry: lexicalEntry
        };
        return this.httpMgr.doGet(this.serviceName, "getLexicalEntryLanguage", params);
    }

}
