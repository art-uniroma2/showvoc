import { Injectable } from '@angular/core';
import {SVProperties} from "../utils/SVProperties";
import {lastValueFrom} from "rxjs";

@Injectable()
export class AppConfigService {
  private config: any;
  constructor(private svProp: SVProperties) {}

  /**
   * Loads the system configuration by initializing the startup system settings.
   *
   * @return {Promise<boolean>} A promise that resolves to true if the configuration was loaded successfully,
   * or false if an error occurs during the process.
   */
  async loadConfig(): Promise<boolean> {
    try {
      this.config = await lastValueFrom(this.svProp.initStartupSystemSettings());
      return true;
    } catch (error) {
      console.error('Error loading configuration:', error);
      return false;
    }
  }
}