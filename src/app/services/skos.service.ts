import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AnnotatedValue, IRI, Literal, ResAttribute, Resource } from '../models/Resources';
import { HttpManager, STRequestOptions, STRequestParams } from "../utils/HttpManager";
import { ResourceDeserializer } from '../utils/ResourceUtils';
import { SettingsManager } from '../utils/SettingsManager';
import { SVContext } from '../utils/SVContext';

@Injectable()
export class SkosServices {

  private serviceName = "SKOS";

  constructor(private httpMgr: HttpManager, private settingsMgr: SettingsManager) { }

  //====== Concept services ====== 

  /**
   * Returns the topConcepts of the given scheme
   * @param schemes
   * @return an array of top concepts
   */
  getTopConcepts(timestamp: number, schemes?: IRI[], broaderProps?: IRI[], narrowerProps?: IRI[], includeSubProperties?: boolean, options?: STRequestOptions):
    Observable<{ concepts: AnnotatedValue<IRI>[], timestamp: number }> {
    let params: STRequestParams = {
      schemes: schemes,
      broaderProps: broaderProps,
      narrowerProps: narrowerProps,
      includeSubProperties: includeSubProperties,
    };
    options = new STRequestOptions().merge(options);
    if (!options.ctxLangs) {
      options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
    }
    return this.httpMgr.doGet(this.serviceName, "getTopConcepts", params, options).pipe(
      map(stResp => {
        return {
          concepts: ResourceDeserializer.createIRIArray(stResp),
          timestamp: timestamp
        };
      })
    );
  }

  /**
   * 
   * @param schemes 
   * @param broaderProps 
   * @param narrowerProps 
   * @param includeSubProperties 
   */
  countTopConcepts(schemes?: IRI[], broaderProps?: IRI[], narrowerProps?: IRI[], includeSubProperties?: boolean): Observable<number> {
    let params: STRequestParams = {
      schemes: schemes,
      broaderProps: broaderProps,
      narrowerProps: narrowerProps,
      includeSubProperties: includeSubProperties,
    };
    return this.httpMgr.doGet(this.serviceName, "countTopConcepts", params);
  }

  /**
   * Returns the narrowers of the given concept
   * @param concept
   * @param schemes schemes where the narrower should belong
   * @return an array of narrowers
   */
  getNarrowerConcepts(concept: IRI, schemes?: IRI[], broaderProps?: IRI[], narrowerProps?: IRI[], includeSubProperties?: boolean, options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
    let params: STRequestParams = {
      concept: concept,
      schemes: schemes,
      broaderProps: broaderProps,
      narrowerProps: narrowerProps,
      includeSubProperties: includeSubProperties
    };
    options = new STRequestOptions().merge(options);
    if (!options.ctxLangs) {
      options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
    }
    return this.httpMgr.doGet(this.serviceName, "getNarrowerConcepts", params, options).pipe(
      map(stResp => {
        return ResourceDeserializer.createIRIArray(stResp);
      })
    );
  }

  /**
   * Returns the broaders of the given concept
   * @param concept
   * @param schemes schemes where the broaders should belong
   * @return an array of broaders
   */
  getBroaderConcepts(concept: IRI, schemes: IRI[], options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
    let params: STRequestParams = {
      concept: concept,
      schemes: schemes
    };
    options = new STRequestOptions().merge(options);
    if (!options.ctxLangs) {
      options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
    }
    return this.httpMgr.doGet(this.serviceName, "getBroaderConcepts", params, options).pipe(
      map(stResp => {
        return ResourceDeserializer.createIRIArray(stResp);
      })
    );
  }


  //====== Scheme services ======

  /**
   * Returns the list of available skos:ConceptScheme (New service)
   * @return an array of schemes
   */
  getAllSchemes(options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
    let params: STRequestParams = {};
    options = new STRequestOptions().merge(options);
    if (!options.ctxLangs) {
      options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
    }
    return this.httpMgr.doGet(this.serviceName, "getAllSchemes", params, options).pipe(
      map(stResp => {
        return ResourceDeserializer.createIRIArray(stResp);
      })
    );
  }

  /**
   * Checks if a scheme is empty
   * @param scheme
   */
  isSchemeEmpty(scheme: IRI): Observable<boolean> {
    let params: STRequestParams = {
      scheme: scheme
    };
    return this.httpMgr.doGet(this.serviceName, "isSchemeEmpty", params);
  }

  /**
   * Returns an array of all the schemes with the attribute "inScheme" to true if the given concept is in the scheme.
   * @param concept
   */
  getSchemesMatrixPerConcept(concept: IRI, options?: STRequestOptions) {
    let params: STRequestParams = {
      concept: concept
    };
    options = new STRequestOptions().merge(options);
    if (!options.ctxLangs) {
      options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
    }
    return this.httpMgr.doGet(this.serviceName, "getSchemesMatrixPerConcept", params, options).pipe(
      map(stResp => {
        return ResourceDeserializer.createIRIArray(stResp);
      })
    );
  }

  /**
   * Returns an array of schemes of the given concept. This invokes the same getSchemesMatrixPerConcept service,
   * but it filters out the schemes with attribute "inScheme" false
   * @param concept
   */
  getSchemesOfConcept(concept: IRI, options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
    let params: STRequestParams = {
      concept: concept
    };
    options = new STRequestOptions().merge(options);
    if (!options.ctxLangs) {
      options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
    }
    return this.httpMgr.doGet(this.serviceName, "getSchemesMatrixPerConcept", params, options).pipe(
      map(stResp => {
        let allSchemes: AnnotatedValue<IRI>[] = ResourceDeserializer.createIRIArray(stResp);
        let schemes: AnnotatedValue<IRI>[] = [];
        allSchemes.forEach((s: AnnotatedValue<IRI>) => {
          if (s.getAttribute(ResAttribute.IN_SCHEME)) {
            schemes.push(s);
          }
        });
        return schemes;
      })
    );
  }

  //====== Label services ======

  /**
   * Returns the alternative skos labels for the given concept in the given language
   * @param concept
   * @param language
   */
  getAltLabels(concept: IRI, language: string): Observable<AnnotatedValue<Literal>[]> {
    let params: STRequestParams = {
      concept: concept,
      language: language,
    };
    return this.httpMgr.doGet(this.serviceName, "getAltLabels", params).pipe(
      map(stResp => {
        return ResourceDeserializer.createLiteralArray(stResp);
      })
    );
  }


  //====== Collection services ======

  /**
   * Gets the root collections
   */
  getRootCollections(options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
    let params: STRequestParams = {};
    options = new STRequestOptions().merge(options);
    if (!options.ctxLangs) {
      options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
    }
    return this.httpMgr.doGet(this.serviceName, "getRootCollections", params, options).pipe(
      map(stResp => {
        return ResourceDeserializer.createIRIArray(stResp);
      })
    );
  }

  /**
   * Get the nested collections of a container collection
   * @param container the URI of the container collection
   */
  getNestedCollections(container: Resource, options?: STRequestOptions): Observable<AnnotatedValue<IRI>[]> {
    let params: STRequestParams = {
      container: container
    };
    options = new STRequestOptions().merge(options);
    if (!options.ctxLangs) {
      options.ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(options?.ctxProject));
    }
    return this.httpMgr.doGet(this.serviceName, "getNestedCollections", params, options).pipe(
      map(stResp => {
        return ResourceDeserializer.createIRIArray(stResp);
      })
    );
  }

}