import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConfigurableExtensionFactory, ExtensionFactory, NonConfigurableExtensionFactory, Scope, Settings } from '../models/Plugins';
import { HttpManager } from "../utils/HttpManager";

@Injectable()
export class ExtensionsServices {

  private serviceName = "Extensions";

  constructor(private httpMgr: HttpManager) { }

  getExtensionPoints(scopes?: Scope) {
    let params: any = {};
    if (scopes != null) {
      params.scopes = scopes;
    }
    return this.httpMgr.doGet(this.serviceName, "getExtensionPoints", params);
  }

  getExtensionPoint(identifier: string) {
    let params: any = {
      identifier: identifier
    };
    return this.httpMgr.doGet(this.serviceName, "getExtensionPoint", params);
  }

  getExtensions(extensionPointID: string): Observable<ExtensionFactory[]> {
    let params: any = {
      extensionPointID: extensionPointID
    };
    return this.httpMgr.doGet(this.serviceName, "getExtensions", params).pipe(
      map(stResp => {
        let exts: ExtensionFactory[] = [];
        for (const r of stResp) {
          let extFact: ExtensionFactory;

          //distinguish between Configurable and NonConfigurable ExtensionFactory
          let configColl: any[] = r.configurations;
          if (configColl != null) {
            let configurations: Settings[] = [];
            for (const conf of configColl) {
              configurations.push(Settings.parse(conf));
            }
            extFact = new ConfigurableExtensionFactory(r.id, r.name, r.description,
              r.extensionType, r.scope, r.configurationScopes, configurations);
          } else {
            extFact = new NonConfigurableExtensionFactory(r.id, r.name, r.description,
              r.extensionType, r.settingsScopes);
          }

          exts.push(extFact);
        }
        exts.sort((e1: ExtensionFactory, e2: ExtensionFactory) => {
          return e1.name.localeCompare(e2.name);
        });
        return exts;
      })
    );
  }

}