import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SearchMode } from '../models/Properties';
import { IRI, Literal } from '../models/Resources';
import { GlobalSearchResult, SearchResultDetails, TranslationResult } from '../models/Search';
import { HttpManager, STRequestOptions } from "../utils/HttpManager";

@Injectable()
export class GlobalSearchServices {

  private serviceName = "GlobalSearch";

  constructor(private httpMgr: HttpManager) { }

  createIndex() {
    let params: any = {};
    return this.httpMgr.doPost(this.serviceName, "createIndex", params);
  }

  clearSpecificIndex(projectName: string) {
    let params: any = {
      projectName: projectName
    };
    return this.httpMgr.doPost(this.serviceName, "clearSpecificIndex", params);
  }

  search(searchString: string,
    langs?: string[],
    projects?: string[],
    maxResults?: number,
    searchInLocalName?: boolean,
    caseSensitive?: boolean,
    searchMode?: SearchMode
  ): Observable<GlobalSearchResult[]> {
    let params: any = {
      searchString: searchString,
      langs: langs,
      projects: projects,
      maxResults: maxResults,
      searchInLocalName: searchInLocalName,
      caseSensitive: caseSensitive,
      searchMode: searchMode
    };
    let options: STRequestOptions = new STRequestOptions({
      errorHandlers: [
        { className: "org.apache.lucene.index.IndexNotFoundException", action: 'skip' },
        { className: "org.apache.lucene.search.IndexSearcher.TooManyClauses", action: 'skip' }
      ]
    });
    return this.httpMgr.doPost(this.serviceName, "search", params, options).pipe(
      map(results => {
        let parsedSearchResults: GlobalSearchResult[] = [];
        results.forEach(element => {
          let details: SearchResultDetails[] = [];
          element.details.forEach(detail => {
            details.push({
              matchedValue: new Literal(detail.matchedValue, detail.lang),
              predicate: new IRI(detail.predicate),
              type: detail.type
            });
          });
          let r: GlobalSearchResult = {
            resource: new IRI(element.resource),
            resourceLocalName: element.resourceLocalName,
            resourceType: new IRI(element.resourceType),
            role: element.role,
            repository: element.repository,
            details: details
          };
          parsedSearchResults.push(r);
        });
        return parsedSearchResults;
      })
    );
  }

  translation(searchString: string, searchLangs: string[], transLangs: string[], caseSensitive?: boolean, debug?: boolean): Observable<TranslationResult[]> {
    let params: any = {
      searchString: searchString,
      searchLangs: searchLangs,
      transLangs: transLangs,
      caseSensitive: caseSensitive,
      debug: debug,
    };
    let options: STRequestOptions = new STRequestOptions({
      errorHandlers: [
        { className: "org.apache.lucene.index.IndexNotFoundException", action: 'skip' },
        { className: "org.apache.lucene.search.IndexSearcher.TooManyClauses", action: 'skip' }
      ]
    });
    return this.httpMgr.doGet(this.serviceName, "translation", params, options);
  }

}