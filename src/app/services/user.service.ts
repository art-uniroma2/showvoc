import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { from, Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { BasicModalsServices } from '../modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from '../modal-dialogs/Modals';
import { Project } from '../models/Project';
import { AuthServiceMode } from '../models/Properties';
import { ShowVocConstants } from '../models/ShowVoc';
import { SamlLevel, User } from "../models/User";
import { AuthorizationEvaluator } from '../utils/AuthorizationEvaluator';
import { HttpManager, STRequestOptions, STRequestParams } from "../utils/HttpManager";
import { SettingsManager } from '../utils/SettingsManager';
import { SVContext } from '../utils/SVContext';

@Injectable()
export class UserServices {

    private serviceName = "Users";

    constructor(private httpMgr: HttpManager, private basicModals: BasicModalsServices, private settingsMgr: SettingsManager, private router: Router) { }

    /**
     * 
     * @param email
     * @param password
     * @param givenName
     * @param familyName
     */
    registerUser(email: string, password: string, givenName: string, familyName: string): Observable<User> {
        let params: STRequestParams = {
            email: email,
            password: password,
            givenName: givenName,
            familyName: familyName,
            clientHostAddress: this.getClientHostAddress(),
            appCtx: ShowVocConstants.appCtx
        };
        return this.httpMgr.doPost(this.serviceName, "registerUser", params).pipe(
            map(stResp => {
                return User.parse(stResp);
            })
        );
    }

    verifyUserEmail(email: string, token: string) {
        let params = {
            email: email,
            token: token,
            clientHostAddress: this.getClientHostAddress(),
            appCtx: ShowVocConstants.appCtx
        };
        let options: STRequestOptions = new STRequestOptions({
            errorHandlers: [
                { className: 'it.uniroma2.art.semanticturkey.user.EmailVerificationExpiredException', action: 'skip' },
            ]
        });
        return this.httpMgr.doPost(this.serviceName, "verifyUserEmail", params, options);
    }

    activateRegisteredUser(email: string, token: string) {
        let params = {
            email: email,
            token: token,
            appCtx: ShowVocConstants.appCtx
        };
        let options: STRequestOptions = new STRequestOptions({
            errorHandlers: [
                { className: 'it.uniroma2.art.semanticturkey.user.UserActivationExpiredException', action: 'skip' },
            ]
        });
        return this.httpMgr.doPost(this.serviceName, "activateRegisteredUser", params, options);
    }

    createUser(email: string, password: string, givenName: string, familyName: string): Observable<User> {
        let params: STRequestParams = {
            email: email,
            password: password,
            givenName: givenName,
            familyName: familyName,
        };
        return this.httpMgr.doPost(this.serviceName, "createUser", params).pipe(
            map(stResp => {
                return User.parse(stResp);
            })
        );
    }

    /**
     * Enables or disables a user
     * @param email email of the user to enable/disable
     * @param enabled true enables the user, false disables the user
     */
    enableUser(email: string, enabled: boolean): Observable<User> {
        let params: STRequestParams = {
            email: email,
            enabled: enabled,
            appCtx: ShowVocConstants.appCtx
        };
        return this.httpMgr.doPost(this.serviceName, "enableUser", params).pipe(
            map(stResp => {
                return User.parse(stResp);
            })
        );
    }

    /**
     * Deletes a user
     * @param email
     */
    deleteUser(email: string) {
        let params: STRequestParams = {
            email: email
        };
        return this.httpMgr.doPost(this.serviceName, "deleteUser", params);
    }

    /**
     * Different responses expected:
     * - response contains valid user object => this means that a user is logged and it is returned.
     * - response contains empty user object => this means that no user is logged, but at least a user is registered, so returns null
     * - response contains no user object => this means that no user is registered at all, redirects to registration or login page according the auth mode (respectively Default or SAML).
     *      In both these two cases returns null even if the response will not be read given the redirect
     */
    getUser(): Observable<User> {
        let params: STRequestParams = {};
        return this.httpMgr.doGet(this.serviceName, "getUser", params).pipe(
            mergeMap(stResp => {
                if (stResp.user != null) { //user object in response => serialize it. It could be empty (no logged user) or not (user logged)
                    let user: User = User.parse(stResp.user);
                    if (user != null) {
                        if (SVContext.getSystemSettings().authService == AuthServiceMode.SAML && user.isSamlUser()) { //special case: logged user is a "mockup" user for SAML login, so redirect to the registration page
                            let firstAccessPar: string = user.getSamlLevel() == SamlLevel.LEV_1 ? "1" : "0"; //saml level tells if the registering user is the 1st (lev_1) or not (lev_2)
                            this.router.navigate(['/registration/' + firstAccessPar], { queryParams: { email: user.getEmail(), givenName: user.getGivenName(), familyName: user.getFamilyName() } });
                            return of(null);
                        } else {
                            SVContext.setLoggedUser(user);
                            return this.settingsMgr.initSettingsAtUserAccess().pipe(
                                map(() => {
                                    return user;
                                })
                            );
                        }
                    } else {
                        return of(null);
                    }
                } else { //no user object in the response => there is no user registered
                    if (SVContext.getSystemSettings().authService == AuthServiceMode.Default) {
                        this.router.navigate(["/registration/1"]);
                        return of(null);
                    } else {
                        //SAML
                        return from(
                            this.basicModals.alert({ key: "COMMONS.STATUS.WARNING" }, { key: "MESSAGES.NO_USER_REGISTERED_SAML_MODE" }, ModalType.warning).then(
                                () => {
                                    this.router.navigate(["/login"]);
                                    return null;
                                }
                            )
                        );
                    }
                }
            })
        );
    }

    /**
     * Lists all the registered users
     */
    listUsers(): Observable<User[]> {
        let params: STRequestParams = {};
        return this.httpMgr.doGet(this.serviceName, "listUsers", params).pipe(
            map(stResp => {
                let users: User[] = User.createUsersArray(stResp);
                users.sort((u1: User, u2: User) => {
                    return u1.getGivenName().localeCompare(u2.getGivenName());
                });
                return users;
            })
        );
    }

    listUsersBoundToProject(project: Project): Observable<User[]> {
        let params: STRequestParams = {
            projectName: project.getName()
        };
        return this.httpMgr.doGet(this.serviceName, "listUsersBoundToProject", params).pipe(
            map(stResp => {
                let users: User[] = User.createUsersArray(stResp);
                users.sort((u1: User, u2: User) => {
                    return u1.getGivenName().localeCompare(u2.getGivenName());
                });
                return users;
            })
        );
    }

    /**
     * Returns the capabilities of the current logged user in according the roles he has in the current project.
     * Note: this is used just in projectListModal and not in projectComponent,
     * since the latter is accessed only by the admin that doesn't require authorization check and has no capabilities
     */
    listUserCapabilities(): Observable<string[]> {
        let params: STRequestParams = {};
        return this.httpMgr.doGet(this.serviceName, "listUserCapabilities", params).pipe(
            map(stResp => {
                AuthorizationEvaluator.initEvalutator(stResp);
                return stResp;
            })
        );
    }

    /**
     * Updates givenName of the given user. Returns the updated user.
     * @param email email of the user to update
     * @param givenName
     */
    updateUserGivenName(email: string, givenName: string): Observable<User> {
        let params: STRequestParams = {
            email: email,
            givenName: givenName,
        };
        return this.httpMgr.doPost(this.serviceName, "updateUserGivenName", params).pipe(
            map(stResp => {
                return User.parse(stResp);
            })
        );
    }

    /**
     * Updates familyName of the given user. Returns the updated user.
     * @param email email of the user to update
     * @param familyName
     */
    updateUserFamilyName(email: string, familyName: string): Observable<User> {
        let params: STRequestParams = {
            email: email,
            familyName: familyName,
        };
        return this.httpMgr.doPost(this.serviceName, "updateUserFamilyName", params).pipe(
            map(stResp => {
                return User.parse(stResp);
            })
        );
    }

    /**
     * Updates givenName of the given user. Returns the updated user.
     * @param email email of the user to update
     * @param givenName
     */
    updateUserEmail(email: string, newEmail: string): Observable<User> {
        let params: STRequestParams = {
            email: email,
            newEmail: newEmail,
        };
        return this.httpMgr.doPost(this.serviceName, "updateUserEmail", params).pipe(
            map(stResp => {
                return User.parse(stResp);
            })
        );
    }

    /**
     *
     * @param email
     */
    forgotPassword(email: string) {
        let params: STRequestParams = {
            email: email,
            clientHostAddress: this.getClientHostAddress(),
            appCtx: ShowVocConstants.appCtx
        };
        return this.httpMgr.doPost(this.serviceName, "forgotPassword", params);
    }

    /**
     * @param email
     * @param token
     */
    resetPassword(email: string, token: string) {
        let params: STRequestParams = {
            email: email,
            token: token,
            appCtx: ShowVocConstants.appCtx
        };
        return this.httpMgr.doPost(this.serviceName, "resetPassword", params);
    }

    /**
     * 
     * @param email
     * @param oldPassword 
     * @param newPassword 
     */
    changePassword(email: string, oldPassword: string, newPassword: string) {
        let params: STRequestParams = {
            email: email,
            oldPassword: oldPassword,
            newPassword: newPassword
        };
        return this.httpMgr.doPost(this.serviceName, "changePassword", params);
    }

    private getClientHostAddress(): string {
        return location.protocol+"//"+location.hostname+((location.port !="") ? ":"+location.port : "")+location.pathname;
    }

}
