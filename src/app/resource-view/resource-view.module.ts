import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule, NgbPopoverModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { PreferencesModule } from '../preferences/preferences.module';
import { WidgetModule } from '../widget/widget.module';
import { ResViewSectionFilterDefaultsModalComponent } from './modals/res-view-section-filter-defaults-modal.component';
import { ResourceViewModalComponent } from './modals/resource-view-modal.component';
import { ResViewModalsServices } from './modals/resource-view-modal.service';
import { ResViewSettingsModalComponent } from './modals/resource-view-settings-modal.component';
import { ChartsRendererComponent } from './renderer/cv-renderer/charts-renderer.component';
import { CustomViewsRendererComponent } from './renderer/cv-renderer/custom-view-renderer.component';
import { MapRendererComponent } from './renderer/cv-renderer/map-renderer.component';
import { SingleValueRendererComponent } from './renderer/cv-renderer/single-value-renderer.component';
import { VectorRendererComponent } from './renderer/cv-renderer/vector-renderer.component';
import { PredicateObjectsRendererComponent } from './renderer/pred-obj-renderer.component';
import { SectionRendererComponent } from './renderer/section-renderer.component';
import { ValueRendererComponent } from './renderer/value-renderer.component';
import { ResourceViewTabComponent } from './resource-view-tab/resource-view-tab.component';
import { ResourceViewTabsetComponent } from './resource-view-tabset/resource-view-tabset.component';
import { ResourceViewComponent } from './resource-view.component';

@NgModule({
  declarations: [
    ChartsRendererComponent,
    CustomViewsRendererComponent,
    MapRendererComponent,
    SingleValueRendererComponent,
    VectorRendererComponent,
    PredicateObjectsRendererComponent,
    ResourceViewComponent,
    ResourceViewModalComponent,
    ResViewSectionFilterDefaultsModalComponent,
    ResViewSettingsModalComponent,
    SectionRendererComponent,
    ValueRendererComponent,
    ResourceViewTabComponent,
    ResourceViewTabsetComponent
  ],
  imports: [
    CommonModule,
    DragDropModule,
    FormsModule,
    NgbDropdownModule,
    NgbPopoverModule,
    NgbTooltipModule,
    PreferencesModule,
    TranslateModule,
    WidgetModule,
  ],
  exports: [
    ResourceViewComponent,
    ResourceViewTabsetComponent,
    ResViewSectionFilterDefaultsModalComponent,
  ],
  providers: [
    ResViewModalsServices
  ]
})
export class ResourceViewModule { }
