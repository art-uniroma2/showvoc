import { ChangeDetectorRef, Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { forkJoin, Observable, Subscription } from 'rxjs';
import { finalize, map } from 'rxjs/operators';
import { GraphModalServices } from '../graph/modals/graph-modal.service';
import { Project } from '../models/Project';
import { ValueFilterLanguages } from '../models/Properties';
import { AccessMethod, AnnotatedValue, IRI, LocalResourcePosition, PredicateObjects, RDFResourceRolesEnum, RemoteResourcePosition, ResAttribute, Resource, ResourcePosition, Value } from '../models/Resources';
import { PropertyFacet, ResourceViewCtx, ResViewSection, ValueClickEvent } from '../models/ResourceView';
import { SemanticTurkey } from '../models/Vocabulary';
import { ResourceViewServices } from '../services/resource-view.service';
import { ResourcesServices } from '../services/resources.service';
import { STRequestOptions } from '../utils/HttpManager';
import { ResourceDeserializer, ResourceUtils, SortAttribute } from '../utils/ResourceUtils';
import { SettingsManager } from '../utils/SettingsManager';
import { SVContext } from '../utils/SVContext';
import { ResViewModalsServices } from './modals/resource-view-modal.service';
import { MetadataRegistryServices } from '../services/metadata-registry.service';

@Component({
  selector: 'resource-view',
  templateUrl: './resource-view.component.html',
  host: { class: "vbox" },
  standalone: false
})
export class ResourceViewComponent {

  @Input() resource: Resource;
  @Input() context: ResourceViewCtx;
  @Input() project: Project;
  @Output() update: EventEmitter<AnnotatedValue<Resource>> = new EventEmitter<AnnotatedValue<Resource>>(); //(useful to notify parent tab that resource is updated)
  @Output() valueClick: EventEmitter<ValueClickEvent> = new EventEmitter<ValueClickEvent>();

  annotatedResource: AnnotatedValue<Resource>;

  resourcePosition: ResourcePosition;
  resourcePositionDetails: string;
  localCurrentProject: boolean; //in case of local resource position, tells if the resource belongs to the current project (local resource could also belong to local project which is not the current accessed)
  accessMethod: AccessMethod;

  loading: boolean = false;

  private showInferredPristine: boolean = false; //useful to decide whether repeat the getResourceView request once the includeInferred changes
  showInferred: boolean = false;

  rendering: boolean = true; //tells if the resource shown inside the sections should be rendered

  private valueFilterLangEnabled: boolean;

  unknownHost: boolean = false; //tells if the resource view of the current resource failed to be fetched due to a UnknownHostException
  unexistingResource: boolean = false; //tells if the requested resource does not exist (empty description)

  btnGraphAvailable: boolean = true;
  btnSettingsAvailable: boolean = true;

  //sections
  private resViewResponse: any = null; //to store the getResourceView response and avoid to repeat the request when user switches on/off inference
  sections: ResViewSection[] = [
    ResViewSection.types,
    ResViewSection.topconceptof,
    ResViewSection.schemes,
    ResViewSection.lexicalizations,
    ResViewSection.lexicalForms,
    ResViewSection.lexicalSenses,
    ResViewSection.classaxioms,
    ResViewSection.broaders,
    ResViewSection.superproperties,
    ResViewSection.equivalentProperties,
    ResViewSection.disjointProperties,
    ResViewSection.subPropertyChains,
    ResViewSection.subterms,
    ResViewSection.domains,
    ResViewSection.ranges,
    ResViewSection.facets,
    ResViewSection.notes,
    ResViewSection.denotations,
    ResViewSection.evokedLexicalConcepts,
    ResViewSection.constituents,
    ResViewSection.rdfsMembers,
    ResViewSection.members,
    ResViewSection.membersOrdered,
    ResViewSection.labelRelations,
    ResViewSection.formRepresentations,
    ResViewSection.imports,
    ResViewSection.properties,
  ];
  customSections: ResViewSection[];
  resViewSections: { [key: string]: PredicateObjects[] } = {};
  sectionOrder: { [key: string]: number }; //map sections->position
  private propertyFacets: PropertyFacet[] = null;

  private requestOptions: STRequestOptions;

  private pendingGetResViewSubscription: Subscription; //for unsubscribing from pending RV initialization in case input resource changes

  AccessMethod = AccessMethod;

  constructor(
    private resViewService: ResourceViewServices,
    private resourcesService: ResourcesServices,
    private metadataRegistryService: MetadataRegistryServices,
    private settingsMgr: SettingsManager,
    private resViewModals: ResViewModalsServices,
    private graphModals: GraphModalServices,
    private cdRef: ChangeDetectorRef,
  ) { }


  ngOnChanges(changes: SimpleChanges) {
    this.requestOptions = STRequestOptions.getRequestOptions(SVContext.getProject(this.project));

    this.showInferred = this.settingsMgr.getResourceViewPreferences(SVContext.getProject(this.project)).inference;
    this.rendering = this.settingsMgr.getResourceViewPreferences(SVContext.getProject(this.project)).rendering;

    //graph available if RV is not in modal and has no project overriding the default
    this.btnGraphAvailable = this.project == null && this.context != ResourceViewCtx.modal;
    //settings available only if there is no project overriding the default
    this.btnSettingsAvailable = this.project == null;

    if (changes['resource'] && changes['resource'].currentValue) {
      //if not the first change, avoid to refresh res view if resource is not changed
      if (!changes['resource'].firstChange) {
        let prevRes: Resource = changes['resource'].previousValue;
        if (prevRes.equals(this.resource)) {
          return;
        }
      }
      this.annotatedResource = new AnnotatedValue(this.resource);
      this.buildResourceView(this.resource);//refresh resource view when Input resource changes
    }
  }

  ngOnDestroy() {
    this.pendingGetResViewSubscription?.unsubscribe();
  }

  buildResourceView(res: Resource) {
    this.pendingGetResViewSubscription?.unsubscribe();

    this.showInferredPristine = this.showInferred;
    this.loading = true;

    //reset all...
    for (let s of Object.keys(this.resViewSections)) {
      this.resViewSections[s] = null;
    }
    this.resourcePosition = null;
    this.accessMethod = null;
    this.localCurrentProject = false;

    this.pendingGetResViewSubscription = this.resViewService.getResourceView(res, this.showInferred, null, this.requestOptions).pipe(
      finalize(() => {
        this.loading = false;
      })
    ).subscribe({
      next: stResp => {
        this.resViewResponse = stResp;

        this.annotatedResource = ResourceDeserializer.createResource(this.resViewResponse.resource);
        this.update.emit(this.annotatedResource);

        this.initResourcePosition();
        this.fillSections();
        this.unknownHost = false;
      },
      error: (err: Error) => {
        if (err.name.endsWith("UnknownHostException")) {
          this.unknownHost = true;
        }
      }
    });
  }

  private initResourcePosition() {
    this.resourcePosition = ResourcePosition.deserialize(this.annotatedResource.getAttribute(ResAttribute.RESOURCE_POSITION));
    this.accessMethod = this.annotatedResource.getAttribute(ResAttribute.ACCESS_METHOD);

    if (this.resourcePosition instanceof LocalResourcePosition) {
      this.localCurrentProject = this.resourcePosition.project == SVContext.getProject(this.project).getName();
      this.resourcePositionDetails = this.resourcePosition.project;
    } else if (this.resourcePosition instanceof RemoteResourcePosition) {
      this.metadataRegistryService.getDatasetMetadata(this.resourcePosition.datasetMetadata).subscribe(
        metadata => {
          if (metadata.title != null) {
            this.resourcePositionDetails = metadata.title + ", " + metadata.uriSpace;
          } else {
            this.resourcePositionDetails = metadata.uriSpace;
          }
        }
      );
    } //else is unknown => the UI gives the possibility to discover the dataset
  }

  /**
   * Fill all the sections of the RV. This not requires that the RV description is fetched again from server,
   * in fact if the user switches on/off the inference, there's no need to perform a new request.
   */
  private fillSections() {
    //list of section filtered out for the role of the current described resource
    let sectionFilter: ResViewSection[] = this.settingsMgr.getResViewSectionFilter(SVContext.getProject(this.project))[this.annotatedResource.getRole()];
    if (sectionFilter == null) {
      sectionFilter = []; //to prevent error later (in sectionFilter.indexOf(section))
    }

    //init template and custom sections (if any)
    let rvSettings = this.settingsMgr.getResourceViewProjectSettings(SVContext.getProject(this.project));
    this.sectionOrder = {};
    if (rvSettings.templates) {
      let templateForRole: ResViewSection[] = rvSettings.templates[this.annotatedResource.getRole()];
      if (templateForRole != null) {
        for (let i = 0; i < templateForRole.length; i++) {
          this.sectionOrder[templateForRole[i]] = i;
        }
      }
    }
    if (Object.keys(this.sectionOrder).length == 0) { //template not provided => init a default
      [
        ResViewSection.types, ResViewSection.classaxioms, ResViewSection.topconceptof, ResViewSection.schemes,
        ResViewSection.broaders, ResViewSection.superproperties, ResViewSection.equivalentProperties,
        ResViewSection.disjointProperties, ResViewSection.subPropertyChains, ResViewSection.constituents,
        ResViewSection.subterms, ResViewSection.domains, ResViewSection.ranges, ResViewSection.facets,
        ResViewSection.datatypeDefinitions, ResViewSection.lexicalizations, ResViewSection.lexicalForms,
        ResViewSection.lexicalSenses, ResViewSection.denotations, ResViewSection.evokedLexicalConcepts,
        ResViewSection.notes, ResViewSection.members, ResViewSection.membersOrdered, ResViewSection.labelRelations,
        ResViewSection.formRepresentations, ResViewSection.imports,
        ResViewSection.rdfsMembers, ResViewSection.properties
      ].forEach((section, idx) => {
        this.sectionOrder[section] = idx;
      });
    }
    if (rvSettings.customSections) {
      this.customSections = Object.keys(rvSettings.customSections) as ResViewSection[];
      this.customSections.forEach(section => {
        this.resViewSections[section] = this.initSection(section as ResViewSection, sectionFilter, true);
      });
    }

    this.resViewSections[ResViewSection.broaders] = this.initSection(ResViewSection.broaders, sectionFilter, true);
    this.resViewSections[ResViewSection.classaxioms] = this.initSection(ResViewSection.classaxioms, sectionFilter, true);
    this.resViewSections[ResViewSection.constituents] = this.initSection(ResViewSection.constituents, sectionFilter, false); //ordered server-side
    this.resViewSections[ResViewSection.denotations] = this.initSection(ResViewSection.denotations, sectionFilter, true);
    this.resViewSections[ResViewSection.disjointProperties] = this.initSection(ResViewSection.disjointProperties, sectionFilter, true);
    this.resViewSections[ResViewSection.domains] = this.initSection(ResViewSection.domains, sectionFilter, true);
    this.resViewSections[ResViewSection.equivalentProperties] = this.initSection(ResViewSection.equivalentProperties, sectionFilter, true);
    this.resViewSections[ResViewSection.evokedLexicalConcepts] = this.initSection(ResViewSection.evokedLexicalConcepts, sectionFilter, true);
    this.resViewSections[ResViewSection.facets] = this.initFacetsSection(ResViewSection.facets, sectionFilter, true); //dedicated initialization
    this.resViewSections[ResViewSection.formRepresentations] = this.initSection(ResViewSection.formRepresentations, sectionFilter, true);
    this.resViewSections[ResViewSection.imports] = this.initSection(ResViewSection.imports, sectionFilter, true);
    this.resViewSections[ResViewSection.labelRelations] = this.initSection(ResViewSection.labelRelations, sectionFilter, true);
    this.resViewSections[ResViewSection.lexicalizations] = this.initSection(ResViewSection.lexicalizations, sectionFilter, false); //dedicated sort
    if (this.resViewSections[ResViewSection.lexicalizations]) {
      this.sortLexicalizations(this.resViewSections[ResViewSection.lexicalizations]);
    }
    this.resViewSections[ResViewSection.lexicalForms] = this.initSection(ResViewSection.lexicalForms, sectionFilter, true);
    this.resViewSections[ResViewSection.lexicalSenses] = this.initSection(ResViewSection.lexicalSenses, sectionFilter, true);
    this.resViewSections[ResViewSection.members] = this.initSection(ResViewSection.members, sectionFilter, true);
    this.resViewSections[ResViewSection.membersOrdered] = this.initOrderedMembersSection(ResViewSection.membersOrdered, sectionFilter, true); //dedicated initialization
    this.resViewSections[ResViewSection.notes] = this.initSection(ResViewSection.notes, sectionFilter, true);
    this.resViewSections[ResViewSection.properties] = this.initSection(ResViewSection.properties, sectionFilter, true);
    this.resViewSections[ResViewSection.ranges] = this.initSection(ResViewSection.ranges, sectionFilter, true);
    this.resViewSections[ResViewSection.rdfsMembers] = this.initSection(ResViewSection.rdfsMembers, sectionFilter, true); //ordered server-side
    this.resViewSections[ResViewSection.schemes] = this.initSection(ResViewSection.schemes, sectionFilter, true);
    this.resViewSections[ResViewSection.subPropertyChains] = this.initSection(ResViewSection.subPropertyChains, sectionFilter, true);
    this.resViewSections[ResViewSection.subterms] = this.initSection(ResViewSection.subterms, sectionFilter, true);
    this.resViewSections[ResViewSection.superproperties] = this.initSection(ResViewSection.superproperties, sectionFilter, true);
    this.resViewSections[ResViewSection.topconceptof] = this.initSection(ResViewSection.topconceptof, sectionFilter, true);
    this.resViewSections[ResViewSection.types] = this.initSection(ResViewSection.types, sectionFilter, true);

    this.resolveForeignURI();

    if (
      this.annotatedResource.getRole() == RDFResourceRolesEnum.mention &&
      //these sections are always returned, even when resource is not defined, so I need to check also if length == 0
      (!this.resViewResponse[ResViewSection.lexicalizations] || this.resViewResponse[ResViewSection.lexicalizations].length == 0) &&
      (!this.resViewResponse[ResViewSection.properties] || this.resViewResponse[ResViewSection.properties].length == 0) &&
      (!this.resViewResponse[ResViewSection.types] || this.resViewResponse[ResViewSection.types].length == 0) &&
      //sections optional
      !this.resViewResponse[ResViewSection.broaders] &&
      !this.resViewResponse[ResViewSection.classaxioms] &&
      !this.resViewResponse[ResViewSection.constituents] &&
      !this.resViewResponse[ResViewSection.datatypeDefinitions] &&
      !this.resViewResponse[ResViewSection.denotations] &&
      !this.resViewResponse[ResViewSection.disjointProperties] &&
      !this.resViewResponse[ResViewSection.domains] &&
      !this.resViewResponse[ResViewSection.equivalentProperties] &&
      !this.resViewResponse[ResViewSection.evokedLexicalConcepts] &&
      !this.resViewResponse[ResViewSection.facets] &&
      !this.resViewResponse[ResViewSection.formRepresentations] &&
      !this.resViewResponse[ResViewSection.imports] &&
      !this.resViewResponse[ResViewSection.labelRelations] &&
      !this.resViewResponse[ResViewSection.lexicalForms] &&
      !this.resViewResponse[ResViewSection.lexicalSenses] &&
      !this.resViewResponse[ResViewSection.members] &&
      !this.resViewResponse[ResViewSection.membersOrdered] &&
      !this.resViewResponse[ResViewSection.notes] &&
      !this.resViewResponse[ResViewSection.ranges] &&
      !this.resViewResponse[ResViewSection.rdfsMembers] &&
      !this.resViewResponse[ResViewSection.subPropertyChains] &&
      !this.resViewResponse[ResViewSection.schemes] &&
      !this.resViewResponse[ResViewSection.subterms] &&
      !this.resViewResponse[ResViewSection.superproperties] &&
      !this.resViewResponse[ResViewSection.topconceptof]
    ) {
      this.unexistingResource = true;
    } else {
      this.unexistingResource = false;
    }
  }


  private initSection(section: ResViewSection, sectionFilter: ResViewSection[], sort: boolean): PredicateObjects[] {
    let poList: PredicateObjects[];
    let sectionJson: any = this.resViewResponse[section];
    if (sectionJson != null) {
      if (Array.isArray(sectionJson) && sectionFilter.indexOf(section) == -1) {
        poList = ResourceDeserializer.createPredicateObjectsList(sectionJson);
        this.filterPredObjList(poList);
        if (sort) {
          this.sortObjects(poList);
        }
      }
    }
    return poList;
  }


  /**
   * Facets section has a structure different from the other (object list and predicate-object list),
   * so it requires a parser ad hoc (doesn't use the parsers in ResourceDeserializer)
   */
  private initFacetsSection(section: ResViewSection, sectionFilter: ResViewSection[], sort: boolean): PredicateObjects[] {
    let poList: PredicateObjects[];
    let facetsSectionJson: any = this.resViewResponse[section];
    if (facetsSectionJson != null) {
      this.propertyFacets = [];
      for (let facetName in facetsSectionJson) {
        if (facetName == "inverseOf") continue;
        this.propertyFacets.push({
          name: facetName,
          value: facetsSectionJson[facetName].value,
          explicit: facetsSectionJson[facetName].explicit
        });
      }
      //parse inverseOf section in facets
      poList = ResourceDeserializer.createPredicateObjectsList(facetsSectionJson.inverseOf);
      this.filterPredObjList(poList);
      if (sort) {
        this.sortObjects(poList);
      }
    }
    return poList;
  }

  private initOrderedMembersSection(section: ResViewSection, sectionFilter: ResViewSection[], sort: boolean): PredicateObjects[] {
    let poList: PredicateObjects[];
    let sectionJson: any = this.resViewResponse[section];
    if (sectionJson != null && sectionFilter.indexOf(section) == -1) {
      poList = ResourceDeserializer.createPredicateObjectsList(sectionJson);
      //response doesn't declare the "explicit" for the collection members, set the attribute based on the explicit of the collection
      for (const po of poList) { //for each pred-obj-list
        let collections = po.getObjects();
        for (const coll of collections) { //for each collection (member list, should be just 1)
          if (coll.getAttribute(ResAttribute.EXPLICIT)) { //set member explicit only if collection is explicit
            let members: AnnotatedValue<Resource>[] = coll.getAttribute(ResAttribute.MEMBERS);
            for (const m of members) {
              m.setAttribute(ResAttribute.EXPLICIT, true);
            }
          }
        }
      }
      this.filterPredObjList(poList);
      if (sort) {
        this.sortObjects(poList);
      }
    }
    return poList;
  }

  private filterPredObjList(predObjList: PredicateObjects[]) {
    this.filterInferredFromPredObjList(predObjList);
    this.filterValueLanguageFromPrefObjList(predObjList);
  }

  /**
   * Based on the showInferred param, filter out or let pass inferred information in a predicate-objects list
   */
  private filterInferredFromPredObjList(predObjList: PredicateObjects[]) {
    if (!this.showInferred) {
      for (let i = 0; i < predObjList.length; i++) {
        let objList: AnnotatedValue<Value>[] = predObjList[i].getObjects();
        for (let j = 0; j < objList.length; j++) {
          let objGraphs: IRI[] = objList[j].getTripleGraphs();
          if (objGraphs.some(g => g.getIRI() == SemanticTurkey.inferenceGraph)) {
            objList.splice(j, 1);
            j--;
          }
        }
        //after filtering the objects list, if the predicate has no more objects, remove it from predObjList
        if (objList.length == 0) {
          predObjList.splice(i, 1);
          i--;
        }
      }
    }
  }

  private filterValueLanguageFromPrefObjList(predObjList: PredicateObjects[]) {
    //even if already initialized, get each time the value of valueFilterLangEnabled in order to detect eventual changes of the pref
    let valueFilterLanguage: ValueFilterLanguages = this.settingsMgr.getValueFilterLanguages(SVContext.getProject(this.project));
    this.valueFilterLangEnabled = valueFilterLanguage.enabled;
    if (this.valueFilterLangEnabled) {
      let valueFilterLanguages = valueFilterLanguage.languages;
      if (valueFilterLanguages != null && valueFilterLanguages.length != 0) {
        for (let i = 0; i < predObjList.length; i++) {
          let objList: AnnotatedValue<Value>[] = predObjList[i].getObjects();
          for (let j = 0; j < objList.length; j++) {
            let lang: string = objList[j].getLanguage();
            //remove the object if it has a language not in the languages list of the filter
            if (lang != null && valueFilterLanguages.indexOf(lang) == -1) {
              objList.splice(j, 1);
              j--;
            }
          }
          //after filtering the objects list, if the predicate has no more objects, remove it from predObjList
          if (objList.length == 0) {
            predObjList.splice(i, 1);
            i--;
          }
        }
      }
    }
  }

  private sortObjects(predObjList: PredicateObjects[]) {
    //sort by show if rendering is active, uri otherwise
    let orderAttribute: SortAttribute = this.rendering ? SortAttribute.show : SortAttribute.value;
    for (const po of predObjList) {
      let objList: AnnotatedValue<Value>[] = po.getObjects();
      ResourceUtils.sortResources(objList, orderAttribute);
    }
  }

  private sortLexicalizations(predObjList: PredicateObjects[]) {
    for (const po of predObjList) {
      let objList: AnnotatedValue<Value>[] = po.getObjects();
      if (this.rendering) {
        objList.sort((o1: AnnotatedValue<Value>, o2: AnnotatedValue<Value>) => {
          if (o1.getLanguage() < o2.getLanguage()) return -1;
          if (o1.getLanguage() > o2.getLanguage()) return 1;
          //same lang code, order alphabetically
          return o1.getShow().localeCompare(o2.getShow());
        });
      } else { //in case the rendering is off, sort according the value
        ResourceUtils.sortResources(objList, SortAttribute.value);
      }
    }
  }

  /**
   * Resolves the foreign IRIs of all sections.
   * This method iterates over the values (objects in the sections' poList) and in case there are foreign IRIs (IRI with role mention),
   * tries to resolve them. The foreign IRIs could represent:
   * - resources in local projects (position local)
   * - resources in remote projects (position remote; currently left unresolved)
   * @param pol 
   */
  private resolveForeignURI() {

    let pol: PredicateObjects[] = [];
    for (let section in this.resViewSections) {
      let predObjList = this.resViewSections[section];
      if (predObjList && predObjList.length > 0) {
        predObjList.forEach(po => pol.push(po));
      }
    }

    //collect foreign IRIs, namely IRI values with role mention
    let foreignResources: IRI[] = [];
    pol.forEach(po => {
      po.getObjects().forEach(o => {
        if (o.getValue() instanceof IRI && o.getRole() == RDFResourceRolesEnum.mention) { //if it is a URI mention
          foreignResources.push(o.getValue() as IRI);
        }
      });
    });
    if (foreignResources.length > 0) {
      //retrieve the position of the foreign resources
      this.resourcesService.getResourcesPosition(foreignResources, this.requestOptions).subscribe(
        positionMap => {
          //collect the foreign resources grouped by project
          let localProjectResourcesMap: { [projectName: string]: string[] } = {}; //for each local project -> list of IRI to resolve
          for (let resIri in positionMap) {
            let position = positionMap[resIri];
            if (position.isLocal()) {
              let projectName = (position as LocalResourcePosition).project;
              if (localProjectResourcesMap[projectName] == null) {
                localProjectResourcesMap[projectName] = [resIri];
              } else {
                localProjectResourcesMap[projectName].push(resIri);
              }
            } else if (position.isRemote()) {
              //at the moment nothing to do
            }
          }
          /**
           * Resolves the local project foreign URIs
           */
          //prepare the service invocations for resolving the foreign URIs. One invocation for each project
          let resolveResourcesFn: Observable<void>[] = [];
          let resolvedResources: AnnotatedValue<IRI>[] = [];
          for (let projectName in localProjectResourcesMap) {
            let resources: IRI[] = localProjectResourcesMap[projectName].map(r => new IRI(r));
            /*
            set the rendering languages of the current project as ctxLangs, even if it is accessing an external project.
            This is done to avoid initializing the settings for each project the resolving resources belong to.
            */
            let ctxLangs = this.settingsMgr.getRenderingLanguages(SVContext.getProject(this.project));
            let opt: STRequestOptions = new STRequestOptions({
              ctxProject: new Project(projectName),
              ctxLangs: ctxLangs ? ctxLangs : []
            });
            resolveResourcesFn.push(
              this.resourcesService.getResourcesInfo(resources, opt).pipe(
                map(resources => {
                  resolvedResources = resolvedResources.concat(resources as AnnotatedValue<IRI>[]);
                })
              )
            );
          }
          //invoke all the services, then (the resolvedResources array is filled) search and replace the values in the poList
          forkJoin(resolveResourcesFn).subscribe(
            () => {
              pol.forEach(po => {
                po.getObjects().forEach((o: AnnotatedValue<Value>, index: number, array: AnnotatedValue<Value>[]) => {
                  if (o.getValue() instanceof IRI && o.getRole() == RDFResourceRolesEnum.mention) {
                    let resolved: AnnotatedValue<IRI> = resolvedResources.find(r => r.getValue().equals(o.getValue())); //search the values among the resolved ones
                    if (resolved != null) { //if found, replace the value
                      array.splice(index, 1);
                      this.cdRef.detectChanges(); //needed for triggering the ngOnChanges in the value-renderer component
                      array.splice(index, 0, resolved);
                    }
                  }
                });
              });
            }
          );
        }
      );
    }
  }

  /**
   * HEADING BUTTON HANDLERS
   */

  switchInferred() {
    this.showInferred = !this.showInferred;

    let rvPrefs = this.settingsMgr.getResourceViewPreferences(SVContext.getProject(this.project));
    rvPrefs.inference = this.showInferred;
    this.settingsMgr.setResourceViewPreferences(SVContext.getProject(this.project), rvPrefs).subscribe();

    if (!this.showInferredPristine) { //resource view has been initialized with showInferred to false, so repeat the request
      this.buildResourceView(this.resource);
    } else { //resource view has been initialized with showInferred to true, so there's no need to repeat the request
      this.loading = true;
      this.fillSections();
      this.loading = false;
    }
  }

  switchRendering() {
    this.rendering = !this.rendering;

    let rvPrefs = this.settingsMgr.getResourceViewPreferences(SVContext.getProject(this.project));
    rvPrefs.rendering = this.rendering;
    this.settingsMgr.setResourceViewPreferences(SVContext.getProject(this.project), rvPrefs).subscribe();
  }

  settings() {
    this.resViewModals.openSettings().then(
      () => { //lang-value filter updates in real time the preference => refresh res-view in both cases, settings dialog closed or dismissed
        this.buildResourceView(this.resource);
      },
      () => {
        this.buildResourceView(this.resource);
      },
    );
  }

  openInDataGraph() {
    this.graphModals.openDataGraph(this.annotatedResource, this.rendering);
  }

  openInModelGraph() {
    if (this.annotatedResource.getValue().isIRI()) {
      this.graphModals.openModelGraph(this.annotatedResource.asAnnotatedIRI(), this.rendering);
    }
  }

  isShowGraphAvailable(): boolean {
    return this.context != ResourceViewCtx.modal; //graph not available in modal
  }

  discoverDataset() {
    this.metadataRegistryService.discoverDataset(this.resource.asIRI()).subscribe(
      () => {
        this.buildResourceView(this.resource);
      }
    );
  }

  /**
   * EVENT HANDLER
   */

  onValueClick(event: ValueClickEvent) {
    this.valueClick.emit(event);
  }

}
