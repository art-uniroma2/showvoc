import { Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CustomViewsServices } from 'src/app/custom-views/custom-views.services';
import { CustomViewData } from 'src/app/custom-views/CustomViews';
import { Project } from 'src/app/models/Project';
import { PredicateLabelSettings } from 'src/app/models/Properties';
import { AnnotatedValue, IRI, PredicateObjects, ResAttribute, Resource, Value } from 'src/app/models/Resources';
import { ResViewSection, ValueClickEvent } from 'src/app/models/ResourceView';
import { SKOS, SKOSXL } from 'src/app/models/Vocabulary';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';

@Component({
  selector: 'pred-obj-renderer',
  templateUrl: './pred-obj-renderer.component.html',
  standalone: false
})
export class PredicateObjectsRendererComponent {

  @Input() project: Project; //when opening RV of an external project
  @Input() section: ResViewSection;
  @Input() resource: Resource;
  @Input() predObjects: PredicateObjects;
  @Input() rendering: boolean = true; //if true the resource should be rendered with the show, with the qname otherwise

  @Output() valueClick: EventEmitter<ValueClickEvent> = new EventEmitter<ValueClickEvent>();

  predicate: AnnotatedValue<IRI>;
  predicateLabel: string;

  objects: AnnotatedValue<Value>[];

  showCustomView: boolean = true;
  customView: CustomViewData;

  constructor(
    private settingsMgr: SettingsManager,
    private cvService: CustomViewsServices,
    private translate: TranslateService
  ) {
    this.translate.onLangChange.subscribe(() => {
      this.initPredicateLabel();
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['predObjects']) {
      this.predicate = this.predObjects.getPredicate();
      this.initPredicateLabel();

      // this.objectsBackup = this.predObjects.getObjects().slice();
      this.objects = this.predObjects.getObjects();

      if (this.predicate.getAttribute(ResAttribute.CUSTOM_VIEW_MODEL) != null) { //predicate has a CV
        this.cvService.getViewData(this.resource, this.predicate.getValue()).subscribe( //retrieve data
          (cvData: CustomViewData) => {
            if (cvData.data.length > 0) { //if data is retrieved correctly, store it and remove the pred-obj
              this.customView = cvData;
              // here I need to filter out from objects those values covered by any CustomView
              this.objects = this.objects.filter(o => //keep object if
                !this.customView.data.some(d => //there is no CV that...
                  d.resource.equals(o.getValue()) //describe such object
                )
              );
            }
          }
        );
      }
    }
  }

  private initPredicateLabel() {
    let predicateIRI: IRI = this.predicate.getValue();

    let predLabelSettings: PredicateLabelSettings = this.settingsMgr.getResourceViewPredLabelSettings(SVContext.getProject(this.project));
    if (predLabelSettings && predLabelSettings.enabled && predLabelSettings.mappings[this.translate.currentLang]) {
      //settings defined, enabled and available in the active lang
      this.predicateLabel = predLabelSettings.mappings[this.translate.currentLang][predicateIRI.getIRI()];
    }

    if (!this.predicateLabel) {
      //special cases
      if (predicateIRI.equals(SKOS.prefLabel) || predicateIRI.equals(SKOSXL.prefLabel)) {
        this.predicateLabel = "Preferred Label";
      } else if (predicateIRI.equals(SKOS.altLabel) || predicateIRI.equals(SKOSXL.altLabel)) {
        this.predicateLabel = "Alternative Label";
      } else if (predicateIRI.equals(SKOS.hiddenLabel) || predicateIRI.equals(SKOSXL.hiddenLabel)) {
        this.predicateLabel = "Hidden Label";
      }
    }
    if (!this.predicateLabel) {
      //generic case: split the camel case local name of the predicate
      let predLocalName = predicateIRI.getLocalName();
      let splitted: string = predLocalName.replace(/([a-z])([A-Z])/g, '$1 $2');
      this.predicateLabel = splitted.charAt(0).toLocaleUpperCase() + splitted.slice(1); //upper case the first letter
      // label = splitted;
    }
  }

  onClick(obj: AnnotatedValue<Value>, event: MouseEvent) {
    event.stopPropagation();
    if (obj.getValue() instanceof Resource) {
      this.valueClick.emit(new ValueClickEvent(obj, event));
    }
  }

  onDblClick(obj: AnnotatedValue<Value>) {
    if (obj.getValue() instanceof Resource) {
      this.valueClick.emit(new ValueClickEvent(obj, null, true));
    }
  }

  onValueClick(event: ValueClickEvent) {
    this.valueClick.emit(event);
  }

}