import { Component, EventEmitter, HostBinding, Input, Output, SimpleChanges } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { Project } from 'src/app/models/Project';
import { ClickableValueStyle, ResourceViewPreference } from 'src/app/models/Properties';
import { AnnotatedValue, IRI, Literal, RDFResourceRolesEnum, ResAttribute, Resource, TripleScopes, Value } from 'src/app/models/Resources';
import { ResViewUtils, ValueClickEvent } from 'src/app/models/ResourceView';
import { RDF } from 'src/app/models/Vocabulary';
import { ResourceUtils } from 'src/app/utils/ResourceUtils';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';

@Component({
  selector: 'value-renderer',
  templateUrl: './value-renderer.component.html',
  standalone: false
})
export class ValueRendererComponent {

  @Input() project: Project; //when opening RV of an external project
  @Input() value: AnnotatedValue<Value>;
  @Input() rendering: boolean = true; //if true the resource should be rendered with the show, with the qname otherwise

  @Output() valueClick: EventEmitter<ValueClickEvent> = new EventEmitter<ValueClickEvent>();

  @HostBinding("class.imported") importedClass: boolean = false;
  @HostBinding("class.inferred") inferredClass: boolean = false;
  @HostBinding("class.clickable") clickableClass: boolean = false;

  renderedValue: string;

  lang: string; //language of the resource
  showDatatypeBadge: boolean;
  datatype: AnnotatedValue<IRI>; //datatype of the resource

  collMembers: AnnotatedValue<Value>[]; //in ordered collection members, member list value has the AnnotatedValue members as attribute 

  graphTitle: string;
  valueTooltip: string;

  rvPref: ResourceViewPreference;
  clickableStyle: ClickableValueStyle;

  renderMode: RenderMode;
  safeHtml: SafeHtml; //html content in case literal with rdf:HTML datatype
  splittedLiteral: string[]; //in case of literal containing a link, even elements are plain text, odd elements are url

  constructor(private settingsMgr: SettingsManager, private sanitizer: DomSanitizer, private translateService: TranslateService) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['value'] && changes['value'].currentValue) {
      this.init();
    } else if (changes['rendering']) {
      this.initRenderedValue();
    }
  }

  private init() {
    this.rvPref = this.settingsMgr.getResourceViewPreferences(SVContext.getProject(this.project));

    this.valueTooltip = this.value.getValue().toNT();

    this.initRenderedValue();
    this.initLang();
    this.initMembersColl();
    this.initDatatype();

    //init classes for coloring the value according the scope of the triple imported/inferred
    this.importedClass = this.value.getAttribute(ResAttribute.TRIPLE_SCOPE) == TripleScopes.imported;
    this.inferredClass = this.value.getAttribute(ResAttribute.TRIPLE_SCOPE) == TripleScopes.inferred;
    this.clickableClass = this.value.getValue() instanceof Resource;

    this.clickableStyle = this.settingsMgr.getClickableValueStyle();

    if (this.value.getValue() instanceof Resource) {
      this.renderMode = RenderMode.plainResource; //default mode for resource
      if (this.clickableStyle.widget) {
        this.renderMode = RenderMode.widget;
      } else if (this.value.getValue().isBNode() && this.value.getAttribute(ResAttribute.SHOW_INTERPR) != null) {
        this.renderMode = RenderMode.manchester;
      }
    } else { //Literal
      this.renderMode = RenderMode.plainLiteral; //default mode for literal
      if (this.datatype != null && this.datatype.getValue().equals(RDF.html)) {
        this.safeHtml = this.sanitizer.bypassSecurityTrustHtml((this.value.getValue() as Literal).getLabel());
        this.renderMode = RenderMode.html;
      } else {
        let literalWithLinkInfo = ResViewUtils.getLiteralWithLinkInfo(this.value.getValue());
        if (literalWithLinkInfo.literalWithLink) {
          this.renderMode = RenderMode.literalWithLink;
          this.splittedLiteral = literalWithLinkInfo.splittedLiteral;
        }
      }
    }


    //init the tooltip for the triple graphs
    let graphs: IRI[] = this.value.getTripleGraphs();
    if (graphs.length == 1) {
      this.graphTitle = "Graph: " + graphs[0].getIRI();
    } else if (graphs.length > 1) {
      this.graphTitle = "Graphs:";
      graphs.forEach(g => {
        this.graphTitle += "\n" + g.getIRI();
      });
    }
  }

  private initLang() {
    let hideLang: boolean = this.settingsMgr.getResourceViewHideLiteral(SVContext.getProject(this.project));
    if (!hideLang) {
      this.lang = this.value.getLanguage();
    }
  }

  private initRenderedValue() {
    this.renderedValue = ResourceUtils.getRendering(this.value, this.rendering);
  }

  private initMembersColl() {
    this.collMembers = this.value.getAttribute("members");
    if (this.collMembers) {
      this.collMembers.forEach(c => {
        if (c.getValue() instanceof Resource) {
          c['clickable'] = true;
        }
      });
    }
  }

  private initDatatype() {
    this.showDatatypeBadge = this.rvPref.showDatatypeBadge;
    this.datatype = null; //reset
    let dtIri: IRI;
    if (this.value.getValue() instanceof Literal) { // if it is a literal
      dtIri = (this.value.getValue() as Literal).getDatatype();
    } else { // otherwise, it is a resource, possibly with an additional property dataType (as it could be from a custom form preview)
      dtIri = this.value.getAttribute(ResAttribute.DATA_TYPE);
    }

    if (dtIri != null) { //if there is a datatype
      let show: string = ResourceUtils.getQName(dtIri.getIRI(), SVContext.getPrefixMappings());
      this.datatype = new AnnotatedValue(dtIri, { [ResAttribute.SHOW]: show, [ResAttribute.ROLE]: RDFResourceRolesEnum.dataRange });
    }
  }

  onMemberDblClick(member: AnnotatedValue<Value>, event: Event) {
    event.stopPropagation();
    if (member.getValue() instanceof Resource) {
      this.valueClick.emit(new ValueClickEvent(member, null, true));
    }
  }

  onMemberClick(obj: AnnotatedValue<Value>, event: MouseEvent) {
    event.stopPropagation();
    if (obj.getValue() instanceof Resource) {
      this.valueClick.emit(new ValueClickEvent(obj, event));
    }
  }

}


enum RenderMode {
  html = "html",
  literalWithLink = "literalWithLink",
  manchester = "manchester",
  plainResource = "plainResource",
  plainLiteral = "plainLiteral",
  widget = "widget",
}

