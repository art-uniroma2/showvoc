import { Directive, Input } from "@angular/core";
import { AbstractView } from "src/app/custom-views/CustomViews";
import { AnnotatedValue, Value } from "src/app/models/Resources";
import { ValueClickEvent } from "src/app/models/ResourceView";
import { AbstractViewRendererComponent } from "./abstract-view-renderer";

@Directive()
export abstract class AbstractSingleViewRendererComponent extends AbstractViewRendererComponent {

  @Input() view: AbstractView;

  isInferred: boolean = false;

  /**
   * Emit a doubleClick event on the resource described in the CV (unless otherwise specified, e.g. a single data of a chart)
   * @param res 
   */
  onDoubleClick(res?: AnnotatedValue<Value>) {
    if (res == null) {
      res = this.view.resource;
    }
    this.valueClick.emit(new ValueClickEvent(res, null, true));
  }

  onClick(event: MouseEvent) {
    this.valueClick.emit(new ValueClickEvent(this.view.resource, event));
  }

}