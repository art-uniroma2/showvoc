import { Directive, EventEmitter, Input, Output } from "@angular/core";
import { ResViewSection, ValueClickEvent } from "src/app/models/ResourceView";

@Directive()
export abstract class AbstractViewRendererComponent {

  @Input() section: ResViewSection;
  @Input() rendering: boolean;

  @Output() valueClick: EventEmitter<ValueClickEvent> = new EventEmitter<ValueClickEvent>();

  constructor() { }

  /**
   * normalizes the input data in a format compliant for the view renderer
   */
  protected abstract processInput(): void;

}