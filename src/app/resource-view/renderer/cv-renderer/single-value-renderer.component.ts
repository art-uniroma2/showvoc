import { Component, Input } from "@angular/core";
import { AbstractSingleValueView } from "src/app/custom-views/CustomViews";
import { AnnotatedValue, IRI } from "src/app/models/Resources";
import { ResourcesServices } from "src/app/services/resources.service";
import { AbstractSingleViewRendererComponent } from "./abstract-single-view-renderer";

@Component({
  selector: "single-value-renderer",
  templateUrl: "./single-value-renderer.component.html",
  styles: [`
    :host {
        display: block;
    }
    .multivalue {
        outline: 1px solid #eee;
        border-radius: 5px;
        margin-right: 5px;
    }
    `],
  standalone: false
})
export class SingleValueRendererComponent extends AbstractSingleViewRendererComponent {

  @Input() view: AbstractSingleValueView;


  constructor(private resourceService: ResourcesServices) {
    super();
  }

  ngOnInit() {
    this.view.values.forEach(v => {
      if (v.resource.getValue() instanceof IRI) {
        //getResourcesInfo instead of getResourceDescription since the latter requires that the provided resource is locally defined (so prevent error in case of external reference)
        this.resourceService.getResourcesInfo([v.resource.getValue() as IRI]).subscribe(
          (annRes: AnnotatedValue<IRI>[]) => {
            v.resource = annRes.find(r => r.getValue().equals(v.resource.getValue()));
          }
        );
      }
    });
  }

  protected processInput(): void {
    //Nothing to do
  }


}