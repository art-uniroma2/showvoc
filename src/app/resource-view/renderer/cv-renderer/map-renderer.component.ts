import { Component, Input, SimpleChanges } from "@angular/core";
import { AbstractSparqlBasedView, AreaView, PointView, RouteView } from "src/app/custom-views/CustomViews";
import { GeoPoint } from "src/app/widget/leaflet-map/leaflet-map.component";
import { AbstractSingleViewRendererComponent } from "./abstract-single-view-renderer";

@Component({
  selector: "map-renderer",
  templateUrl: "./map-renderer.component.html",
  host: { class: "hbox" },
  styles: [`
      :host {
          height: 300px;
          width: 100%;
          padding: 3px;
      }
  `],
  standalone: false
})
export class MapRendererComponent extends AbstractSingleViewRendererComponent {

  @Input() view: AbstractSparqlBasedView;

  //input data needs converted in point or set of points
  point: GeoPoint; //a point to be represented (with a marker) on the map
  route: GeoPoint[]; //a set of points to be represented (with a polyline) on the map
  area: GeoPoint[];

  constructor() {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['view']) {
      this.processInput();
    }
  }

  processInput() {
    /**
     * here I need to detect if I need to draw a point or a polyline (route/area)
     * the fact that the data types are compliant with the map view is already granted from the parent component
     */
    if (this.view instanceof PointView) {
      this.point = {
        location: this.view.location,
        lat: Number.parseFloat(this.view.latitude.getLabel()),
        lng: Number.parseFloat(this.view.longitude.getLabel())
      };
    } else if (this.view instanceof RouteView) {
      this.route = this.view.locations.map(l => {
        return {
          location: l.location,
          lat: Number.parseFloat(l.latitude.getLabel()),
          lng: Number.parseFloat(l.longitude.getLabel())
        };
      });
    } else if (this.view instanceof AreaView) {
      this.area = this.view.locations.map(l => {
        return {
          location: l.location,
          lat: Number.parseFloat(l.latitude.getLabel()),
          lng: Number.parseFloat(l.longitude.getLabel())
        };
      });
    }
  }

}