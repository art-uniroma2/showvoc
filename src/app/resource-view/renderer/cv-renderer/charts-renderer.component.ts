import { ChangeDetectorRef, Component, Input } from "@angular/core";
import { AbstractSparqlBasedView, SeriesCollectionView, SeriesView, ViewsEnum } from "src/app/custom-views/CustomViews";
import { ChartData, ChartSeries } from "src/app/widget/charts/NgxChartsUtils";
import { AbstractSingleViewRendererComponent } from "./abstract-single-view-renderer";

@Component({
  selector: "charts-renderer",
  templateUrl: "./charts-renderer.component.html",
  host: { class: "hbox" },
  styles: [`
      :host {
          height: 300px;
          width: 100%;
          padding: 3px;
      }
      :host:hover {
          background-color: #e5f3ff;
      }
  `],
  standalone: false
})
export class ChartsRendererComponent extends AbstractSingleViewRendererComponent {

  @Input() view: AbstractSparqlBasedView;

  //input data needs to be converted in data compliant with charts
  series: ChartData[]; //a series of chart data 
  seriesCollection: ChartSeries[];

  compliantViews: ViewsEnum[];
  activeView: ViewsEnum; //currently selected/rendered view

  //input of bar chart
  xAxisLabel: string;
  yAxisLabel: string;

  viewInitialized: boolean;

  constructor(private changeDetectorRef: ChangeDetectorRef) {
    super();
  }

  ngOnInit() {
    this.compliantViews = [];
    if (this.view instanceof SeriesView) {
      this.compliantViews = [ViewsEnum.bar, ViewsEnum.pie];
    } else if (this.view instanceof SeriesCollectionView) {
      this.compliantViews = [ViewsEnum.line];
    }
    if (this.compliantViews.length > 0) {
      if (this.compliantViews.includes(this.view.defaultView)) {
        this.activeView = this.view.defaultView;
      } else {
        this.activeView = this.compliantViews[0];
      }
    }

    this.processInput();
  }

  ngAfterViewInit() {
    this.changeDetectorRef.detectChanges();
    this.viewInitialized = true;
  }

  processInput() {
    if (this.view instanceof SeriesView) {
      this.xAxisLabel = this.view.series_label;
      this.yAxisLabel = this.view.value_label;
      //convert to ChartData[]
      this.series = this.view.data.map(d => {
        let cd: ChartData = {
          name: d.name.stringValue(),
          value: Number.parseFloat(d.value.getLabel()),
          extra: {
            valueDatatype: d.value.getDatatype().getIRI(),
            nameResource: d.name
          }
        };
        return cd;
      });
    } else if (this.view instanceof SeriesCollectionView) {
      this.xAxisLabel = this.view.series_label;
      this.yAxisLabel = this.view.value_label;
      this.seriesCollection = [];
      //convert to ChartSeries[]
      this.view.series.forEach(s => {
        let series = s.data.map(d => {
          let cd: ChartData = {
            name: d.name.stringValue(),
            value: Number.parseFloat(d.value.getLabel()),
            extra: {
              valueDatatype: d.value.getDatatype().getIRI(),
              nameResource: d.name
            }
          };
          return cd;
        });
        let chartSeries: ChartSeries = {
          name: s.series_name.stringValue(),
          series: series,
        };
        this.seriesCollection.push(chartSeries);
      });
    }
  }

  onWidgetChanged() {
    //hack to make chart initialized well into the container
    this.viewInitialized = false;
    this.changeDetectorRef.detectChanges();
    this.viewInitialized = true;
  }

}