import { Component, Input } from "@angular/core";
import { AbstractVectorView, CustomViewRenderedValue } from "src/app/custom-views/CustomViews";
import { Resource } from "src/app/models/Resources";
import { ResourcesServices } from "src/app/services/resources.service";
import { NTriplesUtil } from "src/app/utils/ResourceUtils";
import { AbstractViewRendererComponent } from "./abstract-view-renderer";
import { ValueClickEvent } from "src/app/models/ResourceView";

@Component({
  selector: "vector-renderer",
  templateUrl: "./vector-renderer.component.html",
  styles: [`
      .cv-table {
          border: 1px solid #ddd;
          border-radius: 3px;
          padding: 4px;
      }
      .cv-table th { border-top: 0px }
      .cv-table tr { height: 36px }
      ::ng-deep .renderingText { word-break: break-word } 
  `],
  standalone: false
})
export class VectorRendererComponent extends AbstractViewRendererComponent {

  @Input() views: AbstractVectorView[];

  headers: string[];

  constructor(private resourcesService: ResourcesServices) {
    super();
  }

  ngOnInit() {
    this.headers = this.views[0].values.map(v => v.field);

    let resToAnnotate: Resource[] = [];
    this.views.forEach(view => {
      view.values.forEach(v => {
        if (v.resource == null) return;
        if (v.resource.getValue() instanceof Resource && !resToAnnotate.some(r => r.equals(v.resource.getValue()))) {
          resToAnnotate.push(v.resource.getValue());
        }
      });
    });

    //headers might be both human readable string (if label is provided in dynamic vector) or a NT-serialized propery, in this case, annotate them as well
    this.headers.forEach(h => {
      try {
        let hIri = NTriplesUtil.parseIRI(h);
        resToAnnotate.push(hIri);
      } catch {
        //just ignore
      }
    });

    if (resToAnnotate.length > 0) {
      this.resourcesService.getResourcesInfo(resToAnnotate).subscribe(
        annValues => {
          this.headers.forEach((h, i, self) => {
            let annotated = annValues.find(a => a.getValue().toNT() == h);
            if (annotated != null) {
              self[i] = annotated.getShow();
            }
          });
          this.views.forEach(view => {
            view.values.forEach((v, i, self) => {
              if (v.resource == null) return;
              let annotated = annValues.find(a => a.equals(v.resource));
              if (annotated != null) {
                self[i].resource = annotated;
              }
            });
          });
        }
      );
    }
  }

  processInput() {
    //Nothing to do
  }

  openResource(view: AbstractVectorView, event: MouseEvent) {
    this.valueClick.emit(new ValueClickEvent(view.resource, event, true));
  }

  onValueDoubleClick(value: CustomViewRenderedValue) {
    if (value.resource.getValue().isResource()) {
      this.valueClick.emit(new ValueClickEvent(value.resource, null, true));
    }
  }

  onValueClick(value: CustomViewRenderedValue, event: MouseEvent) {
    if (value.resource.getValue().isResource()) {
      this.valueClick.emit(new ValueClickEvent(value.resource, event));
    }
  }

}