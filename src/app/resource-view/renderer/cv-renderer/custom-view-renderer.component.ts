import { Component, EventEmitter, Input, Output, SimpleChanges } from "@angular/core";
import { AbstractView, AdvSingleValueView, AreaView, BindingMapping, CustomViewCategory, CustomViewData, CustomViewModel, CustomViewObjectDescription, CustomViewRenderedValue, CustomViewVariables, DynamicVectorView, PointView, PropertyChainView, RouteView, SeriesCollectionView, SeriesView, SparqlBasedValueDTO, StaticVectorView, UpdateMode } from "src/app/custom-views/CustomViews";
import { AnnotatedValue, Literal, Resource, Value } from "src/app/models/Resources";
import { ResViewSection, ValueClickEvent } from "src/app/models/ResourceView";

@Component({
  selector: "custom-views-renderer",
  templateUrl: "./custom-view-renderer.component.html",
  styles: [`
      :host {
          display: block;
      }
    `],
  standalone: false
})
export class CustomViewsRendererComponent {

  /**
   * INPUTS / OUTPUTS
   */

  @Input() cvData: CustomViewData;
  @Input() objects: AnnotatedValue<Value>[]; //list of objects available for the given predicate (to map with the resources referenced in customViews and to keep track of tripleScope computed by getResourceView)
  @Input() rendering: boolean;
  @Input() section: ResViewSection;
  @Output() valueClick: EventEmitter<ValueClickEvent> = new EventEmitter<ValueClickEvent>();

  /**
   * ATTRIBUTES
   */

  customViews: AbstractView[];

  category: CustomViewCategory;

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['cvData']) {
      // this.predicate = this.predicateCustomView.predicate;
      this.initCustomViewData();
    }
  }

  private initCustomViewData() {
    //convert the data to a proper view structure according the model type
    this.customViews = [];

    if (this.cvData.model == CustomViewModel.point) {
      //for point, each element of data is expected to contain the bindings location, latitude and longitude
      this.cvData.data.forEach(d => {
        let descr: SparqlBasedValueDTO = d.description as SparqlBasedValueDTO;
        let v: PointView = new PointView(this.getAnnotatedValue(d), this.cvData.defaultView);
        v.allowEdit = descr.updateMode != UpdateMode.none;
        let pointDescr: BindingMapping = descr.bindingsList[0]; //for sure there is only one BingingMapping which describes the only point
        v.location = pointDescr[CustomViewVariables.location] as Resource;
        v.latitude = pointDescr[CustomViewVariables.latitude] as Literal;
        v.longitude = pointDescr[CustomViewVariables.longitude] as Literal;
        this.customViews.push(v);
      });
    } else if (this.cvData.model == CustomViewModel.area) {
      //for area, each element of data is expected to contain a list of points, containing in turn the bindings location, latitude and longitude
      this.cvData.data.forEach(d => {
        let descr: SparqlBasedValueDTO = d.description as SparqlBasedValueDTO;
        let v: AreaView = new AreaView(this.getAnnotatedValue(d), this.cvData.defaultView);
        v.allowEdit = descr.updateMode != UpdateMode.none;
        v.routeId = descr.bindingsList[0][CustomViewVariables.route_id] as Resource; //by construction route ID is the same for each record
        descr.bindingsList.forEach(b => {
          v.locations.push({
            location: b[CustomViewVariables.location] as Resource,
            latitude: b[CustomViewVariables.latitude] as Literal,
            longitude: b[CustomViewVariables.longitude] as Literal
          });
        });
        this.customViews.push(v);
      });
    } else if (this.cvData.model == CustomViewModel.route) {
      //route is built as area
      this.cvData.data.forEach(d => {
        let descr: SparqlBasedValueDTO = d.description as SparqlBasedValueDTO;
        let v: RouteView = new RouteView(this.getAnnotatedValue(d), this.cvData.defaultView);
        v.allowEdit = descr.updateMode != UpdateMode.none;
        v.routeId = descr.bindingsList[0][CustomViewVariables.route_id] as Resource; //by construction route ID is the same for each record
        descr.bindingsList.forEach(b => {
          v.locations.push({
            location: b[CustomViewVariables.location] as Resource,
            latitude: b[CustomViewVariables.latitude] as Literal,
            longitude: b[CustomViewVariables.longitude] as Literal
          });
        });
        this.customViews.push(v);
      });
    } else if (this.cvData.model == CustomViewModel.series) {
      /* 
      for series, each element of data is expected to contain the bindings series_id, series_label, value_label (which are the same for all element of the series)
      and pairs name-value
      */
      this.cvData.data.forEach(d => {
        let descr: SparqlBasedValueDTO = d.description as SparqlBasedValueDTO;
        let v: SeriesView = new SeriesView(this.getAnnotatedValue(d), this.cvData.defaultView);
        v.allowEdit = descr.updateMode != UpdateMode.none;
        //series_id, series_label and value_label are supposed to be the same for all the data
        v.series_id = descr.bindingsList[0][CustomViewVariables.series_id] as Resource;
        if (descr.bindingsList[0][CustomViewVariables.series_label] != null) {
          v.series_label = descr.bindingsList[0][CustomViewVariables.series_label].stringValue();
        }
        if (descr.bindingsList[0][CustomViewVariables.value_label] != null) {
          v.value_label = descr.bindingsList[0][CustomViewVariables.value_label].stringValue();
        }
        descr.bindingsList.forEach(b => {
          v.data.push({
            name: b[CustomViewVariables.name] as Resource,
            value: b[CustomViewVariables.value] as Literal
          });
        });
        this.customViews.push(v);
      });
    } else if (this.cvData.model == CustomViewModel.series_collection) {
      /* 
      for series_collection, each element of data is expected to contain the bindings series_collection_id, series_label, value_label (which are the same for all element of the series)
      series_name and pairs name-value
      */
      this.cvData.data.forEach(d => {
        let descr: SparqlBasedValueDTO = d.description as SparqlBasedValueDTO;
        let v: SeriesCollectionView = new SeriesCollectionView(this.getAnnotatedValue(d), this.cvData.defaultView);
        v.allowEdit = descr.updateMode != UpdateMode.none;
        //series_collection_id, series_label and value_label are supposed to be the same for all the data
        v.series_collection_id = descr.bindingsList[0][CustomViewVariables.series_collection_id] as Resource;
        if (descr.bindingsList[0][CustomViewVariables.series_label] != null) {
          v.series_label = descr.bindingsList[0][CustomViewVariables.series_label].stringValue();
        }
        if (descr.bindingsList[0][CustomViewVariables.value_label] != null) {
          v.value_label = descr.bindingsList[0][CustomViewVariables.value_label].stringValue();
        }
        descr.bindingsList.forEach(b => {
          let seriesName = b[CustomViewVariables.series_name];
          let data = {
            name: b[CustomViewVariables.name] as Resource,
            value: b[CustomViewVariables.value] as Literal
          };
          let series = v.series.find(s => s.series_name.equals(seriesName));
          if (series) {
            series.data.push(data);
          } else {
            v.series.push({
              series_name: seriesName,
              data: [data]
            });
          }
        });
        this.customViews.push(v);
      });
    } else if (this.cvData.model == CustomViewModel.property_chain) {
      /* 
      for property_chain, each element of data is expected to contain a list of CustomViewRenderedValue
      (should be only one, since it's a single value preview, but since it supports the validation, there could be multiple values, e.g. one in main graph, one in staging-remove)
      */
      this.cvData.data.forEach(d => {
        let descr: CustomViewRenderedValue[] = d.description as CustomViewRenderedValue[];
        let v: PropertyChainView = new PropertyChainView(this.getAnnotatedValue(d), this.cvData.defaultView);
        v.values = descr;
        this.customViews.push(v);
      });
    } else if (this.cvData.model == CustomViewModel.adv_single_value) {
      /* 
      for adv_single_value, each element of data is expected to contain a list of CustomViewRenderedValue
      (should be only one, since it's a single value preview, but in order to align the structures of single-value CV, it is aligned to property_chain which may contain multi values)
      */
      this.cvData.data.forEach(d => {
        let descr: CustomViewRenderedValue[] = d.description as CustomViewRenderedValue[];
        let v: AdvSingleValueView = new AdvSingleValueView(this.getAnnotatedValue(d), this.cvData.defaultView);
        v.values = descr;
        this.customViews.push(v);
      });
    } else if (this.cvData.model == CustomViewModel.static_vector) {
      this.cvData.data.forEach(d => {
        let descr: CustomViewRenderedValue[] = d.description as CustomViewRenderedValue[];
        let v: StaticVectorView = new StaticVectorView(this.getAnnotatedValue(d), this.cvData.defaultView);
        v.values = descr;
        this.customViews.push(v);
      });
    } else if (this.cvData.model == CustomViewModel.dynamic_vector) {
      this.cvData.data.forEach(d => {
        let descr: CustomViewRenderedValue[] = d.description as CustomViewRenderedValue[];
        let v: DynamicVectorView = new DynamicVectorView(this.getAnnotatedValue(d), this.cvData.defaultView);
        v.values = descr;
        this.customViews.push(v);
      });
    }

    this.customViews.sort((cv1, cv2) => {
      return this.objects.indexOf(cv1.resource) - this.objects.indexOf(cv2.resource);
    });

    //for the same predicate, model and category are the same for each cv, so it's ok to take just the first
    this.category = this.customViews[0].category;
  }

  private getAnnotatedValue(cvObjDescr: CustomViewObjectDescription) {
    let resource = this.objects.find(o => o.getValue().equals(cvObjDescr.resource));
    if (!resource) {
      resource = new AnnotatedValue(cvObjDescr.resource);
    }
    return resource;
  }

  /**
   * METHODS
   */


  /**
   * Events forwarding
   */

  onValueClick(event: ValueClickEvent) {
    this.valueClick.emit(event);
  }

}