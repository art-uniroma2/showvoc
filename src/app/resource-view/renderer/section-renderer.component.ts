import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Project } from 'src/app/models/Project';
import { ResViewSectionsCustomization } from 'src/app/models/Properties';
import { PredicateObjects, Resource } from 'src/app/models/Resources';
import { ResViewSection, ValueClickEvent } from 'src/app/models/ResourceView';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { TranslationUtils } from 'src/app/utils/TranslationUtils';

@Component({
  selector: 'renderer',
  templateUrl: './section-renderer.component.html',
  host: { class: "renderer" },
  standalone: false
})
export class SectionRendererComponent {

  @Input() project: Project; //when opening RV of an external project
  @Input() section: ResViewSection;
  @Input() resource: Resource;
  @Input() poList: PredicateObjects[];
  @Input() rendering: boolean = true; //if true the resource should be rendered with the show, with the qname otherwise

  @Output() valueClick: EventEmitter<ValueClickEvent> = new EventEmitter<ValueClickEvent>();

  showHeader: boolean;
  sectionTitle: string;
  sectionDescription: string;

  constructor(private settingsMgr: SettingsManager, private translate: TranslateService) {
    this.translate.onLangChange.subscribe(() => {
      this.initSectionDisplayInfo();
    });
  }

  ngOnInit() {
    this.showHeader = this.settingsMgr.getShowSectionHeader(SVContext.getProject(this.project));

    this.initSectionDisplayInfo();
  }

  private initSectionDisplayInfo() {
    //reset info (to prevent them holding the old value in a different language, in case onLangChange was fired)
    this.sectionTitle = null;
    this.sectionDescription = null;
    if (this.showHeader) {
      let sectionCustomization: ResViewSectionsCustomization = this.settingsMgr.getResViewSectionsCustomization(SVContext.getProject(this.project));
      if (sectionCustomization?.[this.translate.currentLang]?.[this.section]) {
        let title = sectionCustomization[this.translate.currentLang][this.section].label;
        let descr = sectionCustomization[this.translate.currentLang][this.section].description;
        if (title != null && title != "") {
          this.sectionTitle = title;
        }
        if (descr != null && descr != "") {
          this.sectionDescription = descr;
        }
      }
      if (this.sectionTitle == null) { //if no custom title, apply the default
        this.sectionTitle = this.translate.instant(TranslationUtils.getResViewSectionTranslationKey(this.section)).toLocaleUpperCase();
      }
    }
  }


  onValueClick(event: ValueClickEvent) {
    this.valueClick.emit(event);
  }
}