import { ChangeDetectorRef, Component, EventEmitter, Output } from '@angular/core';
import { AnnotatedValue, Resource } from 'src/app/models/Resources';
import { ValueClickEvent } from 'src/app/models/ResourceView';

@Component({
  selector: 'resource-view-tabset',
  templateUrl: './resource-view-tabset.component.html',
  styleUrls: ['./resource-view-tabset.component.css'],
  host: { class: "vbox" },
  standalone: false
})
export class ResourceViewTabsetComponent {

  //tells to the parent component that there are no more RV open
  @Output() empty: EventEmitter<void> = new EventEmitter<void>();
  @Output() valueClick: EventEmitter<ValueClickEvent> = new EventEmitter<ValueClickEvent>();

  tabs: Tab[] = [];
  activeTab: Tab;

  constructor(private cdRef: ChangeDetectorRef) { }

  selectResource(resource: AnnotatedValue<Resource>, newTab?: boolean) {
    //search for a tab describing the given resource
    let tab = this.tabs.find(t => t.resource.getValue().equals(resource.getValue()));
    if (tab != null) { //resource already open in a tab => select it
      this.selectTab(tab);
    } else { //resource not yet open in a tab => open it
      if (newTab || this.tabs.length == 0) {
        this.addTab(resource);
      } else {
        this.activeTab.resource = resource;
      }
    }
  }

  private addTab(resource: AnnotatedValue<Resource>) {
    let tab: Tab = { resource: resource };
    this.tabs.push(tab);

    this.cdRef.detectChanges();
    this.activeTab = tab;
  }

  selectTab(t: Tab) {
    this.activeTab = t;
  }

  closeTab(tab: Tab) {
    //remove the tab to close
    let idxTabToClose: number = this.tabs.indexOf(tab);
    this.tabs.splice(idxTabToClose, 1);
    if (this.tabs.length == 0) { //no more tabs
      this.empty.emit();
    } else { //other tabs present => active another tab
      if (tab == this.activeTab) { //closed tab is the active one
        //select the previous tab, or the following in case the closed tab was the first one
        if (idxTabToClose > 0) {
          this.selectTab(this.tabs[idxTabToClose - 1]);
        } else {
          this.selectTab(this.tabs[idxTabToClose]);
        }
      }
    }
  }

  closeAll() {
    this.tabs = [];
    this.empty.emit();
  }


  /**
   * When changes on resource view make change the show of the resource, update the resource of the tab
   * so that the header of the tab shows the updated resource.
   */
  onResourceUpdate(resource: AnnotatedValue<Resource>, tab: Tab) {
    tab.resource = resource;
  }

  onValueClick(event: ValueClickEvent) {
    if (event.shift) {
      //shift+click => propagate the event to be handled in the parent (eventually open RV in the side tabset)
      this.valueClick.emit(event);
    } else if (event.ctrl) {
      //ctrl+click => open internally to this tabset
      this.selectResource(event.value, true);
    } else if (event.double) {
      //double click => open in the same tabset
      this.selectResource(event.value, false);
    }
  }

  onTabMouseDown(event: MouseEvent, tab: Tab) {
    if (event.button == 1) { //wheel/middle click => close tab
      this.closeTab(tab);
    } else if (event.shiftKey) { //shift click on tab => emit event so tabset move tab to side tabset
      this.valueClick.emit(new ValueClickEvent(tab.resource, event));
      if (this.activeTab != tab) {
        //shift + click on unfocused tab => move and close it
        event.stopImmediatePropagation(); //stop propagation to prevent ngb nav to autofocus the tab
        event.preventDefault(); //to prevent opening SV on a new browser window
        this.closeTab(tab);
      }
    }
  }

}


interface Tab {
  resource: AnnotatedValue<Resource>;
}