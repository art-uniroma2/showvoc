import { Component, ElementRef, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Project } from 'src/app/models/Project';
import { Resource } from 'src/app/models/Resources';
import { ResourceViewCtx } from 'src/app/models/ResourceView';
import { UIUtils } from 'src/app/utils/UIUtils';

@Component({
  selector: 'resource-view-modal',
  templateUrl: './resource-view-modal.component.html',
  standalone: false
})
export class ResourceViewModalComponent {

  @Input() resource: Resource;
  @Input() project: Project;

  context: ResourceViewCtx = ResourceViewCtx.modal;

  constructor(public activeModal: NgbActiveModal, private elementRef: ElementRef) { }

  ngAfterViewInit() {
    UIUtils.setFullSizeModal(this.elementRef);
  }

  ok() {
    this.activeModal.close();
  }

  close() {
    this.activeModal.dismiss();
  }

}
