import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ResViewTemplate, SectionFilterPreference } from "src/app/models/Properties";
import { AbstractDefaultsModal } from "src/app/preferences/default-settings-modal/abstract-defaults-modal";
import { SettingsServices } from "src/app/services/settings.service";
import { SettingsManager } from "src/app/utils/SettingsManager";
import { SVContext } from "src/app/utils/SVContext";

@Component({
  selector: "res-view-section-filter-defaults-modal",
  templateUrl: "./res-view-section-filter-defaults-modal.component.html",
  standalone: false
})
export class ResViewSectionFilterDefaultsModalComponent extends AbstractDefaultsModal {

  settingName: string = SettingsManager.PROP_NAME.resViewPartitionFilter;

  template: ResViewTemplate; //template on which base the sections filter

  userDefault: SectionFilterPreference;
  projectDefault: SectionFilterPreference;

  constructor(
    activeModal: NgbActiveModal,
    settingsService: SettingsServices,
    settingsMgr: SettingsManager
  ) {
    super(activeModal, settingsService, settingsMgr);
  }

  ngOnInit() {
    this.template = this.settingsMgr.getResourceViewProjectSettings(SVContext.getProject()).templates;
    super.init();
  }

  restoreProjDefaultImpl() {
    return this.settingsMgr.setResViewSectionFilter(SVContext.getWorkingProject(), null);
  }

  applyUserDefaultImpl() {
    return this.settingsMgr.setResViewSectionFilter(SVContext.getWorkingProject(), this.userDefault);
  }



}