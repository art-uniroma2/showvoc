import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalOptions } from 'src/app/modal-dialogs/Modals';
import { ResViewSettingsModalComponent } from './resource-view-settings-modal.component';

@Injectable()
export class ResViewModalsServices {

    constructor(private modalService: NgbModal) { }

    /**
     * Opens a resource view in a modal
     * @param resource 
     */
    openSettings() {
        const modalRef: NgbModalRef = this.modalService.open(ResViewSettingsModalComponent, new ModalOptions('lg'));
        return modalRef.result;
    }


}