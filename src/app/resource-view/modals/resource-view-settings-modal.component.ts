import { Component } from "@angular/core";
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { BasicModalsServices } from "src/app/modal-dialogs/basic-modals/basic-modals.service";
import { ModalOptions, ModalType } from "src/app/modal-dialogs/Modals";
import { Scope } from 'src/app/models/Plugins';
import { ClickableValueStyle, ResViewTemplate, SectionFilterPreference, ValueFilterLanguages } from 'src/app/models/Properties';
import { LocalStorageManager } from "src/app/utils/LocalStorageManager";
import { SettingsManager, SettingsManagerID } from 'src/app/utils/SettingsManager';
import { SVContext } from 'src/app/utils/SVContext';
import { ToastService } from 'src/app/widget/toast/toast-service';
import { ResViewSectionFilterDefaultsModalComponent } from "./res-view-section-filter-defaults-modal.component";

@Component({
  selector: "res-view-settings-modal",
  templateUrl: "./resource-view-settings-modal.component.html",
  standalone: false
})
export class ResViewSettingsModalComponent {

  sectionFilter: SectionFilterPreference;
  template: ResViewTemplate; //template on which base the sections filter

  showDatatype: boolean;
  showSectionHeader: boolean;
  hideLiteralLang: boolean;

  clickableValueStyle: ClickableValueStyle = new ClickableValueStyle();

  langValueFilter: ValueFilterLanguages;

  showGuidelines: boolean;

  constructor(
    public activeModal: NgbActiveModal,
    private settingsMgr: SettingsManager,
    private modalService: NgbModal,
    private basicModals: BasicModalsServices,
    private toastService: ToastService
  ) { }

  ngOnInit() {
    let resViewPreferences = this.settingsMgr.getResourceViewPreferences(SVContext.getWorkingProject());

    this.showDatatype = resViewPreferences.showDatatypeBadge;

    this.initSectionVisibility();

    this.showSectionHeader = this.settingsMgr.getShowSectionHeader(SVContext.getWorkingProject());

    this.initClickableValueStyle();

    this.initLangValueFilter();

    this.initHideLiteralLang();

    this.showGuidelines = !LocalStorageManager.getItem(LocalStorageManager.RES_VIEW_GUIDELINES_DISMISSED);
  }

  onShowDatatypeChange() {
    let resViewPreferences = this.settingsMgr.getResourceViewPreferences(SVContext.getWorkingProject());
    resViewPreferences.showDatatypeBadge = this.showDatatype;
    this.settingsMgr.setResourceViewPreferences(SVContext.getWorkingProject(), resViewPreferences).subscribe();
  }

  // res view sections

  onShowSectionHeaderChange() {
    this.settingsMgr.setShowSectionHeader(SVContext.getWorkingProject(), this.showSectionHeader).subscribe();
  }

  private initSectionVisibility() {
    this.sectionFilter = this.settingsMgr.getResViewSectionFilter(SVContext.getWorkingProject());
    this.template = this.settingsMgr.getResourceViewProjectSettings(SVContext.getWorkingProject())?.templates;
  }

  onSectionFilterChange() {
    this.settingsMgr.setResViewSectionFilter(SVContext.getWorkingProject(), this.sectionFilter).subscribe();
  }

  setSectionVisibilityUserDefault() {
    this.basicModals.confirm({ key: "COMMONS.ACTIONS.SET_AS_DEFAULT" }, { key: "MESSAGES.SET_USER_DEFAULT_CONFIRM" }, ModalType.warning).then(
      () => {
        this.settingsMgr.setDefaultResViewSectionFilter(this.sectionFilter, Scope.USER).subscribe();
      },
      () => { }
    );
  }

  /**
   * Reset the preference to the default, namely remove set the PUSettings, so if there is a default it is retrieved through the fallback
   */
  showSectionVisibilityDefault() {
    const modalRef: NgbModalRef = this.modalService.open(ResViewSectionFilterDefaultsModalComponent, new ModalOptions("lg"));
    modalRef.result.then(
      () => {
        this.settingsMgr.initSettings(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SVContext.getWorkingProject()).subscribe(
          () => {
            this.initSectionVisibility();
          }
        );
      },
      () => { }
    );
  }

  // clickable value style

  private initClickableValueStyle() {
    let style = this.settingsMgr.getClickableValueStyle();
    if (style != null) {
      this.clickableValueStyle.bold = style.bold;
      this.clickableValueStyle.color = style.color;
      this.clickableValueStyle.underlined = style.underlined;
      this.clickableValueStyle.widget = style.widget;
    }
  }

  onClickableStyleChanged() {
    this.settingsMgr.setClickableValueStyle(this.clickableValueStyle).subscribe();
  }

  resetClickableStyle() {
    this.settingsMgr.setClickableValueStyle(null).subscribe(
      () => {
        this.initClickableValueStyle();
      }
    );
  }

  // hide lang

  initHideLiteralLang() {
    this.hideLiteralLang = this.settingsMgr.getResourceViewHideLiteral(SVContext.getWorkingProject());
  }

  onHideLiteralLangChange() {
    this.settingsMgr.setResourceViewHideLiteral(SVContext.getProject(), this.hideLiteralLang).subscribe();
  }


  // langValueFilter

  initLangValueFilter() {
    this.langValueFilter = this.settingsMgr.getValueFilterLanguages(SVContext.getWorkingProject());
  }

  onLangValueFilterChange() {
    this.settingsMgr.setValueFilterLanguages(SVContext.getProject(), this.langValueFilter).subscribe();
  }

  restoreLangValueFilterDefault() {
    this.settingsMgr.setValueFilterLanguages(SVContext.getProject(), null).subscribe(
      () => {
        this.toastService.show({ key: "COMMONS.STATUS.OPERATION_DONE" }, { key: "MESSAGES.DEFAULT_RESTORED" }, { toastClass: "bg-success" });
        //reinit the setting, so that the defaults are computed again
        this.settingsMgr.initSettings(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SVContext.getProject()).subscribe(
          () => {
            this.initLangValueFilter();
          }
        );
      }
    );
  }

  setLangValueFilterAsUserDefault() {
    this.settingsMgr.setDefaultValueFilterLanguages(this.langValueFilter, Scope.USER).subscribe(
      () => {
        this.toastService.show({ key: "COMMONS.STATUS.OPERATION_DONE" }, { key: "MESSAGES.DEFAULT_UPDATED" }, { toastClass: "bg-success" });
      }
    );
  }

  closeGuideline(dontShowAgain: boolean) {
    this.showGuidelines = false;
    if (dontShowAgain) {
      LocalStorageManager.setItem(LocalStorageManager.RES_VIEW_GUIDELINES_DISMISSED, true);
    }
  }


  ok() {
    this.activeModal.close();
  }

  close() {
    this.activeModal.dismiss();
  }

}