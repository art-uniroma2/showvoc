import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MetadataStoredContribution } from 'src/app/models/Contribution';
import { MdrVoc, StMdr } from 'src/app/models/Vocabulary';

@Component({
    selector: "metadata-contrib-details-modal",
    templateUrl: "./metadata-contribution-details-modal.component.html",
    standalone: false
})
export class MetadataContributionDetailsModalComponent {

    @Input() contribution: MetadataStoredContribution;

    dereferenciationMap: { [uri: string]: string } = {
        null: "Unknown",
        [MdrVoc.standardDereferenciation.getIRI()]: "Yes",
        [MdrVoc.noDereferenciation.getIRI()]: "No",
    };
    dereferenciationSystem: string; //show of the dereferenciation system according the above map
    sparqlNoAggregation: boolean;

    constructor(public activeModal: NgbActiveModal) { }

    ngOnInit() {
        let derefSysIri: string = null;
        if (this.contribution.dereferenciationSystem != null) {
            derefSysIri = this.contribution.dereferenciationSystem.getIRI();
        }
        this.dereferenciationSystem = this.dereferenciationMap[derefSysIri];

        this.sparqlNoAggregation = this.contribution.sparqlLimitations.some(l => l.getIRI() == "<" + StMdr.noAggregation.getIRI() + ">");
    }

    close() {
        this.activeModal.dismiss();
    }

}