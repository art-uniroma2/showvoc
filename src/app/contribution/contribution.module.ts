import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { WidgetModule } from '../widget/widget.module';
import { ContributionsManagerComponent } from './administration/contributions-manager.component';
import { ContributionComponent } from './contribution.component';
import { DevelopmentContributionDetailsModalComponent } from './development/development-contribution-details-modal.component';
import { DevelopmentContributionComponent } from './development/development-contribution.component';
import { DevProjectCreationModalComponent } from './development/development-project-creation-modal.component';
import { LoadDevResourceComponent } from './development/load-dev.component';
import { MetadataContributionDetailsModalComponent } from './metadata/metadata-contribution-details-modal.component';
import { MetadataContributionComponent } from './metadata/metadata-contribution.component';
import { LoadStableResourceComponent } from './stable/load-stable.component';
import { StableContributionDetailsModalComponent } from './stable/stable-contribution-details-modal.component';
import { StableContributionComponent } from './stable/stable-contribution.component';
import { StableProjectCreationModalComponent } from './stable/stable-project-creation-modal.component';

@NgModule({
    declarations: [
        ContributionComponent,
        ContributionsManagerComponent,
        DevelopmentContributionComponent,
        DevelopmentContributionDetailsModalComponent,
        DevProjectCreationModalComponent,
        LoadDevResourceComponent,
        LoadStableResourceComponent,
        MetadataContributionComponent,
        MetadataContributionDetailsModalComponent,
        StableContributionComponent,
        StableContributionDetailsModalComponent,
        StableProjectCreationModalComponent,
    ],
    imports: [
        CommonModule,
        DragDropModule,
        FormsModule,
        TranslateModule,
        WidgetModule,
    ],
    exports: [
        ContributionsManagerComponent
    ],
    providers: []
})
export class ContributionModule { }
