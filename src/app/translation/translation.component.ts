import { Component, QueryList, ViewChildren } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BasicModalsServices } from '../modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from '../modal-dialogs/Modals';
import { SharedModalsServices } from '../modal-dialogs/shared-modals/shared-modals.service';
import { Language, Languages } from '../models/LanguagesCountries';
import { TranslationResult } from '../models/Search';
import { GlobalSearchServices } from '../services/global-search.service';
import { LocalStorageManager } from '../utils/LocalStorageManager';
import { TranslationResultsetComponent } from './translation-resultset.component';

@Component({
  selector: 'translation-component',
  templateUrl: './translation.component.html',
  host: { class: "pageComponent" },
  styles: [`
        .grouped-result + .grouped-result {
            margin-top: 1rem;
        }
    `],
  standalone: false
})
export class TranslationComponent {

  @ViewChildren(TranslationResultsetComponent) viewChildrenResult: QueryList<TranslationResultsetComponent>;


  searchString: string;
  lastSearch: string;

  sourceLangs: Language[] = [];
  targetLangs: Language[] = [];

  caseSensitive: boolean;

  groupedResults: { [repId: string]: TranslationResult[] };
  accessibleMap: { [repId: string]: boolean }; //for each dataset tells if it is accessible: repo is open and user authorized (possible auth limitations only for SuperUser)

  loading: boolean = false;

  includeClosedDatasets: boolean = true;
  filteredRepoIds: string[]; //id (eventually filtered) of the repositories of the results, useful to iterate over them in the view

  resultsCount: number;
  excludedResultsCount: number; //results excluded due filters (currently just due the "only open" dataset filter)
  excludedRepoCount: number; //repo excluded due filters

  constructor(
    private globalSearchService: GlobalSearchServices,
    private basicModals: BasicModalsServices,
    private sharedModals: SharedModalsServices
  ) { }

  ngOnInit() {
    this.initCookies();
  }

  searchKeyHandler() {
    if (this.searchString != null && this.searchString.trim() != "") {
      this.translate();
    }
  }

  translate() {
    if (this.sourceLangs.length == 0) {
      this.basicModals.alert({ key: "COMMONS.STATUS.WARNING" }, { key: "You need to select at least a source language" }, ModalType.warning);
      return;
    }
    if (this.targetLangs.length == 0) {
      this.basicModals.alert({ key: "COMMONS.STATUS.WARNING" }, { key: "You need to select at least a target language" }, ModalType.warning);
      return;
    }

    this.lastSearch = this.searchString;
    this.loading = true;
    this.globalSearchService.translation(this.searchString, this.sourceLangs.map(l => l.tag), this.targetLangs.map(l => l.tag), this.caseSensitive)
      .pipe(finalize(() => { this.loading = false; }))
      .subscribe(
        (results: TranslationResult[]) => {
          //group the results by repository ID 
          this.groupedResults = {};
          results.forEach(r => {
            let resultsInRepo: TranslationResult[] = this.groupedResults[r.repository.id];
            if (resultsInRepo != null) {
              resultsInRepo.push(r);
            } else {
              this.groupedResults[r.repository.id] = [r];
            }
            /* 
            post processing of response content:
            - no need to show empty rows for those languages that don't have translations
            - sort translation values with this order: skos:prefLabel, skos:altLabel, other predicates, skos:hiddenLabel
            */
            r.translations = r.translations.filter(t => t.values.some(v => v.type == "lexicalization")); //filter out from translations those elements which have no values
          });

          //compute auth map
          this.accessibleMap = {};
          Object.keys(this.groupedResults).forEach(repoId => {
            this.accessibleMap[repoId] = this.isResultAccessible(this.groupedResults[repoId][0]);
          });

          this.filterSearchResults();
        },
        (err: Error) => {
          if (err.name.endsWith("IndexNotFoundException")) {
            this.basicModals.alert({ key: "SEARCH.INDEX_NOT_FOUND" }, { key: "MESSAGES.SEARCH_INDEX_NOT_FOUND" }, ModalType.warning);
          }
        }
      );
  }

  private filterSearchResults() {
    this.resultsCount = 0;
    this.excludedResultsCount = 0;
    this.excludedRepoCount = 0;

    //collect the repositories ID according the filter
    this.filteredRepoIds = [];
    Object.keys(this.groupedResults).forEach(repoId => {
      if (this.includeClosedDatasets || !this.includeClosedDatasets && this.groupedResults[repoId][0].repository.open) {
        this.filteredRepoIds.push(repoId);
        this.resultsCount += this.groupedResults[repoId].length;
      } else {
        this.excludedRepoCount++;
        this.excludedResultsCount += this.groupedResults[repoId].length;
      }
    });
    this.filteredRepoIds.sort();
  }

  /**
    * Tells if a given search result is accessible, namely if the repository/project (which the result belongs to) is open
    * @param result
    */
  private isResultAccessible(result: TranslationResult): boolean {
    return result.repository.open;
  }

  expandAll() {
    this.viewChildrenResult.forEach(r => {
      r.collapsed = false;
    });
  }
  collapseAll() {
    this.viewChildrenResult.forEach(r => {
      r.collapsed = true;
    });
  }

  /**======================
   * Filters
   * ======================*/

  switchLangs() {
    let temp = this.sourceLangs.slice();
    this.sourceLangs = this.targetLangs;
    this.targetLangs = temp;
    this.updateCookies();
  }

  editSourceLangs() {
    this.sharedModals.selectLanguages({ key: "COMMONS.ACTIONS.SELECT_LANGUAGES" }, this.sourceLangs.map(l => l.tag)).then(
      (languages: string[]) => {
        this.sourceLangs = Languages.fromTagsToLanguages(languages);
        this.updateCookies();
      }
    );
  }

  editTargetLangs() {
    this.sharedModals.selectLanguages({ key: "COMMONS.ACTIONS.SELECT_LANGUAGES" }, this.targetLangs.map(l => l.tag)).then(
      (languages: string[]) => {
        this.targetLangs = Languages.fromTagsToLanguages(languages);
        this.updateCookies();
      }
    );
  }

  removeLang(langs: Language[], index: number) {
    langs.splice(index, 1);
    this.updateCookies();
  }

  onCaseSensitiveChange() {
    this.updateCookies();
    if (this.sourceLangs.length > 0 && this.targetLangs.length > 0 && this.searchString && this.searchString.trim().length > 0) {
      this.translate();
    }
  }

  //Projects

  disableClosedProjectFilter() {
    this.includeClosedDatasets = true;
    this.updateProjectFilter();
  }

  updateProjectFilter() {
    this.updateCookies();
    if (this.groupedResults) { //if search results are available, filter them
      this.filterSearchResults();
    }
  }





  private initCookies() {
    //Projects: only open
    this.includeClosedDatasets = LocalStorageManager.getItem(LocalStorageManager.TRANSLATION_FILTERS_ONLY_OPEN_PROJECTS);
    //case sensitive
    this.caseSensitive = LocalStorageManager.getItem(LocalStorageManager.TRANSLATION_CASE_SENSITIVE);
    //Languages
    let sourceLangsCookie = LocalStorageManager.getItem(LocalStorageManager.TRANSLATION_SOURCE_LANGUAGES);
    if (sourceLangsCookie != null) {
      this.sourceLangs = Languages.fromTagsToLanguages(sourceLangsCookie);
    }
    let targetLangsCookie = LocalStorageManager.getItem(LocalStorageManager.TRANSLATION_TARGET_LANGUAGES);
    if (targetLangsCookie != null) {
      this.targetLangs = Languages.fromTagsToLanguages(targetLangsCookie);
    }
  }
  private updateCookies() {
    LocalStorageManager.setItem(LocalStorageManager.TRANSLATION_FILTERS_ONLY_OPEN_PROJECTS, this.includeClosedDatasets);
    LocalStorageManager.setItem(LocalStorageManager.TRANSLATION_CASE_SENSITIVE, this.caseSensitive);
    LocalStorageManager.setItem(LocalStorageManager.TRANSLATION_SOURCE_LANGUAGES, this.sourceLangs.map(l => l.tag));
    LocalStorageManager.setItem(LocalStorageManager.TRANSLATION_TARGET_LANGUAGES, this.targetLangs.map(l => l.tag));

  }

}