import { Value } from '../models/Resources';

export class LocalStorageManager {

  public static APP_VERSION = "sv_version";

  public static SHOW_DEPRECATED = "tree_list.show_deprecated";
  public static STRUCTURE_SORT = "tree_list.sort_"; //the role of the structure is appended as suffix to the prop name (e.g. tree_list.rendering_concept)

  public static PROJECT_COLLAPSED_DIRS = "project.collapsed_dirs"; //comma separated names of collapsed directories

  public static ALIGNMENT_VIEW_RENDERING = "alignment.view.rendering";

  public static CORRESPONDENCES_VIEW_RENDERING = "correspondences.view.rendering";

  public static DATASETS_FILTER_ONLY_OPEN_PROJECTS = "datasets.filters.only_open_projects";
  public static DATASETS_FILTER_ONLY_FAVORITES = "datasets.filters.only_favorites";

  public static DATASETS_FACETS = "datasets.facets";

  public static GLOBAL_SEARCH_FILTERS_ONLY_OPEN_PROJECTS = "global_search.filters.only_open_projects";
  public static GLOBAL_SEARCH_FILTERS_LANGUAGES = "global_search.filters.languages";
  public static GLOBAL_SEARCH_SETTINGS_CASE_SENSITIVE = "global_search.settings.case_sensitive";
  public static GLOBAL_SEARCH_SETTINGS_IN_LOCALNAME = "global_search.settings.in_localname";
  public static GLOBAL_SEARCH_SETTINGS_MODE = "global_search.settings.mode";

  public static GLOBAL_SEARCH_FILTERS_DATASETS_FILTER_TYPE = "global_search.filters.datasets_filter_type";
  public static GLOBAL_SEARCH_FILTERS_DATASETS_FILTER_SELECTION = "global_search.filters.datasets_filter_selection";
  public static GLOBAL_SEARCH_FILTERS_DATASETS_FILTER_GROUPS = "global_search.filters.datasets_filter_groups";

  public static TRANSLATION_FILTERS_ONLY_OPEN_PROJECTS = "translation.filters.only_open_projects";
  public static TRANSLATION_CASE_SENSITIVE = "translation.case_sensitive";
  public static TRANSLATION_SOURCE_LANGUAGES = "translation.source_languages";
  public static TRANSLATION_TARGET_LANGUAGES = "translation.target_languages";

  public static METADATA_TYPE_DISTRIBUTIONS_CHART_TYPE = "metadata.type_distributions.chart_type";
  public static METADATA_LEX_SETS_CHART_TYPE = "metadata.lexicalization_sets.chart_type";

  public static EXPLORE_HIDE_WARNING_MODAL_RES_VIEW = "explore.hide_warning_open_modal_res_view";

  public static RES_VIEW_GUIDELINES_DISMISSED = "resview.guidelines_dismissed";

  public static WARNING_ADMIN_CHANGE_USER_TYPE = "warnings.administration.change_user_type";
  public static WARNING_CUSTOM_ROOT = "warnings.ui.tree.cls.customroot";
  public static WARNING_INVOKABLE_REPORTER_WITH_CUSTOM_SERVICE = "warnings.invokable_reporter.reporter_with_custom_service";


  public static getItem(name: string): any {
    let itemValue = localStorage.getItem(name);
    if (itemValue) {
      try {
        return JSON.parse(itemValue);
      } catch {
        //prevent app from failure in case of unparseable value (e.g. old stored plain values)
        return null;
      }
    }
    return itemValue;
  }

  public static setItem(name: string, value: any) {
    value = this.convertValue(value);
    if (value == null) {
      LocalStorageManager.deleteItem(name);
      return;
    }
    localStorage.setItem(name, value);
  }

  /**
   * Removes specified item
   */
  public static deleteItem(name: string) {
    localStorage.removeItem(name);
  }

  private static convertValue(value: any): any {
    if (Array.isArray(value)) {
      // if (value.length > 0) { 
      //NOTE: also empty list must be written, otherwise if empty list are removed, it automatically triggers the fallback mechanism
      let stringArray: string[] = [];
      value.forEach((v: any) => {
        if (v instanceof Value) {
          stringArray.push(v.toNT());
        } else {
          stringArray.push(v);
        }
      });
      return JSON.stringify(stringArray);
    } else if (value instanceof Map) {
      if (value.size > 0) {
        let stringMap: { [key: string]: string } = {};
        value.forEach((v: any, key: string) => {
          if (v instanceof Value) {
            stringMap[key] = v.toNT();
          } else {
            stringMap[key] = v;
          }
        });
        return JSON.stringify(stringMap);
      }
    } else if (value instanceof Value) {
      return JSON.stringify(value.toNT());
    } else if (value != null) {
      return JSON.stringify(value);
    }
    return null;
  }


  static updateRoutine(appVersion: string) {
    let localStorageVersion = localStorage.getItem(LocalStorageManager.APP_VERSION);

    let currentVersion = SemVer.parse(appVersion);
    if (localStorageVersion != null) {
      let lsv = SemVer.parse(localStorageVersion);
      if (currentVersion.compareTo(lsv) > 0) { //current version > local storage
        //do something
        // console.log("new version detected", currentVersion, ">", lsv);
      }
    } else {
      /*
      local storage version never set, for sure data is about version < 3.1.0
      V 3.1.0 changed how values are written-to/read-from localStorage, so reset storage to prevent errors
      */
      localStorage.clear();
    }
    localStorage.setItem(LocalStorageManager.APP_VERSION, appVersion);
  }

}



class SemVer {
  major: number;
  minor: number;
  patch: number;
  snapshot: boolean;

  constructor(major: number, minor: number, patch: number, snapshot?: boolean) {
    this.major = major;
    this.minor = minor;
    this.patch = patch;
    this.snapshot = snapshot;
  }

  static parse(versionString: string): SemVer {
    let versionRegex = /^(\d+)\.(\d+)\.(\d+)(-SNAPSHOT)?$/;
    let match = versionString.match(versionRegex);

    if (!match) {
      return null; // Not a valid version string
    }
    let major = parseInt(match[1], 10);
    let minor = parseInt(match[2], 10);
    let patch = parseInt(match[3], 10);
    let snapshot = match[4] != null;
    return new SemVer(major, minor, patch, snapshot);
  }

  /**
   * Compare the SemVer with the arg.
   * Return 
   * number > 1 if the current version in greater than the provided one
   * number < 1 if the current version in lower than the provided one
   * 0 if the versions are equals
   * @param v 
   */
  compareTo(v: SemVer): number {
    let majorCompare = this.major - v.major;
    if (majorCompare != 0) {
      return majorCompare;
    }
    let minorCompare = this.minor - v.minor;
    if (minorCompare != 0) {
      return minorCompare;
    }
    let revisionCompare = this.patch - v.patch;
    if (revisionCompare != 0) {
      return revisionCompare;
    }
    if (this.snapshot && v.snapshot || !this.snapshot && !v.snapshot) {
      return 0;
    } else if (this.snapshot && !v.snapshot) {
      return -1;
    } else {
      return 1;
    }
  }
}