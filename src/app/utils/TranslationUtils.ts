import { TranslateService } from "@ngx-translate/core";
import { DataPanel } from "../models/DataStructure";
import { ResViewSection } from "../models/ResourceView";
import { ConceptTreeVisualizationMode, InstanceListVisualizationMode, LexEntryVisualizationMode } from "../models/Properties";
import { RDFResourceRolesEnum } from "../models/Resources";

export class TranslationUtils {

    static getTranslatedText(textOrTranslation: TextOrTranslation, translateService: TranslateService): string {
        if (textOrTranslation == null) return null;
        if (typeof textOrTranslation == "string") {
            return textOrTranslation;
        } else {
            return translateService.instant(textOrTranslation.key, textOrTranslation.params);
        }
    }

    /**
     * Replaces the values of the given keys with the related translation in the provided object.
     * Values assigned to keysToTranslate are expected to be TextOrTranslation.
     * Returns the same object with the values translated
     * @param object 
     * @param keysToTranslate 
     * @param translateService 
     */
    static translateObject(object: { [key: string]: any }, keysToTranslate: string[], translateService: TranslateService): { [key: string]: any } {
        if (object == null) return null;
        for (let key of keysToTranslate) {
            if (object[key] != null) {
                object[key] = this.getTranslatedText(object[key], translateService);
            }
        }
        return object;
    }

    public static readonly dataPanelTranslationMap: { [panel: string]: string } = {
        [DataPanel.cls]: 'DATA.CLASS.CLASS',
        [DataPanel.concept]: 'DATA.CONCEPT.CONCEPT',
        [DataPanel.conceptScheme]: 'DATA.SCHEME.SCHEME',
        [DataPanel.skosCollection]: 'DATA.COLLECTION.COLLECTION',
        [DataPanel.property]: 'DATA.PROPERTY.PROPERTY',
        [DataPanel.limeLexicon]: 'DATA.LEXICON.LEXICON',
        [DataPanel.ontolexLexicalEntry]: 'DATA.LEX_ENTRY.LEX_ENTRY',
        [DataPanel.alignments]: 'ALIGNMENTS.ALIGNMENTS',
        [DataPanel.correspondence]: 'CORRESPONDENCES.CORRESPONDENCES',
    };

    public static readonly resViewSectionTranslationMap: { [section: string]: string } = {
        [ResViewSection.broaders]: "RESOURCE_VIEW.SECTIONS.BROADERS",
        [ResViewSection.classaxioms]: "RESOURCE_VIEW.SECTIONS.CLASS_AXIOMS",
        [ResViewSection.constituents]: "RESOURCE_VIEW.SECTIONS.CONSTRITUENST",
        [ResViewSection.datatypeDefinitions]: "RESOURCE_VIEW.SECTIONS.DATATYPE_DEFINITIONS",
        [ResViewSection.denotations]: "RESOURCE_VIEW.SECTIONS.DENOTATIONS",
        [ResViewSection.disjointProperties]: "RESOURCE_VIEW.SECTIONS.DISJOINT_PROPERTIES",
        [ResViewSection.domains]: "RESOURCE_VIEW.SECTIONS.DOMAINS",
        [ResViewSection.equivalentProperties]: "RESOURCE_VIEW.SECTIONS.EQUIVALENT_PROPERTIES",
        [ResViewSection.evokedLexicalConcepts]: "RESOURCE_VIEW.SECTIONS.EVOKED_LEXICAL_CONCEPTS",
        [ResViewSection.facets]: "RESOURCE_VIEW.SECTIONS.FACETS",
        [ResViewSection.formRepresentations]: "RESOURCE_VIEW.SECTIONS.FORM_PRESENTATIONS",
        [ResViewSection.imports]: "RESOURCE_VIEW.SECTIONS.IMPORTS",
        [ResViewSection.labelRelations]: "RESOURCE_VIEW.SECTIONS.LABEL_RELATIONS",
        [ResViewSection.lexicalForms]: "RESOURCE_VIEW.SECTIONS.LEXICAL_FORMS",
        [ResViewSection.lexicalSenses]: "RESOURCE_VIEW.SECTIONS.LEXICAL_SENSES",
        [ResViewSection.lexicalizations]: "RESOURCE_VIEW.SECTIONS.LEXICALIZATIONS",
        [ResViewSection.members]: "RESOURCE_VIEW.SECTIONS.MEMBERS",
        [ResViewSection.membersOrdered]: "RESOURCE_VIEW.SECTIONS.MEMBERS_ORDERED",
        [ResViewSection.notes]: "RESOURCE_VIEW.SECTIONS.NOTES",
        [ResViewSection.properties]: "RESOURCE_VIEW.SECTIONS.PROPERTIES",
        [ResViewSection.ranges]: "RESOURCE_VIEW.SECTIONS.RANGES",
        [ResViewSection.rdfsMembers]: "RESOURCE_VIEW.SECTIONS.RDFS_MEMBERS",
        [ResViewSection.schemes]: "RESOURCE_VIEW.SECTIONS.SCHEMES",
        [ResViewSection.subPropertyChains]: "RESOURCE_VIEW.SECTIONS.SUBPROPERTY_CHAINS",
        [ResViewSection.subterms]: "RESOURCE_VIEW.SECTIONS.SUBTERMS",
        [ResViewSection.superproperties]: "RESOURCE_VIEW.SECTIONS.SUPERPROPERTIES",
        [ResViewSection.topconceptof]: "RESOURCE_VIEW.SECTIONS.TOP_CONCEPT_OF",
        [ResViewSection.types]: "RESOURCE_VIEW.SECTIONS.TYPES",
    };
    /**
     * Returns the translation key, if missing returns the same section enum
     * @param section
     * @returns 
     */
    public static getResViewSectionTranslationKey(section: ResViewSection): string {
        return this.resViewSectionTranslationMap[section] || section;
    }

    public static rolesTranslationMap: { [role: string]: string } = {
        [RDFResourceRolesEnum.annotationProperty]: "DATA.PROPERTY.ANNOTATION_PROPERTY",
        [RDFResourceRolesEnum.cls]: "DATA.CLASS.CLASS",
        [RDFResourceRolesEnum.concept]: "DATA.CONCEPT.CONCEPT",
        [RDFResourceRolesEnum.conceptScheme]: "DATA.SCHEME.CONCEPT_SCHEME",
        // [RDFResourceRolesEnum.dataRange]: "",
        [RDFResourceRolesEnum.datatypeProperty]: "DATA.PROPERTY.DATATYPE_PROPERTY",
        [RDFResourceRolesEnum.individual]: "DATA.INSTANCE.INSTANCE",
        [RDFResourceRolesEnum.limeLexicon]: "DATA.LEXICON.LEXICON",
        [RDFResourceRolesEnum.objectProperty]: "DATA.PROPERTY.OBJECT_PROPERTY",
        // [RDFResourceRolesEnum.ontolexForm]: "",
        [RDFResourceRolesEnum.ontolexLexicalEntry]: "DATA.LEX_ENTRY.LEX_ENTRY",
        // [RDFResourceRolesEnum.ontolexLexicalSense]: "",
        // [RDFResourceRolesEnum.ontology]: "",
        [RDFResourceRolesEnum.ontologyProperty]: "DATA.PROPERTY.ONTOLOGY_PROPERTY",
        [RDFResourceRolesEnum.property]: "DATA.PROPERTY.PROPERTY",
        [RDFResourceRolesEnum.skosCollection]: "DATA.COLLECTION.COLLECTION",
        // [RDFResourceRolesEnum.skosOrderedCollection]: "",
        // [RDFResourceRolesEnum.xLabel]: "",
    };
    public static getRoleTranslationKey(role: RDFResourceRolesEnum): string {
        return this.rolesTranslationMap[role] || role;
    }

    public static visualizationModeTranslationMap: { [key: string]: string } = {
        [ConceptTreeVisualizationMode.hierarchyBased]: "DATA.COMMONS.VISUALIZATION_MODE.HIERARCHY_BASED",
        [ConceptTreeVisualizationMode.searchBased]: "DATA.COMMONS.VISUALIZATION_MODE.SEARCH_BASED",
        [InstanceListVisualizationMode.standard]: "DATA.COMMONS.VISUALIZATION_MODE.STANDARD",
        [LexEntryVisualizationMode.indexBased]: "DATA.COMMONS.VISUALIZATION_MODE.INDEX_BASED",
        // covered by ConceptTreeVisualizationMode.searchBased
        // [InstanceListVisualizationMode.searchBased]: "DATA.COMMONS.VISUALIZATION_MODE.SEARCH_BASED", 
        // covered by ConceptTreeVisualizationMode.searchBased
        // [LexEntryVisualizationMode.searchBased]: "DATA.COMMONS.VISUALIZATION_MODE.SEARCH_BASED", 
    };

}

/**
 * string if the provided text doesn't need to be translated
 * Translation if needs to be translated
 */
export type TextOrTranslation = string | Translation;

/**
 * key is the translation key, params is the optional interpolation params object
 */
export interface Translation { key: string, params?: { [key: string]: any} }