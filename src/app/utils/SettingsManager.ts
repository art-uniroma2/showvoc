import { Injectable } from '@angular/core';
import { forkJoin, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataPanel, DataPanelLabelSetting } from '../models/DataStructure';
import { Language } from '../models/LanguagesCountries';
import { Scope, Settings } from '../models/Plugins';
import { Project, ProjectLabelCtx } from '../models/Project';
import { ClassTreePreference, ClickableValueStyle, ConceptTreePreference, DatasetTab, InstanceListPreference, LexicalEntryListPreference, PredicateLabelSettings, PreferencesUtils, ProjectVisualization, ResourceViewPreference, ResourceViewProjectSettings, ResViewSectionsCustomization, SearchSettings, SectionFilterPreference, ValueFilterLanguages } from '../models/Properties';
import { IRI, RDFResourceRolesEnum, Value } from '../models/Resources';
import { OntoLex, OWL, RDFS, SKOS } from '../models/Vocabulary';
import { SettingsServices } from '../services/settings.service';
import { STRequestOptions } from './HttpManager';
import { LocalStorageManager } from './LocalStorageManager';
import { NTriplesUtil } from './ResourceUtils';
import { SVContext } from './SVContext';
import { SVEventHandler } from './SVEventHandler';


/**
 * This service is meant to be the main responsible for getting/setting Settings.
 * It is responsible to make the management of Settings independent from the type of user currently logged.
 * That is, either if the current user is a visitor or is an actual logged user (Admin or SuperUser/PM), 
 * the component can use SettingsManager.getSettings method to get the Settings from the proper source.
 * 
 * Visitor user can't rely on Settings stored on ST (otherwise any visitor would change the Settings to any other visitor),
 * so visitor's Settings are stored on (and retrieved from) LocalStorage, while Admin/PM settings are stored on (and retrieved from) ST.
 * Given that, this service initializes the Settings getting them from ST or LocalStorage according the type of user.
 * 
 * A further difference is between the management of the fallback mechanism
 * (see here for details: https://semanticturkey.uniroma2.it/doc/user/settings.jsf). 
 * Logged users can exploit the fallback mechanism provided by getSettings ST API which fallbacks on defaults 
 * in case of a given Setting has not been set at a given level (e.g. PU setting not set => fallback to default at Project => User => System levels).
 * Since visitor, as said, doesn't use getSettings API, this service needs to replicate the fallback mechanism (only for such user type)
 * and thus needs to initalize defaults as well.
 */

@Injectable()
export class SettingsManager {

  static PROP_NAME = {
    activeLexicon: "activeLexicon",
    activeSchemes: "activeSchemes",
    allowAnonymous: "allowAnonymous",
    allowConceptTreeVisualizationChange: "allowConceptTreeVisualizationChange",
    allowInstanceListVisualizationChange: "allowInstanceListVisualizationChange",
    allowLexEntryListVisualizationChange: "allowLexEntryListVisualizationChange",
    allowLexEntryIndexLengthChange: "allowLexEntryIndexLengthChange",
    authService: "authService",
    classTree: "classTree",
    clickableValueStyle: "clickableValueStyle",
    conceptTree: "conceptTree",
    dataPanelLabelMappings: "dataPanelLabelMappings",
    disableContributions: "disableContributions",
    favoriteDatasets: "favoriteDatasets",
    filterValueLanguages: "filterValueLanguages",
    graphViewPartitionFilter: "graphViewPartitionFilter",
    hiddenDataPanelsOntolex: "hiddenDataPanelsOntolex",
    hiddenDataPanelsRdfsOwl: "hiddenDataPanelsRdfsOwl",
    hiddenDataPanelsSkos: "hiddenDataPanelsSkos",
    hiddenShowVocTabs: "hiddenShowVocTabs",
    hideLiteralGraphNodes: "hideLiteralGraphNodes",
    homeContent: "homeContent",
    instanceList: "instanceList",
    inverseRewritingRules: "inverseRewritingRules",
    languages: "languages",
    lexEntryList: "lexEntryList",
    mail: "mail",
    projectVisualization: "projectVisualization",
    projectRendering: "projectRendering",
    projectsSearchGroups: "projectsSearchGroups",
    remoteConfigs: "remoteConfigs",
    renderingClassTree: "renderingClassTree",
    renderingCollectionTree: "renderingCollectionTree",
    renderingConceptTree: "renderingConceptTree",
    renderingInstanceList: "renderingInstanceList",
    renderingLexEntryList: "renderingLexEntryList",
    renderingLexiconList: "renderingLexiconList",
    renderingPropertyTree: "renderingPropertyTree",
    renderingSchemeList: "renderingSchemeList",
    resourceView: "resourceView",
    resViewPartitionFilter: "resViewPartitionFilter",
    resViewHideLang: "resViewHideLang",
    resViewPredLabelMappings: "resViewPredLabelMappings",
    resViewSectionsCustomization: "resViewSectionsCustomization",
    resViewShowSectionHeader: "resViewShowSectionHeader",
    rewritingRules: "rewritingRules",
    searchSettings: "searchSettings",
    showFlags: "showFlags",
    showvoc: "showvoc",
    xkosTabEnabled: "xkosTabEnabled",
  };

  settingsCache: SettingsCache = new SettingsCache();
  defaultsCache: DefaultsCache = new DefaultsCache();

  constructor(private settingsService: SettingsServices, private eventHandler: SVEventHandler) { }

  initSettings(scope: Scope, componentID: string, project?: Project): Observable<void> {
    if ((scope == Scope.PROJECT_USER || scope == Scope.USER) && SVContext.getLoggedUser() == null) {
      //project-user and user settings are stored on LocalStorage for visitor user, so no need to init them from ST
      return of(null);
    } else {
      let reqOpt: STRequestOptions;
      if (scope == Scope.PROJECT || scope == Scope.PROJECT_USER) {
        reqOpt = new STRequestOptions({ ctxProject: project });
      }
      return this.settingsService.getSettings(componentID, scope, reqOpt).pipe(
        map(settings => {
          this.storeSettingsToCache(scope, componentID, settings, project);
        })
      );
    }
  }

  /**
   * This should be called only from project settings editor by admin/PM. 
   * It is meant to work for also closed projects and init/stores settings in cache as well so that 
   * components that need the settings find them already initialized
   * (e.g. rendering editor that needs proj languages, rv settings editor that needs rvSettings,
   * propertyTree browser (for custom sections editor) that needs rendering langs and search settings)
   * 
   * @param componentID
   * @param project 
   */
  initProjSettingsForAdministration(scope: Scope, componentID: string, project: Project): Observable<void> {
    return this.settingsService.getSettingsForProjectAdministration(componentID, scope, project).pipe(
      map(settings => {
        this.storeSettingsToCache(scope, componentID, settings, project);
      })
    );
  }

  getSetting(scope: Scope, componentID: string, propName: string, project?: Project, defaultValue?: any): any {
    let value: any;
    if ((scope == Scope.PROJECT_USER || scope == Scope.USER) && SVContext.getLoggedUser() == null) {
      //PU or user settings for visitor => get from LocalStorage
      let itemKey = this.getLocalStorageSettingKey(scope, componentID, propName, project);
      value = LocalStorageManager.getItem(itemKey);
      this.log("from LocalStorage", scope, propName, value);
    } else { // logged in user => get from stored Settings
      value = this.getFromCache(scope, componentID, propName, project);
      this.log("from cache", scope, propName, value);
    }
    if (value == null && SVContext.getLoggedUser() == null) {
      /* 
      fallback to defaults only for visitor since the visitor cannot exploit fallback mechanism implemented by getSettings ST API 
      (visitor settings are stored on LocalStorage, getSetting is not invoked)
      */
      if (scope == Scope.USER || scope == Scope.PROJECT) { //user and project have system as only available defaultScope (default(user,system) and default(project, system))
        value = this.getDefault(scope, componentID, propName, Scope.SYSTEM, project);
      } else if (scope == Scope.PROJECT_USER) {
        //pu has three different defaults, in order: project, user, system (default(pu,project) default(pu,user) default(pu,system))
        value = this.getDefault(scope, componentID, propName, Scope.PROJECT, project);
        if (value == null) { //still null, see default at user level
          value = this.getDefault(scope, componentID, propName, Scope.USER, project);
        }
        if (value == null) { //still null, see default at system level
          value = this.getDefault(scope, componentID, propName, Scope.SYSTEM, project);
        }
      } //other scope has not any defaults
    }
    if (value == null) {
      value = defaultValue;
    }
    return value;
  }

  setSetting(scope: Scope, componentID: string, propName: string, propValue: any, project?: Project): Observable<void> {
    if (SVContext.getLoggedUser() == null) {
      let itemKey = this.getLocalStorageSettingKey(scope, componentID, propName, project);
      LocalStorageManager.setItem(itemKey, propValue);
      return of(null);
    } else {
      this.updateSettingToCache(scope, componentID, propName, propValue, project);
      let reqOpt: STRequestOptions;
      if (scope == Scope.PROJECT || scope == Scope.PROJECT_USER) {
        reqOpt = new STRequestOptions({ ctxProject: project });
      }
      return this.settingsService.storeSetting(componentID, scope, propName, this.convertSettingValue(propValue), reqOpt);
    }
  }


  private getFromCache(scope: Scope, componentID: string, propName: string, project?: Project) {
    //No need to do null-safe checks cause it is assumed that init method is invoked previously
    this.log("getFromCache", scope, (project ? project.getName() : ""), componentID, propName, this.settingsCache);
    if (scope == Scope.PROJECT || scope == Scope.PROJECT_USER) {
      return this.settingsCache[scope][project.getName()][componentID][propName];
    } else {
      return this.settingsCache[scope][componentID][propName];
    }
  }

  private storeSettingsToCache(scope: Scope, componentID: string, settings: Settings, project?: Project) {
    if (scope == Scope.PROJECT || scope == Scope.PROJECT_USER) {
      if (this.settingsCache[scope][project.getName()] == null) {
        this.settingsCache[scope][project.getName()] = {};
      }
      this.settingsCache[scope][project.getName()][componentID] = settings.getPropertiesAsMap();
    } else {
      this.settingsCache[scope][componentID] = settings.getPropertiesAsMap();
    }
    this.log("storeSettingsToCache", scope, (project ? project.getName() : ""), componentID, this.settingsCache);
  }

  private updateSettingToCache(scope: Scope, componentID: string, propName: string, propValue: any, project?: Project) {
    /*
    here below, before updating the cache, do the null-safe check, 
    because a specific setting might be never initialize
    e.g. when clearing the data of a project, the active schemes/lexicon might be reset
    but if such project was never accessed, their settings were never initialized/cached
    */
    if (scope == Scope.PROJECT || scope == Scope.PROJECT_USER) {
      if (this.settingsCache[scope]?.[project.getName()]?.[componentID]) {
        this.settingsCache[scope][project.getName()][componentID][propName] = propValue;
      }
    } else {
      if (this.settingsCache[scope]?.[componentID]) {
        this.settingsCache[scope][componentID][propName] = propValue;
      }
    }
    this.log("updateSettingToCache", scope, (project ? project.getName() : ""), componentID, propName, propValue, this.settingsCache);
  }


  initDefaults(scope: Scope, componentID: string, defaultScope?: Scope, project?: Project): Observable<void> {
    let reqOpt: STRequestOptions;
    if (scope == Scope.PROJECT || defaultScope == Scope.PROJECT) {
      reqOpt = new STRequestOptions({ ctxProject: project });
    }
    let reqFn = this.settingsService.getSettingsDefault(componentID, scope, defaultScope, reqOpt);
    return reqFn.pipe(
      map(settings => {
        this.storeDefaultsToCache(scope, componentID, settings, defaultScope, project);
      })
    );
  }

  getDefault(scope: Scope, componentID: string, propName: string, defaultScope?: Scope, project?: Project, defaultValue?: any): any {
    this.log("getDefault", scope, (defaultScope ? defaultScope : ""), (project ? project.getName() : ""), componentID, propName, this.defaultsCache);
    let value: any;
    if (scope == Scope.PROJECT_USER) {
      if (defaultScope == Scope.USER && SVContext.getLoggedUser() == null) {
        //default PU settings at user level for visitor => get from LocalStorage
        let itemKey = this.getLocalStorageDefaualtKey(scope, componentID, propName, defaultScope, project);
        value = LocalStorageManager.getItem(itemKey);
      } else if (defaultScope == Scope.PROJECT) {
        if (this.defaultsCache[scope][defaultScope][project.getName()] == undefined || this.defaultsCache[scope][defaultScope][project.getName()]?.[componentID] == undefined) {
          console.warn("defaults", scope, defaultScope, project.getName(), componentID, "undefined");
        }
        value = this.defaultsCache[scope][defaultScope][project.getName()]?.[componentID]?.[propName];
      } else { //default PU at USER or SYSTEM 
        if (this.defaultsCache[scope][defaultScope][componentID] == undefined) {
          console.warn("defaults", scope, defaultScope, componentID, "undefined");
        }
        value = this.defaultsCache[scope][defaultScope][componentID]?.[propName];
      }
    } else {
      if (this.defaultsCache[scope][componentID] == undefined) {
        console.warn("defaults", scope, componentID, "undefined");
      }
      value = this.defaultsCache[scope][componentID][propName];
    }
    this.log("from default cache", scope, defaultScope, propName, value);
    if (value == null) {
      value = defaultValue;
    }
    return value;
  }

  /**
   * 
   * @param scope 
   * @param componentID 
   * @param propName 
   * @param propValue 
   * @param defaultScope 
   * @param project needed only in case scope-defaultScope pair is PROJECT_USER-PROJECT (default PU settings at project level/for a given project)
   */
  setDefault(scope: Scope, componentID: string, propName: string, propValue: any, defaultScope?: Scope, project?: Project): Observable<any> {
    if (SVContext.getLoggedUser() == null) {
      //visitor user can set only PU settings default at user level which are stored in LocalStorage 
      if (scope == Scope.PROJECT_USER && defaultScope == Scope.USER) {
        let itemKey = this.getLocalStorageDefaualtKey(scope, componentID, propName, defaultScope, project);
        LocalStorageManager.setItem(itemKey, propValue);
        return of(null);
      } else {
        console.error("Invalid setDefault for visitor: scope", scope, "defaultScope", defaultScope, "project", project);
        return of(null);
      }
    } else {
      if (
        scope == Scope.PROJECT_USER && defaultScope == Scope.USER || //in case of default user no need to provide user, server will take the logged one
        scope == Scope.USER && defaultScope == Scope.SYSTEM ||
        scope == Scope.PROJECT && defaultScope == Scope.SYSTEM
      ) {
        return this.settingsService.storeSettingDefault(componentID, scope, defaultScope, propName, this.convertSettingValue(propValue));
      } else if (scope == Scope.PROJECT_USER && defaultScope == Scope.PROJECT) {
        return this.settingsService.storePUSettingProjectDefault(componentID, project, propName, this.convertSettingValue(propValue));
      } else {
        return of(null); //temp (never needed so far, implement this when necessary)
      }
    }
  }

  private storeDefaultsToCache(scope: Scope, componentID: string, settings: Settings, defaultScope?: Scope, project?: Project) {
    if (scope == Scope.PROJECT_USER) {
      if (defaultScope == Scope.PROJECT) {
        if (this.defaultsCache[scope][defaultScope][project.getName()] == null) {
          this.defaultsCache[scope][defaultScope][project.getName()] = {};
        }
        this.defaultsCache[scope][defaultScope][project.getName()][componentID] = settings.getPropertiesAsMap();
      } else { //default PU at USER or SYSTEM 
        this.defaultsCache[scope][defaultScope][componentID] = settings.getPropertiesAsMap();
      }
    } else {
      this.defaultsCache[scope][componentID] = settings.getPropertiesAsMap();
    }
    this.log("storeDefaultsToCache", scope, defaultScope, (project ? project.getName() : ""), componentID, this.defaultsCache);
  }


  private getLocalStorageSettingKey(scope: Scope, componentID: string, propName: string, project?: Project) {
    /*
    examples:
    - PROJECT_USER.MyProject.SemanticTurkeyCoreSettingsManager#aProp
    - USER.SemanticTurkeyCoreSettingsManager#aProp
    */
    let key = scope + "." +
      ((scope == Scope.PROJECT || scope == Scope.PROJECT_USER) ? (project.getName() + ".") : "") +
      componentID.substring(componentID.lastIndexOf(".") + 1) + "#" +
      propName;
    if (SVContext.getWorld() != null) {
      key = "[" + SVContext.getWorld() + "]." + key;
    }
    return key;
  }

  private getLocalStorageDefaualtKey(scope: Scope, componentID: string, propName: string, defaultScope?: Scope, project?: Project) {
    /*
    examples:
    - DEF.PROJECT_USER.PROJECT.MyProject.SemanticTurkeyCoreSettingsManager#aProp
    - DEF.USER.SemanticTurkeyCoreSettingsManager#aProp
    */
    return "DEF." + scope + "." +
      (defaultScope ? (defaultScope + ".") : "") +
      (defaultScope == Scope.PROJECT ? (project.getName() + ".") : "") +
      componentID.substring(componentID.lastIndexOf(".") + 1) + "#" +
      propName;
  }

  private convertSettingValue(value: any): any {
    let valueAsString: any;
    if (Array.isArray(value)) {
      // if (value.length > 0) { 
      //NOTE: also empty list must be written, otherwise if empty list are removed, it automatically triggers the fallback mechanism
      let stringArray: string[] = [];
      value.forEach((v: any) => {
        if (v instanceof Value) {
          stringArray.push(v.toNT());
        } else {
          stringArray.push(v);
        }
      });
      valueAsString = stringArray;
    } else if (value instanceof Map) {
      if (value.size > 0) {
        let stringMap: { [key: string]: string } = {};
        value.forEach((v: any, key: string) => {
          if (v instanceof Value) {
            stringMap[key] = v.toNT();
          } else {
            stringMap[key] = v;
          }
        });
        // valueAsString = JSON.stringify(stringMap);
        valueAsString = stringMap;
      }
    } else if (value instanceof Value) {
      valueAsString = value.toNT();
    } else if (value != null) {
      valueAsString = value;
    }
    return valueAsString;
  }


  /* 
  Init the settings needed at user access:
  - User settings (e.g. clickableValueStyle, projectVisualization)
  Only for visitor (since fallback mechanism is implemented client side, it can't exploit the mechanism provided by getSettings)
  - PU and User defaults (Sys level) for fallback mechanism
  This method has been "centralized" here since it is used in multiple points of the code: 
  - after an explicit login 
  - after a refresh, when a user is already logged in and it is fetched from ST
  Note: system defaults are init only after user access because ST allows only authenticated user to invoke the get defaults APIs
  */
  initSettingsAtUserAccess(): Observable<any> {
    let initUserSettings = this.initSettings(Scope.USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager);

    let initFnList = [initUserSettings];

    if (SVContext.getLoggedUser() == null) {
      let initPuSysDefaults = this.initDefaults(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.SYSTEM);
      let initUserSysDefaults = this.initDefaults(Scope.USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.SYSTEM);
      let initProjSysDefaults = this.initDefaults(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.SYSTEM);
      initFnList.push(initPuSysDefaults);
      initFnList.push(initUserSysDefaults);
      initFnList.push(initProjSysDefaults);
      //PU default at user level? should be no need since visitor shouldn't be allowed to set its defaults on ST
    }

    return forkJoin(initFnList).pipe(
      map(() => {
        //init rendering for datasets' labels, this needs to be done after all the init functions to exploits the fallback to defaults mechanism
        ProjectLabelCtx.renderingEnabled = this.getProjectRendering();
      })
    );
  }

  /**
   * Init settings needed when accessing a project:
   * - Project settings (e.g. resourceView, languages)
   * - PU core settings (e.g. activeSchemes, conceptTree, classTree, ...)
   * - PU rendering settings (rendering languages)
   * - PU core defaults at proj+sys level (for fallback mechanism)
   * - PU rendering defaults at proj+sys level (for fallback mechanism)
   * This method has been "centralized" here since it is used in multiple points of the code: 
   * - when accessing data/ page (HTTP resolution)
   * - when accessing a page protected by ProjectGuard and the working project in the context is different from the one that is being accessed
   */
  initSettingsAtProjectAccess(project: Project): Observable<any> {
    let initProjSettings = this.initSettings(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, project);
    let initPuCoreSettings = this.initSettings(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, project);
    let initPuRenderingSettings = this.initSettings(Scope.PROJECT_USER, SettingsManagerID.RenderingEngine, project);

    let initFnList = [initProjSettings, initPuCoreSettings, initPuRenderingSettings];

    if (SVContext.getLoggedUser() == null) {
      let initPuCoreProjDefaults = this.initDefaults(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.PROJECT, project);
      let initPuCoreSysDefaults = this.initDefaults(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.SYSTEM, project);
      let initPuRenderingProjDefaults = this.initDefaults(Scope.PROJECT_USER, SettingsManagerID.RenderingEngine, Scope.PROJECT, project);
      let initPuRenderingSysDefaults = this.initDefaults(Scope.PROJECT_USER, SettingsManagerID.RenderingEngine, Scope.SYSTEM, project);
      initFnList.push(initPuCoreProjDefaults);
      initFnList.push(initPuCoreSysDefaults);
      initFnList.push(initPuRenderingProjDefaults);
      initFnList.push(initPuRenderingSysDefaults);
    }
    return forkJoin(initFnList);
  }

  /**
   * TODO do update actions needed when user update settings for current project (see ResViewProjectSettingsComponent for usages example)
   * @param project 
   */
  // onSettingsUpdated(scope: Scope, componentID: string, project?: Project): Observable<any> {
  //     let updateFn: Observable<any>[] = [];
  //     if (scope == Scope.PROJECT) {
  //         if (SVContext.getWorkingProject() != null && SVContext.getWorkingProject().getName() == project.getName()) {
  //             this.settingsMgr.initSettings(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project).subscribe();
  //         }
  //     }
  //     if (updateFn.length > 0) {
  //         return forkJoin(updateFn);
  //     } else {
  //         return of(null);
  //     }
  // }

  // onDefaultsUpdated(scope: Scope, componentID: string, settings: Settings, defaultScope?: Scope, project?: Project): Observable<any> {
  //     let updateFn: Observable<any>[] = [];

  //     if (scope == Scope.PROJECT_USER) {
  //         if (defaultScope == Scope.PROJECT) {
  //             value = this.defaultsCache[scope][defaultScope][project.getName()][componentID][propName];
  //         } else { //default PU at USER or SYSTEM 
  //             value = this.defaultsCache[scope][defaultScope][componentID][propName];
  //         }
  //     } else if (scope == Scope.PROJECT) {
  //         value = this.defaultsCache[scope][project.getName()][componentID][propName];
  //     } else {
  //         value = this.defaultsCache[scope][componentID][propName];
  //     }


  //     if (SVContext.getWorkingProject() != null && SVContext.getWorkingProject().getName() == project.getName()) {
  //         this.settingsMgr.initSettings(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, this.project).subscribe();
  //     }
  //     return updateFn;
  // }


  /*
  common getters/setters
  */

  //PROJECT_USER

  getActiveLexicon(project: Project): IRI {
    let lexicon = this.getSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.activeLexicon, project);
    if (lexicon != null) {
      return NTriplesUtil.parseIRI(lexicon);
    } else {
      return null;
    }
  }
  setActiveLexicon(project: Project, lexicon: IRI): Observable<void> {
    if (SVContext.getWorkingProject()?.getName() == project.getName()) {
      this.eventHandler.lexiconChangedEvent.emit(lexicon);
    }

    return this.setSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.activeLexicon, lexicon, project);
  }

  getActiveSchemes(project: Project): IRI[] {
    let activeSchemes: string[] = this.getSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.activeSchemes, project, []);
    return activeSchemes.map(s => NTriplesUtil.parseIRI(s));
  }
  setActiveSchemes(project: Project, schemes: IRI[]): Observable<void> {
    if (SVContext.getWorkingProject()?.getName() == project.getName()) {
      this.eventHandler.schemeChangedEvent.emit(schemes);
    }
    return this.setSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.activeSchemes, schemes, project);
  }

  getStructureRendering(role: RDFResourceRolesEnum, project: Project): boolean {
    let settingEnum: string = SettingsMgrUtils.roleToPanelRenderingSetting[role];
    return this.getSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, settingEnum, project, false);
  }
  setStructureRendering(role: RDFResourceRolesEnum, project: Project, rendering: boolean): Observable<void> {
    let settingEnum: string = SettingsMgrUtils.roleToPanelRenderingSetting[role];
    return this.setSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, settingEnum, rendering, project);
  }


  getSearchSettings(project: Project): SearchSettings {
    let searchSettings = this.getSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.searchSettings, project, new SearchSettings());
    let completeSetting = new SearchSettings();
    PreferencesUtils.mergePreference(completeSetting, searchSettings); //fill undefined options with client-side defaults
    return completeSetting;
  }
  setSearchSettings(project: Project, searchSettings: SearchSettings): Observable<void> {
    this.eventHandler.searchPrefsUpdatedEvent.emit();
    return this.setSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.searchSettings, searchSettings, project);
  }

  getValueFilterLanguages(project: Project): ValueFilterLanguages {
    return this.getSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.filterValueLanguages, project, new ValueFilterLanguages());
  }
  setValueFilterLanguages(project: Project, filter: ValueFilterLanguages): Observable<void> {
    return this.setSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.filterValueLanguages, filter, project);
  }

  getConceptTreeSettings(project: Project): ConceptTreePreference {
    let defaultPref: ConceptTreePreference = new ConceptTreePreference();
    let storedSettings = this.getSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.conceptTree, project, defaultPref);
    PreferencesUtils.mergePreference(defaultPref, storedSettings); //fill undefined options with client-side defaults
    return defaultPref;
  }
  setConceptTreeSettings(project: Project, settings: ConceptTreePreference): Observable<void> {
    return this.setSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.conceptTree, settings, project);
  }

  getClassTreeSettings(project: Project): ClassTreePreference {
    let defaultPref: ClassTreePreference = new ClassTreePreference(project.getModelType());
    let storedSettings = this.getSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.classTree, project, defaultPref);
    PreferencesUtils.mergePreference(defaultPref, storedSettings); //fill undefined options with client-side defaults
    return defaultPref;
  }
  setClassTreeSettings(project: Project, settings: ClassTreePreference): Observable<void> {
    return this.setSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.classTree, settings, project);
  }

  getInstanceListSettings(project: Project): InstanceListPreference {
    let defaultPref: InstanceListPreference = new InstanceListPreference();
    let storedSettings = this.getSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.instanceList, project, defaultPref);
    PreferencesUtils.mergePreference(defaultPref, storedSettings); //fill undefined options with client-side defaults
    return defaultPref;

  }
  setInstanceListSettings(project: Project, settings: InstanceListPreference): Observable<void> {
    return this.setSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.instanceList, settings, project);
  }

  getLexEntryListSettings(project: Project): LexicalEntryListPreference {
    let defaultPref: LexicalEntryListPreference = new LexicalEntryListPreference();
    let storedSettings = this.getSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.lexEntryList, project, defaultPref);
    PreferencesUtils.mergePreference(defaultPref, storedSettings); //fill undefined options with client-side defaults
    return defaultPref;
  }
  setLexEntryListSettings(project: Project, settings: LexicalEntryListPreference): Observable<void> {
    return this.setSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.lexEntryList, settings, project);
  }

  getResourceViewPreferences(project: Project): ResourceViewPreference {
    let defaultPref = new ResourceViewPreference();
    //some of the properties of RV prefs are still not moved to ST, so take it from LocalStorage
    let storedSetting = this.getSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.resourceView, project, defaultPref);
    PreferencesUtils.mergePreference(defaultPref, storedSetting);
    return defaultPref;
  }
  setResourceViewPreferences(project: Project, setting: ResourceViewPreference): Observable<void> {
    return this.setSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.resourceView, setting, project);
  }

  getResViewSectionFilter(project: Project): SectionFilterPreference {
    return this.getSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.resViewPartitionFilter, project, {});
  }
  setResViewSectionFilter(project: Project, setting: SectionFilterPreference): Observable<void> {
    return this.setSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.resViewPartitionFilter, setting, project);
  }

  getResourceViewHideLiteral(project: Project): boolean {
    return this.getSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.resViewHideLang, project, false);
  }
  setResourceViewHideLiteral(project: Project, hide: boolean): Observable<void> {
    return this.setSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.resViewHideLang, hide, project);
  }

  getShowSectionHeader(project: Project): boolean {
    return this.getSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.resViewShowSectionHeader, project, false);
  }
  setShowSectionHeader(project: Project, show: boolean): Observable<void> {
    return this.setSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.resViewShowSectionHeader, show, project);
  }

  getGraphSectionFilter(project: Project): SectionFilterPreference {
    return this.getSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.graphViewPartitionFilter, project, {});
  }
  setGraphSectionFilter(project: Project, setting: SectionFilterPreference): Observable<void> {
    return this.setSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.graphViewPartitionFilter, setting, project);
  }

  getHideLiteralGraphNodes(project: Project): boolean {
    return this.getSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.hideLiteralGraphNodes, project);
  }
  setHideLiteralGraphNodes(project: Project, show: boolean): Observable<void> {
    return this.setSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.hideLiteralGraphNodes, show, project);
  }

  getPanelFilter(project: Project): DataPanel[] {
    let settingEnum = SettingsMgrUtils.modelToHiddenDataPanelSetting[project.getModelType()];
    return this.getSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, settingEnum, project, []);
  }
  setPanelFilter(project: Project, tabs: DataPanel[]): Observable<void> {
    let settingEnum = SettingsMgrUtils.modelToHiddenDataPanelSetting[project.getModelType()];
    return this.setSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, settingEnum, tabs, project);
  }

  /**
   * Sometimes it is needed to init just the RenderingEngine settings (e.g. when resolving rendering of resources in global search).
   * Use this method in such case
   * @param project 
   */
  initRenderingLanguages(project: Project): Observable<any> {
    let initPuRenderingSettings = this.initSettings(Scope.PROJECT_USER, SettingsManagerID.RenderingEngine, project);
    let initFnList = [initPuRenderingSettings];
    /*
    Currently (on July 2024) this method is only used in search.component during the resolution of the rendering of the results.
    When calling getResourcesInfo, it is necessary to initialize the rendering languages for each projects having resources returned by the search.
    To prevent the invokation of 3 services for each project (PU settings + PU proj defaults + PU sys defaults), the defaults have been excluded.
    Indeed, there's no need to init also the defaults since, in the above case, the default mechanism for retrieving rendering langs is alreay implemented server side, 
    so the fallback chain client side can be avoided and the client send the ctx_lang only if set in the local storage.
    */
    // if (SVContext.getLoggedUser() == null) {
    //     let initPuRenderingProjDefaults = this.initDefaults(Scope.PROJECT_USER, SettingsManagerID.RenderingEngine, Scope.PROJECT, project);
    //     let initPuRenderingSysDefaults = this.initDefaults(Scope.PROJECT_USER, SettingsManagerID.RenderingEngine, Scope.SYSTEM, project);
    //     initFnList.push(initPuRenderingProjDefaults);
    //     initFnList.push(initPuRenderingSysDefaults);
    // }
    return forkJoin(initFnList);
  }
  getRenderingLanguages(project: Project): string[] {
    let langs: string[];
    let setting: string = this.getSetting(Scope.PROJECT_USER, SettingsManagerID.RenderingEngine, SettingsManager.PROP_NAME.languages, project);
    if (setting) {
      langs = setting.split(",");
    }
    if (langs && langs.length == 1 && langs[0] == "*") {
      langs = null;
    }
    return langs;
  }
  setRenderingLanguages(project: Project, languages: string[]): Observable<void> {
    let setting = null;
    if (languages) {
      setting = (languages.length == 0) ? "*" : languages.join(",");
    }
    return this.setSetting(Scope.PROJECT_USER, SettingsManagerID.RenderingEngine, SettingsManager.PROP_NAME.languages, setting, project);
  }

  getHiddenTabs(project: Project): DatasetTab[] {
    return this.getSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.hiddenShowVocTabs, project, []);
  }
  setHiddenTabs(project: Project, tabs: DatasetTab[]): Observable<void> {
    return this.setSetting(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.hiddenShowVocTabs, tabs, project);
  }

  //USER

  getClickableValueStyle(): ClickableValueStyle {
    return this.getSetting(Scope.USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.clickableValueStyle, null, new ClickableValueStyle());
  }
  setClickableValueStyle(style: ClickableValueStyle): Observable<void> {
    return this.setSetting(Scope.USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.clickableValueStyle, style);
  }

  getFavoriteDatasets(): string[] {
    return this.getSetting(Scope.USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.favoriteDatasets, null, []);
  }
  setFavoriteDatasets(datasets: string[]): Observable<void> {
    return this.setSetting(Scope.USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.favoriteDatasets, datasets);
  }

  getProjectRendering(): boolean {
    return this.getSetting(Scope.USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.projectRendering, null, false);
  }
  setProjectRendering(rendering: boolean): Observable<void> {
    //update rendering option also in ProjectLabelCtx (why? read explanation in comment on ProjectLabelCtx class)
    ProjectLabelCtx.renderingEnabled = rendering;
    return this.setSetting(Scope.USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.projectRendering, rendering);
  }



  //PROJECT

  getResourceViewProjectSettings(project: Project): ResourceViewProjectSettings {
    return this.getSetting(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.resourceView, project, new ResourceViewProjectSettings());
  }

  getResourceViewPredLabelSettings(project: Project): PredicateLabelSettings {
    return this.getSetting(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.resViewPredLabelMappings, project, new PredicateLabelSettings());
  }


  getProjectLanguages(project: Project): Language[] {
    return this.getSetting(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.languages, project, []);
  }

  getXkosTabEnabled(project: Project): boolean {
    return this.getSetting(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.xkosTabEnabled, project, false);
  }
  setXkosTabEnabled(project: Project, enabled: boolean): Observable<void> {
    return this.setSetting(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.xkosTabEnabled, enabled, project);
  }

  getAllowConceptTreeVisualizationChange(project: Project): boolean {
    return this.getSetting(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.allowConceptTreeVisualizationChange, project, true);
  }
  setAllowConceptTreeVisualizationChange(project: Project, allow: boolean): Observable<void> {
    return this.setSetting(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.allowConceptTreeVisualizationChange, allow, project);
  }

  getAllowInstanceListVisualizationChange(project: Project): boolean {
    return this.getSetting(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.allowInstanceListVisualizationChange, project, true);
  }
  setAllowInstanceListVisualizationChange(project: Project, allow: boolean): Observable<void> {
    return this.setSetting(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.allowInstanceListVisualizationChange, allow, project);
  }

  getAllowLexEntryListVisualizationChange(project: Project): boolean {
    return this.getSetting(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.allowLexEntryListVisualizationChange, project, true);
  }
  setAllowLexEntryListVisualizationChange(project: Project, allow: boolean): Observable<void> {
    return this.setSetting(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.allowLexEntryListVisualizationChange, allow, project);
  }

  getAllowLexEntryListIndexLengthChange(project: Project): boolean {
    return this.getSetting(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.allowLexEntryIndexLengthChange, project, true);
  }
  setAllowLexEntryListIndexLengthChange(project: Project, allow: boolean): Observable<void> {
    return this.setSetting(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.allowLexEntryIndexLengthChange, allow, project);
  }

  getDataPanelsLabels(project: Project): DataPanelLabelSetting {
    return this.getSetting(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.dataPanelLabelMappings, project);
  }
  setDataPanelsLabels(project: Project, setting: DataPanelLabelSetting): Observable<void> {
    return this.setSetting(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.dataPanelLabelMappings, setting, project);
  }

  getResViewSectionsCustomization(project: Project): ResViewSectionsCustomization {
    return this.getSetting(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.resViewSectionsCustomization, project);
  }
  setResViewSectionsCustomization(project: Project, setting: ResViewSectionsCustomization): Observable<void> {
    return this.setSetting(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.resViewSectionsCustomization, setting, project);
  }

  /*
  DEFAULTS
   */


  //PU

  setDefaultRenderingLanguages(languages: string[], defaultScope?: Scope, project?: Project): Observable<void> {
    let setting = null;
    if (languages) {
      setting = (languages.length == 0) ? "*" : languages.join(",");
    }
    return this.setDefault(Scope.PROJECT_USER, SettingsManagerID.RenderingEngine, SettingsManager.PROP_NAME.languages, setting, defaultScope, project);
  }

  setDefaultValueFilterLanguages(languages: ValueFilterLanguages, defaultScope?: Scope, project?: Project): Observable<void> {
    return this.setDefault(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.filterValueLanguages, languages, defaultScope, project);
  }

  setDefaultHiddenDataPanels(hiddenPanels: DataPanel[], model: string, defaultScope?: Scope, project?: Project): Observable<void> {
    let settingName: string = SettingsMgrUtils.modelToHiddenDataPanelSetting[model];
    return this.setDefault(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, settingName, hiddenPanels, defaultScope, project);
  }

  setDefaultHiddenTabs(hiddenTabs: DatasetTab[], defaultScope?: Scope, project?: Project): Observable<void> {
    return this.setDefault(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.hiddenShowVocTabs, hiddenTabs, defaultScope, project);
  }

  setDefaultResViewSectionFilter(setting: SectionFilterPreference, defaultScope?: Scope, project?: Project): Observable<void> {
    return this.setDefault(Scope.PROJECT_USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.resViewPartitionFilter, setting, defaultScope, project);
  }

  //USER

  setDefaultClickableValueStyle(value: ClickableValueStyle): Observable<void> {
    return this.setDefault(Scope.USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.clickableValueStyle, value, Scope.SYSTEM);
  }

  setDefaultProjectVisualization(value: ProjectVisualization): Observable<void> {
    return this.setDefault(Scope.USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.projectVisualization, value, Scope.SYSTEM);
  }

  setDefaultProjectRendering(rendering: boolean): Observable<void> {
    return this.setDefault(Scope.USER, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.projectRendering, rendering, Scope.SYSTEM);
  }

  //PROJ

  setDefaultResViewProjectSettings(resViewProjSettings: ResourceViewProjectSettings): Observable<void> {
    return this.setDefault(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.resourceView, resViewProjSettings, Scope.SYSTEM);
  }

  setDefaultPredLabelSettings(settings: PredicateLabelSettings): Observable<void> {
    return this.setDefault(Scope.PROJECT, SettingsManagerID.SemanticTurkeyCoreSettingsManager, SettingsManager.PROP_NAME.resViewPredLabelMappings, settings, Scope.SYSTEM);
  }


  resetSettings(scope: Scope) {
    this.settingsCache[scope] = {};
  }
  resetDefaults(scope: Scope, defaultScope?: Scope) {
    if (scope == Scope.PROJECT_USER) {
      this.defaultsCache[scope][defaultScope] = {};
    } else {
      this.defaultsCache[scope] = {};
    }
  }


  private debug: boolean = false;
  private log(message?: any, ...optionalParams: any[]) {
    if (this.debug) {
      console.log(message, ...optionalParams);
    }
  }

}

export enum SettingsManagerID {
  SemanticTurkeyCoreSettingsManager = "it.uniroma2.art.semanticturkey.settings.core.SemanticTurkeyCoreSettingsManager",
  RenderingEngine = "it.uniroma2.art.semanticturkey.extension.extpts.rendering.RenderingEngine"
}

class SettingsCache {
  [Scope.SYSTEM]: { [componentID: string]: any } = {};
  [Scope.PROJECT]: { [projName: string]: { [componentID: string]: any } } = {};
  [Scope.PROJECT_USER]: { [projName: string]: { [componentID: string]: any } } = {};
  [Scope.USER]: { [componentID: string]: any } = {};
}

class DefaultsCache {
  [Scope.PROJECT]: { [componentID: string]: any } = {}; //default project settings (system level => so for all projects)
  [Scope.USER]: { [componentID: string]: any } = {}; //default user settings (system level => so for all users)
  [Scope.PROJECT_USER] = { //default PU settings at different level
    [Scope.PROJECT]: {}, //projName -> componentID -> settings (default PU settings for a given project (for all users))
    [Scope.USER]: {}, //componentID -> settings (default PU settings for (a given) current user (cross-projects))
    [Scope.SYSTEM]: {} //componentID -> settings (default PU settings for all users and all projects)
  };
}

export class SettingsMgrUtils {

  static roleToPanelRenderingSetting: { [panelRole: string]: string } = {
    [RDFResourceRolesEnum.cls]: SettingsManager.PROP_NAME.renderingClassTree,
    [RDFResourceRolesEnum.concept]: SettingsManager.PROP_NAME.renderingConceptTree,
    [RDFResourceRolesEnum.conceptScheme]: SettingsManager.PROP_NAME.renderingSchemeList,
    [RDFResourceRolesEnum.individual]: SettingsManager.PROP_NAME.renderingInstanceList,
    [RDFResourceRolesEnum.limeLexicon]: SettingsManager.PROP_NAME.renderingLexiconList,
    [RDFResourceRolesEnum.ontolexLexicalEntry]: SettingsManager.PROP_NAME.renderingLexEntryList,
    [RDFResourceRolesEnum.property]: SettingsManager.PROP_NAME.renderingPropertyTree,
    [RDFResourceRolesEnum.skosCollection]: SettingsManager.PROP_NAME.renderingCollectionTree,
  };

  static modelToHiddenDataPanelSetting: { [modelUri: string]: string } = {
    [SKOS.uri]: SettingsManager.PROP_NAME.hiddenDataPanelsSkos,
    [OWL.uri]: SettingsManager.PROP_NAME.hiddenDataPanelsRdfsOwl,
    [RDFS.uri]: SettingsManager.PROP_NAME.hiddenDataPanelsRdfsOwl,
    [OntoLex.uri]: SettingsManager.PROP_NAME.hiddenDataPanelsOntolex,
  };

}