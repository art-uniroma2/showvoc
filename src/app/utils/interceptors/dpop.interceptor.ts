import { HttpEvent, HttpHandler, HttpInterceptor, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { OAuthService } from "angular-oauth2-oidc";
import { from, Observable, switchMap } from "rxjs";
import { SVContext } from "../SVContext";

/*
DPoP is a security mechanism used in OAuth 2.0 to bind an access token to the client making the request.
It prevents attacks like 'replay attacks' or misuse of stolen tokens by requiring the client to prove it owns the token for every request.
### How does DPoP work? ###
    1. Key Pair Generation:
        The client generates a private-public key pair.
    2. **DPoP Proof (JWT)**:
        For every API request, the client creates a signed **JWT (JSON Web Token)** containing:
            - The HTTP method (`GET`, `POST`, etc.).
            - The request URL.
            - A unique identifier (`jti`) and a timestamp (`iat`).

    3. **Sending the DPoP Proof**:
        The signed JWT is included in the `DPoP` HTTP header, alongside the token in the `Authorization` header.
    4. **Server Validation**:
        The server validates the JWT signature, ensures that it matches the current request, and that the client is authorized.

 ### Purpose and Benefits of DPoP ###
1. Prevents Replay Attacks: Tokens cannot be reused without proving possession of the private key.
2. Improves OAuth 2.0 Security: Ensures that only the authenticated client can use the token.
3. Token Binding: Binds the token to a specific client and context.

DPoP enhances OAuth 2.0 by ensuring tokens can only be used by the party owning their corresponding private key. It’s useful when security is critical and token misuse is a serious concern.

 */
@Injectable()
export class DpopInterceptor implements HttpInterceptor {
  private keyPair: CryptoKeyPair | null = null;

  constructor(private oauthService: OAuthService) {
    this.generateKeyPair();
  }

  /**
   * Intercepts HTTP requests and applies custom logic based on conditions such as authentication,
   * specific request URLs, and token operations.
   *
   * @param {HttpRequest<any>} request - The HTTP request object to be intercepted.
   * @param {HttpHandler} next - The next handler in the HTTP request chain.
   * @return {Observable<HttpEvent<any>>} An observable sequence of the HTTP response event.
   */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const accessToken = this.oauthService.getAccessToken();
    //const idToken = this.oauthService.getIdToken();

    if (SVContext.getSystemSettings()?.authService !== "OAuth2") { // Skip interceptor logic
      return next.handle(request);
    }


    if (request.url.includes('/.well-known') ||
      request.url.includes('/keys') ||
      request.url.includes('/logout') ||
      request.url.includes('/revoke') ||
      request.url.includes('/getStartupSettings')
    ) {
      return next.handle(request);
    }


    if (request.url.includes('/token')) {
      return from(this.createDpopToken(request.method, request.url)).pipe(
        switchMap(dpopToken => {
          const modifiedBody = this.addAudienceToHttpParams(request.body); // If enables this line you'll get a JWT access token instead of an opaque token
          const modifiedRequest = request.clone({
            setHeaders: {
              DPoP: dpopToken,
            },
            body: modifiedBody // If enables this line you'll get a JWT access token instead of an opaque token
          });
          return next.handle(modifiedRequest);
        })
      );
    }

    const authReq = request.clone({
      setHeaders: {
        Authorization: `Bearer ${accessToken}`
      }
    });

    return next.handle(authReq);
  }

  /**
   * Generates an ECDSA key pair using the P-256 curve and stores it in the keyPair property.
   * The generated key pair can be used for cryptographic signing and verification.
   *
   * @return {Promise<void>} A promise that resolves once the key pair is successfully generated and stored.
   */
  private async generateKeyPair(): Promise<void> {
    this.keyPair = await window.crypto.subtle.generateKey(
      {
        name: "ECDSA",
        namedCurve: "P-256",
      },
      true,
      ["sign", "verify"]
    );
  }

  /**
   * Exports the public key as a JSON Web Key (JWK) object.
   *
   * @return {Promise<object>} A promise that resolves to an object representing the public key in JWK format.
   * @throws {Error} If the key pair is not generated.
   */
  private async exportPublicKeyAsJWK(): Promise<object> {
    if (!this.keyPair) {
      throw new Error("Key pair not generated");
    }
    const publicKeyJwk = await window.crypto.subtle.exportKey("jwk", this.keyPair.publicKey);
    return publicKeyJwk;
  }

  /**
   * Creates a DPoP (Demonstration of Proof-of-Possession) token for securing an HTTP request.
   *
   * @param {string} httpMethod - The HTTP method (e.g., "GET", "POST") for which the token is generated.
   * @param {string} httpUri - The target URI of the HTTP request.
   *
   * @return {Promise<string>} A promise that resolves to the generated DPoP token as a string.
   * @throws {Error} If the key pair has not been generated.
   */
  private async createDpopToken(httpMethod: string, httpUri: string): Promise<string> {
    if (!this.keyPair) {
      throw new Error("Key pair not generated");
    }

    const jwk = await this.exportPublicKeyAsJWK();

    const header = {
      alg: "ES256",
      typ: "dpop+jwt",
      jwk: jwk
    };

    const payload = {
      htm: httpMethod,
      htu: httpUri,
      iat: Math.floor(Date.now() / 1000),  // Current time in seconds
      jti: crypto.randomUUID()  // Unique identifier for the token
    };

    const base64UrlEncode = (obj: object): string =>
      btoa(JSON.stringify(obj)).replace(/\+/g, "-").replace(/\//g, "_").replace(/=+$/, "");
    const encodedHeader = base64UrlEncode(header);
    const encodedPayload = base64UrlEncode(payload);
    const data = new TextEncoder().encode(`${encodedHeader}.${encodedPayload}`);


    const signature = await window.crypto.subtle.sign(
      { name: "ECDSA", hash: "SHA-256" },
      this.keyPair.privateKey,
      data
    );

    const base64UrlSignature = btoa(String.fromCharCode(...new Uint8Array(signature)))
      .replace(/\+/g, "-")
      .replace(/\//g, "_")
      .replace(/=+$/, "");

    return `${encodedHeader}.${encodedPayload}.${base64UrlSignature}`;
  }

  /**
   * Appends a predefined audience parameter to the provided HttpParams object.
   * It's used to get a JWT access token instead of an opaque token and indicates for whom the token is intended.
   * Basically, it defines which API or service is authorized to use the token.
   *
   * @param {HttpParams} body - The initial HttpParams object to which the audience parameter will be added.
   * @return {HttpParams} A new HttpParams object with the audience parameter appended.
   */
  private addAudienceToHttpParams(body: HttpParams): HttpParams {
    return body.append('audience', 'FfQzhwnpYzQ0FlUplPomGBiC6cJYHT6jpqGioeZumPLKc9ODQDdGECUjrl0lZXW0tgh8s1t8xDS5MdWESg56hP-wVU4Cr9ce0uJPnUNwXnheO');
  }
}

