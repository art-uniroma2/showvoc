import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Language, Languages } from '../models/LanguagesCountries';
import { Scope } from '../models/Plugins';
import { AuthServiceMode, SystemSettings } from '../models/Properties';
import { SettingsServices } from '../services/settings.service';
import { SVContext } from './SVContext';
import { SettingsManager, SettingsManagerID } from './SettingsManager';

@Injectable()
export class SVProperties {

    constructor(private settingsService: SettingsServices) {
    }

    getShowFlags(): boolean {
        return SVContext.getSystemSettings().showFlags;
    }

    setHomeContent(homeContent: string): Observable<void> {
        SVContext.getSystemSettings().homeContent = homeContent;
        return this.settingsService.storeSetting(SettingsManagerID.SemanticTurkeyCoreSettingsManager, Scope.SYSTEM, SettingsManager.PROP_NAME.homeContent, homeContent);
    }

    initStartupSystemSettings(): Observable<void> {
        return this.settingsService.getStartupSettings().pipe(
            map(settings => {
                let systemSettings: SystemSettings = new SystemSettings();
                systemSettings.showFlags = settings.getPropertyValue(SettingsManager.PROP_NAME.showFlags);
                systemSettings.homeContent = settings.getPropertyValue(SettingsManager.PROP_NAME.homeContent);
                let systemLanguages: Language[] = settings.getPropertyValue(SettingsManager.PROP_NAME.languages);
                Languages.sortLanguages(systemLanguages);
                systemSettings.languages = systemLanguages;
                systemSettings.disableContributions = settings.getPropertyValue(SettingsManager.PROP_NAME.disableContributions);
                let authServiceValue = settings.getPropertyValue(SettingsManager.PROP_NAME.authService);
                if (authServiceValue in AuthServiceMode) {
                    systemSettings.authService = authServiceValue;
                }
                systemSettings.allowAnonymous = settings.getPropertyValue(SettingsManager.PROP_NAME.allowAnonymous);
                SVContext.setSystemSettings(systemSettings);
            })
        );
    }

}