import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateFn, Router } from '@angular/router';
import { forkJoin, Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { BasicModalsServices } from '../modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from '../modal-dialogs/Modals';
import { Project } from '../models/Project';
import { MetadataServices } from '../services/metadata.service';
import { ProjectsServices } from '../services/projects.service';
import { UserServices } from '../services/user.service';
import { AuthorizationEvaluator, STActionsEnum } from './AuthorizationEvaluator';
import { DatatypeValidator } from './DatatypeValidator';
import { QueryParamService } from './QueryParamService';
import { SettingsManager } from './SettingsManager';
import { SVContext } from './SVContext';

/**
 * In ShowVoc there are these kinds of users:
 * - anonymous: no user logged in (allowed only if system setting "allowAnonymous" is true, which is automatically set at the initialization of the app)
 * - user: simple user, without any authorities
 * - super user: user with system-wide SuperUser role 
 * - admin: user with system-wide Administrator role
 */

/**
 * When accessing any page under the VisitorAuthGuard, the loggedUser is retrieved from the application context (or from ST if not yet retrieved).
 * In case no user of any kind is logged, allows access only if SYSTEM setting allowAnonymous is true
 */
export const visitorAuthGuard = () => {
  const settingsMgr = inject(SettingsManager);
  const userService = inject(UserServices);

  let loggedUser = SVContext.getLoggedUser();
  if (loggedUser != null) { //user in context initialized => allow access
    return of(true);
  } else { // user in context not initialized => retrieve it
    if (SVContext.getSystemSettings().authService === "OAuth2") { // Avoid following the getUser in case of authMode = “OAuth2”
      return of(false);
    }
    return userService.getUser().pipe(
      mergeMap(user => {
        if (user) { //retrieved authenticated user
          return of(true);
        } else { //user not authenticated (but available in ST user base, otherwise getUser would have redirected to registration page)
          //if system allows anonymous users, then init settings (that will be taken from LocalStorage) like if a user is authenticated
          if (SVContext.getSystemSettings().allowAnonymous) {
            return settingsMgr.initSettingsAtUserAccess().pipe(
              map(() => {
                return true;
              })
            );
          } else {
            //if allowAnonymous is false, deny access
            return of(false);
          }
        }
      })
    );
  }
};

/**
 * Guard that protects pages accessible only by admin users
 */
export const adminAuthGuard = () => {
  const router = inject(Router);
  const userService = inject(UserServices);

  let loggedUser = SVContext.getLoggedUser();
  if (loggedUser != null) { //logged user initialized in the context => check if it is admin
    return of(loggedUser.isAdmin());
  } else { //logged user not initialized => init
    return userService.getUser().pipe(
      mergeMap(user => {
        if (user && user.isAdmin()) {
          return of(true);
        } else { //no logged user (getUser returned null), or logged user is not admin
          router.navigate(['/home']);
          return of(false);
        }
      })
    );
  }
};

/**
 * Guard that protects pages accessible only by SuperUser (or admin)
 */
export const superUserAuthGuard = () => {
  const router = inject(Router);
  const userService = inject(UserServices);

  let loggedUser = SVContext.getLoggedUser();
  if (loggedUser != null) { //logged user initialized in the context => check if it is admin
    return of(loggedUser.isSuperUser(false));
  } else { //logged user not initialized => init
    return userService.getUser().pipe(
      mergeMap(user => {
        if (user && user.isSuperUser(false)) {
          return of(true);
        } else { //no logged user (getUser returned null), or logged user is not superuser
          router.navigate(['/home']);
          return of(false);
        }
      })
    );
  }
};

/**
 * Guard that allows access to a page to authenticated users (not anonymous) 
 */
export const authenticatedUserAuthGuard = () => {
  const router = inject(Router);
  const userService = inject(UserServices);

  let loggedUser = SVContext.getLoggedUser();
  if (loggedUser != null) { //logged user initialized in the context => check if it is admin
    return of(true);
  } else { //logged user not initialized => init
    return userService.getUser().pipe(
      mergeMap(user => {
        if (user) {
          return of(true);
        } else { //no logged user (getUser returned null), or logged user is not superuser
          router.navigate(['/home']);
          return of(false);
        }
      })
    );
  }
};


/**
 * The datasets-view page and its children need a project to be selected/initialized. This guard ensures that.
 */
export const projectGuard: CanActivateFn = (route: ActivatedRouteSnapshot) => {
  const router = inject(Router);
  const userService = inject(UserServices);
  const projectService = inject(ProjectsServices);
  const metadataService = inject(MetadataServices);
  const settingsMgr = inject(SettingsManager);
  const dtValidator = inject(DatatypeValidator);
  const basicModals = inject(BasicModalsServices);
  const queryParamService = inject(QueryParamService);

  return visitorAuthGuard()
    .pipe(
      mergeMap(() => {
        let ctxProject: Project = SVContext.getWorkingProject(); //project set in the context, eventually from the dataset page
        let paramProject = route.paramMap.get('id'); //project ID set as parameter url (e.g. /#/dataset/MyDataset/...)

        //project available in the context check if it is the same passed as url param
        if (ctxProject != null && ctxProject.getName() == paramProject) {
          return of(true);
        } else { //the context project was not initialized, or it was not the project passed via url param 

          //the project was changed, so set the flag in the context, so the CustomReuseStrategy knows if to reattach or reload a route
          SVContext.setResetRoutes(true);

          return projectService.listProjects(null, true).pipe( //retrieve project with a service invocation
            mergeMap(projects => {
              let p: Project = projects.find(p => p.getName() == paramProject);
              if (p != null) { //project fount
                SVContext.initProjectCtx(p);
                let projInitFunctions: Observable<any>[] = [
                  metadataService.getNamespaceMappings().pipe(map(prefNs => { SVContext.setPrefixMappings(prefNs); })),
                  dtValidator.initDatatypeRestrictions(),
                  settingsMgr.initSettingsAtProjectAccess(p),
                  userService.listUserCapabilities() //get the capabilities for the user
                ];
                return forkJoin(projInitFunctions).pipe(
                  map(() => true)
                );
              } else { //project not found, redirect to home
                basicModals.alert({ key: "DATASETS.STATUS.DATASET_NOT_FOUND" }, { key: "MESSAGES.UNEXISTING_OR_INACCESSIBLE_DATASET", params: { datasetId: paramProject } }, ModalType.warning).then(
                  () => {
                    /* queryParamsHandling doesn't work in canActivate (https://stackoverflow.com/a/45843291/5805661)
                    in order to preserve query params I need to set them manually */
                    let queryParams = queryParamService.getPreservedQueryParams();
                    router.navigate(["/home"], { queryParams: queryParams });
                  }
                );
                return of(false);
              }
            })
          );
        }
      })
    );
};


/**
 * Guard that protects pages by evaluating the user capabilities required for a specific list of actions
 * e.g.
 * canActivate: [capabilitiesAuthGuard], data: { actions: [STActionsEnum.<actionEnum1>, STActionsEnum.<actionEnum2>] }
 * In order to be accessed, logged user requires 
 */
export const capabilitiesAuthGuard = (route: ActivatedRouteSnapshot) => {
  let actions: STActionsEnum[] = route.data.actions;
  let auth = true;
  for (let a of actions) {
    if (!AuthorizationEvaluator.isAuthorized(a)) {
      auth = false;
      break;
    }
  }
  return auth;
};