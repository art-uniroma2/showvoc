export class Cookie {

    private static TRANSLATE_LANG = "translate.lang";
    /* the available (hardcoded) l10n languages match the pattern [a-z]{2} (e.g. en, it, ...)
    anyway the languages list is extendable and here I support also the usage of locales (e.g. en-US, en-gb, ...) */
    private static LANG_PATTER = "^[a-zA-Z]{2}(-[a-zA-Z]{2})?$";

    public static getTranslationLangCookie(): string {
        let myWindow: any = window;
        let name = Cookie.TRANSLATE_LANG;
        name = myWindow.escape(name);
        let regexp = new RegExp('(?:^' + name + '|;\\s*' + name + ')=(.*?)(?:;|$)', 'g');
        let result = regexp.exec(document.cookie);
        let cookieValue = (result === null) ? null : myWindow.unescape(result[1]);
        
        let localeRegexp = new RegExp(this.LANG_PATTER);
        if (!localeRegexp.test(cookieValue)) {
            return null;
        }
        else {
            return cookieValue;
        }
    }

    public static setTranslationLangCookie(value: string, attrs?: CookieAttr) {

        let myWindow: any = window;
        let name = Cookie.TRANSLATE_LANG;

        //validate the provided value: if it's not a valid locale, skip the rest of the code (prevent header manipulation via cookies)
        let localeRegexp = new RegExp(this.LANG_PATTER);
        if (!localeRegexp.test(value)) {
            return;
        }

        let cookieStr = myWindow.escape(name) + '=' + myWindow.escape(value) + ';';
        let expires = 365 * 10; //10 years
        let dtExpires = new Date(new Date().getTime() + expires * 1000 * 60 * 60 * 24);
        cookieStr += 'expires=' + dtExpires.toUTCString() + ';';

        let path: string = (attrs && attrs.path) ? attrs.path : null;
        if (path) {
            cookieStr += "path=" + path + ";";
        }
        document.cookie = cookieStr;
    }

}


/**
 * Note: path is useful for the translate.lang cookie which if it is set for http://<hostname>/vocbench3 
 * is blocked by the browser for requests toward http://<hostname>/semanticturkey since they have different path
 */
export interface CookieAttr {
    expires?: number;
    path?: string;
}