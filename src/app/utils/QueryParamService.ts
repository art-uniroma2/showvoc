import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { filter } from 'rxjs/operators';
import { SVContext } from './SVContext';
import { ShowVocUrlParams } from '../models/ShowVoc';

@Injectable({
    providedIn: 'root'
})
export class QueryParamService {


    private ctx_world: string;
    private hideDatasetName: string;
    private hideNav: string;

    constructor(private router: Router, private location: Location) {
        // Subscribe to router events to retain the URL parameters on navigation
        this.router.events
            .pipe(filter(event => event instanceof NavigationEnd))
            .subscribe(() => {
                let paramsToAdd = this.getPreservedQueryParams();
                //add the params to the URL
                if (Object.keys(paramsToAdd).length > 0) {
                    const urlTree = this.router.createUrlTree([], {
                        queryParams: paramsToAdd,
                        queryParamsHandling: 'merge',
                        preserveFragment: true
                    });
                    this.location.go(urlTree.toString());
                }
            });
    }

    getPreservedQueryParams() {
        const queryParams: { [key: string]: string } = {};
        if (this.ctx_world) {
            queryParams[ShowVocUrlParams.ctx_world] = this.ctx_world;
        }
        if (this.hideDatasetName) {
            queryParams[ShowVocUrlParams.hideDatasetName] = this.hideDatasetName;
        }
        if (this.hideNav) {
            queryParams[ShowVocUrlParams.hideNav] = this.hideNav;
        }
        return queryParams;
    }

    // Set the ctx_world, hideTab, or hideDataset values as needed
    public setCtxWorld(value: string): void {
        SVContext.setWorld(value);
        this.ctx_world = value;
    }
    public getCtxWorld(): string {
        return this.ctx_world;
    }

    public setHideTab(value: string): void {
        this.hideNav = value;
    }
    public getHideTab(): string {
        return this.hideNav;
    }

    public setHideDatasetName(value: string): void {
        this.hideDatasetName = value;
    }
    public getHideDatasetName(): string {
        return this.hideDatasetName;
    }

}
