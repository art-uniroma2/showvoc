import { AnnotatedValue, Literal, Resource, Value } from "./Resources";

export enum ResViewSection {
  broaders = "broaders",
  classaxioms = "classaxioms",
  constituents = "constituents",
  datatypeDefinitions = "datatypeDefinitions",
  denotations = "denotations",
  disjointProperties = "disjointProperties",
  domains = "domains",
  equivalentProperties = "equivalentProperties",
  evokedLexicalConcepts = "evokedLexicalConcepts",
  facets = "facets",
  formRepresentations = "formRepresentations",
  imports = "imports",
  labelRelations = "labelRelations",
  lexicalizations = "lexicalizations",
  lexicalForms = "lexicalForms",
  lexicalSenses = "lexicalSenses",
  members = "members",
  membersOrdered = "membersOrdered",
  notes = "notes",
  properties = "properties",
  ranges = "ranges",
  rdfsMembers = "rdfsMembers",
  schemes = "schemes",
  subPropertyChains = "subPropertyChains",
  subterms = "subterms",
  superproperties = "superproperties",
  topconceptof = "topconceptof",
  types = "types"
}

export class ResViewUtils {

  public static orderedResourceViewSections: ResViewSection[] = [
    ResViewSection.types,
    ResViewSection.classaxioms,
    ResViewSection.topconceptof,
    ResViewSection.schemes,
    ResViewSection.broaders,
    ResViewSection.superproperties,
    ResViewSection.equivalentProperties,
    ResViewSection.disjointProperties,
    ResViewSection.subPropertyChains,
    ResViewSection.subterms,
    ResViewSection.domains,
    ResViewSection.ranges,
    ResViewSection.facets,
    ResViewSection.lexicalizations,
    ResViewSection.lexicalForms,
    ResViewSection.lexicalSenses,
    ResViewSection.denotations,
    ResViewSection.evokedLexicalConcepts,
    ResViewSection.notes,
    ResViewSection.members,
    ResViewSection.membersOrdered,
    ResViewSection.labelRelations,
    ResViewSection.formRepresentations,
    ResViewSection.imports,
    ResViewSection.constituents,
    ResViewSection.rdfsMembers,
    ResViewSection.properties
  ];

  /**
   * Given a value, return support info telling if the value is a literal containing a link.
   * This method is placed here because it implements logic shared by two components (value-renderer and rdf-resource)
   * @param value
   * @returns 
   */
  static getLiteralWithLinkInfo(value: Value) {
    let literalWithLink = false;
    let splittedLiteral: string[] = null;
    if (value instanceof Literal) {
      let label = value.getLabel();
      let regexToken = /(((ftp|https?):\/\/)[-\w@:%_+.~#?,&//=]+)|((mailto:)?[_.\w-][_.\w-!#$%&'*+-/=?^_`.{|}~]+@([\w][\w-]+\.)+[a-zA-Z]{2,3})/g;
      let urlArray: string[] = [];

      let matchArray: RegExpExecArray;
      while ((matchArray = regexToken.exec(label)) !== null) {
        urlArray.push(matchArray[0]);
      }

      if (urlArray.length > 0) {
        literalWithLink = true;
        splittedLiteral = [];
        for (let i = 0; i < urlArray.length; i++) {
          let idx: number = 0;
          let urlStartIdx: number = label.indexOf(urlArray[i]);
          let urlEndIdx: number = label.indexOf(urlArray[i]) + urlArray[i].length;
          splittedLiteral.push(label.substring(idx, urlStartIdx)); //what there is before url
          splittedLiteral.push(label.substring(urlStartIdx, urlEndIdx)); //url
          idx = urlEndIdx;
          label = label.substring(idx);
          //what there is between url and the end of the string
          if (urlArray[i + 1] == null && idx != label.length) { //if there is no further links but there is text after last url
            splittedLiteral.push(label); //push label, namely the rest of the value string
          }
        }
      }
    }
    return {
      literalWithLink: literalWithLink,
      splittedLiteral: splittedLiteral
    };
  }

}

export class PropertyFacet {
  name: string;
  value: boolean;
  explicit: boolean;
}

export enum PropertyFacetsEnum {
  symmetric = "symmetric",
  asymmetric = "asymmetric",
  functional = "functional",
  inverseFunctional = "inverseFunctional",
  reflexive = "reflexive",
  irreflexive = "irreflexive",
  transitive = "transitive"
}

export enum ResourceViewCtx {
  modal = "modal"
}

export class ValueClickEvent {
  ctrl?: boolean; //to open in a new tabset
  shift?: boolean; //to open in the side tabset
  double?: boolean; //to open in a new tabset
  value: AnnotatedValue<Resource>;

  constructor(value: AnnotatedValue<Resource>, event?: MouseEvent, double?: boolean) {
    this.value = value;
    this.ctrl = event?.metaKey || event?.ctrlKey;
    this.shift = event?.shiftKey;
    this.double = double;
  }
}