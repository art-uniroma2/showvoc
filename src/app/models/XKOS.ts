
/**
 * Additional attributes of AnnotatedValue of correspondences resource
 */
export enum CorrespondencesAttr {
    //getCorrespondences
    numAss = "numAss",
    comparesA = "comparesA",
    comparesB = "comparesB",
}

export enum AssociationAttr {
    //getAssociations
    sourceConcept = "xkos:sourceConcept",
    targetConcept = "xkos:targetConcept",
}