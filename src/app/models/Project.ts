import { LocalStorageManager } from '../utils/LocalStorageManager';
import { Settings } from './Plugins';
import { EDOAL, OntoLex, OWL, RDFS, SKOS, SKOSXL } from './Vocabulary';

export class Project {
  private name: string;
  private baseURI: string;
  private defaultNamespace: string;
  private accessible: boolean;
  private historyEnabled: boolean;
  private validationEnabled: boolean;
  private model: string;
  private lexicalizationModel: string;
  private open: boolean;
  private openAtStartup: boolean;
  private visibility: ProjectVisibility;
  private repositoryLocation: { location: "remote" | "local", serverURL?: string };
  private status: { status: string, message?: string };
  private facets: Settings;
  private description: string;
  private labels: { [lang: string]: string } = {}; //lang->labels

  constructor(name?: string) {
    if (name != undefined) {
      this.name = name;
    }
  }

  public setName(name: string) {
    this.name = name;
  }

  /**
   * Returns the name of the project. 
   * If labelRequired is true (if not provided is false by default),
   * returns the rendering label according the rendering status and the language
   * @param label 
   * @returns 
   */
  public getName(labelRequired: boolean = false): string {
    if (labelRequired) {
      return this.getLabel();
    } else {
      return this.name;
    }
  }

  public getLabel(): string {
    //returns the label if rendering is enabled and if label in the given language exists
    if (ProjectLabelCtx.renderingEnabled && this.labels[ProjectLabelCtx.language]) {
      return this.labels[ProjectLabelCtx.language];
    } else {
      return this.name;
    }
  }

  public setBaseURI(baseURI: string) {
    this.baseURI = baseURI;
  }

  public getBaseURI(): string {
    return this.baseURI;
  }

  public setDefaultNamespace(defaultNamespace: string) {
    this.defaultNamespace = defaultNamespace;
  }

  public getDefaultNamespace(): string {
    return this.defaultNamespace;
  }

  public setAccessible(accessible: boolean) {
    this.accessible = accessible;
  }

  public isAccessible(): boolean {
    return this.accessible;
  }

  public setHistoryEnabled(enabled: boolean) {
    this.historyEnabled = enabled;
  }

  public isHistoryEnabled(): boolean {
    return this.historyEnabled;
  }

  public setValidationEnabled(enabled: boolean) {
    this.validationEnabled = enabled;
  }

  public isValidationEnabled(): boolean {
    return this.validationEnabled;
  }

  public setModelType(modelType: string) {
    this.model = modelType;
  }
  public getModelType(prettyPrint?: boolean): string {
    if (prettyPrint) {
      return Project.getPrettyPrintModelType(this.model);
    }
    return this.model;
  }

  public setLexicalizationModelType(lexicalizationModel: string) {
    this.lexicalizationModel = lexicalizationModel;
  }
  public getLexicalizationModelType(prettyPrint?: boolean): string {
    if (prettyPrint) {
      return Project.getPrettyPrintModelType(this.lexicalizationModel);
    }
    return this.lexicalizationModel;
  }

  public static getPrettyPrintModelType(modelType: string) {
    if (modelType == RDFS.uri) {
      return "RDFS";
    } else if (modelType == OWL.uri) {
      return "OWL";
    } else if (modelType == SKOS.uri) {
      return "SKOS";
    } else if (modelType == SKOSXL.uri) {
      return "SKOSXL";
    } else if (modelType == OntoLex.uri) {
      return "OntoLex";
    } else if (modelType == EDOAL.uri) {
      return "Alignment";
    } else {
      return modelType;
    }
  }

  public setOpen(open: boolean) {
    this.open = open;
  }
  public isOpen(): boolean {
    return this.open;
  }

  public getRepositoryLocation() {
    return this.repositoryLocation;
  }
  public setRepositoryLocation(repositoryLocation: { location: "remote" | "local", serverURL?: string }) {
    this.repositoryLocation = repositoryLocation;
  }
  public isRepositoryRemote(): boolean {
    return this.repositoryLocation.location == "remote";
  }

  public setStatus(status: { status: string, message?: string }) {
    this.status = status;
  }
  public getStatus(): { status: string, message?: string } {
    return this.status;
  }

  public setDescription(description: string) {
    this.description = description;
  }
  public getDescription(): string {
    return this.description;
  }

  public setFacets(facets: Settings) {
    this.facets = facets;
  }

  public getFacets(): Settings {
    return this.facets;
  }

  public setOpenAtStartup(openAtStartup: boolean) {
    this.openAtStartup = openAtStartup;
  }

  public getOpenAtStartup(): boolean {
    return this.openAtStartup;
  }

  public setVisibility(visibility: ProjectVisibility) {
    this.visibility = visibility;
  }

  public getVisibility(): ProjectVisibility {
    return this.visibility;
  }

  public setLabels(labels: { [key: string]: string }) {
    if (labels != null) {
      this.labels = labels;
    }
  }

  public getLabels(): { [key: string]: string } {
    return this.labels;
  }
}

export enum BackendTypesEnum {
  graphdb_FreeSail = "graphdb:FreeSail",
  openrdf_NativeStore = "openrdf:NativeStore",
  openrdf_MemoryStore = "openrdf:MemoryStore"
}

export enum AccessLevel {
  R = "R",
  RW = "RW"
}

export enum ProjectVisibility {
  PUBLIC = "PUBLIC",
  AUTHORIZED = "AUTHORIZED",
  PRISTINE = "PRISTINE",
}

export class Repository {
  public id: string;
  public location: string;
  public description: string;
  public readable: boolean;
  public writable: boolean;
}

export class RepositorySummary {
  public id: string;
  public description: string;
  public remoteRepoSummary: RemoteRepositorySummary;
}
export class RemoteRepositorySummary {
  public serverURL: string;
  public repositoryId: string;
  public username: string;
  public password: string;
}

export enum RepositoryAccessType {
  CreateLocal = "CreateLocal",
  CreateRemote = "CreateRemote",
  AccessExistingRemote = "AccessExistingRemote",
}

export class RepositoryAccess {
  private type: RepositoryAccessType;
  private configuration: RemoteRepositoryAccessConfig;

  constructor(type: RepositoryAccessType) {
    this.type = type;
  }

  public setConfiguration(configuration: RemoteRepositoryAccessConfig) {
    this.configuration = configuration;
  }

  public stringify(): string {
    let repoAccess: any = {
      "@type": this.type,
    };
    //if the repository access is remote, add the configuration
    if (this.type == RepositoryAccessType.CreateRemote || this.type == RepositoryAccessType.AccessExistingRemote) {
      repoAccess.serverURL = this.configuration.serverURL;
      repoAccess.username = this.configuration.username;
      repoAccess.password = this.configuration.password;
    }
    return JSON.stringify(repoAccess);
  }
}

export class RemoteRepositoryAccessConfig {
  public serverURL: string;
  public username: string;
  public password: string;
}

export class ExceptionDAO {
  public message: string;
  public type: string;
  public stacktrace: string;
}

export enum ProjectFacets {
  category = "category",
  dir = "dir",
  organization = "organization",
  prjHistoryEnabled = "prjHistoryEnabled",
  prjLexModel = "prjLexModel",
  prjModel = "prjModel",
  prjValidationEnabled = "prjValidationEnabled",
}

export enum ProjectViewMode {
  list = "list",
  facet = "facet",
}

export class ProjectUtils {

  //history and validation are not foreseen in SV
  static projectFacetsTranslationStruct: { [facet: string]: string } = {
    // [ProjectFacets.prjHistoryEnabled]: "MODELS.PROJECT.HISTORY",
    [ProjectFacets.category]: "COMMONS.CATEGORY",
    [ProjectFacets.organization]: "COMMONS.ORGANIZATION",
    [ProjectFacets.prjLexModel]: "MODELS.PROJECT.LEXICALIZATION",
    [ProjectFacets.prjModel]: "COMMONS.MODEL",
    // [ProjectFacets.prjValidationEnabled]: "MODELS.PROJECT.VALIDATION",
  };

  /**
  * Convert a facets map (which can have nested map as value) to a flat map.
  * E.g.
  * { key: value1, key2: { key21: value21, key22: value22 }, key3: [value31, value32] }
  * to
  * { key: value1, key21: value21, key22: value22, key3: [value31, value32] }
  * @param facets
  * @returns
  */
  static flattenizeFacetsMap(facets: { [key: string]: any }): { [key: string]: string | string[] } {
    let fMap: { [key: string]: string | string[] } = {};
    for (let fKey in facets) {
      if (Array.isArray(facets[fKey])) { //value is an array, keep it as-is
        fMap[fKey] = facets[fKey];
      } else if (facets[fKey] instanceof Object) { //value is a nested map, flattenize it
        let nestedMap = this.flattenizeFacetsMap(facets[fKey]);
        for (let nestedKey in nestedMap) {
          fMap[nestedKey] = nestedMap[nestedKey];
        }
      } else { //plain string value
        fMap[fKey] = facets[fKey];
      }
    }
    return fMap;
  }

}

/**
 * map facetName -> value -> count
 */
export interface FacetsValueCountMap {
  [facetName: string]: {
    [facetValue: string]: number
  }
}

/**
 * This class keeps the rendering status and the language for the rendering of the projects.
 * I need to declare it here (instead of in SVContext, for example) since it
 * needs to be references in Project#getName() without injecting any service (SettingsManager and/or TranslateService) and 
 * preventing cycling dependencies (e.g. VBContext -> Project -> VBContext).
 * - renderingEnabled needs to be updated each time it is changed from the project list panel (in dataset manager dashboard or in datasets page)
 * - language needs to be updated each time the i18n language is changed
 */
export class ProjectLabelCtx {
  public static renderingEnabled: boolean = false;
  public static language: string;
}



export class DatasetViewsUtils {

  static retrieveCollapsedDirectoriesCookie(): string[] {
    let collapsedDirs: string[] = [];
    let collapsedDirsCookie: string = LocalStorageManager.getItem(LocalStorageManager.PROJECT_COLLAPSED_DIRS);
    if (collapsedDirsCookie != null) {
      try { //cookie might be not parsed, in case return empty list
        if (Array.isArray(collapsedDirsCookie)) {
          collapsedDirs = collapsedDirsCookie;
          collapsedDirs.forEach((dir, index, list) => { //replace the serialized "null" directory with the null value
            if (dir == "null") list[index] = null;
          });
        }
      } catch {
        console.warn("unable to parse " + LocalStorageManager.PROJECT_COLLAPSED_DIRS + ": " + collapsedDirsCookie);
      }
    }
    return collapsedDirs;
  }

  static storeCollpasedDirectoriesCookie(datasetDirs: DatasetDirEntryI[]) {
    let collapsedDirs: string[] = [];
    datasetDirs.forEach(pd => {
      if (!pd.open) {
        let dirNameValue = pd.dirName != null ? pd.dirName : "null";
        collapsedDirs.push(dirNameValue);
      }
    });
    LocalStorageManager.setItem(LocalStorageManager.PROJECT_COLLAPSED_DIRS, collapsedDirs);
  }
}

export interface DatasetDirEntryI {
  dirName: string;
  dirDisplayName: string;
  open: boolean;
}