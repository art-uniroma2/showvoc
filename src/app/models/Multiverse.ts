export interface WorldInfo {
    worldName: string;
    userCount: number;
}