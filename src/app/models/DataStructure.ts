import { OntoLex, OWL, RDFS, SKOS } from "./Vocabulary";

export enum DataPanel {
    cls = "cls",
    concept = "concept",
    skosCollection = "skosCollection",
    conceptScheme = "conceptScheme",
    property = "property",
    limeLexicon = "limeLexicon",
    ontolexLexicalEntry = "ontolexLexicalEntry",
    alignments = "alignments",
    correspondence = "correspondence",
}

export interface DataPanelLabelSetting {
    [lang: string]: DataPanelLabelMap
}
export interface DataPanelLabelMap {
    [panel: string]: string
}

export class DataStructureUtils {

    /**
     * Default tabs available for models (unless hidden via settings)
     */
    static readonly modelPanelsMap: { [key: string]: DataPanel[] } = {
        [RDFS.uri]: [
            DataPanel.cls,
            DataPanel.property
        ],
        [OWL.uri]: [
            DataPanel.cls,
            DataPanel.property
        ],
        [SKOS.uri]: [
            DataPanel.cls,
            DataPanel.concept,
            DataPanel.conceptScheme,
            DataPanel.skosCollection,
            DataPanel.property
        ],
        [OntoLex.uri]: [
            DataPanel.cls,
            DataPanel.concept,
            DataPanel.conceptScheme,
            DataPanel.skosCollection,
            DataPanel.property,
            DataPanel.limeLexicon,
            DataPanel.ontolexLexicalEntry
        ],
    };

    /* 
    priority order of panel to activate by default (if available)
    e.g. in SKOS, the default active panel should be concept, if missing, then conceptScheme, if missing then skosCollection
    */
    static readonly panelsPriority: { [key: string]: DataPanel[] } = {
        [RDFS.uri]: [
            DataPanel.cls,
            DataPanel.property
        ],
        [OWL.uri]: [
            DataPanel.cls,
            DataPanel.property
        ],
        [SKOS.uri]: [
            DataPanel.concept,
            DataPanel.conceptScheme,
            DataPanel.skosCollection
        ],
        [OntoLex.uri]: [
            DataPanel.limeLexicon,
            DataPanel.ontolexLexicalEntry,
            DataPanel.concept,
            DataPanel.conceptScheme
        ]
    };

}