import { Language } from './LanguagesCountries';
import { ProjectViewMode } from './Project';
import { ResViewSection } from './ResourceView';
import { OWL, RDF, RDFS } from './Vocabulary';

export class PreferencesUtils {
  /**
   * Merge the default preferences object with the one returned by the Settings.getSettings() service
   * Useful to keep the default values to those properties not set and thus not returned by the above service.
   * @param localPref 
   * @param settingsProperty 
   */
  public static mergePreference(localPref: any, settingsProperty: any) {
    Object.keys(localPref).forEach(prop => {
      if (settingsProperty[prop] != null) {
        localPref[prop] = settingsProperty[prop];
      }
    });
  }
}

export class SearchSettings {
  public stringMatchMode: SearchMode = SearchMode.contains;
  public useURI: boolean = false;
  public useLocalName: boolean = true;
  public useNotes: boolean = false;
  public restrictLang: boolean = false;
  public languages: string[] = [];
  public includeLocales: boolean = false;
  public useAutocompletion: boolean = false;
  public restrictActiveScheme: boolean = true;
  public classIndividualSearchMode: ClassIndividualPanelSearchMode = ClassIndividualPanelSearchMode.all;
}

export enum SearchMode {
  startsWith = "startsWith",
  contains = "contains",
  endsWith = "endsWith",
  exact = "exact",
  fuzzy = "fuzzy"
}

export class SearchUtils {
  static stringMatchModesMap: { [key: string]: { labelTranslationKey: string, symbol: string } } = {
    [SearchMode.startsWith]: { labelTranslationKey: "SEARCH.SETTINGS.STARTS_WITH", symbol: "α.." },
    [SearchMode.contains]: { labelTranslationKey: "SEARCH.SETTINGS.CONTAINS", symbol: ".α." },
    [SearchMode.endsWith]: { labelTranslationKey: "SEARCH.SETTINGS.ENDS_WITH", symbol: "..α" },
    [SearchMode.exact]: { labelTranslationKey: "SEARCH.SETTINGS.EXACT", symbol: "α" },
    [SearchMode.fuzzy]: { labelTranslationKey: "SEARCH.SETTINGS.FUZZY", symbol: "~α" }
  };
}

export enum StatusFilter {
  NOT_DEPRECATED = "NOT_DEPRECATED",
  ONLY_DEPRECATED = "ONLY_DEPRECATED",
  UNDER_VALIDATION = "UNDER_VALIDATION",
  UNDER_VALIDATION_FOR_DEPRECATION = "UNDER_VALIDATION_FOR_DEPRECATION",
  ANYTHING = "ANYTHING",
}

export enum ClassIndividualPanelSearchMode {
  onlyClasses = "onlyClasses",
  onlyInstances = "onlyInstances",
  all = "all"
}

export class ClassTreePreference {
  showInstancesNumber: boolean;
  rootClass: string; //NT serialization or root class
  filter: ClassTreeFilter;

  constructor(modelTypeIri: string) {
    this.showInstancesNumber = false;
    this.rootClass = (modelTypeIri == RDFS.uri) ? RDFS.resource.toNT() : OWL.thing.toNT();
    this.filter = new ClassTreeFilter();
  }

}
export class ClassTreeFilter {
  enabled: boolean = true;
  map: { [key: string]: string[] } = { //map where keys are the URIs of a class and the values are the URIs of the subClasses to filter out
    [RDFS.resource.getIRI()]: [
      OWL.allDifferent.getIRI(), OWL.allDisjointClasses.getIRI(), OWL.allDisjointProperties.getIRI(),
      OWL.annotation.getIRI(), OWL.axiom.getIRI(), OWL.negativePropertyAssertion.getIRI(), OWL.ontology.getIRI(),
      RDF.list.getIRI(), RDF.property.getIRI(), RDF.statement.getIRI(),
      RDFS.class.getIRI(), RDFS.container.getIRI(), RDFS.literal.getIRI(),
    ],
    [OWL.thing.getIRI()]: [OWL.nothing.getIRI(), OWL.namedIndividual.getIRI()]
  };
}

export class InstanceListPreference {
  visualization: InstanceListVisualizationMode = InstanceListVisualizationMode.standard;
  safeToGoLimit: number = 1000;
}

export enum InstanceListVisualizationMode {
  searchBased = "searchBased",
  standard = "standard"
}

export class ConceptTreePreference {
  visualization: ConceptTreeVisualizationMode = ConceptTreeVisualizationMode.hierarchyBased;
  safeToGoLimit: number = 1000;
}

/**
 * checksum: string - it is a representation of the request params (it could be a concat of the params serialization)
 * safe: boolean tells if the tree/list is safe to be initialized, namely if the amount of elements (root/items) are under a safety limit
 */
export interface SafeToGoMap { [checksum: string]: SafeToGo }
export interface SafeToGo { safe: boolean, count?: number }

export enum ConceptTreeVisualizationMode {
  searchBased = "searchBased",
  hierarchyBased = "hierarchyBased"
}

export class LexicalEntryListPreference {
  visualization: LexEntryVisualizationMode = LexEntryVisualizationMode.indexBased;
  indexLength: number = 1;
  safeToGoLimit: number = 1000;
}

export enum LexEntryVisualizationMode {
  searchBased = "searchBased",
  indexBased = "indexBased"
}

export class ValueFilterLanguages {
  languages: string[] = [];
  enabled: boolean = false;
}

export interface ResViewTemplate {
  [role: string]: ResViewSection[]; //map resource type => included sections
}

export class SectionFilterPreference {
  [role: string]: ResViewSection[]; //role is a RDFResourceRoleEnum, values are only the hidden sections
}

/**
 * Class that represents the global application settings
 */
export class SystemSettings {
  showFlags: boolean = true;
  homeContent: string;
  languages: Language[];
  disableContributions: boolean = false;
  authService: AuthServiceMode = AuthServiceMode.Default;
  allowAnonymous: boolean = false;
}

export class ShowVocSettings {
  vbConnectionConfig: VocBenchConnectionShowVocSettings;
  disableContributions: boolean;
}

export class VocBenchConnectionShowVocSettings {
  vbURL: string;
  stHost: string;
  adminEmail: string;
  adminPassword: string;
}

export enum AuthServiceMode {
  Default = "Default",
  SAML = "SAML",
  OAuth2 = "OAuth2"
}

export class ResourceViewPreference {
  rendering: boolean = true;
  inference: boolean = false;
  showDatatypeBadge: boolean = true;
}

export class ResourceViewProjectSettings {
  customSections: { [key: string]: CustomSection } = {}; //map name -> CustomSection
  templates: RoleSectionsMap = {}; //map role -> sections
}

export interface RoleSectionsMap {
  [key: string]: ResViewSection[]
}

export class PredicateLabelSettings {
  enabled: boolean;
  mappings: { [lang: string]: PredLabelMapping } = {}; // map { lang -> { pred -> label } }
}
export interface PredLabelMapping { [pred: string]: string }

export interface ResViewSectionsCustomization {
  [lang: string]: ResViewSectionInfoMapping // lang -> section -> label+desc
}
export interface ResViewSectionInfoMapping {
  [section: string]: ResViewSectionDisplayInfo
}
export interface ResViewSectionDisplayInfo {
  label?: string;
  description?: string;
}

export class CustomSection {
  matchedProperties: string[];
}

export class ClickableValueStyle {
  widget: boolean = false;
  bold: boolean = false;
  underlined: boolean = false;
  color?: string;
}


export class ProjectVisualization {
  mode: ProjectViewMode = ProjectViewMode.list;
  facetBagOf?: string;
}

export enum DatasetTab {
  data = "data",
  sparql = "sparql",
  metadata = "metadata",
  downloads = "downloads",
  tools = "tools",
}