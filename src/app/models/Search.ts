import { ProjectVisibility } from './Project';
import { SearchMode } from './Properties';
import { Literal, IRI, RDFResourceRolesEnum } from './Resources';

export class GlobalSearchResult {
    resource: IRI;
    resourceLocalName: string;
    resourceType: IRI;
    role: RDFResourceRolesEnum;
    repository: SearchResultRepo;
    details: SearchResultDetails[];
    show?: string; //not in the response, computed later for each result
}
export class SearchResultDetails {
    matchedValue: Literal;
    predicate: IRI;
    type: "note" | "label";
}

export class TripleForSearch {
    predicate: IRI;
    searchString: string;
    mode: SearchMode;
}

export class TranslationResult {
    resource: string;
    resourceLocalName: string;
    resourceType: string;
    role: RDFResourceRolesEnum;
    repository: SearchResultRepo;
    matches: TranslationDetail[];
    descriptions: TranslationDetail[];
    translations: TranslationDetail[];
}

export interface SearchResultRepo {
    id: string;
    open: boolean;
    visibility: ProjectVisibility;
    labels?: { [lang: string]: string }; //lang->labels
}

export class TranslationDetail {
    lang: string;
    values: TranslationValue[];
}

export class TranslationValue {
    value: string;
    predicate: string;
    type: "lexicalization" | "note";
}

export interface GlobalSearchProjectsGroup {
    groupName: string;
    groupDescription?: string;
    projectNames: string[];
}