import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { WidgetModule } from '../widget/widget.module';
import { AssociationsSearchbarComponent } from './associations-searchbar.component';
import { CorrespondencesListNodeComponent } from './correspondences-list/correspondences-list-node.component';
import { CorrespondencesListPanelComponent } from './correspondences-list/correspondences-list-panel.component';
import { CorrespondencesListComponent } from './correspondences-list/correspondences-list.component';
import { CorrespondencesViewComponent } from './correspondences-view.component';

@NgModule({
    declarations: [
        AssociationsSearchbarComponent,
        CorrespondencesListComponent,
        CorrespondencesListNodeComponent,
        CorrespondencesListPanelComponent,
        CorrespondencesViewComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        NgbDropdownModule,
        TranslateModule,
        WidgetModule
    ],
    exports: [
        CorrespondencesListPanelComponent,
        CorrespondencesViewComponent
    ],
    providers: []
})
export class CorrespondencesModule { }
