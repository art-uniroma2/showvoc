import { Component, ViewChild } from '@angular/core';
import { BasicModalsServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { SharedModalsServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { AnnotatedValue, IRI, RDFResourceRolesEnum, Resource } from 'src/app/models/Resources';
import { AbstractListPanel } from 'src/app/structures/lists/abstract-list-panel';
import { SettingsManager } from 'src/app/utils/SettingsManager';
import { SVEventHandler } from 'src/app/utils/SVEventHandler';
import { CorrespondencesListComponent } from './correspondences-list.component';

@Component({
  selector: 'correspondences-list-panel',
  templateUrl: './correspondences-list-panel.component.html',
  host: { class: "vbox" },
  standalone: false
})
export class CorrespondencesListPanelComponent extends AbstractListPanel<IRI> {

  panelRole: RDFResourceRolesEnum = "correspondence" as RDFResourceRolesEnum; //not used in this case, so I can force an unexisting role

  rendering: boolean = false; //override the default in AbstractPanel

  @ViewChild(CorrespondencesListComponent) viewChildList: CorrespondencesListComponent;


  constructor(basicModals: BasicModalsServices, sharedModals: SharedModalsServices, eventHandler: SVEventHandler, settingsMgr: SettingsManager) {
    super(basicModals, sharedModals, eventHandler, settingsMgr);
  }

  refresh() {
    this.viewChildList.init();
  }

  openAt(node: AnnotatedValue<IRI>) {
    //N.A.
    void node; //workaroung to avoid "defined but never used" warning
  }

  handleSearchResults(results: AnnotatedValue<Resource>[]) {
    //N.A.
    void results; //workaroung to avoid "defined but never used" warning
  }

}