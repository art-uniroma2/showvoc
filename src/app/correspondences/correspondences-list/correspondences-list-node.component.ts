import { Component, SimpleChanges } from '@angular/core';
import { AnnotatedValue, IRI } from 'src/app/models/Resources';
import { CorrespondencesAttr } from 'src/app/models/XKOS';
import { AbstractNode } from 'src/app/structures/abstract-node';
import { ResourceUtils } from 'src/app/utils/ResourceUtils';

@Component({
  selector: 'correspondences-list-node',
  templateUrl: './correspondences-list-node.component.html',
  styles: [`
    .ellipsable-content {
        display: -webkit-box; 
        -webkit-box-orient: vertical; 
        -webkit-line-clamp: 2; 
        overflow: hidden; 
        text-overflow: ellipsis; 
    }
`],
  standalone: false
})
export class CorrespondencesListNodeComponent extends AbstractNode<IRI> {

  correspondenceRendering: string;

  sourceScheme: AnnotatedValue<IRI>;
  targetScheme: AnnotatedValue<IRI>;
  schemeARendering: string;
  schemeBRendering: string;

  ngOnInit() {
    this.sourceScheme = this.node.getAttribute('ann_' + CorrespondencesAttr.comparesA);
    this.targetScheme = this.node.getAttribute('ann_' + CorrespondencesAttr.comparesB);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['rendering']) {
      this.initRenderingLabels();
    }
  }

  private initRenderingLabels() {
    this.correspondenceRendering = ResourceUtils.getRendering(this.node, this.rendering);
  }

}