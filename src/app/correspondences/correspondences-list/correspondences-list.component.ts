import { ChangeDetectorRef, Component } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { AnnotatedValue, IRI, RDFResourceRolesEnum } from 'src/app/models/Resources';
import { CorrespondencesAttr } from 'src/app/models/XKOS';
import { ResourcesServices } from 'src/app/services/resources.service';
import { XkosServices } from 'src/app/services/xkos.service';
import { AbstractList } from 'src/app/structures/lists/abstract-list';
import { SVEventHandler } from 'src/app/utils/SVEventHandler';

@Component({
  selector: 'correspondences-list',
  templateUrl: './correspondences-list.component.html',
  host: { class: "structureComponent" },
  standalone: false
})
export class CorrespondencesListComponent extends AbstractList<IRI> {

  structRole: RDFResourceRolesEnum = null; //this is used for the search, which in this component is not available, so it can be null

  constructor(private xkosService: XkosServices, private resourceService: ResourcesServices,
    eventHandler: SVEventHandler, changeDetectorRef: ChangeDetectorRef) {
    super(eventHandler, changeDetectorRef);
  }

  initImpl() {
    this.loading = true;
    this.xkosService.getCorrespondences(true).pipe(
      finalize(() => { this.loading = false; })
    ).subscribe(correspondences => {
      let toAnnotate: Set<string> = new Set<string>();
      correspondences.forEach(c => {
        let comparesA = c.getAttribute(CorrespondencesAttr.comparesA);
        if (comparesA) {
          toAnnotate.add(comparesA);
        }
        let comparesB = c.getAttribute(CorrespondencesAttr.comparesB);
        if (comparesB) {
          toAnnotate.add(comparesB);
        }
      });
      this.resourceService.getResourcesInfo([...toAnnotate].map(a => new IRI(a))).subscribe(
        annotated => {
          /*
          annotate compared schemes in separated attributes of the correspondence AnnotatedValue.
          So, ann_comparesA instead of replacing comparesA
          */
          correspondences.forEach(c => {
            let schemeA: string = c.getAttribute(CorrespondencesAttr.comparesA);
            let annotatedA: AnnotatedValue<IRI> = annotated.find(a => a.getValue().getIRI() == schemeA);
            if (annotatedA) {
              c.setAttribute('ann_' + CorrespondencesAttr.comparesA, annotatedA);
            }

            let schemeB: string = c.getAttribute(CorrespondencesAttr.comparesB);
            let annotatedB: AnnotatedValue<IRI> = annotated.find(a => a.getValue().getIRI() == schemeB);
            if (annotatedB) {
              c.setAttribute('ann_' + CorrespondencesAttr.comparesB, annotatedB);
            }
          });
          this.nodes = correspondences;
          this.sortNodes();
        }
      );

    });
  }

}