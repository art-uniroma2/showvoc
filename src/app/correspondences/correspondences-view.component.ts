import { Component, Input, SimpleChanges } from '@angular/core';
import FileSaver from 'file-saver';
import { forkJoin, Observable, of } from 'rxjs';
import { defaultIfEmpty, finalize, map, mergeMap } from 'rxjs/operators';
import { SharedModalsServices } from '../modal-dialogs/shared-modals/shared-modals.service';
import { PrefixMapping } from '../models/Metadata';
import { Project } from '../models/Project';
import { RDFFormat } from '../models/RDFFormat';
import { AnnotatedValue, IRI, LocalResourcePosition, ResourcePosition } from '../models/Resources';
import { AssociationAttr, CorrespondencesAttr } from '../models/XKOS';
import { ExportServices } from '../services/export.service';
import { ResourcesServices } from '../services/resources.service';
import { XkosServices } from '../services/xkos.service';
import { STRequestOptions } from '../utils/HttpManager';
import { LocalStorageManager } from '../utils/LocalStorageManager';
import { NTriplesUtil } from '../utils/ResourceUtils';
import { SettingsManager } from '../utils/SettingsManager';
import { SVContext } from '../utils/SVContext';
import { AssociationSearchData } from './associations-searchbar.component';

@Component({
  selector: 'correspondences-view',
  templateUrl: './correspondences-view.component.html',
  host: { class: "vbox" },
  standalone: false
})
export class CorrespondencesViewComponent {

  @Input() correspondence: AnnotatedValue<IRI>;

  rendering: boolean = false;

  loading: boolean = false;

  sourceScheme: AnnotatedValue<IRI>;
  targetScheme: AnnotatedValue<IRI>;

  sourceDatasetInfo: DatasetInfo;
  targetDatasetInfo: DatasetInfo;

  associations: Association[];

  private lastSearch: AssociationSearchData;

  prefixNsMapping: PrefixMapping[];

  exportFormats: RDFFormat[];
  exporting: boolean = false;

  constructor(private xkosService: XkosServices, private exportService: ExportServices, private resourcesService: ResourcesServices, private settingsMgr: SettingsManager, private sharedModals: SharedModalsServices) { }

  ngOnInit() {
    let renderingStored = LocalStorageManager.getItem(LocalStorageManager.CORRESPONDENCES_VIEW_RENDERING);
    if (typeof renderingStored == "boolean") {
      this.rendering = renderingStored;
    }

    this.exportService.getOutputFormats().subscribe(
      formats => {
        this.exportFormats = formats;
      }
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['correspondence']) {
      this.sourceDatasetInfo = null;
      this.targetDatasetInfo = null;
      this.lastSearch = null;
      this.page = -1;
      this.initAssociations();
    }
  }

  private initAssociations() {
    this.sourceScheme = this.correspondence.getAttribute('ann_' + CorrespondencesAttr.comparesA);
    this.targetScheme = this.correspondence.getAttribute('ann_' + CorrespondencesAttr.comparesB);

    let fn = this.xkosService.getAssociations(this.correspondence.getValue());
    this.getAssociationsImpl(fn);
  }

  private listFilteredAssociations() {
    let sourceProject = this.sourceDatasetInfo.project;
    let renderingLangs = this.settingsMgr.getRenderingLanguages(sourceProject);
    let sortLang = renderingLangs?.length > 0 ? renderingLangs[0] : null;
    let fn = this.xkosService.filterAssociations(this.correspondence.getValue(), this.lastSearch.searchString, this.lastSearch.searchMode,
      null, null, true, this.lastSearch.useLocalName, this.lastSearch.useURI, this.lastSearch.useNotes, null, sortLang);
    this.getAssociationsImpl(fn);
  }

  private getAssociationsImpl(listAssociationsFn: Observable<AnnotatedValue<IRI>[]>) {
    this.page = -1;
    this.loading = true;
    listAssociationsFn.pipe(finalize(() => { this.loading = false; })).subscribe(
      associations => {
        this.loading = false;
        this.associations = [];
        associations.forEach(a => {
          this.associations.push({
            association: a,
            source: this.parseAssociatedConcepts(a.getAttribute(AssociationAttr.sourceConcept)),
            target: this.parseAssociatedConcepts(a.getAttribute(AssociationAttr.targetConcept)),
          });
        });
        //paging
        this.totPage = Math.ceil(this.associations.length / this.pageSize);
        this.pageSelectorOpts = Array.from({ length: this.totPage }, (v, k) => k);
        this.pageSelected = 0;
        this.nextPage(); //so to annotate the concepts in first page 
      }
    );
  }

  expandAssociation(association: Association, index: number) {
    this.xkosService.getSingleAssociation(association.association.getValue()).subscribe(
      associations => {
        let a = associations[0];
        let expandedAssociation = {
          association: a,
          source: this.parseAssociatedConcepts(a.getAttribute(AssociationAttr.sourceConcept)),
          target: this.parseAssociatedConcepts(a.getAttribute(AssociationAttr.targetConcept)),
        };

        let toAnnotate: Set<string> = new Set<string>();
        expandedAssociation.source.concepts.forEach(c => toAnnotate.add(c.getValue().getIRI()));
        expandedAssociation.target.concepts.forEach(c => toAnnotate.add(c.getValue().getIRI()));

        this.annotateAssociations([expandedAssociation]).subscribe(
          () => {
            this.associations[index] = expandedAssociation;
          }
        );
      }
    );
  }

  private parseAssociatedConcepts(assConceptAttr: string): AssociatedConcepts {
    let concepts: AnnotatedValue<IRI>[] = [];
    let more: boolean = false;
    if (assConceptAttr) {
      if (
        (assConceptAttr.startsWith("[") && assConceptAttr.endsWith("]")) ||
        (assConceptAttr.startsWith("{") && assConceptAttr.endsWith("}"))
      ) {
        more = assConceptAttr.startsWith("{");
        assConceptAttr = assConceptAttr.substring(1, assConceptAttr.length - 1);
      }
      concepts = assConceptAttr.split(",").map(c => new AnnotatedValue(NTriplesUtil.parseIRI(c.trim())));
    }
    return {
      concepts: concepts,
      more: more
    };
  }

  onSearch(searchData: AssociationSearchData) {
    this.page = 0;
    //empty search => reinit "standard" mappings (not filtered)
    if (searchData.searchString == null || searchData.searchString.trim() == "") {
      this.lastSearch = null;
      this.initAssociations();
    } else {
      this.lastSearch = searchData;
      this.listFilteredAssociations();
    }
  }

  export(format: RDFFormat) {
    this.exporting = true;
    this.xkosService.exportAssociations(format, this.correspondence.getValue()).pipe(
      finalize(() => { this.exporting = false; })
    ).subscribe(
      blob => {
        FileSaver.saveAs(blob, "export." + format.defaultFileExtension);
      }
    );
  }

  openResource(resource: AnnotatedValue<IRI>, target?: boolean) {
    let ctxProject: Project;
    if (target) {
      ctxProject = this.targetDatasetInfo.settingsInitialized ? this.targetDatasetInfo.project : null;
    } else {
      ctxProject = this.sourceDatasetInfo.settingsInitialized ? this.sourceDatasetInfo.project : null;
    }
    this.sharedModals.openResourceView(resource.getValue(), ctxProject);
  }

  switchRendering() {
    this.rendering = !this.rendering;
    LocalStorageManager.setItem(LocalStorageManager.CORRESPONDENCES_VIEW_RENDERING, this.rendering);
  }

  /* ==========================
   * Paging
   * ==========================*/

  page: number = -1;
  totPage: number;
  pageSize: number = 50;

  pageSelectorOpts: number[] = [];
  pageSelected: number;

  from: number = 0;
  to: number = this.pageSize;

  prevPage() {
    this.page--;
    this.annotatePage();
  }

  nextPage() {
    this.page++;
    this.annotatePage();
  }

  goToPage() {
    if (this.page != this.pageSelected) {
      this.page = this.pageSelected;
      this.annotatePage();
    }
  }

  private annotatePage() {
    let toAnnotate: Association[] = [];

    this.from = this.page * this.pageSize;
    this.to = this.from + this.pageSize;
    for (let i = this.from; i < Math.min(this.to, this.associations.length); i++) {
      if (!this.associations[i].annotated) {
        toAnnotate.push(this.associations[i]);
      }
    }

    if (toAnnotate.length > 0) {
      this.loading = true;
      this.annotateAssociations(toAnnotate).pipe(
        finalize(() => { this.loading = false; })
      ).subscribe();
    }
  }

  private annotateAssociations(associations: Association[]): Observable<any> {
    return this.initDatasetsInfo().pipe(
      mergeMap(() => {
        let toAnnotateSourceIRIs: IRI[] = [];
        let toAnnotateTargetIRIs: IRI[] = [];
        associations.filter(a => !a.annotated).forEach(a => {
          a.source.concepts.forEach(c => toAnnotateSourceIRIs.push(c.getValue()));
          a.target.concepts.forEach(c => toAnnotateTargetIRIs.push(c.getValue()));
        });
        //remove duplicates
        toAnnotateSourceIRIs = toAnnotateSourceIRIs.filter((item: IRI, pos: number) => toAnnotateSourceIRIs.findIndex(el => el.equals(item)) == pos);
        toAnnotateTargetIRIs = toAnnotateTargetIRIs.filter((item: IRI, pos: number) => toAnnotateTargetIRIs.findIndex(el => el.equals(item)) == pos);

        let annotateConceptsFn: Observable<any>[] = [];
        if (toAnnotateSourceIRIs.length > 0) {
          annotateConceptsFn.push(this.getAnnotateResourcesFn(associations, toAnnotateSourceIRIs, true));
        }
        if (toAnnotateTargetIRIs.length > 0) {
          annotateConceptsFn.push(this.getAnnotateResourcesFn(associations, toAnnotateTargetIRIs, false));
        }
        return forkJoin(annotateConceptsFn).pipe(defaultIfEmpty(null));
      })
    );
  }

  private getAnnotateResourcesFn(associations: Association[], toAnnotateIRIs: IRI[], source: boolean): Observable<void> {
    let reqOpt: STRequestOptions;
    let datasetInfo = source ? this.sourceDatasetInfo : this.targetDatasetInfo;
    if (datasetInfo.position.isLocal()) {
      reqOpt = new STRequestOptions({ ctxProject: datasetInfo.project, ctxConsumer: SVContext.getWorkingProject() });
    }
    return this.resourcesService.getResourcesInfo(toAnnotateIRIs, reqOpt).pipe(
      map((annotatedValues: AnnotatedValue<IRI>[]) => {
        for (let a of associations) {
          let conceptList = source ? a.source.concepts : a.target.concepts;
          conceptList.forEach((concept, index, list) => {
            let annotated = annotatedValues.find(a => a.getValue().equals(concept.getValue()));
            list[index] = annotated;
          });
          a.annotated = true;
        }
      })
    );
  }

  /**
   * Initialize info of the two datasets of the associations:
   * - retrieve the position of resources (a sample from both side, namely the first concept found from source and target concepts)
   * - if the resources belong to a local project, init the settings in order to be able to annotate them (getResourcesInfo need access to the rendering langs)
   */
  private initDatasetsInfo(): Observable<any> {
    if (this.sourceDatasetInfo && this.targetDatasetInfo) {
      return of(null); //info already initialized
    } else {
      this.sourceDatasetInfo = { position: null, settingsInitialized: false };
      this.targetDatasetInfo = { position: null, settingsInitialized: false };
      return this.initResourcesPositions().pipe(
        mergeMap(() => {
          return this.initProjectsSettings();
        })
      );
    }
  }

  private initResourcesPositions(): Observable<any> {
    //check the position of the first target concept of the first association
    let sourceSample = this.associations.find(a => a.source.concepts.length > 0).source.concepts[0].getValue();
    let targetSample = this.associations.find(a => a.target.concepts.length > 0).target.concepts[0].getValue();
    let sourcePositionFn = this.resourcesService.getResourcePosition(sourceSample).pipe(
      map((position: ResourcePosition) => {
        this.sourceDatasetInfo.position = position;
        if (position.isLocal()) {
          this.sourceDatasetInfo.project = new Project((position as LocalResourcePosition).project);
        }
      })
    );
    let targetPositionFn = this.resourcesService.getResourcePosition(targetSample).pipe(
      map((position: ResourcePosition) => {
        this.targetDatasetInfo.position = position;
        if (position.isLocal()) {
          this.targetDatasetInfo.project = new Project((position as LocalResourcePosition).project);
        }
      })
    );
    return forkJoin([sourcePositionFn, targetPositionFn]);
  }

  private initProjectsSettings() {
    let setupProjectsFn: Observable<any>[] = [];
    if (this.sourceDatasetInfo.position.isLocal() && this.sourceDatasetInfo.project.getName() != SVContext.getWorkingProject().getName()) {
      setupProjectsFn.push(
        this.settingsMgr.initSettingsAtProjectAccess(this.sourceDatasetInfo.project).pipe(
          map(() => { this.sourceDatasetInfo.settingsInitialized = true; })
        )
      );
    }
    if (this.targetDatasetInfo.position.isLocal() && this.targetDatasetInfo.project.getName() != SVContext.getWorkingProject().getName()) {
      setupProjectsFn.push(
        this.settingsMgr.initSettingsAtProjectAccess(this.targetDatasetInfo.project).pipe(
          map(() => { this.targetDatasetInfo.settingsInitialized = true; })
        )
      );
    }
    return forkJoin(setupProjectsFn).pipe(defaultIfEmpty(null));

  }

}

interface Association {
  source: AssociatedConcepts;
  association: AnnotatedValue<IRI>;
  target: AssociatedConcepts;
  annotated?: boolean;
}

interface AssociatedConcepts {
  concepts: AnnotatedValue<IRI>[];
  more: boolean;
}

interface DatasetInfo {
  position: ResourcePosition;
  settingsInitialized: boolean;
  project?: Project;
}