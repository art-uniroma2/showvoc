import { Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalOptions } from '../modal-dialogs/Modals';
import { Project } from '../models/Project';
import { SearchMode, SearchSettings } from '../models/Properties';
import { SearchSettingsModalComponent } from '../structures/search-bar/search-settings-modal.component';
import { SettingsManager } from '../utils/SettingsManager';

@Component({
  selector: 'associations-searchbar',
  templateUrl: './associations-searchbar.component.html',
  standalone: false
})
export class AssociationsSearchbarComponent {

  @Input() sourceProject: Project;
  @Input() disabled: boolean;

  @Output() search: EventEmitter<AssociationSearchData> = new EventEmitter<AssociationSearchData>();

  //search
  searchLoading: boolean;
  searchStr: string;
  searchSettings: SearchSettings = new SearchSettings(); //init just to prevented error on UI
  stringMatchModes: { labelTranslationKey: string, value: SearchMode, symbol: string }[] = [
    { labelTranslationKey: "SEARCH.SETTINGS.STARTS_WITH", value: SearchMode.startsWith, symbol: "α.." },
    { labelTranslationKey: "SEARCH.SETTINGS.CONTAINS", value: SearchMode.contains, symbol: ".α." },
    { labelTranslationKey: "SEARCH.SETTINGS.ENDS_WITH", value: SearchMode.endsWith, symbol: "..α" },
    { labelTranslationKey: "SEARCH.SETTINGS.EXACT", value: SearchMode.exact, symbol: "α" },
    { labelTranslationKey: "SEARCH.SETTINGS.FUZZY", value: SearchMode.fuzzy, symbol: "~α" }
  ];

  constructor(private settingsMgr: SettingsManager, private modalService: NgbModal) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['sourceProject'] && changes['sourceProject'].currentValue) {
      this.initSearch();
    }
  }

  initSearch() {
    this.searchSettings = this.settingsMgr.getSearchSettings(this.sourceProject);
  }

  openSearchSettings() {
    const modalRef: NgbModalRef = this.modalService.open(SearchSettingsModalComponent, new ModalOptions());
    modalRef.componentInstance.project = this.sourceProject;
    modalRef.componentInstance.roles = [];
    return modalRef.result;
  }

  doSearch() {
    let searchData: AssociationSearchData = {
      searchString: this.searchStr,
      searchMode: this.searchSettings.stringMatchMode,
      useLocalName: this.searchSettings.useLocalName,
      useURI: this.searchSettings.useURI,
      useNotes: this.searchSettings.useNotes,
    };
    this.search.emit(searchData);
  }

  updateSearchMode(mode: SearchMode, event: Event) {
    event.stopPropagation();
    this.searchSettings.stringMatchMode = mode;
    this.settingsMgr.setSearchSettings(this.sourceProject, this.searchSettings).subscribe();
  }

}

export interface AssociationSearchData {
  searchString: string;
  searchMode: SearchMode;
  useLocalName: boolean,
  useURI: boolean,
  useNotes: boolean,
}