# Reference to Semantic Turkey Change Log
This changelog is limited to changes brought exclusively to the ShowVoc client application.
The full changelog of the platform includes also changes brought to the Semantic Turkey backend:

https://bitbucket.org/art-uniroma2/semantic-turkey/src/master/ChangeLog.txt

# 5.0.0 (dd-mm-202y)
  * Major and pervasive code changes, in particular:
    * updated angular to version 19
    * updated typescript to version 5
    * updated nodejs compliant version to >= 18 (recommended 20.15.0)
    * updated bootstrap to version 5 and ng-bootstrap to version 18
    * updated fontawesome to version 6
    * dropped yasgui-yasqe (no more supported) and replaced with @triply/yasgui v4
    * updated ngx-codemirror to version 7
    * updated d3 to version 7 and ngx-charts to version 21
  * Introduced datasets faceted search in Datasets page
  * Revised lists/trees and ResourceView navigation: 
    * Click -> Opens the resource in the active tab of the main tabset
    * Ctrl + Click -> opens the resource in a new tab of the main tabset
    * Shift + Click -> opens the resource in the active tab of the side tabset (appears if not visible)
    * Shift + Ctrl + Click -> opens the resource in a new tab of the side tabset
    * Middle/Wheel Click on a tab -> closes it
  * Revised management of datasets status:
    * status can be changed only when dataset is closed
    * making a dataset public automatically generates index and metadata
  * Improved checks on accessibility of Search and Translation results
  * Enabled import and export of Custom Services
  * Enabled import and export of Invokable Reports
  * Enabled clickable external links in ResourceView literal values
  * Migrated persistence of lists/trees rendering settings from LocalStorage to backend
  * Allowed custom URI schemes in URI validation regexp
  * Fixed missing resolution of target datasets in Alignments page
  * Fixed an issue that caused changes to the 'allow visualization' setting to be ignored in list/tree settings
  * Fixed an issue that included subProperties when filtering alignments by predicate
  * Fixed an issue that prevented hyperlink to be rendered in Resource View when "show datatype badge" is disabled
  * Fixed an issue that caused Resource View customizations to be ignored when opening external resources in XKOS correspondences view
  * Minor bugfixes

# 4.0.0 (08-11-2024)
  * Moved default port to 1983 to avoid collisions with VocBench on plain installations using default values
  * Introduced support for anonymous user (i.e. truly unauthenticated access)
    * Dropped support for the visitor user (which was automatically logged-in by the ShowVoc client)
    * Added possibility to deny access to unauthenticated users
  * Enabled customization of visible tabs in the Dataset page
  * Enabled customization of the visible panels in the Data page and introduced custom labels for them
  * Revised management of multitab Resource View and added a tooltip for informing about ctrl+click combo
  * Extended resolution of foreign IRIs to all ResourceView sections (previously it was limited to the "other
    properties" section)
  * Supported visualization of HTML content for literals with datatype rdf:HTML
  * Enabled customization of Resource View sections header with the possibility to provide label and description
  * Enable rendering of predicates in ResourceView with customizable labels (and related editor for custom labels)
  * Added an option for not displaying the language code for literal values
  * Ported Custom Views from VocBench
  * Introduced "Multiverse", a mechanism for defining multiple coexisting "worlds" with different settings
  * Enabled autocompletion to search in trees and lists (it can be activated with a setting)
  * Added a filter for groups of datasets in Global Search
  * Supported renaming of stored SPARQL queries
  * Hidden the password in several configuration editors
  * Fixed issue that made changes on root class tree setting not persistent

# 3.2.0 (09-07-2024)
  * Added new "Correspondences" tab for the visualization of XKOS correspondences
  * Improved search functionality in "Alignments" page
  * Enabled sorting of alignments based on the rendering language
  * Enabled rendering of dataset labels in Alignments table and graph
  * Added visualization of the ontology resource in Metadata page
  * Created a new editor for default users settings
  * Moved dataset downloadable distributions to a new dedicated Downloads page
  * Allowed external links as dataset distributions
  * Added possibility to explicilty set default namespace during creation of a dataset
  * Minor bugfixes

# 3.1.0 (22-04-2024)
  * Added possibility to expand/collapse datasets groups in Datasets page when facet-based visualization mode is enabled
  * Added possibility to mark datasets as favorites
  * Enabled datasets restriction and search settings customizations to Global Search
  * Added possibility to configure the Rendering Engine
  * Enabled possibility to choose between resource ordering by URI or Rendering in trees and lists
  * Introduced navigation of ResourceView based on tabs (through combination of CTRL+click or double clicks)
  * Implemented Custom Sections in ResourceView
  * Added possibility to customize the default template of a ResourceView
  * Added possibility to customize style of clickable values in ResourceView
  * Implemented sections filter and enabled the possibility to explicitly show sections' headers in Resource View
  * Enabled dereferenceability of foreign values in ResourceView
  * Added the display of value datatypes in ResourceView
  * Enabled export of alignment mappings
  * Included alignments mappings to download distribution
  * Added possibility to expand/collapse results in Translation page
  * Enabled creation of users with the possibility to be assigned as manager to a dataset
  * Added possibility for administrator/PM to set a default schemes selection for a given project
  * Added possibility for user to set a default rendering languages selection
  * reorganization of buttons/options for the graph views
  * Major refactoring of settings management
  * Fixed issue that allowed visitors to delete stored configuration

# 3.0.0 (12-11-2023)
  * Enabled hyperlinks in dataset descriptions
  * Added option "open at startup" to datasets administration 
  * Added jar build
  * Fixed a bug that prevented to access properties in Data page through direct links
  * Minor bugfixes

# 2.4.2 (06-09-2023)
  * Fixed a bug that made navigation bar showing in datasets/ subroutes despite hideNav=true URL parameter
  * Fixed sanitization of carriage return in dataset name
  * Fixed issue that caused error message showing up when accessing alignments

# 2.4.1 (24-07-2023)
  * Enabled read-only access to SPARQL query storage to non authenticated users
  * Added possibility to expand/collapse all results of Global Search
  * Removed doubled info about skos:OrderedCollection members
  * Fixed inferred triples not showing in ResourceView

# 2.4.0 (14-04-2023)
  * Added an interface for Translation API
  * Enabled folding by dataset of Golbal Search results
  * Enabled dataset folding system based on facets
  * Added support for dataset labeling
  * Widely adopted usage of localStorage in place of Cookies
  * Fixed bug that prevented to access Data page and to focus resources through direct links
  * Fixed a bug that prevented to customize the class tree root
  * Added missing translations
  * Minor bugfixes and UI improvements

# 2.3.0 (28-10-2022)
  * Added possibility to insert custom content into the home page blank section
  * Enabled visualization of ordered collection members in ResourceView
  * Resolved issues in Data page when dataset name is too long
  * Fixed a bug that prevented to delete files uploaded in distributions panel
  * Fixed minor UI issues

# 2.2.1 (19-10-2022)
  * Minor changes

# 2.2.0 (27-09-2022)
  * Added model-oriented graph and class-diagram (uml-like)
  * Implemented the facet-based visualization of datasets
  * Implemented MetadataRegistry
  * Introduced Customizable Reports for internal users
  * Introduced Custom Services for internal users
  * Implemented Custom Search
  * Implemented storage of SPARQL queries (for authenticated user only)
  * Added parameterized SPARQL queries (for authenticated user only)
  * Added statistical charts in Metadata page
  * Implemented support for HTTP resolution and content-negotiation
  * Fixed performances issues by replacing usage setTimeout() with ChangeDetectorRef#detectChanges(). Thanks to Saku Seppälä for the contribution!

# 2.1.0 (23-06-2022)
  * Changed default Dataset landing tab from metadata to data
  * fixed resources resolution and alignment bugs linked to issues in the MDR
  * Fixed authorization issues for SuperUser
  * Minor changes and bugfixes

# 2.0.0 (12-05-2022)
  * Enabled support to SAML authentication
  * Added a metadata summary page for each dataset
  * Added possibility to create downloadable distribution of datasets
  * Added new kind of authorized user: SuperUser. SuperUser create projects and then get the role Project Manager automatically assigned to them
  * Added completion for endpoints to use in "locally federated" (i.e. different repositories on the same GraphDB instance) SPARQL queries
  * Added advanced search
  * Fixed a bug that prevented to automatically change the alphabetic index in Lexical Entry panel after a search
  * Fixed a bug that prevented to show search results in Alignments tab when the target dataset was not available

# 1.2.1 (07-02-2022)
  * Added the possibility to edit credentials for accessing a remote repo
  * Preserved hideNav param when selecting an alignment in Alignments tab
  * Fixed bug when creating dataset and not changing the configuration through the combo-box: in that case no configuration
    was sent (instead of the default one on the combobox: GraphDBFree) and native-store was set instead
  * Added the count of hidden results in Search page

# 1.2.0 (26-01-2022)
  * Added possibility to create multiple administrators
  * Made dialogs draggable
  * Added localization for Spanish, thanks to Juan Antonio Pastor Sanchez for the contribution!
  * Added Manchester Syntax highlighting in resource view
  * Added possibility to hide the dataset name in the Dataset view through the URL parameter "hideDatasetName"

# 1.1.1 (28-10-2021)
  * Added localization for French, thanks to Nathalie Vedovotto for the contribution!
  * Fixed a bug that broke any download link
  * Fixed a bug that caused an error when administrator tries to edit project settings
  * Minor bugfixes

# 1.0.1 (21-09-2021)
  * Added possibility to hide the top navigation bar to visitor users through the URL parameter "hideNav"
  * Added app logo and favicon
  * Minor bugfixes and improvements

# 1.0.0 (02-08-2021)
  * Enabled deletion of remote repositories referred by a deleted dataset
  * Improved management of Remote Repository configurations
  * Added possibility to clear dataset data
  * Introduced project facets
  * Converted alignments list to a tree
  * Implemented search in alignments
  * Enabled possibility to customize ShowVoc instance name
  * Enabled possibility to deactivate users contributions
  * Adopted the new Settings services
  * Added german l10n, thanks to Roland Wingerter for the contribution!
  * Fixed missing error message in case of exception during the project creation
  * Updated the settings rendering widgets to deal with non-backward compatible changes in their representation returned by the server
  * Updated Angular cli to version 10.2.0 and Angular version to ^10.2.2
  * Updated ng-bootstrap dependency to version 7.0.0

<em>Note: Since the project has been renamed from PMKI to ShowVoc, the version numbering has restarted from 1.0.0</em>

# 1.0.0 (31-07-2020)
  * First release of the system